import React from "react";
import PropTypes from "prop-types";
import Icon from "$components/Icon";
import * as st from "./Badge.styles";

const Badge = ({
  children,
  icon = null,
  textColor = "unset",
  backgroundColor,
  borderRadius = "9999px",
  ...rest
}) => (
  <st.Container
    textColor={textColor}
    backgroundColor={backgroundColor}
    borderRadius={borderRadius}
    {...rest}
  >
    {icon && (
      <st.Icon>
        <Icon icon={icon} size={20} />
      </st.Icon>
    )}
    <st.Content>{children}</st.Content>
  </st.Container>
);

Badge.propTypes = {
  icon: PropTypes.string,
  children: PropTypes.node.isRequired,
  textColor: PropTypes.string,
  backgroundColor: PropTypes.string,
};

Badge.defaultProps = {
  icon: null,
  textColor: "primary",
  backgroundColor: "second",
};

export default Badge;
