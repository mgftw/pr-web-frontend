import styled from "styled-components";
import tw from "twin.macro";

import colors from "$constants/colors";

export const Container = styled.div`
  ${tw`flex px-2 py-1 items-center justify-between content-center grow-0 rounded-full tracking-wide text-sm font-karla font-normal shrink-0`}
  ${({ textColor, backgroundColor, borderRadius }) => `
    color: ${colors?.[textColor] ?? textColor};
    background-color: ${colors?.[backgroundColor] ?? backgroundColor};
    border-radius: ${borderRadius};
    height: fit-content;
  `}
`;
export const Content = styled.div``;
export const Icon = styled.div`
  ${tw`mr-2 text-xl`}
`;
