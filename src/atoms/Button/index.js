import styled from "styled-components";
import { existy } from "is_js";

const Base = styled.button`
  cursor: pointer;
  outline: none;
  border: none;
  height: ${(props) => props.height || 3}rem;
  width: fit-content;
  padding-left: ${(props) => (existy(props.spacing) ? props.spacing : 4)}rem;
  padding-right: ${(props) => (existy(props.spacing) ? props.spacing : 4)}rem;
  font-weight: bold;
`;

export const Button = styled(Base)`
  color: ${(props) => props.color || "#ededed"};
  border-radius: 1.5rem;
  background-color: ${(props) => props.backgroundColor || "#2b9b93"};
`;

export const TextButton = styled(Base)`
  color: ${(props) => props.color || "#ededed"};
  background-color: inherit;
  height: ${(props) => props.height || 2.0}rem;
  border-bottom: 2px solid ${(props) => props.color || "#ededed"};
`;

export default Button;
