import { parse, stringify } from "@node-steam/vdf";

export const parseKeyValues = (kvContent) => {
  const finalObject = parse(kvContent);
  return finalObject;
};

export const objectToKv = (obj) => {
  return stringify(obj);
};
