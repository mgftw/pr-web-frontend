export const getStringInitials = (name, formatShort = true) => {
  if (!name) return "";
  // remove brackets and apostrophes
  const formatName = name.replace(/\([^)]*\)/, "").replace(/'+/g, "");
  // get the first character in each word
  let initials = (formatName.match(/\b(\w)/g) ?? []).join("").toUpperCase();

  if (formatShort) {
    // if 1 word name, display 4 characters of word
    if (initials.length === 1) {
      initials = formatName.match(/\b\w{4}/g);
    }
    // if 2 word name, display 2 character of each word
    else if (initials.length === 2) {
      initials = formatName.match(/\b\w{2}/g).join("");
    }
  }

  return initials;
};

export const getOrdinal = (value) => {
  const base = value % 100;
  if (base > 3 && base < 21) return "th";
  switch (base % 10) {
    case 1:
      return "st";
    case 2:
      return "nd";
    case 3:
      return "rd";
    default:
      return "th";
  }
};
