import { toast } from "react-toastify";

import store from "$redux/store";
import { validate } from "$api/authApi";

export const isLoggedIn = () => {
  const { token } = store.getState().sessionState;

  return token !== null;
};

export const getAccessToken = () => store.getState().sessionState.token;

export const validateOrRedirect = (token, history) => {
  if (!token) {
    history.replace("/auth");
    return;
  }

  validate(token).catch((error) => {
    const { status } = error.response;
    if (status === 401) {
      toast.error("Session expired");
      history.replace("/auth");
    } else {
      toast.error("Unable to connect to PR. Are you online?");
      history.replace("/auth");
    }
  });
};
