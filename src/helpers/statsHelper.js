const CLAN_BASE_EXP = 25000;
const CLAN_EXP_MULTIPLIER = 9.61;
export const getRequiredClanLevelUpRequirement = (level) =>
  Math.round(CLAN_BASE_EXP * level * CLAN_EXP_MULTIPLIER);
