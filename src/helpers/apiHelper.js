import axios from "axios";
import { API_URL } from "$constants/url";
import { getAccessToken } from "$helpers/sessionHelper";

const apiClient = axios.create({
  baseURL: API_URL,
  timeout: 10000,
});

apiClient.interceptors.request.use((config) => {
  return {
    ...config,
    headers: {
      ...config.headers,
      Authorization: `Bearer ${getAccessToken()}`,
    },
  };
});

export { apiClient };
