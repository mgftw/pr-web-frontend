export const formatCurrency = (amount) => {
  if (amount === null) {
    return "--";
  }
  if (Math.abs(amount) > 1000000000) {
    return `${(amount / 1000000000).toFixed(1).replace(".0", "")}B`;
  }
  if (Math.abs(amount) > 1000000) {
    return `${(amount / 1000000).toFixed(1).replace(".0", "")}M`;
  }
  if (Math.abs(amount) > 1000) {
    return `${(amount / 1000).toFixed(1).replace(".0", "")}K`;
  }

  return `${amount}`;
};
