import axios from "axios";
import { API_URL } from "$constants/url";

import { getAccessToken } from "$helpers/sessionHelper";

const PREFIX = `api/quests`;

export const getQuests = () => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}`,
  })
    .then((res) => res.data)
    .catch((error) => {
      console.log(error);
    });
};
