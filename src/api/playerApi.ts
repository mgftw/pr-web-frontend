import axios from "axios";
import { API_URL } from "$constants/url";

import { getAccessToken } from "$helpers/sessionHelper";
import { IPlayerClassProgress } from "types/entities";

const PREFIX = `api/players`;

export const getMyProfile = () => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/me`,
  }).then((res) => res.data);
};

export const getMyAccessFlags = () => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/me/access_flags`,
  }).then((res) => res.data);
};

export const getPlayerProfile = (steamId) => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/${steamId}`,
  }).then((res) => res.data);
};

export const getMyInventory = () => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/me/inventory`,
  }).then((res) => res.data);
};

export const getMyBuffs = () => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/me/owned_buffs`,
  }).then((res) => res.data);
};

export const getMyBuffLists = () => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/me/buff_lists`,
  }).then((res) => res.data);
};

export const getMyBuffSlots = () => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/me/buff_slots`,
  }).then((res) => res.data);
};

export const getMyEventProgress = () => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/me/event_progress`,
  }).then((res) => res.data);
};

export const equipBuff = ({ templateId, buffId, slotId }) => {
  return axios({
    method: "post",
    data: {
      buff_id: buffId,
      slot_id: slotId,
    },
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/me/buff_lists/${templateId}/equip`,
  }).then((res) => res.data);
};

export const createBuffList = ({ name, description }) => {
  if (description.length == 0) description = " ";
  return axios({
    method: "post",
    data: {
      name,
      description,
    },
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/me/buff_lists/`,
  }).then((res) => res.data);
};

export const deleteBuffList = (id) => {
  return axios({
    method: "delete",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/me/buff_lists/${id}`,
  }).then((res) => res.data);
};

export const updateBuffList = (id, { name, description }) => {
  if (description.length == 0) description = " ";
  return axios({
    method: "patch",
    data: {
      name,
      description,
    },
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/me/buff_lists/${id}`,
  }).then((res) => res.data);
};

export const switchClass = (targetClass) =>
  axios
    .patch(
      `${API_URL}/${PREFIX}/me/specialization`,
      {
        class_id: targetClass,
      },
      {
        headers: {
          Authorization: `Bearer ${getAccessToken()}`,
        },
      },
    )
    .then((res) => res.data);

export const getMyQuests = () => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/me/quests`,
  })
    .then((res) => res.data)
    .catch((error) => {
      console.log(error);
    });
};

export const getMyClassInfo = () => {
  return axios<IPlayerClassProgress[]>({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/me/specialization`,
  }).then((res) => res.data);
};
