import axios from "axios";
import { API_URL } from "$constants/url";

import { getAccessToken } from "$helpers/sessionHelper";

const PREFIX = `api/server`;

export const getEvent = () => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/event`,
  })
    .then((res) => res.data)
    .catch((error) => {
      console.log(error);
    });
};

export const getFortressOccupation = () =>
  axios
    .get(`${API_URL}/${PREFIX}/fortress`, {
      headers: {
        Authorization: `Bearer ${getAccessToken()}`,
      },
    })
    .then((res) => res.data);

export const getClanRanking = (type) =>
  axios
    .get(`${API_URL}/${PREFIX}/clan_ranking`, {
      params: {
        type,
      },
      headers: {
        Authorization: `Bearer ${getAccessToken()}`,
      },
    })
    .then((res) => res.data);

export const getAllItems = () =>
  axios
    .get(`${API_URL}/${PREFIX}/all_items`, {
      headers: {
        Authorization: `Bearer ${getAccessToken()}`,
      },
    })
    .then((res) => res.data);
