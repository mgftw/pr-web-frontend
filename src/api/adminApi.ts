import { QuestDataToSend } from "$components/editors/QuestEditor/types";
import { apiClient } from "$helpers/apiHelper";
// Am I an admin?
export const validateIfAdmin = () => {
  return apiClient.get("/api/admins/me").then((response) => response.data);
};

// Servers

export const listServers = () =>
  apiClient.get("/api/admins/servers").then((response) => response.data);

export const createServer = (data) =>
  apiClient.post("/api/admins/servers", data).then((response) => response.data);

export const deleteServer = (serverId: number) =>
  apiClient.delete(`/api/admins/servers/${serverId}`).then((response) => response.data);

export const updateItemIcon = (itemId: number, file) => {
  const data = new FormData();
  data.append("icon", file);
  return apiClient.post(`/api/admins/resources/items/${itemId}/icon`, data);
};

export const updateBuffIcon = (buffId: number, file) => {
  const data = new FormData();
  data.append("icon", file);
  return apiClient.post(`/api/admins/resources/buffs/${buffId}/icon`, data);
};

export const updateNPCIcon = (npcId: number, file) => {
  const data = new FormData();
  data.append("icon", file);
  return apiClient.post(`/api/admins/resources/npcs/${npcId}/icon`, data);
};

export const createQuest = (quest: QuestDataToSend) => {
  return apiClient.post(`/api/admins/quests`, { quest });
};

export const updateQuest = (questId: number, quest: QuestDataToSend) => {
  return apiClient.put(`/api/admins/quests/${questId}`, { quest });
};
