import axios from "axios";
import { API_URL } from "$constants/url";

import { getAccessToken } from "$helpers/sessionHelper";

const PREFIX = `api/clans`;

export const getClanInfo = (clanId) => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/${clanId}`,
  }).then((res) => res.data);
};

export const getClanMembers = (clanId) => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/${clanId}/members`,
  }).then((res) => res.data);
};

export const changeClanAvatar = (clanId, file) => {
  const data = new FormData();
  data.append("avatar", file);
  return axios.post(`${API_URL}/${PREFIX}/${clanId}/avatar`, data, {
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
  });
};
