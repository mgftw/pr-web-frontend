import axios from "axios";
import { API_URL } from "$constants/url";

import { getAccessToken } from "$helpers/sessionHelper";

const PREFIX = `api/server`;

export const getGlobalDataInfo = () => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/global_state`,
  }).then((res) => res.data);
};
