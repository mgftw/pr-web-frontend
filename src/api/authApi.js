import axios from "axios";
import { API_URL } from "$constants/url";

// Validate the token and see if the user is still
// correctly authenticated.
export const validate = (token) => {
  return axios({
    method: "get",
    headers: {
      Authorization: `Bearer ${token}`,
    },
    url: `${API_URL}/auth/validate`,
  });
};

export const logout = () => {};
