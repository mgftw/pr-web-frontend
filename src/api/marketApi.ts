import axios from "axios";
import { type IBlackMarketItem } from "types/entities";
import { API_URL } from "$constants/url";

import { getAccessToken } from "$helpers/sessionHelper";

const PREFIX = `api/market`;

export const getBlackMarketInfo = async () => {
  const result = await axios<IBlackMarketItem[]>({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}/black_market`,
  });

  return result.data;
};
