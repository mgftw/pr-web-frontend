import axios from "axios";
import { API_URL } from "$constants/url";

import { getAccessToken } from "$helpers/sessionHelper";
import { IBuff } from "types/entities";

const PREFIX = `api/buffs`;

export const getBuffs = async () => {
  const res = await axios<IBuff[]>({
    method: "get",
    headers: {
      Authorization: `Bearer ${getAccessToken()}`,
    },
    url: `${API_URL}/${PREFIX}`,
  });

  return res.data;
};
