import styled from "styled-components";
import { rem } from "polished";
import tw from "twin.macro";
import colors from "$constants/colors";

export const Container = styled.div`
  ${tw`rounded-full border-solid border-4 border-quaternary`}
  ${({ size, gradient }) => `
    height: ${rem(`${size}px`)};
    width: ${rem(`${size}px`)};
    background: radial-gradient(${gradient.start}, ${gradient.end});
  `}
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
`;

export const Icon = styled.img`
  color: #fff;
  object-fit: contain;
  height: 60%;
  width: 60%;
`;

export const Level = styled.div`
  ${tw`font-karla text-primary rounded-full m-1`}
  display: flex;
  align-items: center;
  justify-content: center;
  ${({ size }) => `
    height: ${rem(`${size * 0.08}px`)};
    width: ${rem(`${size * 0.08}px`)};
    padding: ${rem(`${size * 0.08}px`)};
    font-size: ${rem(`${size * 0.15}px`)};
    border: 4px solid ${colors.red};
    outline: 4px solid ${colors.primary};
  `}
`;

export const LevelContainer = styled.div`
  ${tw`font-karla text-primary bg-red rounded-full`}
  bottom: 0;
  right: 0;
  position: absolute;
  ${({ size }) => `
    transform: translate(${rem(`${size * 0.1}px`)}, ${rem(`${size * 0.1}px`)});
  `}
  border: 4px solid ${colors.second};
`;
