import React from "react";
import PropTypes from "prop-types";

import { SURVIVOR_CLASSES } from "$constants/types";
import { getClassGradient } from "$constants/colors";

import debufferIcon from "$assets/class-icon/debuffer.svg";
import agileIcon from "$assets/class-icon/agile.svg";
import controllerIcon from "$assets/class-icon/controller.svg";
import gladiatorIcon from "$assets/class-icon/gladiator.svg";
import hunterIcon from "$assets/class-icon/hunter.svg";
import supportIcon from "$assets/class-icon/support.svg";
import technicianIcon from "$assets/class-icon/technician.svg";

import * as st from "./ClassEmblem.styles";

const getClassIcon = (classId) => {
  switch (classId) {
    case SURVIVOR_CLASSES.TECHNICIAN: {
      return technicianIcon;
    }
    case SURVIVOR_CLASSES.SUPPORT: {
      return supportIcon;
    }
    case SURVIVOR_CLASSES.CONTROLLER: {
      return controllerIcon;
    }
    case SURVIVOR_CLASSES.AGILE: {
      return agileIcon;
    }
    case SURVIVOR_CLASSES.DEBUFFER: {
      return debufferIcon;
    }
    case SURVIVOR_CLASSES.HUNTER: {
      return hunterIcon;
    }
    case SURVIVOR_CLASSES.GLADIATOR: {
      return gladiatorIcon;
    }
    default:
      return null;
  }
};

const ClassEmblem = ({ classId, size, classLevel }) => (
  <st.Container size={size} gradient={getClassGradient(classId)}>
    <st.Icon src={getClassIcon(classId)} />

    {classLevel && (
      <st.LevelContainer size={size}>
        <st.Level size={size}>{classLevel}</st.Level>
      </st.LevelContainer>
    )}
  </st.Container>
);

ClassEmblem.propTypes = {
  classId: PropTypes.number.isRequired,
  classLevel: PropTypes.number,
  size: PropTypes.number,
};

ClassEmblem.defaultProps = {
  classLevel: null,
  size: 32,
};

export default ClassEmblem;
