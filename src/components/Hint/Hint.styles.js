import styled from "styled-components";

import tw from "twin.macro";

export const Hint = styled.div`
  ${tw`flex flex-col p-4`}
  max-width: 30rem;
  > div {
    ${tw`flex items-center justify-start`}
  }

  i {
    ${tw`mr-2 text-lg text-primary`}
  }

  h4 {
    ${tw`font-oswald uppercase text-primary text-lg m-0 p-0`}
  }

  p {
    ${tw`m-0 p-0 font-karla text-secondary text-left`}
  }
`;
