import React from "react";

import PropTypes from "prop-types";
import Icon from "$components/Icon";

import * as st from "./Hint.styles";

const Hint = ({ children, icon, renderIcon, title }) => (
  <st.Hint>
    <div>
      {icon && (
        <i>
          <Icon icon={icon} />
        </i>
      )}
      {renderIcon && <i>{renderIcon()}</i>}
      <h4>{title}</h4>
    </div>
    <p>{children}</p>
  </st.Hint>
);

Hint.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  icon: PropTypes.string,
  renderIcon: PropTypes.func,
};

Hint.defaultProps = {
  icon: null,
  renderIcon: null,
};

export default Hint;
