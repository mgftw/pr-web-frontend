import colors from "$constants/colors";
import { BUCKET_URL } from "$constants/url";
import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div<{ $borderWidth?: number; $borderColor?: string }>(
  ({ $borderWidth, $borderColor }) => [
    tw`h-full w-full relative bg-primary rounded-full border-3 border-quaternary`,
    `
    container-type: inline-size;
    overflow: hidden;
  `,
    ...($borderWidth ? [`border-width: ${$borderWidth}px;`] : []),
    ...($borderColor ? [`border-color: ${colors[$borderColor]};`] : []),
  ],
);

export const Content = styled.div<{ $url: string }>(({ $url }) => [
  tw`relative h-full w-full flex-grow-0 mr-2 rounded bg-transparent`,
  `
    box-sizing: border-box;
    background-image: url(${$url});
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
  `,
]);

const NPCIcon = ({
  npcId,
  size = "small",
  borderWidth,
  borderColor,
}: {
  npcId: number;
  size?: "small" | "medium" | "large";
  borderWidth?: number;
  borderColor?: string;
}) => {
  return (
    <Container $borderWidth={borderWidth} $borderColor={borderColor}>
      <Content $url={`${BUCKET_URL}/icons/npc/${npcId}/${size}.png`} />
    </Container>
  );
};

export default NPCIcon;
