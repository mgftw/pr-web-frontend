import FullScreen from "$components/Layouts/FullScreen";
import TabBar from "$components/TabBar";
import { Outlet } from "react-router";

import * as css from "./styles";
import SideMenu from "$components/SideMenu/SideMenu";
import { useState } from "react";
import PlayerBar from "$components/PlayerBar/PlayerBar";

import Icon from "$components/Icon";

const BasePage = ({
  icon,
  title,
  tabOptions,
  addNavbarSpacing = true,
}: {
  icon: string;
  title: string;
  addNavbarSpacing?: boolean;
  tabOptions: {
    label: string;
    regex: string | RegExp;
    url: string;
  }[];
}) => {
  const [isSideMenuOpen, setIsSideMenuOpen] = useState(false);

  const onOpenMenu = () => {
    setIsSideMenuOpen(true);
  };

  const onCloseMenu = () => {
    setIsSideMenuOpen(false);
  };

  return (
    <FullScreen>
      <SideMenu isOpen={isSideMenuOpen} onClose={onCloseMenu} />
      <css.Container>
        <css.Header>
          <css.TitleContainer>
            <css.Title>
              <css.Icon>
                <Icon icon={icon} size={22} />
              </css.Icon>
              {title}
            </css.Title>
            <css.TabBarContainer>
              <TabBar options={tabOptions} />
            </css.TabBarContainer>
          </css.TitleContainer>
          <css.PlayerBarContainer>
            <PlayerBar onHamburgerTrigger={onOpenMenu} />
          </css.PlayerBarContainer>
        </css.Header>
        <css.Content $addNavbarSpacing={addNavbarSpacing}>
          <Outlet />
        </css.Content>
      </css.Container>
    </FullScreen>
  );
};

export default BasePage;
