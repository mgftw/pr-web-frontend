import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`pt-5 pl-0  flex-grow w-full`}
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export const Header = styled.div`
  ${tw`ml-5`}
  display: flex;
`;

export const Content = styled.div<{ $addNavbarSpacing?: boolean }>(
  ({ $addNavbarSpacing = true }) => [
    tw`pt-3 h-full`,
    `overflow: scroll;`,
    $addNavbarSpacing ? tw`ml-5` : "",
  ],
);

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const TabBarContainer = styled.div`
  ${tw`ml-4`}
`;

export const PlayerBarContainer = styled.div`
  ${tw`mr-5`}
  display: flex;
  align-items: flex-start;
  justify-content: flex-end;
  flex: 1;
`;

export const TitleWrapper = styled.div`
  display: flex;
  column-gap: 2rem;
  min-width: 40rem;
`;

export const Title = styled.div`
  ${tw`flex font-oswald text-2xl uppercase font-bold items-center`}
`;

export const Icon = styled.div`
  ${tw`font-karla rounded-full bg-primary text-zero`}

  display: flex;
  align-items: center;
  font-weight: bold;
  justify-content: center;
  padding: 0.35rem;
  margin-right: 0.75rem;
`;

export const Description = styled.div`
  ${tw`mt-2 text-lg text-tertiary`}
`;
