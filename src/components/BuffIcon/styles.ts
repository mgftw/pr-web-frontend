import styled, { keyframes } from "styled-components";
import tw from "twin.macro";
import { motion } from "framer-motion";

import { buffCategoryColors } from "$constants/colors";

const categoryColors = {
  quest: "#6E837C",
  event: "transparent",
  versus: "#B85B5B",
  craft: "#B88D5B",
};

const bgAnim = keyframes`
50% {
  background-position: 100% 50%;
}
`;

const borderWidth = "1px";

export const Container = styled.div`
  ${tw`h-full w-full relative`}
  container-type: inline-size;
`;

export const Placeholder = styled.div<{ $subCat: number }>(({ $subCat }) => [
  tw`h-full w-full font-oswald text-center rounded float-left`,
  `
    container-type: inline-size;
    background-color: ${buffCategoryColors[Object.keys(buffCategoryColors)[$subCat - 1]]};
    box-shadow: 0px 0px 1px 2px black inset;
    text-shadow:
      -1px -1px 0 #000,
      1px -1px 0 #000,
      -1px 1px 0 #000,
      1px 1px 0 #000;
    line-height: 38px;
    font-size: 35cqw;
    display: flex;
    align-items: center;
    justify-content: center;
  `,
]);

export const Content = styled.div<{ $url: string; $category: number }>(({ $url, $category }) => [
  tw`relative h-full w-full flex-grow-0 mr-2 rounded bg-transparent`,
  `
    box-sizing: border-box;
    background-image: url(${$url});
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
  `,
  $category !== null
    ? `box-shadow: 0px 0px 3px 2px ${categoryColors[$category]}, 0px 0px 1px 2px black inset;`
    : "",
]);

export const Handler = styled.div`
  ${tw`h-full w-full`}
  position: relative;
  cursor: pointer;
  z-index: 2;
`;

export const DetailsContent = styled(motion.div)<{ $isVisible: boolean }>`
  position: fixed;
  top: 3rem;
  right: 20rem;
  transform: translateX(14rem);
  ${({ $isVisible }) => !$isVisible && `pointer-events: none; z-index: 300;`}
`;

export const QuantityContainer = styled.div`
  ${tw`text-primary absolute bottom-0 right-0 mb-2 mr-2 rounded w-8 text-right font-bold`}
  text-shadow: 0px 0px 2px #2a2a2a;
`;

export const BuffDescription = styled.div`
  ${tw`pt-2 pb-2 pr-2 pl-2 w-64 flex rounded-lg`}
`;

export const ContentSmall = styled.div<{ $url: string }>`
  ${tw`h-24 w-24 rounded-lg bg-primary`}
  ${({ $url }) => `background-image: url(${$url})`};
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
`;

export const Info = styled.div`
  ${tw`w-48`}
`;

export const Name = styled.div`
  ${tw`font-oswald`}
`;

export const Description = styled.div`
  ${tw`font-karla`}
`;

export const Decorator = styled.div`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  z-index: 1;
  font-size: 2.5rem;
  text-transform: uppercase;
  color: white;
  background: #222;
  border-radius: ${borderWidth};

  &::after {
    position: absolute;
    content: "";
    top: calc(-1 * ${borderWidth});
    left: calc(-1 * ${borderWidth});
    z-index: -1;
    width: calc(100% + ${borderWidth} * 2);
    height: calc(100% + ${borderWidth} * 2);
    background: linear-gradient(
      60deg,
      hsl(224, 85%, 66%),
      hsl(269, 85%, 66%),
      hsl(314, 85%, 66%),
      hsl(359, 85%, 66%),
      hsl(44, 85%, 66%),
      hsl(89, 85%, 66%),
      hsl(134, 85%, 66%),
      hsl(179, 85%, 66%)
    );
    background-size: 300% 300%;
    background-position: 0 50%;
    border-radius: calc(2 * ${borderWidth});
    animation: ${bgAnim} 4s alternate infinite;
    filter: blur(1px);
  }
`;
