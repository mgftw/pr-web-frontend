import { BUCKET_URL } from "$constants/url";

import * as st from "./styles";

import { getStringInitials } from "$helpers/stringHelper";

const types = {
  "|Q|": "quest",
  "|E|": "event",
  "|V|": "versus",
  "|C|": "craft",
};

const BuffIcon = ({
  buff,
  showOutline = false,
  size = "small",
}: {
  buff: {
    name: string;
    description: string;
    buff_id: number;
    sub_category: number;
  };
  showOutline?: boolean;
  size?: "small" | "medium" | "large";
}) => {
  const match = buff.name.match(/\|(C|E|V|Q)\|/);
  const category = match !== null && showOutline === true ? types[match[0]] : null;
  let subCat = buff.sub_category;

  if (subCat <= 0) subCat = 7;

  let { name } = buff;
  if (match !== null) {
    name = name.replace(`${match[0]} `, "");
  }
  const initials = getStringInitials(name);

  const isSpecialBuff = category === "event";

  return (
    <st.Container>
      {isSpecialBuff && <st.Decorator />}
      <st.Handler>
        <st.Placeholder $subCat={subCat}>{initials}</st.Placeholder>
        <st.Content
          title={name}
          $url={`${BUCKET_URL}/icons/buff/${buff.buff_id}/${size}.png`}
          $category={category}
        />
      </st.Handler>
    </st.Container>
  );
};

export default BuffIcon;
