import { createGlobalStyle } from "styled-components";
import tw from "twin.macro";

export default createGlobalStyle`
  body, html{
    font-size: 14px;
    ${tw`font-karla`}
  }

  body {
    font-size: 14px;
    padding: 0;
    margin: 0;

    .tippy-popper {
      max-width: inherit !important;
      transition-duration: 0ms !important;
    }

    .tippy-tooltip {
      background-color: #333 !important;
    }

    .Toastify__toast--error {
      background: #B85B5B;
      border-radius: 5px;
    }

    .Toastify__toast-body {
      font-family: Karla;
    }
  }

  @media
  only screen and (-webkit-min-device-pixel-ratio: 2) and ( max-width: 2000px),
  only screen and (   min--moz-device-pixel-ratio: 2) and ( max-width: 2000px),
  only screen and (     -o-min-device-pixel-ratio: 2/1) and ( max-width: 2000px),
  only screen and (        min-device-pixel-ratio: 2) and ( max-width: 2000px),
  only screen and (                min-resolution: 192dpi) and ( max-width: 2000px),
  only screen and (                min-resolution: 2dppx) and ( max-width: 2000px) { 
      body, html {
          font-size: 16px;
      }
  }
`;
