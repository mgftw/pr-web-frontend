import styled from "styled-components";
import tw from "twin.macro";

export const ProgressBarContainer = styled.div`
  ${tw`bg-first flex items-center justify-end font-karla`}
  width: 100%;
  height: 6px;
  ${({ height }) => height !== null && `height: ${height}px;`}
  ${({ width }) => width !== null && `width: ${width}px;`}
  ${({ value, color, backColor }) =>
    value !== null && `background: linear-gradient(90deg, ${color} ${value}%, ${backColor} 0%);`}

  p {
    ${tw`m-0 p-0 mr-2`}
    font-size: 90%;
    color: white;
    mix-blend-mode: difference;
  }
`;
