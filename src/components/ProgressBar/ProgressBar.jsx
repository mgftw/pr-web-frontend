import React from "react";
import PropTypes from "prop-types";
import colors from "$constants/colors";

import * as st from "./styles";

const ProgressBar = ({ height, width, value, color, backColor, showPercent }) => {
  return (
    <st.ProgressBarContainer
      height={height}
      width={width}
      value={value}
      color={colors[color]}
      backColor={colors[backColor]}
    >
      {showPercent && <p>{value}%</p>}
    </st.ProgressBarContainer>
  );
};

ProgressBar.propTypes = {
  height: PropTypes.number,
  width: PropTypes.number,
  value: PropTypes.number.isRequired,
  color: PropTypes.string,
  backColor: PropTypes.string,
  showPercent: PropTypes.bool,
};

ProgressBar.defaultProps = {
  color: "primary",
  backColor: "first",
  width: null,
  height: 4,
  showPercent: false,
};

export default ProgressBar;
