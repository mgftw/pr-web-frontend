import styled from "styled-components";
import tw from "twin.macro";

import colors from "$constants/colors";

export const Container = styled.div`
  ${tw`bg-first p-4`}
  border-radius: 0.5rem;
  display: grid;
  grid-template-rows: min-content min-content 1fr;
  overflow: auto;

  ${({ backgroundColor }) =>
    backgroundColor ? `background-color: ${colors[backgroundColor] ?? backgroundColor};` : null}
`;

export const Header = styled.div`
  height: 2.2rem;
`;

export const Title = styled.div`
  ${tw`self-start flex items-center text-base font-bold font-karla`}
`;

export const Divider = styled.div`
  height: 3px;
  background-color: ${colors.zero};
`;

export const Body = styled.div`
  ${tw`mt-3 mb-1`}
  max-height: 100%;
  overflow: auto;
`;
