import React from "react";
import PropTypes from "prop-types";

import * as st from "./ModuleCard.styles";

const ModuleCard = ({ children, title, backgroundColor }) => (
  <st.Container backgroundColor={backgroundColor}>
    <st.Header>
      <st.Title>{title}</st.Title>
    </st.Header>
    <st.Divider />
    <st.Body>{children}</st.Body>
  </st.Container>
);

ModuleCard.propTypes = {
  backgroundColor: PropTypes.string,
  title: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

ModuleCard.defaultProps = {
  backgroundColor: null,
  children: null,
  title: null,
};

export default ModuleCard;
