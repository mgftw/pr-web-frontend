import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`flex flex-row`}
`;

export const Tab = styled.div<{ isActive?: boolean }>`
  ${tw`bg-zero text-primary px-5 py-2 text-base rounded ml-1 mr-1 first:ml-0 font-oswald uppercase`}
  cursor: pointer;

  ${({ isActive }) => isActive && tw`bg-primary text-zero`}
`;
