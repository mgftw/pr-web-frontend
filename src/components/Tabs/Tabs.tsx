import * as st from "./Tabs.styles";

const Tabs = ({ children }) => <st.Container>{children}</st.Container>;

Tabs.Tab = st.Tab;

export default Tabs;
