import { Tooltip } from "react-tippy";

import InventoryItemDetails from "$components/InventoryItemDetails";
import InventoryItemIcon from "$components/InventoryItemIcon";
import * as st from "./styles";

const InventoryItem = ({
  size = "large",
  quantity = 1,
  item,
  showQuantity = true,
}: {
  item: {
    name: string;
    description: string;
    id: number;
  };
  quantity?: string | number;
  size?: "small" | "medium" | "large";
  showQuantity?: boolean;
}) => {
  return (
    <Tooltip trigger="mouseenter" html={<InventoryItemDetails item={item} />}>
      <st.Content $size={size}>
        <InventoryItemIcon item={item} size={size} />
        {showQuantity && quantity && <st.Quantity>{quantity}</st.Quantity>}
      </st.Content>
    </Tooltip>
  );
};

export default InventoryItem;
