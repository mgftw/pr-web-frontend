import styled from "styled-components";
import tw from "twin.macro";

export const Content = styled.div<{ $size: string }>(({ $size }) => [
  tw`h-full w-full flex-grow-0 mr-2 rounded-lg text-3xl`,
  `
  ${(() => {
    switch ($size) {
      case "small": {
        return "height: 2.5rem; width: 2.5rem;";
      }
      case "medium": {
        return "height: 3.5rem; width: 3.5rem;";
      }
      case "large": {
        return "height: 5rem; width: 5rem;";
      }
      default: {
        return "";
      }
    }
  })()}
  position: relative;
`,
]);

export const Quantity = styled.div`
  ${tw`font-karla text-sm rounded-full bg-quaternary font-bold px-2`}
  font-size: 40%;
  position: absolute;
  bottom: -10%;
  right: -10%;
  z-index: 10;
`;

export const ItemDescription = styled.div`
  ${tw`pt-2 pb-2 pr-2 pl-2 bg-second flex rounded-lg text-sm`}
  width: 24rem;
`;

export const Info = styled.div`
  ${tw`w-64 ml-8`}
`;

export const Name = styled.div`
  ${tw`font-oswald text-lg`}
`;

export const Description = styled.div`
  ${tw`font-karla`}
`;

export const Placeholder = styled.div`
  ${tw`h-20 w-20 font-oswald bg-secondary text-center rounded-lg float-left absolute text-xl`}
  box-shadow: 0px 0px 1px 2px black inset;
  text-shadow: 0px 0px 5px #111;
  line-height: 4.75rem;
`;
