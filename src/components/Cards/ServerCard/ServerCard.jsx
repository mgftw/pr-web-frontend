import React from "react";
import PropTypes from "prop-types";

import * as st from "./ServerCard.styles";

export const ServerCard = ({ server, onDelete }) => (
  <st.Container>
    <div>{server?.name}</div>
    <div>{server?.region}</div>
    <div>{server?.type}</div>
    <div>{server?.token}</div>
    <div>
      <button type="button" onClick={onDelete}>
        Delete
      </button>
    </div>
  </st.Container>
);

ServerCard.propTypes = {
  server: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    region: PropTypes.string,
    token: PropTypes.string,
    type: PropTypes.string,
  }).isRequired,
  onDelete: PropTypes.func,
};

ServerCard.defaultProps = {
  onDelete: () => {},
};

export default ServerCard;
