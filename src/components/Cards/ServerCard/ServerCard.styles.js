import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`flex p-4`}

  > div {
    padding-right: 0.5rem;
  }
`;
