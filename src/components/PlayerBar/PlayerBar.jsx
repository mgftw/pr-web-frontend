import React from "react";

import ProgressBar from "$components/ProgressBar";
import Icon from "$components/Icon";

import * as st from "./styles";
import { useMyProfile } from "$hooks/useMyProfile";

const PlayerBar = ({ onHamburgerTrigger }) => {
  const { profile: stats } = useMyProfile();

  return (
    <st.Container>
      <st.AvatarContainer>
        <st.Avatar url={stats.avatar_url} />
      </st.AvatarContainer>
      <st.MainInfo>
        <st.Name>{stats.name ?? "Loading..."}</st.Name>
        <st.ExpProgressContainer>
          <ProgressBar height={4} width={240} value={Math.floor(stats.exp_percent * 100)} />
          <st.LevelPercent>
            {stats.exp_percent > 1 ? 100 : Math.floor(stats.exp_percent * 100)}%
          </st.LevelPercent>
        </st.ExpProgressContainer>
      </st.MainInfo>
      <st.BarTriggerContainer onClick={onHamburgerTrigger}>
        <Icon icon="icon-hamburger" size={18} />
      </st.BarTriggerContainer>
    </st.Container>
  );
};

export default PlayerBar;
