import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`p-1 flex flex-row bg-second rounded-lg items-center`}
  right: 0;
  top: 0;
`;

export const AvatarContainer = styled.div`
  ${tw`flex items-center flex-col justify-center`}
`;

export const MainInfo = styled.div`
  ${tw`w-32 ml-2 mr-2`}
`;

export const BarTriggerContainer = styled.div`
  ${tw`rounded-full bg-primary h-6 w-6 text-zero flex items-center justify-center`}
  cursor: pointer;
`;

export const Name = styled.div`
  ${tw`text-sm font-bold flex-grow truncate mt-1`}
`;

export const Avatar = styled.div`
  ${tw`rounded-full bg-secondary h-8 w-8`}
  ${({ url }) => (url ? `background-image: url(${url});` : "")}
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

export const Level = styled.div`
  ${tw`text-left leading-loose text-gray-300 text-xs`}
  white-space: nowrap;
  line-height: 1.2;
`;

export const Header = styled.div`
  ${tw`flex`}
`;

export const ExpProgressContainer = styled.div`
  ${tw`flex flex-row items-center`}
  margin-top: 2px;
`;

export const LevelPercent = styled.div`
  ${tw`flex text-center flex-grow justify-center font-bold pl-2 pr-2 text-xs`}
`;

export const BottomCardContainer = styled.div`
  ${tw`mt-10 w-full flex flex-col`}
  z-index: 5;
`;

export const BottomCard = styled.div`
  ${tw`bg-first rounded w-full pt-8 pb-4`}
`;
