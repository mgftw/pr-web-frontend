import { useMemo, useState } from "react";

import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { toast } from "react-toastify";
import { SURVIVOR_CLASSES } from "$constants/types";
import { getClassGradient } from "$constants/colors";

import Modal from "$components/Modal";
import ClassEmblem from "$components/ClassEmblem";

import * as st from "./ClassSwitchModal.styles";
import { getMyClassInfo, switchClass } from "$api/playerApi";
import { MY_CLASS_PROGRESS, MY_PROFILE as QUERY_MY_PROFILE } from "$constants/queries";
import { IPlayerClassProgress } from "types/entities";

const CLASS_ABILITIES = {
  [SURVIVOR_CLASSES.TECHNICIAN]: [
    "Unlocked on becoming Technician:\n" +
      "- Focal Point: Boosts laser sight accuracy 25%.\n" +
      "- Scrap Collector: When Commons/Specials die or Tanks are hurt, they drop scrap you can pick up. Scrap is used for class abilities.\n" +
      "- Mobile Drone: Hold WALK + shove ground with weapon to prep Mobile Drone. Look + press ATTACK to send drone to that area. Drone is akin to Helping Drone but can't hurt tanks. Expires after 30s. Uses 5 scrap. 1 active Mobile Drone max.",

    "Unlocked on Technician Level 1:\n" +
      "- Tracker: Shove a Tank while holding ATTACK to cause its glow to be visible through walls for 30 seconds. Uses 5 scrap. Tanks with no glow are given one for the duration. 45 second immunity.\n" +
      "- Flexible Turret: Your turret now shoots in a 360 degree arc all around it. Pressing USE on your turret now recalls it, allowing you to place it elsewhere. Your turret can also be recalled from the !buy menu if within 300 units.",

    "Unlocked on Technician Level 2:\n" +
      "- Tinker: +1% Drone/Turret enhancement chances, shots to CI/SI have 1/2/3/4/5/6% chance (Assassin Drone x2) to instantly kill/stun/ignite/stagger/cripple/fire bolt. If Tinker activates, hit deals x2 damage.\n" +
      "- Shoulder-Mounted Laser: On use attaches a laser that fires when you shoot every 15 seconds dealing 400 base damage per shot. Uses 10 scrap, lasts whole round, 750 max range. If laser misses SI/Tank, cooldown 5s. (Command: sm_mlaser)",

    "Unlocked on Technician Level 3:\n" +
      "- Guardian Protocols: If Helping/Assassin Drone hasn't shot in 4/8 seconds, gain 30% damage resist against next hit > 10 damage. Once active, drone shots won't cancel the effect.\n" +
      "- Modular Turret: Shove turret to upgrade it by spending scrap up to 9 times. Each upgrade +2% damage/range/fire rate/refire chance. Scrap cost is 3 + current level. Drones gain equal refire chance. Recall turret from afar, costing 5 scrap and 3 upgrade levels (5m CD).",

    "Unlocked on Technician Level 4:\n" +
      "- Accelerate: Use 15 scrap to reduce all ability cooldowns by 30 seconds. 90 second cooldown. (Command: sm_accelerate)\n" +
      "- Barrier: Use 15 scrap to spawn a barrier which lowers infected damage to 1, with 50% damage absorbed by the barrier. Barrier has 2000 HP. 90 second cooldown after Barrier breaks. (Command: sm_barrier)",

    "Unlocked on Technician Level 5:\n" +
      "- Burst Resonance: On use, all drones/turrets on the map release 400 radius shock burst after 3s, dealing 1,500 true damage, stuns 2s through debilitate cooldown, electrocutes 15s. CI/SI are ignited. Tanks hit by multiple bursts take 150 less damage per burst. Uses 40 scrap. 10 minute cooldown, 2 minute global cooldown. For 20s on use, all turrets stay awake, +5 upgrade levels (max 12). (Command: sm_burstres)",
  ],
  [SURVIVOR_CLASSES.SUPPORT]: [
    "Unlocked on becoming Support:\n" +
      "- Clutch Focus: When heal/defibbing an ally, you both resist lethal damage. Poison revive slow is halved. Every 3 damage interrupted revives is 30% slower but ignores interrupts.\n" +
      "- Heroic Sprint: If a pinned/down ally is in view, gain 15% move/recovery speed.\n" +
      "- Assist Return: Every 4 free/revive/heal/defib gives a bonus effect to you. 50% Duration but rises 4% each effect. Ally Revive/Heal/Defib heals allies 5 x AR HP and heals you for AR HP.",

    "Unlocked on Support Level 1:\n" +
      "- Guardian Angel: Shoving an ally gives them 15% defense and 20% move speed for 30 seconds. 20 second cooldown. Reviving an ally gives crowd control/pin immunity and 25% defense for 8 seconds. Being revived gives the same effects, but for half duration.\n" +
      "- Retribution: When ally is pinned, downed or killed gain 15% attack and defense against the attacker for 5 seconds. 15 second cooldown against Tanks.",

    "Unlocked on Support Level 2:\n" +
      "- Fall Cushion: If ally takes 100+ fall damage and lands in 150 range, -30% fall damage, you take remainder. If ally body would be broken from fall death, you die to save body. Body save is once per round on saved ally. +1 Assist Return on 750 damage taken/body save.\n" +
      "- Safety Clear: On ally free/revive/heal/defib, ally gains 3s Safety Clear, shoving near infected for 100 damage/s, clearing bile. Assist Return effect gives you Safety Clear.",

    "Unlocked on Support Level 3:\n" +
      "- Area Support: +25% support AOE skill cooldown + range. Cleansing Pills clears nearby allies. +33% First Aid Saving efficiency, usable on allies. +15% Health/Guardian Dispenser heal.\n" +
      "- Adrenaline Dart: Looking at downed ally in 750 units, press ZOOM with adrenaline to throw dart, reviving them with 25% less health but 15s adrenaline. Missed darts returned. Adrenaline auto equipped on resuscitate if dropped on death. (Clinic swap: WALK+ZOOM)",

    "Unlocked on Support Level 4:\n" +
      "- Gratuitous Offer: -10% health item bounty cost, chance to cost 0 bounty equal to Assist Return, max 30%. 90s cooldown on success.\n" +
      "- CPR: Shove dead body in 15s of death for 5% chance to resurrect them. No shove penalty. On success, revive with half health, 5s Motivate/Guardian Angel/Courage. Hold WALK + shove to carry body with reduced mobility. Shove ground to drop. Dropped on pin/fling/down/death/after 45s.",

    "Unlocked on Support Level 5:\n" +
      "- Blessing: On use, you grant all allies a Blessing, motivating them, removing/blocking slows/statuses/bile, healing and increasing max HP equal to (ally level + 50) and giving 50 HP regen each second. Duration equals Assist Return count (min 15s, max 30s). Each ally affected gives Assist Return. 10 minute cooldown, 2 minute global cooldown. (Command: sm_blessing)",
  ],
  [SURVIVOR_CLASSES.CONTROLLER]: [
    "Unlocked on becoming Controller:\n" +
      "- Rage: On SI pin attempt/1000 non-Tank damage pre-reduction, +1% rage, heal 1% missing HP. +1% rage per 10s w/o rage gain.\n" +
      "- Boxing Bag: +30% SI aggro. +10% defense/damage resist from non-Tank infected. +1% rage on SI aggro redirect if <75%.\n" +
      "- Torment: On use for 20s, +40% defense, aggro CI/SI in 400 range, block CI/SI pin/stagger/slow/lethal damage. Needs 15% rage (no cost), heal max temp HP on use. 3 minute cooldown. (Command: sm_torment)",

    "Unlocked on Controller Level 1:\n" +
      "- Retaliate: On pin block/free, gain attack/defense/slow resist for 10 seconds equal to 5 + (0.2x rage %). Rage bonus halved to Tanks. 20s CD from pin block, pin free ignores CD.\n" +
      "- Overcharge: Doubles maximum temporary health and increases temporary health gain from all sources by 50%. Temporary health is no longer limited by maximum health. Enables self reviving with Pills/Adrenaline.",

    "Unlocked on Controller Level 2:\n" +
      "- Reload Priority: If a weapon reload is interrupted twice in a row by infected pins or Tank punch/rock stuns, you will be immune to these effects for the duration of your next reload.\n" +
      "- Natural Fear: 15% less move/attack speed and slap power to all SI in sight in 500 units. 15% chance to fear/cripple nearby SI for 3 seconds. On SI cripple, heal 50 temp HP, chance to gain rage equal to 1/3 missing rage % if <75%.",

    "Unlocked on Controller Level 3:\n" +
      "- Rage Burst: While pinned, press USE to free all allies in 400 units, flinging SI and stopping pins for 5 seconds. Uses 15% rage but gain 1% rage, heal 50 temp HP per SI affected (max 5). Usable while Mutated but no rage gain/heal.\n" +
      "- Defiance: On use, for 20 seconds all allies block pins/slaps/staggers/CI cripples, gain 5s immunity to Tank punch/rock stuns. Heals max temp HP to team on use. Uses 50% rage. (Command: sm_defiance)",

    "Unlocked on Controller Level 4:\n" +
      "- Hyper Recover: +30% + (0.4x rage %) recovery speed from pins/staggers. Status time -33% from non-Tanks. On pin free, blocks pins/staggers 3s. Permanent health gain +15%. Slows start decaying 25% faster.\n" +
      "- Anti-Mutation Order: -20% SI mutate chance, x2 cooldown. While Mutated, resist non-Tank lethal damage and small fall damage. On Mutation end heal 500 HP or self-revive if downed. Block SI mutation for self/team during Torment/Defiance.",

    "Unlocked on Controller Level 5:\n" +
      "- Staunched Bleeding: On taking 5+ infected non-fatal damage, only 25% is dealt with remainder being dealt at a rate of 25%/second over 3 seconds. Remainder damage paused while damage immune, cleared on death/medkit use, non-Tank damage cleared on mutate end. With Torment, -60s CD, return 50% CI/SI claw damage, first hit on SI heals 50 temp HP. With Defiance, -5% cost, team +20% resilience/recovery speed/slow/damage resist.",
  ],
  [SURVIVOR_CLASSES.AGILE]: [
    "Unlocked on becoming Agile:\n" +
      "- Dodge: You lose 10% max health but gain a 10% chance to evade infected damage. The chance rises as your health drops, double if below 20% HP or down. Chance is halved from Tanks.\n" +
      "- Blur: Special Infected are 15% less likely to target you and you generate 10% less Tank aggro.\n" +
      "- Phase Strike: If you hit, shove or are hit by a common infected, that common infected will no longer be able to body block survivors.",

    "Unlocked on Agile Level 1:\n" +
      "- Evasive Manoeuvres: While a Tank has aggroed you, gain 5% move speed and 10% slow resist. If looking and running away from it, gain an additional 10% move speed and 20% slow resist.\n" +
      "- Limber: Increases fire rate/reload speed of semi automatic weapons by 5%/10%. Gain 5% move speed, 15% slow resist. Paralyze/Broken Leg status duration -25%. Mobility Master cooldown -3s. Lowers fall damage equal to half dodge chance after fall damage (max 20%).",

    "Unlocked on Agile Level 2:\n" +
      "- Surge: On use, +20% dodge chance, +10% move speed, clear/block non-status slows 15 seconds. No Tank dodge penalty during Surge. 90 second cooldown. On CD, -0.25s CD on dodge. (Command: sm_surge)\n" +
      "- Impulse: Gain attack % equal to (class level x 18%) move speed bonuses (max 60% attack). Gives chance to instantly kill SI on hit equal to 5% move speed bonus. From class level 4, pierce defenses equal to 25% move speed bonuses (max 15%).",

    "Unlocked on Agile Level 3:\n" +
      "- Expert Dodge: Lowers max health by another 15% but doubles base chance to dodge and permanently removes Tank dodge chance penalty. On resuscitation, 100% dodge for 3 seconds.\n" +
      "- Catch Up: After being freed from a pin, revived or resuscitated you gain 15% move, reload, deploy, recovery, slow resist and Mobility Master cooldown speed for 5 seconds. Every 10 seconds, a Tank punch will activate Catch Up for 2.5 seconds if not down.",

    "Unlocked on Agile Level 4:\n" +
      "- Breath of Air: Recover 1-15 HP on a successful dodge if below 75% HP. Heal increases the lower your health or if incapacitated.\n" +
      "- Elusive: While pinned, if you dodge damage from the pinner, you gain a chance to free yourself equal to your current dodge chance. On success, all common and special infected will ignore you and you ignore non-status slows for 10 seconds.",

    "Unlocked on Agile Level 5:\n" +
      "- Smoke Bomb: On use, stagger/3s blind to infected in 250 range, become invisible for 10s, making infected (not desperate Tanks) unable to target you. Cannot attack but move at x2 speed, +10% dodge, ignore all slows and claw hits. Has 3 charges, 4 minute recharge. With 0 charges, +50% CD speed. Can stack to extend. Can cancel if < 5s active with ATTACK+ZOOM. On end, gain 5 seconds of Surge/Catch Up/Elusive. (Command: sm_smokebomb)",
  ],
  [SURVIVOR_CLASSES.DEBUFFER]: [
    "Unlocked on becoming Debuffer:\n" +
      "- Antiviral Pills: Shove Special/Tank with Pills for 15s poison, dealing true damage the faster they move, reducing heal effects 25%. 30 second cooldown. 50% less aggro from poison damage. Item not consumed if used on Special.\n" +
      "- Contagious: 33% chance to return debuff from Specials, 2x duration.\n" +
      "- Scourge: On each debuff to an infected, strength of class item based debuffs +1% up to 50%. Gain 1 bounty per infected debuffed.",

    "Unlocked on Debuffer Level 1:\n" +
      "- Muscle Inhibitor: Shove Special/Tank with Adrenaline to inhibit ability to claw attack for 6/3 seconds. 30 second cooldown and immunity. Item not consumed if used on Special. On Tank use, cannot punch stun and -30% punch damage/knockback for 3 seconds.\n" +
      "- Mark: Pressing USE while looking at a Tank increases the damage it takes from all survivors by 8% for 20 seconds. Marks on the same target resets duration. 45 second cooldown.",

    "Unlocked on Debuffer Level 2:\n" +
      "- Gastric Vomit: Biling any infected slows their move and attack speed by 20%. Double to non-Tanks. Decays over time. Cannot refresh until bile expires.\n" +
      "- Berserk Gene: Shove Special/Tank with a Pipe bomb to enrage it, making it move/attack 10% faster, but cannot use non-Fury actives for 8 seconds. 30 second cooldown, 60 second immunity. Item not consumed if used on Special.",

    "Unlocked on Debuffer Level 3:\n" +
      "- Enhanced Mark: Marking an already marked target adds more effects. 2nd mark lowers Tank's speed 8%. 3rd mark lowers Tank's damage 8%. 4th mark lowers Tank's defense 8%. On 4th mark, Tank cannot be marked until expires.\n" +
      "- Inhibiting Vomit: Biling Tanks pauses ability cooldown timers for 5 seconds. Biling a non-self biled Tank reapplies bile effects once. If reapplying an effect, duration halved. Cannot refresh until bile expires.",

    "Unlocked on Debuffer Level 4:\n" +
      "- Crack Shot: SI/Tank headshots have chance to stagger and cripple. Chance based on damage, lower SI chance. 45s immunity. Ignores stagger CD. If Tank aggroing you, clear aggro.\n" +
      "- Over Exposure: +50% Antiviral Pills/Muscle Inhibitor/Mark/Berserk Gene/Inhibiting Vomit time. Every 90 seconds, if Tank has stun/freeze CD, you can inflict other status ignoring CD but -50% status time.",

    "Unlocked on Debuffer Level 5:\n" +
      "- Outbreak: On use, emit noxious bursts around you, each dealing 100 true damage, +1 Outbreak stack, -1% attack/defense/move/attack speed for 5s, after which they decay. First hit staggers, +3 stacks, resets status immunity time. With a stack, inflicting status adds 1 stack. Burst count = Scourge % (min. 20). 10 minute cooldown, 2 minute global cooldown. +10% move speed, -30% knockback during Outbreak/holding debuff items. (Command: sm_outbreak)",
  ],
  [SURVIVOR_CLASSES.HUNTER]: [
    "Unlocked on becoming Hunter:\n" +
      "- Safety First: Friendly blast/fire damage to/from allies and self is negated.\n" +
      "- Scrap Collector: When Commons/Specials die or Tanks are hurt, they drop scrap you can pick up. Scrap is used for class abilities.\n" +
      "- Lure: With secondary, press USE+ZOOM to launch, luring infected to attack it, dealing 50 damage/s. Lure success lowers the more lured. Uses 5 scrap. 10s cooldown. Explodes for 500 blast damage after 20s.",

    "Unlocked on Hunter Level 1:\n" +
      "- Throwable Pouch: Enables holding one of each throwable. Incompatible with Infirmary buff. Press throwable hotkey (default: 3) to swap between them. If throwable is dropped on death, pick it up on resuscitation if not taken. Enables throwables while down.\n" +
      "- Complimentary Munitions: Every 5th throwable thrown/deployed as trap will not consume it. Lowers bounty cost of throwables 10%/special ammo 20%. Special ammo gives 50% more ammo.",

    "Unlocked on Hunter Level 2:\n" +
      "- Augmented Grenades: +30% blast radius and Pipe bombs stagger tanks (15s CD). Vomit clouds persist 10s. Fire deals +80 damage/tick and hurts fire immune Tanks. Grenades ignore break/rebound effects.\n" +
      "- Scout Module: On use, attach Scout Module to next grenade/lure. Module deployed on detonation. Infected in 500 units made visible through walls and slowed. Tanks affected every 5s. Uses 6 scrap, lasts 60 seconds. (Command: sm_scout)",

    "Unlocked on Hunter Level 3:\n" +
      "- Bear Trap: WALK + shove ground to deploy, rooting SI/Tank for 6/2s, deals 750 true damage/s, 30% slow, pause abilities. Roots 3 SI/1 Tank. Crouch + shove returns unused trap. Uses 8 scrap. 20s immunity. 2m expire time. 5s into immunity, bypass once if aggroed by Tank but only 1s root.\n" +
      "- Predator: +10%/20%/30% attack to staggered, stunned, frozen/lured, scouted/rooted infected. -1% defense on each non-immune Bear Trap root, max 10%.",

    "Unlocked on Hunter Level 4:\n" +
      "- Restock: A random throwable is chosen, then 3 minutes later you are given it and 3 scrap. 1s time reduction on SI kill above 60s. Time paused below 60s if you have throwable.\n" +
      "- Splinter Addon: Physical damage to SI has 30% chance (blast 100%) to cause 10s bleed, dealing 100 true damage/s, stopping health regen. To Tanks/bleeding SI, chance on hit to deal 100 true damage, heal 40 HP. Chance = 2.5% damage dealt, 5-50%, x2 if SI/rooted.",

    "Unlocked on Hunter Level 5:\n" +
      "- Stockpile Draw: On use, for 12s gain adrenaline, +50% reload/deploy speed, x2 Splinter Addon values, block slows/punch stuns/knockdowns/SI pins/fire rate slows from statuses. Non custom weapons ignore range dropoff, use reserve ammo on shots, deal area damage. Heal 7.5% damage dealt with shots. +100% Restock CDR. Uses 10 scrap, 2 minute cooldown. (Command: sm_stockpile)",
  ],
};

const getClassString = (classId) => {
  switch (classId) {
    case SURVIVOR_CLASSES.TECHNICIAN:
      return "Technician";
    case SURVIVOR_CLASSES.SUPPORT:
      return "Support";
    case SURVIVOR_CLASSES.CONTROLLER:
      return "Controller";
    case SURVIVOR_CLASSES.AGILE:
      return "Agile";
    case SURVIVOR_CLASSES.DEBUFFER:
      return "Debuffer";
    case SURVIVOR_CLASSES.HUNTER:
      return "Hunter";
    case SURVIVOR_CLASSES.GLADIATOR:
      return "Martial";
    default:
      return null;
  }
};

const getClassDescription = (classId) => {
  switch (classId) {
    case SURVIVOR_CLASSES.TECHNICIAN:
      return `The technician is a master of all things mechanical. Enhancing their drones and turrets, they can act as a second survivor against the hordes of infected. The technician also comes with new tools to further aid themselves and the team.\n
      Gain bonus class exp. for:\n- Damage from Turrets/Drones to infected\n- Using some Technician abilities`;
    case SURVIVOR_CLASSES.SUPPORT:
      return `Supporting the team through thick and thin, the support may be enough to save their team from the jaws of defeat. Capable of helping every player ranging from new to veteran, a team's survivability increases dramatically when around.\n
      Gain bonus class exp. for:\n- Protecting/Freeing/Reviving/Healing/Defibbing teammates\n- Using some Support abilities`;
    case SURVIVOR_CLASSES.CONTROLLER:
      return `Taking the infected for yourself, you attract and repel them giving the rest of the team some breathing room. To compensate for the increased aggro, the Controller has boosted survivability.\n
      Gain bonus class exp. for:\n- Each Special Infected that fails to pin you\n- Using some Controller abilities`;
    case SURVIVOR_CLASSES.AGILE:
      return `The agile focuses on speed and evasion, sacrificing some bulk in exchange for a chance to completely ignore damage. The speed and stealth of the agile can prove to be a useful asset to defeating any foe with minimal return damage.\n
      Gain bonus class exp. for:\n- Dodging damage from the infected\n- Using some Agile abilities`;
    case SURVIVOR_CLASSES.DEBUFFER:
      return `It's difficult to debilitate Tanks or infected outside of stuns or freezing, which brings the debuffer into a world of its own. Capable of inflicting an array of effects upon the infected, the debuffer offers a unique skill set ready to size down any formidable foe.\n
      Gain bonus class exp. for:\n- Each Special/Tank you debuff, stagger or shove\n- Using some Debuffer abilities`;
    case SURVIVOR_CLASSES.HUNTER:
      return `The hunter is the king of explosives, throwables, and traps. Placing traps and lures in the path of the infected, they can devastate groups of hostiles, clearing the way for the team to focus on the bigger threats. Said bigger threats can still suffer great harm from the hunter, however.\n
      Gain bonus class exp. for:\n- Special/Tank Kills or Blast/Fire/Throwable damage to infected\n- Using some Hunter abilities`;
    case SURVIVOR_CLASSES.GLADIATOR:
      return `The Martial's strength is with melee weapons. Being able to gap close and stay in the fray, the Martial excels in melee proficiency, managing to self-sustain and keep themselves alive while also dealing serious damage to anything in range.`;
    default:
      return null;
  }
};

const ClassSwitchModal = ({
  initialClass,
  isOpen,
  onRequestClose,
}: {
  initialClass?: number;
  isOpen: boolean;
  onRequestClose: () => void;
}) => {
  const [selectedClass, setSelectedClass] = useState<number | null>(null);
  const [, setHoveredCircle] = useState<number | null>(null);
  const [lockedCircle, setLockedCircle] = useState<number | null>(null);

  const { data: classInfo = [] } = useQuery({
    queryKey: MY_CLASS_PROGRESS,
    queryFn: () => getMyClassInfo(),
  });

  const classInfoHash = useMemo<Record<number, IPlayerClassProgress>>(() => {
    return classInfo.reduce((prev, next) => {
      return {
        ...prev,
        [next.class_id]: next,
      };
    }, {});
  }, [classInfo]);

  const queryClient = useQueryClient();

  const {
    mutateAsync: doClassChange,
    isLoading: isSwitching,
    error,
  } = useMutation((classId) => switchClass(classId));

  const onChangeClass = async (classId) => {
    if (isSwitching) return;
    try {
      await doClassChange(classId);
      queryClient.invalidateQueries(QUERY_MY_PROFILE);
      toast.success("Class changed");
      setSelectedClass(null);
      onRequestClose();
    } catch (err) {
      toast.error(err?.response?.data?.error ?? err.toString() ?? "Something went wrong");
    }
  };

  const currentClass = selectedClass ?? initialClass;
  const originalDescription = getClassDescription(currentClass);
  const description =
    lockedCircle !== null
      ? CLASS_ABILITIES[currentClass]?.[lockedCircle] || "No description available"
      : originalDescription;

  const handleCircleClick = (number) => {
    setLockedCircle((prevLockedCircle) => (prevLockedCircle === number ? null : number));
  };

  return (
    <Modal isOpen={isOpen}>
      <Modal.Header onRequestClose={onRequestClose}>Switch class</Modal.Header>
      <Modal.Body>
        <st.Container>
          <st.ClassSelector>
            {Object.values(SURVIVOR_CLASSES)
              .filter((classId) => classId !== SURVIVOR_CLASSES.GLADIATOR)
              .map((classId) => (
                <st.ClassOption
                  key={classId}
                  $isActive={classId === currentClass}
                  onClick={() => setSelectedClass(classId)}
                >
                  <ClassEmblem classId={classId} size={80} />
                  <div className="flex items-baseline">
                    <st.ClassName color={getClassGradient(classId).start}>
                      {getClassString(classId)}
                    </st.ClassName>
                    {classInfoHash && classInfoHash[classId] && (
                      <st.ClassLevel>{classInfoHash[classId].class_level}</st.ClassLevel>
                    )}
                    {!(classInfoHash && classInfoHash[classId]) && (
                      <st.ClassLevel>🔒</st.ClassLevel>
                    )}
                  </div>
                </st.ClassOption>
              ))}
          </st.ClassSelector>

          <st.ContentContainer>
            <st.CircleContainer>
              {[0, 1, 2, 3, 4, 5].map((number) => (
                <st.Circle
                  key={number}
                  $isActive={lockedCircle === number}
                  onMouseEnter={() => {
                    if (lockedCircle === null) {
                      setHoveredCircle(number);
                    }
                  }}
                  onMouseLeave={() => {
                    if (lockedCircle === null) {
                      setHoveredCircle(null);
                    }
                  }}
                  onClick={() => handleCircleClick(number)}
                >
                  {number}
                </st.Circle>
              ))}
            </st.CircleContainer>

            <st.ClassInfo>
              <p>{description}</p>
            </st.ClassInfo>
          </st.ContentContainer>

          {/* @ts-expect-error compatibility */}
          {error && <st.Error>{error?.response?.data?.error ?? error.toString()}</st.Error>}
          <st.SwitchClassButton onClick={() => onChangeClass(currentClass)}>
            {isSwitching ? "Switching..." : "Switch class"}
          </st.SwitchClassButton>
        </st.Container>
      </Modal.Body>
    </Modal>
  );
};

ClassSwitchModal.defaultProps = {
  initialClass: null,
};

export default ClassSwitchModal;
