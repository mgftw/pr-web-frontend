import styled from "styled-components";
import tw from "twin.macro";
import colors from "$constants/colors";

export const Container = styled.div`
  width: 70vw;
  align-items: center;
  justify-content: center;
  display: flex;
  flex-direction: column;
`;

export const ClassSelector = styled.ul`
  display: flex;
  align-items: center;
  justify-content: center;
  list-style: none;
  padding: 0;
  margin: 0;
  ${tw`mt-4 mb-4`}
`;

export const ClassOption = styled.li<{ $isActive: boolean }>`
  ${tw`mx-8`}
  display: flex;
  flex-direction: column;
  align-items: center;
  list-style: none;
  cursor: pointer;
  transition: transform 150ms;

  ${({ $isActive }) => ($isActive ? "transform: scale(1.1);" : `filter: grayscale(1);`)}

  &:hover {
    transform: scale(1.1);
  }
  p {
    padding: 0;
    margin: 0;
    ${tw`pt-2 font-oswald uppercase `}
  }
`;

export const ClassName = styled.body`
  ${tw`mt-1 font-bold`}
  color: ${colors.first};
  padding: 0 0.25rem;
  border-radius: 5rem 0 0 5rem;
  ${({ color }) => `
    background-color: ${color};
  `}
`;

export const ClassLevel = styled.body`
  ${tw`mt-1 font-bold`}
  color: ${colors.first};
  padding: 0 0.25rem 0 0.15rem;
  border-radius: 0 5rem 5rem 0;
  background-color: #eee;
  border-left: 1px solid #111;
`;

export const ClassInfo = styled.body`
  ${tw`font-karla text-primary p-4 mt-12 bg-second rounded-md`}
  width: 60%;
  white-space: pre-line;

  > p {
    ${tw`p-0`}
  }
`;

export const SwitchClassButton = styled.button`
  appearance: none;
  border: none;
  outline: none;
  background: none;
  align-self: center;

  transition: transform 150ms;
  cursor: pointer;

  &:hover {
    transform: translateY(-4px);
  }

  ${tw`rounded-md bg-zero text-primary font-oswald py-3 px-6 uppercase text-3xl mt-8 mb-8`}
`;

export const ContentContainer = styled.div`
  display: flex;
  justify-content: center;
  position: relative;
`;

export const CircleContainer = styled.div`
  display: flex;
  flex-direction: row;
  position: absolute;
  top: 0.6vw;
  left: 50%;
  transform: translateX(-50%);
`;

interface CircleProps {
  $isActive: boolean;
}

export const Circle = styled.div<CircleProps>`
  width: 25px;
  height: 25px;
  border-radius: 50%;
  background-color: gray;
  color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 5px;
  cursor: pointer;
  transition: transform 150ms;

  ${({ $isActive }) => ($isActive ? "transform: scale(1.2);" : `filter: grayscale(1);`)}

  &:hover {
    transform: scale(1.4);
  }
`;

export const Error = styled.p`
  ${tw`font-karla text-red`}
`;
