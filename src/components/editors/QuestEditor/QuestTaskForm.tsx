import { Input, Select, SelectItem } from "@nextui-org/react";
import { IQuestTask, QuestTaskType } from "types/entities";

const TaskForm = ({
  task,
  onChangeTask,
}: {
  task: Partial<IQuestTask>;
  onChangeTask(task: Partial<IQuestTask>): void;
}) => {
  const fields = {
    [QuestTaskType.TaskType_Kill]: ["game_class", "amount", "map", "coordinates", "trigger_range"],
    [QuestTaskType.TaskType_Collect]: ["foreign_id", "amount"],
    [QuestTaskType.TaskType_UseNPC]: ["foreign_id"],
    [QuestTaskType.TaskType_Witness]: ["game_class", "duration"],
    [QuestTaskType.TaskType_Custom]: ["game_class", "amount"],
  }[task.task_type];

  const isFieldRequired = (field) => {
    if (["amount", "foreign_id", "game_class"].includes(field)) return true;

    return false;
  };

  const getFieldLabel = (field) => {
    switch (field) {
      case "amount":
        return "Amount";
      case "foreign_id": {
        if (task.task_type === QuestTaskType.TaskType_Collect) {
          return "Quest Item ID";
        } else if (task.task_type === QuestTaskType.TaskType_UseNPC) {
          return "NPC ID";
        }
        return "External ID";
      }
      case "game_class": {
        if (task.task_type === QuestTaskType.TaskType_Witness) {
          return "Witness Class String";
        } else if (task.task_type === QuestTaskType.TaskType_Custom) {
          return "Custom string";
        }
        return "Game Class";
      }
    }

    return field;
  };

  const options = [
    {
      id: QuestTaskType.TaskType_Collect,
      name: "Collect item",
    },
    {
      id: QuestTaskType.TaskType_Kill,
      name: "Kill something",
    },
    {
      id: QuestTaskType.TaskType_UseNPC,
      name: "Use NPC",
    },
    {
      id: QuestTaskType.TaskType_Witness,
      name: "Witness something",
    },
    {
      id: QuestTaskType.TaskType_Custom,
      name: "Custom",
    },
  ];

  return (
    <div>
      <div className="flex gap-2 mb-2">
        <Select
          label="Task Type"
          isRequired
          items={options}
          selectedKeys={task.task_type ? [`${task.task_type}`] : []}
          onSelectionChange={(keys) => {
            const selectedKey = Array.from(keys)[0]; // Convert the Set to an array
            onChangeTask({ ...task, task_type: selectedKey as number });
          }}
        >
          {options.map((opt) => (
            <SelectItem key={opt.id}>{opt.name}</SelectItem>
          ))}
        </Select>
        <Input
          value={task.description}
          label="Task description"
          isRequired
          onChange={(event) => onChangeTask({ ...task, description: event.target.value })}
        />
      </div>
      <div className="grid grid-cols-4 gap-1">
        {(fields ?? []).map((field) => (
          <div key={field}>
            <Input
              value={task[field] ?? ""}
              label={getFieldLabel(field)}
              isRequired={isFieldRequired(field)}
              onChange={(event) => onChangeTask({ ...task, [field]: event.target.value })}
            />
          </div>
        ))}
      </div>
    </div>
  );
};
export default TaskForm;
