import Loader from "$components/Loader";
import NPCIcon from "$components/NPCIcon/NPCIcon";
import questNPCS from "$constants/questNpcs";
import { useQuestData } from "$hooks/useQuestData";
import { Button, Input } from "@nextui-org/react";
import { memo, useCallback, useMemo, useState } from "react";
import { Tooltip } from "react-tippy";
import QuestForm from "./QuestForm";
import { toast } from "react-toastify";
import { QuestDataErrors, QuestDataToSend } from "./types";
import { validateQuestData } from "./utils";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { GLOBAL_QUEST_INFO as QUERY_GLOBAL_QUEST_INFO } from "$constants/queries";

import { createQuest, updateQuest as sendQuestUpdate } from "$api/adminApi";

interface FilterState {
  npcId: number | null;
  query: string;
}

interface QuestFormState {
  questId: number | null;
  isNew: boolean;
}

const QuestEditor = () => {
  const queryClient = useQueryClient();
  const [errors, setErrors] = useState<QuestDataErrors>();
  const [questForm, setQuestForm] = useState<QuestFormState>({
    questId: null,
    isNew: false,
  });

  const { mutateAsync: addQuest, isLoading: isCreatingQuest } = useMutation(
    (data: QuestDataToSend) => createQuest(data),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(QUERY_GLOBAL_QUEST_INFO);
      },
    },
  );

  const { mutateAsync: updateQuest, isLoading: isUpdatingQuest } = useMutation(
    ({ data, questId }: { data: QuestDataToSend; questId: number }) =>
      sendQuestUpdate(questId, data),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(QUERY_GLOBAL_QUEST_INFO);
      },
    },
  );

  const [filters, setFilters] = useState<FilterState>({
    npcId: null,
    query: "",
  });
  const { isLoading, quests } = useQuestData();

  const filteredQuests = useMemo(
    () =>
      quests.filter((quest) => {
        if (!!filters.npcId && filters.npcId !== quest.npc_id) return false;

        if (
          filters.query.length > 0 &&
          !quest.title.toLocaleLowerCase().includes(filters.query.toLocaleLowerCase())
        ) {
          return false;
        }
        return true;
      }),
    [quests, filters],
  );

  const onQuestSelected = useCallback(
    (questId) => {
      if (questForm.isNew) {
        const response = window.confirm(
          "You are creating a new quest and will lose your changes. Are you sure?",
        );

        if (!response) return;
      }
      setQuestForm((form) => ({ ...form, isNew: false, questId }));
    },
    [questForm],
  );

  const onSaveQuest = async (questData: QuestDataToSend) => {
    if (isCreatingQuest || isUpdatingQuest) return;

    const dataErrors = validateQuestData(questData);

    if (Object.keys(dataErrors).length > 0) {
      setErrors(dataErrors);
      toast.error("There are issues with the quest");
      return;
    }

    try {
      await (questForm.isNew
        ? addQuest(questData)
        : updateQuest({ questId: questForm.questId, data: questData }));

      toast.success("Saved.");
    } catch (e) {
      toast.error("Unable to save quest.");
    }
  };

  if (isLoading) {
    return <Loader />;
  }

  return (
    <div className="flex gap-4">
      <div className="w-96">
        <div className="flex gap-1 align-items h-14 items-center w-full overflow-scroll">
          {questNPCS.map((npc) => {
            const isSelected = filters.npcId === npc.id;
            return (
              <div key={npc.id}>
                <Tooltip html={<div>{npc.name}</div>}>
                  <div
                    onClick={() =>
                      setFilters((filters) => ({
                        ...filters,
                        npcId: filters.npcId === npc.id ? null : npc.id,
                      }))
                    }
                    className={`h-10 w-10 cursor-pointer hover:h-14 hover:w-14 transition-all ease-in duration-200 ${isSelected ? "h-14 w-14" : ""}`}
                  >
                    <NPCIcon
                      npcId={npc.id}
                      size="small"
                      borderWidth={2}
                      borderColor={isSelected ? "red" : "quaternary"}
                    />
                  </div>
                </Tooltip>
              </div>
            );
          })}
        </div>
        <div className="flex gap-1">
          <Input
            value={filters.query}
            placeholder="Search for quest"
            onChange={(event) =>
              setFilters((filters) => ({ ...filters, query: event.target.value }))
            }
          />
          <Button
            onClick={() =>
              setQuestForm({
                questId: null,
                isNew: true,
              })
            }
          >
            + Add quest{" "}
          </Button>
        </div>

        <div
          className="gap-1 flex flex-col mt-2 overflow-y-scroll"
          style={{
            maxHeight: "80vh",
          }}
        >
          {filteredQuests.map((quest) => (
            <div key={quest.quest_id} onClick={() => onQuestSelected(quest.quest_id)}>
              <div
                className={`flex grow gap-1 items-baseline bg-first p-1 px-2 rounded cursor-pointer border-2 ${questForm.questId === quest.quest_id ? "border-red" : "border-transparent"}`}
              >
                <h2>{quest.is_daily ? "[Daily] " + quest.title : quest.title}</h2>
                <p className="text-quaternary text-sm">Lv. {quest.required_level}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className="flex-1">
        <QuestForm {...questForm} onSave={onSaveQuest} errors={errors} />
      </div>
    </div>
  );
};

export default memo(QuestEditor);
