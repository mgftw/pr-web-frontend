import NPCIcon from "$components/NPCIcon/NPCIcon";
import questNPCS from "$constants/questNpcs";
import { EditorView } from "@codemirror/view";
import { useQuestData } from "$hooks/useQuestData";
import QuestCard from "$pages/QuestsPage/ProgressTab/QuestCard";
import { Button, Checkbox, Input, Select, SelectItem, Skeleton } from "@nextui-org/react";
import { FormEvent, useEffect, useState } from "react";
import { IQuestTask } from "types/entities";
import _omit from "lodash/omit";
import { IoMdTrash } from "react-icons/io";
import MarkdownEditor from "@uiw/react-markdown-editor";
import Tabs from "$components/Tabs";
import { InventoryItemSelect } from "$components/Select";
import useInventoryItemCache from "$hooks/useInventoryItemCache";
import InventoryItem from "$components/InventoryItem";
import { MdSave } from "react-icons/md";
import TaskForm from "./QuestTaskForm";
import { QuestDataErrors, QuestDataToSend } from "./types";

interface QuestFormProps {
  questId: number | null;
  errors?: QuestDataErrors;
  isNew: boolean;
  onSave(quest: QuestDataToSend): void;
}

const defaultValues: QuestDataToSend = {
  required_level: 5000,
  exp_reward: 0,
  cash_reward: 0,
  pill_reward: 0,
};

const defaultTaskValues: Partial<IQuestTask> = {};

type TabOption = "general" | "tasks" | "story" | "rewards" | "preview";

const QuestForm = ({ questId, isNew, onSave, errors }: QuestFormProps) => {
  const { quests, questRegistry } = useQuestData();
  const remoteQuestData = questRegistry[questId];
  const [currentTab, setCurrentTab] = useState<TabOption>("general");

  const [questData, setQuestData] = useState<QuestDataToSend>(defaultValues);

  const itemCache = useInventoryItemCache();

  useEffect(() => {
    if (questId && !isNew) {
      setQuestData(remoteQuestData);
    }
  }, [questId, remoteQuestData, isNew]);

  useEffect(() => {
    if (isNew) {
      setQuestData(defaultValues);
    }
  }, [isNew]);

  if (!questId && !isNew) {
    return <div>Choose a quest or click on the add quest button</div>;
  }

  const onSendQuestUpdate = (event: FormEvent) => {
    event.preventDefault();

    onSave(questData);
  };

  const tabs: { id: TabOption; name: string }[] = [
    {
      id: "general",
      name: "General",
    },
    {
      id: "tasks",
      name: "Tasks",
    },
    {
      id: "story",
      name: "Story",
    },
    {
      id: "rewards",
      name: "Rewards",
    },
    {
      id: "preview",
      name: "Preview",
    },
  ];

  return (
    <form className="flex flex-col bg-second p-4 gap-2 rounded-lg" onSubmit={onSendQuestUpdate}>
      <h1 className="text-red font-oswald uppercase">
        {isNew ? "New quest" : `Editing quest: ${remoteQuestData.title}`}
      </h1>
      <div className="mb-4 flex justify-between">
        <Tabs>
          {tabs.map(({ id, name }) => (
            <Tabs.Tab key={id} onClick={() => setCurrentTab(id)} isActive={currentTab === id}>
              {name}
            </Tabs.Tab>
          ))}
        </Tabs>
        <Button type="submit" startContent={<MdSave />} color="danger">
          Save
        </Button>
      </div>
      <div className={currentTab === "general" ? "flex flex-col gap-2" : "hidden"}>
        {!isNew && <Input value={`${questData.quest_id ?? -1}`} label="Quest ID" isDisabled />}
        <Input
          label="Title"
          isInvalid={!!errors?.title}
          errorMessage={errors?.title}
          isRequired
          value={questData.title}
          onChange={(event) => setQuestData((data) => ({ ...data, title: event.target.value }))}
        />
        <div className="flex gap-2">
          <Select
            items={questNPCS}
            label="NPC"
            isInvalid={!!errors?.npc_id}
            errorMessage={errors?.npc_id}
            isRequired
            placeholder="Select an NPC "
            multiple={false}
            selectedKeys={questData.npc_id ? [`${questData.npc_id}`] : []}
            classNames={{
              base: ["w-9/12"],
            }}
            renderValue={(items) => {
              return items.map((item) => (
                <div key={item.data.id} className="flex gap-2 items-center h-14">
                  <div className="h-6 w-6">
                    <NPCIcon npcId={item.data.id} />
                  </div>
                  <p>{item.data.name}</p>
                </div>
              ));
            }}
            onSelectionChange={(keys) => {
              const selectedKey = Array.from(keys)[0]; // Convert the Set to an array
              setQuestData((data) => ({ ...data, npc_id: selectedKey as number }));
            }}
          >
            {(npc) => (
              <SelectItem key={npc.id} textValue={`${npc.id}`}>
                <div className="flex gap-2 items-center h-10">
                  <div className="h-8 w-8">
                    <NPCIcon npcId={npc.id} />
                  </div>
                  <p>{npc.name}</p>
                </div>
              </SelectItem>
            )}
          </Select>
          <Input
            type="number"
            label="Required Level"
            isRequired
            isInvalid={!!errors?.required_level}
            errorMessage={errors?.required_level}
            value={`${questData.required_level}`}
            classNames={{
              base: ["w-9/12"],
            }}
            onChange={(event) =>
              setQuestData((data) => ({
                ...data,
                required_level: parseInt(event.target.value, 10),
              }))
            }
          />

          <div className="flex gap-2 items-center w-full">
            <Select
              items={quests}
              label="Required Quest"
              placeholder="Choose a quest or leave empty"
              selectedKeys={questData.required_quest ? [`${questData.required_quest}`] : []}
              onSelectionChange={(keys) => {
                const selectedKey = Array.from(keys)[0]; // Convert the Set to an array
                setQuestData((data) => ({ ...data, required_quest: selectedKey as number }));
              }}
            >
              {(quest) => (
                <SelectItem
                  key={quest.quest_id}
                  textValue={`[ID: ${quest.quest_id}] - ${quest.title}`}
                >
                  <div>{`[ID: ${quest.quest_id}] - ${quest.title}`}</div>
                </SelectItem>
              )}
            </Select>
            <Button onClick={() => setQuestData((data) => ({ ..._omit(data, "required_quest") }))}>
              Clear
            </Button>
          </div>
        </div>
        <div>
          <p className="mb-2 mt-2">Quest flags</p>
          <div className="flex gap-8">
            <Checkbox
              isSelected={questData.is_disabled}
              onValueChange={(newValue) =>
                setQuestData((data) => ({ ...data, is_disabled: newValue }))
              }
            >
              Is disabled?
            </Checkbox>
            <Checkbox
              isSelected={questData.is_daily}
              onValueChange={(newValue) =>
                setQuestData((data) => ({
                  ...data,
                  is_daily: newValue,
                  ...(data.is_weekly && newValue ? { is_weekly: false } : {}),
                }))
              }
            >
              Is daily?
            </Checkbox>
            <Checkbox
              isSelected={questData.is_weekly}
              onValueChange={(newValue) =>
                setQuestData((data) => ({
                  ...data,
                  is_weekly: newValue,
                  ...(data.is_daily && newValue ? { is_daily: false } : {}),
                }))
              }
            >
              Is weekly?
            </Checkbox>
          </div>
        </div>
      </div>
      <div className={currentTab === "tasks" ? "flex flex-col gap-2" : "hidden"}>
        <Button
          onClick={() => {
            setQuestData((data) => ({
              ...data,
              tasks: [...(data.tasks ?? []), { ...defaultTaskValues }],
            }));
          }}
        >
          +Add task
        </Button>
        <div className="pl-0 flex flex-col gap-2">
          {(questData.tasks ?? []).map((task, idx) => (
            <div key={`${questId}${isNew}-${idx}`} className="bg-zero p-4 rounded">
              <div className="flex mb-4">
                <div className="flex-1">
                  <h6>Task {idx + 1}</h6>
                </div>
                <Button
                  isIconOnly
                  size="sm"
                  color="danger"
                  onClick={() => {
                    setQuestData((data) => ({
                      ...data,
                      tasks: data.tasks.filter((_, idx2) => idx !== idx2),
                    }));
                  }}
                >
                  <IoMdTrash />
                </Button>
              </div>
              <TaskForm
                task={task}
                onChangeTask={(newTask) => {
                  const newTasks = questData.tasks ?? [];
                  newTasks[idx] = newTask;
                  setQuestData((data) => ({
                    ...data,
                    tasks: newTasks,
                  }));
                }}
              />
            </div>
          ))}
        </div>
      </div>

      <div className={currentTab === "story" ? "flex flex-col gap-2" : "hidden"}>
        <p>Story</p>
        <MarkdownEditor
          extensions={[EditorView.lineWrapping]}
          visible
          maxWidth="100%"
          previewWidth="50%"
          value={questData.web_story ?? ""}
          height="400px"
          onChange={(value) => setQuestData((data) => ({ ...data, web_story: value }))}
        />

        <p>End dialog</p>
        <MarkdownEditor
          extensions={[EditorView.lineWrapping]}
          visible
          maxWidth="100%"
          previewWidth="50%"
          value={questData.web_end_story ?? ""}
          height="400px"
          onChange={(value) => setQuestData((data) => ({ ...data, web_end_story: value }))}
        />
      </div>

      <div className={currentTab === "rewards" ? "flex flex-col gap-2" : "hidden"}>
        <div className="flex gap-2">
          <Input
            type="number"
            label="Exp reward"
            value={`${questData.exp_reward}`}
            onChange={(event) =>
              setQuestData((data) => ({ ...data, exp_reward: parseInt(event.target.value, 10) }))
            }
          />
          <Input
            type="number"
            label="Cash Reward"
            value={`${questData.cash_reward}`}
            onChange={(event) =>
              setQuestData((data) => ({ ...data, cash_reward: parseInt(event.target.value, 10) }))
            }
          />
          <Input
            type="number"
            label="Pills Reward"
            value={`${questData.pill_reward}`}
            onChange={(event) =>
              setQuestData((data) => ({ ...data, pill_reward: parseInt(event.target.value, 10) }))
            }
          />
        </div>
        <Button
          onClick={() => {
            setQuestData((data) => ({
              ...data,
              item_rewards: [
                ...(data.item_rewards ?? []),
                {
                  item_id: null,
                  quantity: null,
                },
              ],
            }));
          }}
        >
          +Add item reward
        </Button>
        <div className="pl-0 flex flex-col gap-2">
          {(questData.item_rewards ?? []).map((itemReward, idx) => (
            <div key={`${questId}_${isNew}-${idx}`} className="bg-zero p-4 rounded">
              <div className="flex mb-4 gap-2">
                <div className="h-14 w-14 shrink-0">
                  {itemCache[itemReward.item_id] ? (
                    <InventoryItem
                      showQuantity={false}
                      item={itemCache[itemReward.item_id]}
                      size="medium"
                    />
                  ) : (
                    <Skeleton className="h-14 w-14 rounded-xl" />
                  )}
                </div>
                <InventoryItemSelect
                  selectedItemId={itemReward.item_id}
                  onItemChanged={(itemId) => {
                    setQuestData((data) => ({
                      ...data,
                      item_rewards: questData.item_rewards.map((reward, idx2) =>
                        idx2 === idx
                          ? {
                              ...reward,
                              item_id: itemId,
                            }
                          : reward,
                      ),
                    }));
                  }}
                />
                <Input
                  type="number"
                  label="Quantity"
                  isRequired
                  min="1"
                  value={`${itemReward.quantity}`}
                  onChange={(event) => {
                    setQuestData((data) => ({
                      ...data,
                      item_rewards: questData.item_rewards.map((reward, idx2) =>
                        idx2 === idx
                          ? {
                              ...reward,
                              quantity: parseInt(event.target.value, 10),
                            }
                          : reward,
                      ),
                    }));
                  }}
                />

                <Button
                  isIconOnly
                  size="sm"
                  color="danger"
                  onClick={() => {
                    setQuestData((data) => ({
                      ...data,
                      tasks: data.tasks.filter((_, idx2) => idx !== idx2),
                    }));
                  }}
                >
                  <IoMdTrash />
                </Button>
              </div>
            </div>
          ))}
        </div>
      </div>

      <div className={currentTab === "preview" ? "flex flex-col gap-2" : "hidden"}>
        {/* @ts-expect-error this is a temporary way to preview */}
        <QuestCard quest={questData} />
      </div>
    </form>
  );
};

export default QuestForm;
