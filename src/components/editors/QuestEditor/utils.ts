import { QuestDataErrors, QuestDataField, QuestDataToSend } from "./types";

type ValidatorFunc = (value: any) => string | null;

export const validateQuestData = (questData: QuestDataToSend) => {
  // TODO: Add more validations
  const fieldValidators: Partial<Record<QuestDataField, ValidatorFunc>> = {
    title: (value: string) => (value ? null : "Title must be present"),
  };

  // Go over validaotrs
  return Object.entries(fieldValidators).reduce<QuestDataErrors>(
    (errors, [field, validatorFunc]) => {
      const validationResult = validatorFunc(questData[field]);
      if (validationResult === null) return errors;
      return {
        ...errors,
        [field]: validationResult,
      };
    },
    {} as QuestDataErrors,
  );
};
