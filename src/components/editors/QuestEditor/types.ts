import { IQuest, IQuestTask } from "types/entities";

export type QuestDataToSend = Omit<Partial<IQuest>, "tasks"> & {
  tasks?: Partial<IQuestTask>[];
};

export type QuestDataField = keyof QuestDataToSend;
export type QuestDataErrors = Record<QuestDataField, string>;
