import Badge from "$atoms/Badge";
import { formatCurrency } from "$helpers/currencyHelper";

const CurrencyIndicator = ({
  icon,
  amount = null,
  backgroundColor = "second",
  borderRadius = "9999px",
  compress = true,
  ...rest
}: {
  amount: number;
  icon: string;
  backgroundColor?: string;
  borderRadius?: string;
  compress?: boolean;
}) => {
  return (
    <Badge icon={icon} backgroundColor={backgroundColor} borderRadius={borderRadius} {...rest}>
      {compress ? formatCurrency(amount) : amount === null ? "-" : amount.toLocaleString()}
    </Badge>
  );
};

export default CurrencyIndicator;
