import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`flex p-2 bg-second font-bold tracking-widest items-center justify-between text-sm w-full rounded-lg`}
`;

export const IconContainer = styled.div`
  ${tw`pr-1`}
`;

export const CurrencyValue = styled.div`
  ${tw``}
`;
