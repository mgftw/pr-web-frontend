import styled from "styled-components";
import tw from "twin.macro";
import { Link } from "react-router-dom";

export const Container = styled.div`
  ${tw`flex flex-row`}
  height: fit-content;
`;

export const Tab = styled(Link)<{ $isActive: boolean }>`
  ${tw`bg-first text-primary px-5 py-2 text-base rounded ml-3 mr-3 first:ml-0 font-oswald uppercase`}

  ${({ $isActive }) => $isActive && tw`bg-primary text-zero`}

  text-decoration: none;
  height: fit-content;
`;
