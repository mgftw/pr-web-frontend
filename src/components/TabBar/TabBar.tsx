import PropTypes from "prop-types";
import { useLocation } from "react-router-dom";

import * as st from "./styles";

const TabBar = ({
  options,
}: {
  options: {
    label: string;
    regex: string | RegExp;
    url: string;
  }[];
}) => {
  const location = useLocation();
  return (
    <st.Container>
      {options.map((opt) => (
        <st.Tab
          key={opt.label}
          to={opt.url}
          // @ts-expect-error compatibility
          $isActive={opt.regex.test(location.pathname)}
        >
          {opt.label}
        </st.Tab>
      ))}
    </st.Container>
  );
};

TabBar.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      url: PropTypes.string,
      regex: PropTypes.string,
    }),
  ).isRequired,
};

export default TabBar;
