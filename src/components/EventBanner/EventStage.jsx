import React, { forwardRef } from "react";
import tw from "twin.macro";
import colors from "$constants/colors";

import styled from "styled-components";
import useAllItems from "$hooks/useAllItems";
import InventoryItem from "$components/InventoryItem";

const css = {
  Container: styled.div`
    display: flex;
    min-width: 12rem;
    flex-direction: column;
    ${tw`w-96`}
    ${tw`p-4 border-solid border-zero rounded-md bg-zero`}

    ${({ isCurrent }) =>
      isCurrent ? `border: 4px solid ${colors.beige}; background-color ${colors.second};` : ""}
    

    h1 {
      ${tw`text-sm`}
    }
  `,
  ObjectiveList: styled.div``,
  Rewards: styled.div``,
  StageDetails: styled.div`
    ${tw`flex flex-col`}
    ${tw`font-karla font-bold text-primary`}
    text-align: left;
    row-gap: 0.75rem;
  `,
  Objective: styled.li`
    ${tw`font-karla font-normal text-primary p-0 m-0 px-2 py-1 text-xs flex items-center mb-1 w-full rounded-full bg-second bg-quaternary`}
    width: inherit;

    ${({ isCompleted }) => (isCompleted ? `background-color: ${colors.green};` : "")}

    h6 {
      ${tw`text-xs p-0 m-0 font-normal flex-grow`}
      ${({ isCompleted }) => (isCompleted ? `text-decoration: line-through; opacity 0.8;` : "")}
    }
    p {
      ${tw`text-xs p-0 m-0 ml-1 text-xs bg-quaternary rounded-full font-oswald`}

      ${({ isCompleted }) =>
        isCompleted ? `background-color: ${colors.green}; padding-top: 0; padding-bottom: 0` : ""}
    }
  `,
  Objectives: styled.div`
    ${tw`flex flex-col p-0 m-0`}
    list-style: none;
    display: flex;
    height: 100%;
  `,

  Minimum: styled.div`
    ${tw`text-red text-xs`}
  `,
  RewardsContainer: styled.div`
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    row-gap: 0.5rem;
    column-gap: 0.5rem;
  `,
};

export default forwardRef(({ stage, isCurrent, isCompleted }, ref) => {
  const items = useAllItems();
  return (
    <css.Container isCurrent={isCurrent} ref={ref}>
      <h1>Stage {stage?.stage_number ?? "?"}</h1>
      <css.StageDetails>
        <css.Objectives>
          {(stage?.objectives ?? []).map((o) => {
            return (
              <css.Objective key={o.objective_number} isCompleted={isCompleted}>
                <h6>{o.title}</h6>
              </css.Objective>
            );
          })}
        </css.Objectives>

        <css.Minimum>
          {`${stage?.minimum_objectives_to_unlock ?? "?"} ${
            stage?.minimum_objectives_to_unlock === 1 ? "objective" : "objectives"
          } required`}
        </css.Minimum>

        <css.RewardsContainer>
          {(stage?.rewards ?? []).map((reward) => {
            const item = items.find((itm) => itm.id === reward.item_id);
            if (!item) {
              return null;
            }
            return (
              <div key={item.id}>
                <InventoryItem item={item} quantity={reward.amount} size="medium" />
              </div>
            );
          })}
        </css.RewardsContainer>
      </css.StageDetails>
    </css.Container>
  );
});
