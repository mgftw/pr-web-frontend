import React, { useRef, useState, useLayoutEffect } from "react";

import { useQuery } from "@tanstack/react-query";
import { BiCheck } from "react-icons/bi";
import { BUCKET_URL } from "$constants/url";

import EventDescription from "$components/EventDescription";
import { getEvent } from "$api/serverApi";
import { getMyEventProgress } from "$api/playerApi";

import { GLOBAL_EVENT, MY_EVENT_PROGRESS } from "$constants/queries";
import Modal from "$components/Modal";
import * as st from "./EventBanner.styles";
import EventStage from "./EventStage";

const EventDetails = () => {
  const ref = useRef();
  const [isEventInfoOpen, setIsEventInfoOpen] = useState(false);
  const { data: event = {}, isLoading: isLoadingEvent } = useQuery({
    queryKey: GLOBAL_EVENT,
    queryFn: () => getEvent(),
  });

  const { data: progress = {}, isLoading: isLoadingEventProgress } = useQuery({
    queryKey: MY_EVENT_PROGRESS,
    queryfn: () => getMyEventProgress(),
    enabled: Boolean(event.name),
  });

  const currentStage = (event?.stages ?? []).find(
    (stage) => stage?.stage_number === progress?.stage,
  );

  useLayoutEffect(() => {
    if (ref && ref.current) {
      ref.current.scrollIntoView({ behavior: "smooth" });
    }
  }, [currentStage, isEventInfoOpen]);

  if (!event || !event.event_id) {
    return null;
  }

  return (
    <st.Container>
      <st.Content>
        <st.Left>
          <div onClick={() => setIsEventInfoOpen(true)}>
            <st.Banner src={`${BUCKET_URL}/event${event.event_id}.png`} />
          </div>
          {!isLoadingEvent && !isLoadingEventProgress && (
            <st.CurrentStage>
              {progress?.stage <= event.stages.length
                ? `Stage ${progress?.stage ?? "??"} / ${event.stages.length}`
                : `Event Pass Completed!`}
            </st.CurrentStage>
          )}
        </st.Left>
        <st.Right>
          {!isLoadingEvent && !isLoadingEventProgress && (
            <st.EventProgress>
              <st.StageDetails>
                <st.Objectives>
                  {(currentStage?.objectives ?? []).map((o) => {
                    const current =
                      ((progress?.registry ?? []).find((r) => r.event_name === o.event_name) ?? {})
                        .amount ?? 0;
                    return (
                      <st.Objective key={o.objective_number} isCompleted={current > o.amount}>
                        <h6>{o.title}</h6>
                        <p>
                          {current > o.amount ? <BiCheck size={16} /> : `${current} / ${o.amount}`}
                        </p>
                      </st.Objective>
                    );
                  })}
                </st.Objectives>
                <st.Minimum>
                  {progress?.stage <= event.stages.length
                    ? `Complete ${
                        currentStage?.minimum_objectives_to_unlock ?? "?"
                      } objectives to complete the stage`
                    : `All stages complete!`}
                </st.Minimum>
              </st.StageDetails>
            </st.EventProgress>
          )}
        </st.Right>
        <Modal isOpen={isEventInfoOpen}>
          <Modal.Header
            onRequestClose={() => {
              setIsEventInfoOpen(false);
            }}
          >
            Event Pass
          </Modal.Header>
          <Modal.Body>
            <st.EventPassModalContainer>
              <div>
                <EventDescription
                  description={event.description || ""}
                  name={event.name}
                  banner={`${BUCKET_URL}/event${event.event_id}.png`}
                />
              </div>
              <st.EventStagesWrapper>
                <ul>
                  {(event?.stages ?? []).map((stage) => {
                    const isCurrent = (progress?.stage ?? 0) === stage.stage_number;
                    return (
                      <EventStage
                        stage={stage}
                        ref={isCurrent ? ref : undefined}
                        key={stage.stage_number}
                        isCurrent={isCurrent}
                        progress={progress}
                        isCompleted={(progress?.stage ?? 0) > stage.stage_number}
                      />
                    );
                  })}
                </ul>
              </st.EventStagesWrapper>
            </st.EventPassModalContainer>
          </Modal.Body>
        </Modal>
      </st.Content>
    </st.Container>
  );
};

export default EventDetails;
