import styled from "styled-components";
import tw from "twin.macro";
import colors from "$constants/colors";

export const Container = styled.div`
  ${tw`p-4 rounded bg-second`}
`;

export const Title = styled.div`
  ${tw`text-lg text-zero font-bold`}
`;

export const Content = styled.div`
  ${tw`flex`}

  display: grid;
  grid-template-columns: min-content 1fr;
  grid-gap: 1rem;
`;

export const Left = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Right = styled.div`
  ${tw`w-full`}
`;

export const Banner = styled.img`
  ${tw`bg-secondary rounded object-cover w-32 h-16 mb-2`}
  cursor: pointer;
`;

export const Stages = styled.div`
  ${tw`flex w-full`}
`;

export const StageContainer = styled.div`
  ${tw`flex justify-center items-center w-full`}
`;

export const Stage = styled.div`
  ${tw`flex h-8 w-8 rounded-full bg-zero justify-center items-center text-lg`}

  ${({ isDone }) => (isDone ? tw`bg-primary text-zero` : "")}
  cursor: pointer;
`;

export const Line = styled.div`
  ${tw`w-6 bg-zero`}
  height: 3px;
`;

export const StageDetails = styled.div`
  ${tw`flex flex-col`}
  ${tw`font-karla font-bold text-primary`}
  text-align: left;
`;

export const CurrentStage = styled.h5`
  ${tw`p-0 m-0 bg-zero text-primary font-bold rounded-full p-2 text-sm flex-grow-0`}
  width: min-content;
  white-space: nowrap;
`;

export const Objectives = styled.ul`
  ${tw`flex flex-col p-0 m-0`}
  list-style: none;
  display: grid;
  grid-auto-rows: min-content;
`;

export const Objective = styled.li`
  ${tw`font-karla font-normal text-primary p-0 m-0 px-2 py-1 text-sm flex items-center mb-1 w-full rounded-full bg-second bg-quaternary`}
  width: inherit;

  ${({ isCompleted }) => (isCompleted ? `background-color: ${colors.green};` : "")}

  h6 {
    ${tw`text-sm p-0 m-0 font-normal flex-grow`}
    ${({ isCompleted }) => (isCompleted ? `text-decoration: line-through; opacity 0.8;` : "")}
  }
  p {
    ${tw`text-sm p-0 m-0 ml-1 text-xs bg-quaternary rounded-full font-oswald`}

    ${({ isCompleted }) =>
      isCompleted ? `background-color: ${colors.green}; padding-top: 0; padding-bottom: 0` : ""}
  }
`;

export const Rewards = styled.div`
  ${tw`flex flex-col`}
`;

export const Reward = styled.div``;

export const Minimum = styled.div`
  ${tw`text-red pt-2 text-xs`}
`;

export const EventProgress = styled.div``;
export const EventPassModalContainer = styled.div`
  max-width: 1000px;
  display: grid;
  grid-template-columns: 0.3fr 0.7fr;
  column-gap: 1rem;
`;

export const EventStagesWrapper = styled.div`
  width: 100%;
  overflow: scroll;

  > ul {
    display: flex;
    column-gap: 0.5rem;
    margin: 0;
    padding: 0;
    height: 100%;
  }
`;
