import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "../../lib/legacy";

import { validateOrRedirect } from "$helpers/sessionHelper";

class PrivateRoutes extends PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
    token: PropTypes.string,
    history: PropTypes.shape().isRequired,
  };

  static defaultProps = {
    token: null,
  };

  componentDidMount() {
    const { token, history } = this.props;
    validateOrRedirect(token, history);
  }

  render() {
    const { children } = this.props;
    return <>{children}</>;
  }
}

function mapStateToProps(stores) {
  return {
    token: stores.sessionState.token,
  };
}
const mapDispatchToProps = {};

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(PrivateRoutes);
