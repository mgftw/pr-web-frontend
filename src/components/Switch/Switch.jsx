import React from "react";
import PropTypes from "prop-types";

import * as st from "./styles";

const variants = {
  inactive: {
    x: 0,
  },
  active: {
    x: "100%",
  },
};

const Switch = ({ active, onClick }) => (
  <st.Container onClick={onClick} active={active}>
    <st.Ball active={active} variants={variants} animate={active ? "active" : "inactive"} />
  </st.Container>
);

Switch.propTypes = {
  active: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};

Switch.defaultProps = {};

export default Switch;
