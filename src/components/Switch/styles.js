import styled from "styled-components";
import tw from "twin.macro";
import { motion } from "framer-motion";

export const Container = styled.div`
  ${tw`h-4 w-8 rounded-full bg-zero items-center justify-center mr-1 my-1 mx-1`}
  opacity: ${({ active }) => (active ? 1 : 0.3)};
  cursor: pointer;
`;

export const Ball = styled(motion.div)`
  ${tw`h-4 w-4 rounded-full bg-primary`}
`;
