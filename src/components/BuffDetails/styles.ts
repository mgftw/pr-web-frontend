import styled from "styled-components";
import tw from "twin.macro";

import { buffCategoryColors } from "$constants/colors";

export const Container = styled.div`
  ${tw`bg-second rounded-lg p-4`}
  width: 500px;
  z-index: 300;
`;

export const BuffDescription = styled.div`
  ${tw`pr-2 pl-2 flex rounded-lg`}
`;

export const Placeholder = styled.div<{ $subCat: number }>(({ $subCat }) => [
  tw`h-20 w-20 font-oswald text-center rounded-lg float-left absolute text-3xl`,
  `background-color: ${buffCategoryColors[Object.keys(buffCategoryColors)[$subCat - 1]]}
  box-shadow: 0px 0px 1px 2px black inset;
  text-shadow:
    -1px -1px 0 #000,
    1px -1px 0 #000,
    -1px 1px 0 #000,
    1px 1px 0 #000;
  line-height: 4.75rem;
`,
]);

export const BuffIconContainer = styled.div`
  ${tw`h-24 w-24 mr-5`}
  flex-shrink: 0;
  flex-grow: 0;
`;

export const Info = styled.div`
  ${tw``}
`;

export const Name = styled.div`
  ${tw`font-oswald text-xl pb-1 flex items-center`}
`;

export const Category = styled.div`
  ${tw`bg-zero font-karla rounded-lg text-sm text-primary pr-1 pl-1 ml-2`}
  ${({ color }) => `background-color: ${color};`}
  width: fit-content;
  height: fit-content;
  padding-top: 2px;
  padding-bottom: 2px;
`;

export const Description = styled.div`
  ${tw`font-karla text-secondary text-left`}
`;

export const EnhInfo = styled.div`
  ${tw`bg-zero p-2 pt-1 rounded-lg mt-1`}
`;

export const EnhName = styled.div`
  ${tw`font-oswald text-left text-lg`}
`;

export const DescriptionTag = styled.div`
  ${tw`bg-first rounded-lg text-primary p-1 pr-2 pl-2 m-1 mr-0 ml-0 first:mt-2 flex-grow-0`}
  width: fit-content;
`;
