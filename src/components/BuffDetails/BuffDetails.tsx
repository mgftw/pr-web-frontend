import PropTypes from "prop-types";

import * as st from "./styles";

import BuffIcon from "$components/BuffIcon";

const regex = new RegExp("<.*>");

const types = {
  "|Q|": "Quest",
  "|E|": "Event",
  "|V|": "Competitive",
  "|C|": "Craft",
};

const colors = {
  Quest: "#6E837C",
  Event: "#4775BB",
  Competitive: "#B85B5B",
  Craft: "#B88D5B",
};

const BuffDetails = ({ buffInfo }) => {
  if (buffInfo === undefined || buffInfo === null || Object.keys(buffInfo).length === 0)
    return <></>;
  const buff = buffInfo.buff;
  const lines = buff.description ? buff.description.split("\n").filter((s) => s !== "") : [];

  const descLines = lines.filter((line) => !regex.test(line)).join(" ");
  const tags = lines.filter((line) => regex.test(line)).map((tag) => tag.replace(/<|>/g, ""));

  const match = buff.name.match(/\|(C|E|V|Q)\|/);
  const category = match !== null ? types[match[0]] : null;
  let subCat = buff.sub_category;

  if (subCat <= 0) subCat = 7;

  let { name } = buff;
  if (category !== null) {
    name = name.replace(`${match[0]} `, "");
  }

  let enhName, enhDesc;
  if (buffInfo.current_enhancement > 0) {
    enhName = "Enhancement: " + buffInfo.enhancement_data?.name ?? "??";
    const enhDescLines = buffInfo?.enhancement_data?.description
      ? buffInfo.enhancement_data.description.split("\n").filter((s) => s !== "")
      : [];
    enhDesc = enhDescLines.filter((line) => !regex.test(line)).join(" ");
  }

  return (
    <st.Container>
      <st.BuffDescription>
        <st.BuffIconContainer>
          <BuffIcon buff={buff} showOutline size="large" />
        </st.BuffIconContainer>
        <st.Info>
          <st.Name>
            {name.toUpperCase() || "???"}
            {category && <st.Category color={colors[category]}>{category}</st.Category>}
          </st.Name>
          <st.Description>
            {descLines}
            {tags.map((tag) => (
              <st.DescriptionTag key={tag}>{tag}</st.DescriptionTag>
            ))}
          </st.Description>
          {buffInfo.current_enhancement > 0 && (
            <st.EnhInfo>
              <st.EnhName>{enhName}</st.EnhName>
              <st.Description>{enhDesc}</st.Description>
            </st.EnhInfo>
          )}
        </st.Info>
      </st.BuffDescription>
    </st.Container>
  );
};

BuffDetails.propTypes = {
  buffInfo: PropTypes.shape({
    buff: PropTypes.shape({
      name: PropTypes.string,
      description: PropTypes.string,
      id: PropTypes.number,
      sub_category: PropTypes.number,
    }),
  }),
};

export default BuffDetails;
