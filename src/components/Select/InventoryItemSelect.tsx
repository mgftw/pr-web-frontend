import InventoryItemIcon from "$components/InventoryItemIcon";
import useAllItems from "$hooks/useAllItems";
import { Autocomplete, AutocompleteItem } from "@nextui-org/react";
import { useMemo, useState } from "react";

interface InventoryItemSelectProps {
  selectedItemId: number;
  onItemChanged(itemId: number): void;
}

const InventoryItemSelect = ({ selectedItemId, onItemChanged }: InventoryItemSelectProps) => {
  const [query, setQuery] = useState("");
  const items = useAllItems();

  const filtered = useMemo(
    () =>
      query.length > 0
        ? items.filter((item) => item?.name.toLowerCase().includes(query.toLowerCase()))
        : items,
    [query, items],
  );

  return (
    <div className="grow shrink-0 max-w-full">
      <Autocomplete
        label="Change the item"
        description="Use this box to change the item. Actual item is the one in the image"
        aria-label="Select an item"
        placeholder="Search for an item..."
        inputValue={query}
        value={selectedItemId}
        onInputChange={(value) => setQuery(value)}
        onSelectionChange={(selectedKey) => {
          onItemChanged(selectedKey as number);
        }}
      >
        {filtered.map((item) => (
          <AutocompleteItem key={item.id} value={item.id.toString()} textValue={item.name}>
            <div className="flex gap-2 items-center">
              <div className="h-8 w-8 shrink-0 grow-0">
                <InventoryItemIcon item={item} size="small" />
              </div>
              <p>{item.name}</p>
            </div>
          </AutocompleteItem>
        ))}
      </Autocomplete>
    </div>
  );
};

export default InventoryItemSelect;
