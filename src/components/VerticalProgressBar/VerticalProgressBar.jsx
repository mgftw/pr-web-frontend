import React from "react";
import PropTypes from "prop-types";
import colors from "$constants/colors";

import * as st from "./VerticalProgressBar.styles";

const VerticalProgressBar = ({ height, width, value, color, backColor }) => {
  return (
    <st.VerticalProgressBarContainer
      height={height}
      width={width}
      value={value}
      color={colors[color]}
      backColor={colors[backColor]}
    />
  );
};

VerticalProgressBar.propTypes = {
  height: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
  color: PropTypes.string,
  backColor: PropTypes.string,
};

VerticalProgressBar.defaultProps = {
  color: "primary",
  backColor: "first",
};

export default VerticalProgressBar;
