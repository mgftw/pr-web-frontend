import styled from "styled-components";
import tw from "twin.macro";

export const VerticalProgressBarContainer = styled.div`
  ${tw`bg-first`}
  width: 6px;
  height: 100%;
  ${({ height }) => height !== null && `height: ${height}px;`}
  ${({ width }) => width !== null && `width: ${width}px;`}
  ${({ value, color, backColor }) =>
    value !== null && `background: linear-gradient(0deg, ${color} ${value}%, ${backColor} 0%);`}
`;
