import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`flex-col w-64`}
  min-width: 40rem;
`;

export const Title = styled.div`
  ${tw`flex font-oswald text-2xl uppercase font-bold items-center`}
`;

export const Icon = styled.div`
  ${tw`p-2 mr-2 font-karla rounded-full bg-primary text-zero`}
`;

export const Description = styled.div`
  ${tw`mt-2 text-lg text-tertiary`}
`;
