import * as st from "./PageTitle.styles";
import Icon from "$components/Icon";

const PageTitle = ({
  title,
  icon,
  description,
}: {
  title: string;
  description?: string;
  icon: string;
}) => (
  <st.Container>
    <st.Title>
      <st.Icon>
        <Icon icon={icon} size={22} />
      </st.Icon>
      {title}
    </st.Title>
    {description && <st.Description>{description}</st.Description>}
  </st.Container>
);

export default PageTitle;
