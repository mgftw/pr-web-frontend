import PropTypes from "prop-types";

import { BUCKET_URL } from "$constants/url";

import { getStringInitials } from "$helpers/stringHelper";
import * as st from "./InventoryItemIcon.styles";

const types = {
  "|<|": "craft",
};

const InventoryItemIcon = ({ item, size, forceReload }) => {
  // const { buff } = props;
  const match = (item?.name ?? "").match(/\|(C|E|V|Q)\|/);
  const category = types[match?.[0]];

  let { name } = item;
  if (match !== null) {
    name = (name ?? "").replace(`${match[0]} `, "");
  }
  name = name.replace(/\|[^)]*\|/, "");
  const initials = getStringInitials(name);

  const isSpecialItem = [3, 23, 24, 25, 26, 32, 33, 94, 95, 96, 103, 200, 201, 195, 104].includes(
    item?.id,
  );
  return (
    <st.Container>
      {isSpecialItem && <st.Decorator />}
      <st.Handler>
        <st.Placeholder $size={size}>
          <small>{initials}</small>
        </st.Placeholder>
        <st.Content
          title={name}
          $url={`${BUCKET_URL}/icons/item/${item.id}/${size}.png${
            forceReload ? `?t=${new Date().getTime()}` : ""
          }`}
          $category={category}
        />
      </st.Handler>
    </st.Container>
  );
};

InventoryItemIcon.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    description: PropTypes.string,
    id: PropTypes.number,
  }).isRequired,
  size: PropTypes.oneOf(["large", "medium", "small"]),
  forceReload: PropTypes.bool,
};

InventoryItemIcon.defaultProps = {
  size: "large",
  forceReload: false,
};

export default InventoryItemIcon;
