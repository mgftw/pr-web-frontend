import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`bg-first w-12  h-full flex flex-col items-center flex-shrink-0 pt-12`}
`;

export const OptionContainer = styled.div``;

export const Option = styled.div<{ $isSelected: boolean }>`
  ${tw`mt-3 mb-3 w-8 h-8 flex items-center justify-center rounded-full`}

  ${({ $isSelected }) => $isSelected && `background-color: rgba(232, 230, 213, 0.08)`};

  transition: background-color 0.2s;
  &:hover {
    background-color: rgba(232, 230, 213, 0.15);
    transition: background-color 0.2s;
  }
`;

export const TopOptions = styled.div``;

export const MediumOptions = styled.div``;

export const BottomOptions = styled.div``;
