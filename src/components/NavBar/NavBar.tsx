import { useMemo } from "react";
import PropTypes from "prop-types";
import { Tooltip } from "react-tippy";

import Icon from "$components/Icon";
import Link from "$components/Link";

import useAdmin from "$hooks/useAdmin";

import * as st from "./styles";
import { useLocation } from "react-router";

const options = [
  { url: "/dashboard", icon: "icon-home", regex: RegExp("/dashboard?/?.*"), name: "Home" },
  { url: "/profile", icon: "icon-profile", regex: RegExp("/profile?/?.*"), name: "Profile " },
  { url: "/clan", icon: "icon-flag", regex: RegExp("/clan?/?.*"), name: "Clan" },
  { url: "/quest", icon: "icon-book", regex: RegExp("/quest?/?.*"), name: "Quests" },
];

const NavBar = () => {
  const location = useLocation();
  const isAdmin = useAdmin();
  const finalOptions = useMemo(
    () => [
      ...options,
      ...(isAdmin === "yes"
        ? [{ url: "/admin", icon: "icon-admin", regex: RegExp("/admin?/?.*"), name: "Admin" }]
        : []),
    ],
    [isAdmin],
  );

  return (
    <st.Container>
      <st.OptionContainer>
        {finalOptions.map((opt) => (
          <Tooltip title={opt.name} position={"left"}>
            {/* @ts-expect-error compatibility */}
            <Link to={opt.url} key={opt.url}>
              <st.Option key={opt.url} $isSelected={opt.regex.test(location.pathname)}>
                <Icon icon={opt.icon} size={18} />
              </st.Option>
            </Link>
          </Tooltip>
        ))}
      </st.OptionContainer>
    </st.Container>
  );
};

NavBar.propTypes = {
  history: PropTypes.shape({
    replace: PropTypes.func.isRequired,
    location: PropTypes.shape({
      pathname: PropTypes.string,
    }),
  }).isRequired,
  location: PropTypes.shape({
    search: PropTypes.string,
  }).isRequired,
};

export default NavBar;
