import Input from "$components/Input";
import InventoryItem from "$components/InventoryItem";
import useAllItems from "$hooks/useAllItems";
import React, { useState } from "react";
import styled from "styled-components";

const css = {
  Container: styled.div`
    height: 60vh;
    display: grid;
    grid-template-rows: auto 1fr;
    row-gap: 2rem;
  `,
  ItemList: styled.div`
    height: 100%;
    overflow: scroll;

    > div {
      display: grid;
      row-gap: 0.5rem;
      column-gap: 0.5rem;
      padding-top: 0.2rem;
      grid-template-columns: repeat(7, 56px);
    }
  `,
};

const ItemBrowser = () => {
  const [filter, setFilter] = useState(null);
  const items = useAllItems();

  const filteredItems =
    filter && filter !== ""
      ? items.filter((item) => (item.name ?? "").toLowerCase().includes(filter.toLowerCase()))
      : items;
  return (
    <css.Container>
      <Input
        placeholder="Search..."
        value={filter ?? ""}
        onChange={(event) => setFilter(event.target.value)}
      />

      <css.ItemList>
        <div>
          {filteredItems.map((item) => (
            <InventoryItem key={item.id} item={item} size="medium" showQuantity={false} />
          ))}
        </div>
      </css.ItemList>
    </css.Container>
  );
};

export default ItemBrowser;
