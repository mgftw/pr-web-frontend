import { Input } from "@nextui-org/react";
import tw from "twin.macro";
import _omit from "lodash/omit";
import { useDropzone } from "react-dropzone";
import InventoryItem from "$components/InventoryItem";
import useAllItems from "$hooks/useAllItems";
import { useCallback, useState } from "react";
import styled from "styled-components";
import { useMutation } from "@tanstack/react-query";
import { updateItemIcon } from "$api/adminApi";
import { TailSpin } from "react-loading-icons";
import { toast } from "react-toastify";

const css = {
  Container: styled.div`
    ${tw`bg-first p-4`}
    display: grid;
    grid-template-rows: auto 1fr;
    row-gap: 2rem;
    max-height: 86vh;
    overflow: scroll;
  `,
  ItemList: styled.div`
    height: 100%;

    > div {
      display: grid;
      row-gap: 0.5rem;
      column-gap: 0.5rem;
      padding-top: 0.2rem;
      grid-template-columns: repeat(10, auto);
    }
  `,
};

const Icon = ({ item, isLoading, updateIcon }) => {
  const { getRootProps, getInputProps } = useDropzone({
    onDrop: (acceptedFiles) => {
      const file = acceptedFiles[0];
      updateIcon(item.id, file);
    },
    accept: {
      "image/jpeg": [],
      "image/png": [],
    },
    maxFiles: 1,
    maxSize: 8 * 1024 * 1024,
    multiple: false,
  });

  return isLoading ? (
    <TailSpin />
  ) : (
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      <InventoryItem key={item.id} item={item} size="large" quantity={`#${item.id}`} />
    </div>
  );
};

const ItemBrowser = () => {
  const [filter, setFilter] = useState(null);
  const [updateHash, setUpdateHash] = useState({});

  const { mutateAsync: doUpdate } = useMutation((data: { itemId: number; file: any }) =>
    updateItemIcon(data.itemId, data.file),
  );

  const updateIcon = useCallback(
    async (itemId, file) => {
      setUpdateHash((h) => ({
        ...h,
        [itemId]: true,
      }));

      try {
        await doUpdate({ itemId, file });
      } catch (e) {
        toast.error(e.toString());
      } finally {
        setUpdateHash((h) => _omit(h, itemId));
      }
    },
    [doUpdate, setUpdateHash],
  );

  const items = useAllItems().filter((item) => item.name !== "???");

  const filteredItems =
    filter && filter !== ""
      ? items.filter((item) => (item.name ?? "").toLowerCase().includes(filter.toLowerCase()))
      : items;

  return (
    <css.Container>
      <h3>You can drag an image and the item icon will be changed</h3>
      <Input
        placeholder="Search..."
        value={filter ?? ""}
        onChange={(event) => setFilter(event.target.value)}
      />

      <css.ItemList>
        <div>
          {filteredItems.map((item) => (
            <Icon
              key={item.id}
              item={item}
              isLoading={!!updateHash[item.id]}
              updateIcon={updateIcon}
            />
          ))}
        </div>
      </css.ItemList>
    </css.Container>
  );
};

export default ItemBrowser;
