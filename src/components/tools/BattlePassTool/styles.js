import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  height: 92vh;
  padding-botton: 5rem;
  overflow: hidden;
  > div {
    width: 100%;
    display: grid;
    row-gap: 1rem;
    grid-template-rows: auto 1fr;
  }
`;

export const EventContainer = styled.div`
  display: grid;
  height: 100%;
  grid-template-columns: 1fr 2fr 2fr;
  column-gap: 2rem;
`;

export const StageContainer = styled.div`
  display: flex;
  height: 100%;
  overflow: hidden;
  flex-direction: column;
  row-gap: 0.25rem;
`;

export const StageContentContainer = styled.div`
  ${tw`bg-first text-primary px-5 py-2 text-base rounded font-oswald`}
  display: flex;
  overflow: auto;
  max-height: 100%;
  flex-direction: column;
  row-gap: 0.5rem;
`;
export const RewardsContainer = styled.div`
  ${tw`bg-first text-primary px-5 py-2 text-base rounded font-oswald`}
  display: flex;
  height: fit-content;
  overflow: visible;
  max-height: 100%;
  flex-direction: column;
  row-gap: 0.5rem;
`;

export const Stage = styled.div`
  ${tw`bg-first text-primary px-5 py-2 text-base rounded font-oswald uppercase`}
  ${({ isActive }) => isActive && tw`bg-primary text-zero`}
  cursor: pointer;
`;

export const AddStage = styled.div`
  ${tw`border-2 border-primary border-dashed px-5 py-2 text-primary rounded font-oswald uppercase`}
  cursor: pointer;
`;

export const ObjectiveContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 5rem auto;
  column-gap: 0.5rem;
`;

export const SingleRewardContainer = styled.div`
  display: grid;
  grid-template-columns: auto 1fr 5rem auto;
  column-gap: 0.5rem;
`;

export const StagesListContainer = styled.div`
  height: 65vh;
  display: flex;
  flex-direction: column;
  row-gap: 0.25rem;
  overflow: scroll;
`;
