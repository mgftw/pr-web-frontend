/* eslint-disable */

export const exampleItems = {
  premium: {
    vending_machine: {
      id: '1',
      name: 'Vending Machine',
      desc1:
        'The vending machine periodically spawns a random Exp, Cash or FR-Pill toaster to pick up.',
      desc2:
        'Each toaster drop has a 20% chance to become a multi toaster which spawns 10 other toasters.',
      traveller_token_cost: '10',
    },
    gem: {
      id: '2',
      name: '???',
      desc1: '???',
      hidden: '1',
    },
    exp_package: {
      id: '3',
      name: 'Exp/Cash Booster (x2, 30 Days)',
      desc1: 'When activated, increases the exp and cash earned from awards',
    },
    points: {
      id: '4',
      name: '???',
      desc1: '???',
      hidden: '1',
    },
    exp: {
      id: '5',
      name: '???',
      desc1: '???',
      hidden: '1',
    },
    starter_account: {
      id: '6',
      name: '???',
      desc1: '???',
      hidden: '1',
    },
    vending_machine_gold: {
      id: '7',
      name: 'Premium Vending Machine',
      desc1:
        'The vending machine periodically spawns a multi toaster for the users to pick up.',
      desc2: 'Multi toasters will spawn 10 random toasters.',
    },
    enhancement_stone: {
      id: '8',
      name: 'Enhancement Stone',
      desc1:
        'This item no longer has a use. It can be safely sold at the Black Market.',
      black_market_sell_value: '750',
    },
    helms_deep_ticket: {
      id: '9',
      name: 'Helms Deep Ticket',
      desc1: 'Use this ticket to attempt to complete the Helms Deep scenario.',
      desc2: '<Enter scenario server and type !sn to host>',
      desc3: "<Requires 'Hell in the Deeps' quest>",
      show_in_versus_market: '1',
      cost: '75',
    },
    item10: {
      id: '10',
      name: '|Q| Black Market Pass',
      desc1: 'Grants access to the Black Market, where you can buy rare items',
      desc2: "if they're available, or sell your own spare items for cash.",
      desc3: 'To access, either use this item or enter the !bmarket command.',
    },
    item11: {
      id: '11',
      name: '|Q| Specialization Training Kit',
      desc1: 'Allows you to specialize into a class of your choice.',
      desc2: 'Requires training to unlock each class.',
    },
    revert_stone: {
      id: '12',
      name: 'Revert Stone',
      desc1:
        'Will allow you to remove an upgrade or skill, and reseal all of the wasted points and pills.',
      desc2: 'The stone, however, is unable to restore enhancement stones.',
    },
    item13: {
      id: '13',
      name: '|Q| Weapon Configurator',
      desc1: 'Grants access to the Weapon Configurator.',
      desc2: 'To access, use this item, select from the Others section of the',
      desc3: 'Control Panel or enter the command !wconfig into chat.',
      desc4: 'Entering !wconfig @held will open up config for held weapon.',
    },
    guild_stone: {
      id: '14',
      name: '|Auto| Clan Stone',
      desc1:
        'This stone allows everyone to know that you have all the qualities necessary',
      desc2: 'to lead and help them survive. Use it to create a clan.',
    },
    anti_mutation: {
      id: '15',
      name: '|Buff| Mutation Shield (30 Days)',
      desc1:
        'A dose of antibodies that will shield you from the mutation status',
    },
    item16: {
      id: '16',
      name: '|Buff| Mutation Shield (7 Days)',
      desc1: 'A dose of antibodies that will shield you from mutations',
      traveller_token_cost: '7',
    },
    item17: {
      id: '17',
      name: '|Buff| Mutation Shield (1 Day)',
      desc1: 'A dose of antibodies that will shield you from mutations',
    },
    item18: {
      id: '18',
      name: 'Enhancer',
      desc1:
        'The liquid enhancer is a mix of several mutated cells that will instantly',
      desc2:
        'level up the user to Lv. 201 and will prepare its body to endure the',
      desc3: 'later levels.',
      desc4: '<Required level: 200>',
    },
    item19: {
      id: '19',
      name: '???',
      desc1: '???',
      hidden: '1',
    },
    item20: {
      id: '20',
      name: 'Infected Lure',
      desc1: 'Increases the current difficulty to the next level. Can only be',
      desc2: 'used once per round, but can be used after a round was lost.',
      desc3: '<Cannot be used on versus, scenario or on Expert difficulty>',
      desc4: '<Must be used inside the first 30 seconds of the first round>',
      desc5: '<Requires at least 10 players for Expert>',
      black_market_chance: '50',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '30720',
      black_market_pill_cost: '115',
      black_market_blood_cost: '0',
    },
    item21: {
      id: '21',
      name: '|Buff| Toxin Shield (30 Days)',
      desc1: 'Temporarily unlocks the Toxin Shield buff.',
    },
    item22: {
      id: '22',
      name: 'Traveller Token',
      desc1: 'Redeem Traveller Tokens to earn unique items!',
      desc2: '<Use item to access Token Exchange>',
      desc3: '<Max count: 50>',
      max_count: '50',
    },
    item23: {
      id: '23',
      name: 'Exp/Cash Booster (x3, 30 Days)',
      desc1: 'When activated, increases the exp and cash earned from awards',
    },
    item24: {
      id: '24',
      name: 'Exp/Cash Booster (x4, 30 Days)',
      desc1: 'When activated, increases the exp and cash earned from awards',
    },
    item25: {
      id: '25',
      name: 'Exp/Cash Booster (x5, 30 Days)',
      desc1: 'When activated, increases the exp and cash earned from awards',
    },
    item26: {
      id: '26',
      name: 'Exp/Cash Booster (x10, 30 Days)',
      desc1: 'When activated, increases the exp and cash earned from awards',
    },
    item27: {
      id: '27',
      name: 'Unique Buff Unlocker',
      desc1: 'Unlocks the unique buff slot and allow you to equip buffs in it.',
    },
    item28: {
      id: '28',
      name: 'Traveller Value Pack',
      desc1: 'Gives 2 Volatile Tank Blood, 1 Honor Coupon, 1 Cupid Token,',
      desc2: '1 Small Jar of Pills and one random Torn Ticket.',
      desc3: '<Max count: 10>',
      allow_use_all: '1',
      traveller_token_cost: '1',
      max_count: '10',
    },
    item29: {
      id: '29',
      name: '|Buff| Goggles',
      desc1: 'Unlocks the Goggles buff.',
    },
    item30: {
      id: '30',
      name: '|Buff| Piercing Bullets',
      desc1: 'Unlocks the Piercing Bullets buff.',
    },
    item31: {
      id: '31',
      name: '|Buff| Artisan',
      desc1: 'Unlocks the Artisan buff',
    },
    item32: {
      id: '32',
      name: 'Exp/Cash Booster (x20, 30 Days)',
      desc1: 'When activated, increases the exp and cash earned from awards',
    },
    item33: {
      id: '33',
      name: 'Exp/Cash Booster (x50, 30 Days)',
      desc1: 'When activated, increases the exp and cash earned from awards',
    },
    item34: {
      id: '34',
      name: '|Buff| Hunterized',
      desc1: 'Unlocks the Hunterized buff',
    },
    item35: {
      id: '35',
      name: '|Buff| Toxin Shield (7 Days)',
      desc1: 'Temporarily unlocks the Toxin Shield buff.',
    },
    item36: {
      id: '36',
      name: '|Buff| General Status Shield (7 Days)',
      desc1: 'Shields you from all bad status',
    },
    item37: {
      id: '37',
      name: '|Buff| Pet',
      desc1: 'Spawn a friendly charger that will help you.',
    },
    item38: {
      id: '38',
      name: '???',
      desc1: '???',
      hidden: '1',
    },
    item39: {
      id: '39',
      name: '???',
      desc1: '???',
      hidden: '1',
    },
    item40: {
      id: '40',
      name: 'Donor Package (1 Month)',
      desc1: 'Activating this item will grant you donor status',
      desc2: 'for 1 Month from activation date.',
      desc3: '<Must be activated when not donor>',
    },
    item41: {
      id: '41',
      name: 'Donor Package (Lifetime)',
      desc1: 'Activating this item will grant you lifetime donor status.',
    },
    item42: {
      id: '42',
      name: 'Mimic Ticket',
      desc1: 'If a scenario hosted by you is lost, this item will be consumed',
      desc2: 'instead of any other required item.',
      desc3: 'Alternatively, this item can be sent to your clan to gain the',
      desc4: 'same effect for any clan hosted scenario.',
      desc5: 'If sent to clan, it cannot be taken back.',
      desc6:
        '<For clan hosts, must be in clan inventory before scenario start>',
      clan_transfer: '1',
      black_market_chance: '-8',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '103405',
      black_market_pill_cost: '395',
      black_market_blood_cost: '4',
    },
    item43: {
      id: '43',
      name: 'Master Crafter',
      desc1:
        'Enables you to guarantee crafting success by using this item during crafting.',
      desc2: '<Also works with Clan Crafting>',
      black_market_chance: '-8',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '144580',
      black_market_pill_cost: '425',
      black_market_blood_cost: '5',
    },
    item44: {
      id: '44',
      name: 'Charismatic Speech',
      desc1: "If used from clan inventory, lowers clan's fear level by 10.",
      desc2: 'If used from player inventory, increases reputation gain by',
      desc3: '20% for the current round.',
      desc4: "<Doesn't stack with High Reputation buff>",
      category: '3',
      clan_transfer: '1',
      black_market_chance: '25',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '31550',
      black_market_pill_cost: '205',
      black_market_blood_cost: '0',
    },
    item45: {
      id: '45',
      name: 'Itemfinder',
      desc1: 'Upon use, all map items will have their glows visible',
      desc2: 'through walls and have their glow range increased.',
      desc3: "A list of items found is sent to the user's console.",
      desc4: '<Only usable in COOP servers, once per map>',
      black_market_chance: '25',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '39110',
      black_market_pill_cost: '225',
      black_market_blood_cost: '0',
    },
    item46: {
      id: '46',
      name: 'Tank Beacon (Tier 1)',
      desc1: 'Upon use, immediately spawns a random Tier 1 Tank',
      desc2: '<Can only be used on COOP servers>',
      desc3: '<Increases Tank Beacon stacks for this map by 1>',
      desc4: '<Cannot spawn more tanks than human players>',
      desc5: '<Cannot use if Tank Beacon stacks exceed 10>',
      category: '3',
    },
    item47: {
      id: '47',
      name: 'Tank Beacon (Tier 2)',
      desc1: 'Upon use, immediately spawns a random Tier 2 Tank',
      desc2: '<Can only be used on COOP servers>',
      desc3: '<Increases Tank Beacon stacks for this map by 2>',
      desc4: '<Cannot spawn more tanks than human players>',
      desc5: '<Cannot use if Tank Beacon stacks exceed 10>',
      category: '3',
      traveller_token_cost: '15',
    },
    item48: {
      id: '48',
      name: 'Tank Beacon (Tier 3)',
      desc1: 'Upon use, immediately spawns a random Tier 3 Tank',
      desc2: '<Can only be used on COOP servers>',
      desc3: '<Increases Tank Beacon stacks for this map by 3>',
      desc4: '<Cannot spawn more tanks than human players>',
      desc5: '<Cannot use if Tank Beacon stacks exceed 10>',
      category: '3',
      traveller_token_cost: '20',
    },
    item49: {
      id: '49',
      name: "Cupid's Token",
      desc1: 'Use cupid tokens to obtain special unique buffs!',
      desc2: '<Use item to access Token Exchange>',
      black_market_chance: '70',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '37500',
      black_market_pill_cost: '175',
      black_market_blood_cost: '2',
      black_market_sell_value: '20000',
    },
    item50: {
      id: '50',
      name: "|<| Cupid's Token Fragment A",
      desc1: "The fragment of a cupid's token. Gather the 4 fragments",
      desc2: '(A, B, C and D) to create a cupid token.',
    },
    item51: {
      id: '51',
      name: "|<| Cupid's Token Fragment B",
      desc1: "The fragment of a cupid's token. Gather the 4 fragments",
      desc2: '(A, B, C and D) to create a cupid token.',
    },
    item52: {
      id: '52',
      name: "|<| Cupid's Token Fragment C",
      desc1: "The fragment of a cupid's token. Gather the 4 fragments",
      desc2: '(A, B, C and D) to create a cupid token.',
    },
    item53: {
      id: '53',
      name: "|<| Cupid's Token Fragment D",
      desc1: "The fragment of a cupid's token. Gather the 4 fragments",
      desc2: '(A, B, C and D) to create a cupid token.',
    },
    item54: {
      id: '54',
      name: 'Small Jar Of FR-Pills',
      desc1: 'A jar containing 50 FR-Pills',
      model: 'models/w_models/weapons/w_eq_painpills.mdl',
      spawn_on_map: '1',
      chance: '35',
      limit: '1',
      allow_use_all: '1',
    },
    item55: {
      id: '55',
      name: 'Medium Jar Of FR-Pills',
      desc1: 'A jar containing 150 FR-Pills',
      model: 'models/w_models/weapons/w_eq_painpills.mdl',
      spawn_on_map: '1',
      chance: '5',
      limit: '1',
      allow_use_all: '1',
    },
    item56: {
      id: '56',
      name: 'Large Jar Of FR-Pills',
      desc1: 'A jar containing 300 FR-Pills',
      model: 'models/w_models/weapons/w_eq_painpills.mdl',
      spawn_on_map: '1',
      chance: '1',
      limit: '1',
      allow_use_all: '1',
    },
    item57: {
      id: '57',
      name: '|Buff| Love is in the air',
      desc1: 'Unlocks the Love is in the air Buff',
    },
    item58: {
      id: '58',
      name: '|Buff| Rawr... love',
      desc1: 'Unlocks the Rawr... love Buff',
    },
    item59: {
      id: '59',
      name: "|Buff| Cupid's Arrow Buff",
      desc1: "Unlocks the Cupid's Arrow Buff",
    },
    item60: {
      id: '60',
      name: '|Buff| Zombie Eradicator Buff',
      desc1: 'Unlocks the Zombie Eradicator Buff',
    },
    item61: {
      id: '61',
      name: 'Fast Track Ticket',
      desc1:
        'When used, automatically completes a chosen non-scenario non-collect',
      desc2: 'quest task from a repeatable quest.',
    },
    item62: {
      id: '62',
      name: '|Buff| Flaming Sword Buff',
      desc1: 'Unlocks the Flaming Sword Buff',
    },
    item63: {
      id: '63',
      name: 'Superfast Track Ticket',
      desc1: 'When used, automatically completes a chosen repeatable quest.',
    },
    item64: {
      id: '64',
      name: '|ACC| Deer Mask',
      desc1: 'Unlocks the Deer Mask you wear it.',
    },
    item65: {
      id: '65',
      name: "|Buff| It wasn't that high",
      desc1: "Unlocks the It wasn't that high buff",
    },
    item66: {
      id: '66',
      name: '|<| Reinforced Tank Skin',
      desc1: '<Uncommon Crafting Material>',
      desc2: 'Tank skin that can resist a lot of damage.',
      desc3: 'Tanks with defensive abilities have this kind of skin.',
      category: '1',
      clan_transfer: '1',
      show_in_pill_market: '1',
      cost: '250',
      black_market_sell_value: '250',
    },
    item67: {
      id: '67',
      name: '|<| Regenerative Gland',
      desc1: '<Uncommon Crafting Material>',
      desc2: 'A mutated gland that is able to create rapid regenerative cells.',
      desc3: 'Most tanks with regenerative abilities have developed it.',
      category: '1',
      clan_transfer: '1',
      show_in_pill_market: '1',
      cost: '250',
      black_market_sell_value: '250',
    },
    item68: {
      id: '68',
      name: '|<| Corrosive Acid Sample',
      desc1: '<Rare Crafting Material>',
      desc2: 'A sample of corrosive acid carefully wrapped to prevent damage.',
      category: '1',
      black_market_chance: '0',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '88500',
      black_market_pill_cost: '405',
      black_market_blood_cost: '3',
    },
    item69: {
      id: '69',
      name: '|<| Rusted Metal Chunk',
      desc1: '<Crafting Material>',
      desc2:
        'A chunk of rusted metal, capable of being melted down to be reconstructed.',
      desc3:
        'Prior to reconstruction, it must be cleansed of its rust to be properly usable.',
      model: 'models/gibs/metal_gib2.mdl',
      show_in_pill_market: '1',
      cost: '250',
      category: '1',
      clan_transfer: '1',
      offset: '0.0 0.0 2.0',
      spawn_on_map: '1',
      chance: '40',
      limit: '2',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '3525',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '705',
    },
    item70: {
      id: '70',
      name: '|<| Box of empty shells',
      desc1: '<Crafting Material>',
      desc2:
        'A box containing a large number of empty gun shells. They can be refilled',
      desc3: 'with any desired powder or liquid.',
      category: '1',
      clan_transfer: '1',
      show_in_pill_market: '1',
      cost: '250',
      spawn_on_map: '1',
      chance: '10',
      limit: '1',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '4975',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '995',
    },
    item71: {
      id: '71',
      name: '|<| Liquid nitrogen container',
      desc1: '<Rare Crafting Material>',
      desc2: 'A container with some liquid nitrogen inside.',
      category: '1',
      black_market_chance: '0',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '88500',
      black_market_pill_cost: '405',
      black_market_blood_cost: '3',
    },
    item72: {
      id: '72',
      name: '|<| Mutation Refiner',
      desc1: '<Uncommon Crafting Material>',
      desc2: 'A special chemical that stabilizes rapidly mutating cells.',
      desc3: 'Tanks and infected with powerful mutations on their bodies',
      desc4: 'may contain it in their bodies.',
      category: '1',
      clan_transfer: '1',
      show_in_pill_market: '1',
      cost: '250',
      black_market_sell_value: '250',
    },
    item73: {
      id: '73',
      name: '|<| Welding Gun',
      desc1: '<Crafting Material>',
      desc2: 'A gun capable of generating high intensity heat, typically used',
      desc3: 'to melt down metal. Appears to only have enough fuel for 1 use.',
      desc4:
        'It can also be used to create fires if the other material is flammable.',
      show_in_pill_market: '1',
      cost: '250',
      category: '1',
      clan_transfer: '1',
      model: 'models/props_equipment/gas_pump_p13.mdl',
      offset: '0.0 0.0 10.0',
      spawn_on_map: '1',
      chance: '25',
      limit: '1',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '4600',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '920',
    },
    item74: {
      id: '74',
      name: 'Traveller Box',
      desc1:
        'A box containing one of the following items: Infected Lure, Mimic Ticket,',
      desc2:
        'Master Crafter, Charismatic Speech, Itemfinder, Tank Beacon (Tier 1), ',
      desc3:
        'FR-Pill Discount Voucher, Craftable Bag, Area Mutator or Tank Bait.',
      traveller_token_cost: '5',
      allow_use_all: '1',
      black_market_chance: '65',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '23550',
      black_market_pill_cost: '80',
      black_market_blood_cost: '0',
    },
    item75: {
      name: '|Buff| Super Moustachio Bless',
      id: '75',
      desc1: 'Unlocks the Super Moustachio Bless buff',
    },
    item76: {
      name: 'Refund Policy',
      id: '76',
      desc1: 'Increases the chances of the next craft attempt to be successful',
      desc2: 'by 10% for the first item, then 5% for each after.',
      desc3: '<Item is passively consumed when attempting craft>',
      desc4: '<Not consumed if the chance is already 100%>',
    },
    item77: {
      name: 'FR-Pill Discount Voucher',
      id: '77',
      desc1:
        'Allows you to redeem the Voucher to lower the FR-Pills cost of purchasing upgrades,',
      desc2:
        'skills, buffs, crafting, levelling up classes or from the FR-Pill Market by 30%.',
      desc3: '<Maximum cost reduction of 25,000 FR-Pills>',
      desc4: '<Cannot be used in Quick Rank Up>',
    },
    item78: {
      name: 'Craftable Bag',
      id: '78',
      desc1: 'When used, it gives 3 random common crafting material items.',
      spawn_on_map: '1',
      chance: '15',
      limit: '1',
      model: 'models/props_swamp/parachute_bag01.mdl',
      angles: '-90.0 0.0 0.0',
      allow_use_all: '1',
    },
    item79: {
      name: 'Area Mutator',
      id: '79',
      desc1: 'Using this item will force enable a random Map Mutation.',
      desc2:
        'Each active Map Mutation increases exp/cash rewards by 25% (50% for CI/SI kills).',
      desc3: '<Must be used inside the first 30 seconds of the round>',
      desc4:
        "<Must be Advanced/Expert difficulty, can't be used in survival/versus>",
      black_market_chance: '50',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '29950',
      black_market_pill_cost: '115',
      black_market_blood_cost: '0',
    },
    item80: {
      id: '80',
      name: 'Supreme Regeneration Booster',
      desc1:
        'Once activated, you will gain 20HP regeneration per second for 10 minutes',
      desc2: '<Healing always applies>',
      desc3: '<✖VS>',
      model: 'models/w_models/weapons/w_eq_adrenaline.mdl',
      category: '2',
      black_market_chance: '20',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '35105',
      black_market_pill_cost: '150',
      black_market_blood_cost: '1',
    },
    item81: {
      id: '81',
      name: 'Supreme Defense Booster',
      desc1: 'You receive 70% less damage from all sources for 10 minutes.',
      desc2: "<Doesn't affect fall damage>",
      desc3: '<✖VS>',
      model: 'models/w_models/weapons/w_eq_adrenaline.mdl',
      category: '2',
      black_market_chance: '20',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '35105',
      black_market_pill_cost: '150',
      black_market_blood_cost: '1',
    },
    item82: {
      id: '82',
      name: 'Regeneration Booster',
      desc1:
        'Once activated, you will gain 1HP regeneration per second for 10 minutes',
      desc2: '<Healing always applies>',
      desc3: '<✖VS>',
      model: 'models/w_models/weapons/w_eq_adrenaline.mdl',
      category: '2',
    },
    item83: {
      id: '83',
      name: 'Advanced Regeneration Booster',
      desc1:
        'Once activated, you will gain 3HP regeneration per second for 10 minutes',
      desc2: '<Healing always applies>',
      desc3: '<✖VS>',
      model: 'models/w_models/weapons/w_eq_adrenaline.mdl',
      category: '2',
    },
    item84: {
      id: '84',
      name: 'Refined Regeneration Booster',
      desc1:
        'Once activated, you will gain 8HP regeneration per second for 10 minutes',
      desc2: '<Healing always applies>',
      desc3: '<✖VS>',
      model: 'models/w_models/weapons/w_eq_adrenaline.mdl',
      category: '2',
      black_market_chance: '35',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '17855',
      black_market_pill_cost: '70',
      black_market_blood_cost: '0',
    },
    item85: {
      id: '85',
      name: 'Defense Booster',
      desc1: 'You receive 10% less damage from all sources for 10 minutes.',
      desc2: "<Doesn't affect fall damage>",
      desc3: '<✖VS>',
      model: 'models/w_models/weapons/w_eq_adrenaline.mdl',
      category: '2',
    },
    item86: {
      id: '86',
      name: 'Advanced Defense Booster',
      desc1: 'You receive 25% less damage from all sources for 10 minutes.',
      desc2: "<Doesn't affect fall damage>",
      desc3: '<✖VS>',
      model: 'models/w_models/weapons/w_eq_adrenaline.mdl',
      category: '2',
    },
    item87: {
      id: '87',
      name: 'Refined Defense Booster',
      desc1: 'You receive 40% less damage from all sources for 10 minutes.',
      desc2: "<Doesn't affect fall damage>",
      desc3: '<✖VS>',
      model: 'models/w_models/weapons/w_eq_adrenaline.mdl',
      category: '2',
      black_market_chance: '35',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '17855',
      black_market_pill_cost: '70',
      black_market_blood_cost: '0',
    },
    item88: {
      id: '88',
      name: 'Traveller Token Fragment',
      desc1: 'A piece of a Traveller Token. Acquiring 2 converts them to a',
      desc2: 'full Traveller Token.',
      hidden: '1',
    },
    item89: {
      id: '89',
      name: 'Traveller Coupon',
      desc1: 'A coupon given for spending Traveller Tokens.',
      desc2: 'With 3 Traveller Coupons, you can purchase a Precious Box',
      desc3: 'from the Traveller Token Exchange.',
      desc4: 'Earn coupons every 5 tokens not spent on Precious Boxes.',
      desc5: '<Max count: 30>',
      max_count: '30',
    },
    item90: {
      id: '90',
      name: '(GREEN) CEDA Research Notes',
      desc1: '<Crafting Material>',
      desc2: 'Notes from CEDA containing information of the green flu.',
      desc3: 'Reading them will make you more experienced.',
      desc4: 'The color of the seal shows the value of the information.',
      desc5: '<Grants 1,000 experience points>',
      model: 'models/props_downtown/notepad.mdl',
      spawn_on_map: '1',
      chance: '75',
      limit: '5',
      allow_use_all: '1',
      black_market_sell_value: '200',
    },
    item91: {
      id: '91',
      name: '(YELLOW) CEDA Research Notes',
      desc1: 'Notes from CEDA containing information of the green flu.',
      desc2: 'Reading them will make you more experienced.',
      desc3: 'The color of the seal shows the value of the information.',
      desc4: '<Grants 10,000 experience points>',
      model: 'models/props_downtown/notepad.mdl',
      spawn_on_map: '1',
      chance: '50',
      limit: '4',
      allow_use_all: '1',
      black_market_sell_value: '750',
    },
    item92: {
      id: '92',
      name: '(RED) CEDA Research Notes',
      desc1: 'Notes from CEDA containing information of the green flu.',
      desc2: 'Reading them will make you more experienced.',
      desc3: 'The color of the seal shows the value of the information.',
      desc4: '<Grants 100,000 experience points>',
      model: 'models/props_downtown/notepad.mdl',
      announce_on_grab: '1',
      spawn_on_map: '1',
      chance: '10',
      limit: '3',
      allow_use_all: '1',
      black_market_chance: '35',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '12510',
      black_market_pill_cost: '20',
      black_market_blood_cost: '0',
      black_market_sell_value: '5000',
    },
    item93: {
      id: '93',
      name: '(TOP SECRET) CEDA Research Notes',
      desc1: 'Notes from CEDA containing information of the green flu.',
      desc2: 'Reading them will make you more experienced.',
      desc3: 'The color of the seal shows the value of the information.',
      desc4: '<Grants 1,000,000 experience points>',
      model: 'models/props_downtown/notepad.mdl',
      announce_on_grab: '1',
      spawn_on_map: '1',
      chance: '3',
      limit: '1',
      allow_use_all: '1',
      black_market_chance: '15',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '51590',
      black_market_pill_cost: '105',
      black_market_blood_cost: '1',
      black_market_sell_value: '20000',
    },
    item94: {
      id: '94',
      name: 'Exp/Cash Booster (x2, 7 Days)',
      desc1: 'When activated, increases the exp and cash earned from awards',
    },
    item95: {
      id: '95',
      name: 'Exp/Cash Booster (x3, 7 Days)',
      desc1: 'When activated, increases the exp and cash earned from awards',
    },
    item96: {
      id: '96',
      name: 'Exp/Cash Booster (x5, 7 Days)',
      desc1: 'When activated, increases the exp and cash earned from awards',
    },
    item97: {
      id: '97',
      name: 'Wearable Box',
      desc1: "The box contains a random wearable that you don't have!",
      show_in_pill_market: '1',
      cost: '500',
      black_market_sell_value: '20000',
    },
    item98: {
      id: '98',
      name: 'Focused Craftable Bag',
      desc1: 'When used, allows you to select a common crafting material.',
      desc2: 'You will receive the 3 of the chosen material.',
      allow_use_all: '1',
    },
    item99: {
      id: '99',
      name: 'Tank Bait',
      desc1:
        'Using this item will increase Tier 2/3 spawn chances for the current',
      desc2: 'map by 2.5%, up to a maximum of 10% for the map.',
      desc3:
        'If the next predicted Tank to spawn is Tier 1, it will also be rerolled.',
      desc4: '<Cannot be used in survival or versus>',
      category: '3',
      black_market_chance: '45',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '29110',
      black_market_pill_cost: '155',
      black_market_blood_cost: '0',
    },
    item100: {
      id: '100',
      name: '200 Ice bullets',
      desc1:
        '200 ice bullets. Use this item to transfer the bullets and use them.',
      desc2:
        'Ice bullets will slow the target down until they are completely frozen.',
      desc3: '<It is not possible to freeze tanks>',
      desc4: '<Bullets are only consumed on Special Infected/Tank shots>',
    },
    item101: {
      id: '101',
      name: '200 Acid bullets',
      desc1:
        '200 acid bullets. Use this item to transfer the bullets and use them.',
      desc2:
        "Acid bullets will pierce the target's blocking abilities, allowing you",
      desc3: 'to constantly damage them.',
      desc4: '<Bullets are only consumed on Special Infected/Tank shots>',
    },
    item102: {
      id: '102',
      name: '|Special| Legendary Vending Machine',
      desc1:
        'The vending machine will periodically spawn a LEGENDARY toaster for the users to pick up.',
      desc2: '<Warning: Please use when the minimum player requirement is met>',
    },
    item103: {
      id: '103',
      name: 'Exp/Cash Booster (x10, 7 Days)',
      desc1: 'When activated, increases the exp and cash earned from awards',
    },
    item104: {
      id: '104',
      name: '(LEGENDARY) CEDA Research Notes',
      desc1: 'Notes from CEDA containing information of the green flu.',
      desc2: 'Reading them will make you more experienced.',
      desc3: 'The color of the seal shows the value of the information.',
      desc4: 'For class exp., can only be used at Class Level 4 but',
      desc5: 'bypasses single class exp earn restrictions.',
      desc6: '<Grants 25,000,000 experience points>',
      model: 'models/props_downtown/notepad.mdl',
      announce_on_grab: '0',
      spawn_on_map: '0',
      chance: '0',
      limit: '0',
      allow_use_all: '0',
    },
    item105: {
      id: '105',
      name: 'Resilience Booster',
      desc1: 'When used, bad status effect durations are 5% shorter',
      desc2: 'and bonus toaster effect durations are 5% longer',
      desc3: '<✖VS>',
      desc4: '<Lasts 10 minutes>',
      category: '2',
    },
    item106: {
      id: '106',
      name: 'Advanced Resilience Booster',
      desc1: 'When used, bad status effect durations are 15% shorter',
      desc2: 'and bonus toaster effect durations are 15% longer',
      desc3: '<✖VS>',
      desc4: '<Lasts 10 minutes>',
      category: '2',
    },
    item107: {
      id: '107',
      name: 'Refined Resilience Booster',
      desc1: 'When used, bad status effect durations are 30% shorter',
      desc2: 'and bonus toaster effect durations are 30% longer',
      desc3: '<✖VS>',
      desc4: '<Lasts 10 minutes>',
      category: '2',
      black_market_chance: '35',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '17855',
      black_market_pill_cost: '70',
      black_market_blood_cost: '0',
    },
    item108: {
      id: '108',
      name: 'Supreme Resilience Booster',
      desc1: 'When used, bad status effect durations are 50% shorter',
      desc2: 'and bonus toaster effect durations are 50% longer',
      desc3: '<✖VS>',
      desc4: '<Lasts 10 minutes>',
      category: '2',
      black_market_chance: '20',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '35105',
      black_market_pill_cost: '150',
      black_market_blood_cost: '1',
    },
    item109: {
      id: '109',
      name: '|<| Viral Solution',
      desc1: '<Uncommon Crafting Material>',
      desc2:
        'An extremely potent substance known for its toxic properties against humans.',
      desc3:
        'Causes effects ranging from outright death to various mutations on the body.',
      desc4: 'Due to its lethality, it is kept tightly sealed.',
      category: '1',
      clan_transfer: '1',
      show_in_pill_market: '1',
      cost: '250',
      black_market_sell_value: '250',
    },
    item110: {
      id: '110',
      name: 'Gift (from Boomert Mccarter)',
      desc1: 'A gift that can be opened with a BOOMER key',
    },
    item111: {
      id: '111',
      name: 'Gift (from Smokky Hackett)',
      desc1: 'A gift that can be opened with a SMOKER key',
    },
    item112: {
      id: '112',
      name: 'Gift (from Huntery Thornburg)',
      desc1: 'A gift that can be opened with a HUNTER key',
    },
    item113: {
      id: '113',
      name: 'Gift (from Chargerb Jernigan)',
      desc1: 'A gift that can be opened with a CHARGER key',
    },
    item114: {
      id: '114',
      name: 'Golden Gift (from Tankery Hungery)',
      desc1: 'A gift that can only opened with a TANK key',
    },
    item115: {
      id: '115',
      name: 'Special Gift (from The Rabbid Reindeers)',
      desc1: 'A gift that does not need a key to be opened',
    },
    item116: {
      id: '116',
      name: 'Special Gift (from the Bloody Snowmen)',
      desc1: 'A gift that does not need a key to be opened',
    },
    item117: {
      id: '117',
      name: 'Boomer Key',
      desc1: 'A key that can open gifts!',
    },
    item118: {
      id: '118',
      name: 'Smoker Key',
      desc1: 'A key that can open gifts!',
    },
    item119: {
      id: '119',
      name: 'Hunter Key',
      desc1: 'A key that can open gifts!',
    },
    item120: {
      id: '120',
      name: 'Charger Key',
      desc1: 'A key that can open gifts!',
    },
    item121: {
      id: '121',
      name: 'Tank Key',
      desc1: 'A key that can open gifts!',
    },
    item122: {
      name: 'Black Market Voucher',
      desc1:
        'Allows you to redeem this Voucher when purchasing Special Tank Materials',
      desc2: 'in the Black Market to waive the extra resource cost.',
      hidden: '1',
    },
    item123: {
      id: '123',
      name: 'Bitter Chocolate Box',
      desc1: 'A codename for a box what will give you a random items',
      refundable: '1',
      refundid: '49',
      refundcount: '15',
    },
    item124: {
      id: '124',
      name: 'Sweet Chocolate Box',
      desc1: 'A codename for a box what will give you a random items',
      refundable: '1',
      refundid: '49',
      refundcount: '15',
    },
    item125: {
      id: '125',
      name: 'White Chocolate Box',
      desc1: 'A codename for a box what will give you random items.',
      refundable: '1',
      refundid: '49',
      refundcount: '15',
    },
    item126: {
      id: '126',
      name: 'Basic Pickup Enhancer',
      desc1:
        'With multiple of these, picking up a Standard Pile of Money or Green CEDA Research Notes',
      desc2:
        'will consume these to upgrade the picked up item to the next tier.',
      hidden: '1',
    },
    item127: {
      id: '127',
      name: 'Advanced Pickup Enhancer',
      desc1:
        'With multiple of these, picking up a Valuable Pile of Money or Yellow CEDA Research Notes',
      desc2:
        'will consume these to upgrade the picked up item to the next tier.',
      hidden: '1',
    },
    item128: {
      id: '128',
      name: 'Chocolate Egg',
      desc1: 'Exchange eggs to Rufus for great rewards!',
    },
    item129: {
      id: '129',
      name: 'Bitter Chocolate bunny',
      desc1:
        'A yummy chocolate bunny. May contain a random amount of eggs inside!',
    },
    item130: {
      id: '130',
      name: 'White Chocolate bunny',
      desc1:
        'A yummy chocolate bunny. May contain a random amount of eggs inside!',
    },
    item131: {
      id: '131',
      name: 'Sweet Chocolate bunny',
      desc1:
        'A yummy chocolate bunny. May contain a random amount of eggs inside!',
    },
    item132: {
      id: '132',
      name: 'Binder',
      desc1:
        'A binder allows you to convert a buff of your choice into a skill. Meaning',
      desc2:
        "that it will not require a buff slot to be used, but also it can't be",
      desc3:
        'unequipped again. Choose wisely! Binded buffs are only binded in COOP.',
      desc4: '<Must be Lv. 210+, max 3 binded buffs>',
      clan_transfer: '3',
    },
    item133: {
      id: '133',
      name: '|Buff| Slow Resistance',
      desc1: 'Unlocks Slow Resistance buff',
    },
    item134: {
      id: '134',
      name: '|Buff| Knife Recycle',
      desc1: 'Unlocks Knife Recycle buff',
    },
    item135: {
      id: '135',
      name: '|Buff| Fentanyl Injector',
      desc1: 'Unlocks Fentanyl Infector buff',
    },
    item136: {
      id: '136',
      name: '|Buff| Healing Pipebombs',
      desc1: 'Unlocks Healing Pipebombs buff',
    },
    item137: {
      id: '137',
      name: '|Buff| Advanced Morphine Injector',
      desc1: 'Unlocks Advanced Morphine Injector buff',
    },
    item138: {
      id: '138',
      name: '|Buff| Tank Duelist',
      desc1: 'Unlocks Tank Duelist buff',
    },
    item139: {
      id: '139',
      name: '|Buff| Fatal Guard',
      desc1: 'Unlocks Fatal Guard buff',
    },
    item140: {
      id: '140',
      name: '|Buff| Bullet Scavenger',
      desc1: 'Unlocks Bullet Scavenger buff',
    },
    item141: {
      id: '141',
      name: '|Buff| Berserk',
      desc1: 'Unlocks Berserk buff',
    },
    item142: {
      id: '142',
      name: '|Buff| Focused',
      desc1: 'Unlocks Focused buff',
    },
    item143: {
      id: '143',
      name: '|Buff| Tank Party Crasher',
      desc1: 'Unlocks Tank Party Crasher buff',
    },
    item144: {
      id: '144',
      name: '|Buff| Weak Point Spotter',
      desc1: 'Unlocks Weak Point Spotter buff',
    },
    item145: {
      id: '145',
      name: '|Buff| Enhanced Anger Ignition',
      desc1: 'Unlocks Enhanced Anger Ignition buff',
    },
    item146: {
      id: '146',
      name: '|<| Duplication Machine Part',
      desc1: '<Rare Crafting Material>',
      desc2:
        'A part of a duplication machine, which can be used to increase the quantity of',
      desc3:
        'any rare crafting material not purchasable from the FR-Pills Market by one.',
      desc4: '3 parts are required for a full machine.',
      category: '1',
      black_market_chance: '30',
      black_market_category: '3',
      black_market_max_count: '1',
      black_market_cash_cost: '70385',
      black_market_pill_cost: '385',
      black_market_blood_cost: '3',
    },
    item147: {
      id: '147',
      name: '|<| Vinegar',
      desc1: '<Crafting Material>',
      desc2:
        'A bottle of long-since expired vinegar. Not recommended for consumption,',
      desc3: 'but it may possess other uses.',
      show_in_pill_market: '1',
      cost: '250',
      category: '1',
      clan_transfer: '1',
      model: 'models/props_interiors/bottles_shelf_break07.mdl',
      spawn_on_map: '1',
      chance: '30',
      limit: '1',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '4525',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '905',
    },
    item148: {
      id: '148',
      name: '|<| Torn Love Ticket',
      desc1: '<Rare Crafting Material>',
      desc2:
        'A torn piece of a ticket. Gather 3 of them then go to Item Crafting',
      desc3: 'in the Workshop for an attempt to get a full ticket.',
      category: '1',
    },
    item149: {
      id: '149',
      name: '|<| Torn Helms Deep Ticket',
      desc1: '<Rare Crafting Material>',
      desc2:
        'A torn piece of a ticket. Gather 3 of them then go to Item Crafting',
      desc3: 'in the Workshop for an attempt to get a full ticket.',
      category: '1',
    },
    item150: {
      id: '150',
      name: 'Love Ticket',
      desc1: 'Use this to attempt to complete the Heartbroken scenario',
      desc2: 'and obtain a Cupid Token!',
      desc3: '<Enter scenario server and type !sn to host>',
      desc4: "<Requires 'Heartbroken' quest>",
      show_in_versus_market: '1',
      cost: '75',
    },
    item151: {
      id: '151',
      name: '|<| Torn Toxic Ticket',
      desc1: '<Rare Crafting Material>',
      desc2:
        'A torn piece of a ticket. Gather 3 of them then go to Item Crafting',
      desc3: 'in the Workshop for an attempt to get a full ticket.',
      category: '1',
    },
    item152: {
      id: '152',
      name: 'Toxic Ticket',
      desc1:
        'Use this ticket to attempt to complete the Missing Squad scenario.',
      desc2: '<Enter scenario server and type !sn to host>',
      desc3: "<Requires 'Missing Squad' quest>",
      show_in_versus_market: '1',
      cost: '75',
    },
    item153: {
      id: '153',
      name: 'Duplication Machine',
      desc1:
        'Using this item will increase the quantity of any rare crafting material',
      desc2: 'not purchasable from the FR-Pills Market you own by one.',
    },
    item154: {
      id: '154',
      name: 'Prison Key',
      category: '3',
      clan_transfer: '1',
      desc1: "A shiny key that allows you to start the 'Prison' scenario.",
      desc2: 'Talk to Erika to take the daily quest.',
      desc3: '<Must be in clan inventory to be usable>',
    },
    item155: {
      id: '155',
      name: 'Helicopter Fuel',
      category: '3',
      clan_transfer: '1',
      desc1:
        "A canister of fuel that allows you to start the 'Cleanse' scenario.",
      desc2: 'Talk to Erika to take the daily quest.',
      desc3: '<Must be in clan inventory to be usable>',
    },
    item156: {
      id: '156',
      name: 'Stick of Dynamite',
      category: '3',
      clan_transfer: '1',
      desc1:
        "A Stick of Dynamite that allows you to start the 'Special Cargo' scenario.",
      desc2: 'Talk to Erika to take the daily quest.',
      desc3: '<Must be in clan inventory to be usable>',
    },
    item157: {
      id: '157',
      name: 'Transmitter Remote',
      category: '3',
      clan_transfer: '1',
      desc1: "A Remote that allows you to start the 'Awakening' scenario.",
      desc2: 'Talk to Erika to take the daily quest.',
      desc3: '<Must be in clan inventory to be usable>',
    },
    item158: {
      id: '158',
      name: '|<| Leather Straps',
      desc1: '<Crafting Material>',
      desc2: 'Commonly used in belts or watches, leather straps can also be',
      desc3: 'used to quickly and efficiently bond several materials together.',
      model: 'models/gibs/wood_gib01b.mdl',
      category: '1',
      show_in_pill_market: '1',
      cost: '250',
      spawn_on_map: '1',
      chance: '40',
      limit: '2',
      clan_transfer: '1',
      offset: '0.0 0.0 2.0',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '3600',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '720',
    },
    item159: {
      id: '159',
      name: '|<| Surgical Equipment',
      desc1: '<Crafting Material>',
      desc2:
        'Equipment typically used to perform a range of surgeries, from trivial',
      desc3:
        'to life-threatening. Whilst it appears to be used, intense cleaning with',
      desc4: 'something acidic should sterilize it and make it safe for use.',
      model: 'models/props_equipment/surgicaltray_01.mdl',
      category: '1',
      show_in_pill_market: '1',
      cost: '250',
      spawn_on_map: '1',
      chance: '25',
      limit: '1',
      clan_transfer: '1',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '4575',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '915',
    },
    item160: {
      id: '160',
      name: '|<| Steroid Injector',
      desc1: '<Crafting Material>',
      desc2:
        "An injector used to supplement steroids into the user's bloodstream. Can",
      desc3:
        "increase the user's strength slightly but exceesive use can lead to addiction",
      desc4: 'or severe side effects.',
      model: 'models/w_models/weapons/w_eq_adrenaline.mdl',
      category: '1',
      show_in_pill_market: '1',
      cost: '250',
      spawn_on_map: '1',
      chance: '30',
      limit: '1',
      clan_transfer: '1',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '4350',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '870',
    },
    item161: {
      id: '161',
      name: '|<| Custom Weapon Parts',
      desc1: '<Crafting Material>',
      desc2:
        'Black Market parts used to customise weapons. Typically sold at a high price',
      desc3:
        'due to the difficulty in obtaining and distributing. Illegal in most states.',
      model: 'models/props_junk/cardboard_box07.mdl',
      category: '1',
      show_in_pill_market: '1',
      cost: '250',
      offset: '0.0 0.0 5.0',
      spawn_on_map: '1',
      chance: '30',
      limit: '1',
      clan_transfer: '1',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '4450',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '890',
    },
    item162: {
      id: '162',
      name: '|<| Heavy Duty Power Pack',
      desc1: '<Crafting Material>',
      desc2:
        'A mobile Power Pack designed to temporarily power an appliance without the',
      desc3:
        'need of a direct mains feed. It looks fully charged and in good condition.',
      model: 'models/props_lighting/light_battery_rigged_01.mdl',
      category: '1',
      show_in_pill_market: '1',
      cost: '250',
      clan_transfer: '1',
      spawn_on_map: '1',
      chance: '25',
      limit: '1',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '4625',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '925',
    },
    item163: {
      id: '163',
      name: '|<| Depressurized Fire Extinguisher',
      desc1: '<Crafting Material>',
      desc2:
        'A general purpose Fire Extinguisher, used to put out small to medium sized',
      desc3:
        'fires. It appears to have been damaged by gun fire, rupturing the inner',
      desc4:
        'canister, but the remaining contents can still be used once extracted.',
      model: 'models/props/cs_office/fire_extinguisher.mdl',
      category: '1',
      show_in_pill_market: '1',
      cost: '250',
      clan_transfer: '1',
      spawn_on_map: '1',
      chance: '30',
      limit: '1',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '4500',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '900',
    },
    item164: {
      id: '164',
      name: 'Standard Pile of Money',
      desc1: 'A small pile of money. Grants $100 on use.',
      model: 'models/props_collectables/money_wad.mdl',
      spawn_on_map: '1',
      chance: '80',
      limit: '5',
      allow_use_all: '1',
    },
    item165: {
      id: '165',
      name: 'Valuable Pile of Money',
      desc1: 'A small pile of more valuable money. Grants $1000 on use.',
      model: 'models/props_collectables/money_wad.mdl',
      spawn_on_map: '1',
      chance: '50',
      limit: '3',
      allow_use_all: '1',
      candy_type: '5',
      candy_cost: '2',
    },
    item166: {
      id: '166',
      name: 'Huge Pile of Money',
      desc1: 'A large pile of money. Grants $10000 on use.',
      model: 'models/props_waterfront/money_pile.mdl',
      spawn_on_map: '1',
      chance: '20',
      limit: '1',
      allow_use_all: '1',
    },
    item167: {
      id: '167',
      name: '|Buff| One Man Armory',
      desc1: 'Unlocks the One Man Armory buff',
    },
    item168: {
      id: '168',
      name: '|Buff| Discharger',
      desc1: 'Unlocks the Discharger buff',
    },
    item169: {
      id: '169',
      name: '|Buff| Battle Cry',
      desc1: 'Unlocks the Battle Cry buff',
    },
    item170: {
      id: '170',
      name: '|Buff| Spin-up M60',
      desc1: 'Unlocks the Spin-up M60 buff',
    },
    item171: {
      id: '171',
      name: '|Buff| Rocket Launcher',
      desc1: 'Unlocks the Rocket Launcher buff',
    },
    item172: {
      id: '172',
      name: 'Reinforced Crowbar',
      desc1:
        "A heavily reinforced crowbar that allows you to start the 'Trainwreck' scenario.",
      desc2: 'Talk to The Gunsmith to take the daily quest.',
      desc3: '<Enter scenario server and type !sn to host>',
    },
    item173: {
      id: '173',
      name: '|<| Mechanical Parts',
      desc1: '<Rare Crafting Material>',
      desc2:
        "An assortment of Mechanical Parts used by the Ironclad Tank for it's abilities.",
      desc3:
        'Can be carefully disassembled and reassembled into something useful.',
      category: '1',
      clan_transfer: '1',
      black_market_chance: '10',
      black_market_category: '3',
      black_market_max_count: '1',
      black_market_cash_cost: '183590',
      black_market_pill_cost: '850',
      black_market_blood_cost: '7',
      black_market_sell_value: '100000',
    },
    item174: {
      id: '174',
      name: 'Data Drive',
      desc1: "A Data Drive that allows you to start the 'Summit' scenario.",
      desc2: 'Talk to Ethan to take the daily quest.',
      desc3: '<Enter scenario server and type !sn to host>',
    },
    item175: {
      id: '175',
      name: '|<| Tempered Rock',
      desc1: '<Rare Crafting Material>',
      desc2:
        'An extremely tough chunk of rock that was a part of the Craggy Tank.',
      desc3:
        'Can be ground into a fine powder to reinforce other structures or items.',
      category: '1',
      clan_transfer: '1',
      black_market_chance: '10',
      black_market_category: '3',
      black_market_max_count: '1',
      black_market_cash_cost: '183590',
      black_market_pill_cost: '850',
      black_market_blood_cost: '7',
      black_market_sell_value: '100000',
    },
    item176: {
      id: '176',
      name: '|<| Volatile Tank Blood',
      desc1: '<Rare Crafting Material>',
      desc2:
        'A sample of blood from a powerful Tank. The blood itself is super-heated',
      desc3:
        'which is what enables these Tanks to perform their unique abilities.',
      desc4:
        'Caution should be had to avoid contact with flesh as it will cause severe burns.',
      desc5: '<Max count: 500>',
      category: '1',
      clan_transfer: '1',
      max_count: '500',
    },
    item177: {
      id: '177',
      name: '|Buff| Prowess',
      desc1: 'Unlocks Prowess buff',
    },
    item178: {
      id: '178',
      name: 'Precious Box',
      desc1:
        'A box containing one of the following items: Mechanical Parts, Tempered Rock, Broken Flesh',
      desc2:
        'Prison Bar, Sealed Dark Toxic Gas, Razorblade Katana or Expired Flesh Eating Parasite.',
      model: 'models/items/l4d_gift.mdl',
      spawn_on_map: '1',
      chance: '1',
      limit: '1',
      announce_on_grab: '1',
      allow_use_all: '1',
      traveller_token_cost: '10',
      black_market_chance: '25',
      black_market_category: '3',
      black_market_max_count: '1',
      black_market_cash_cost: '101150',
      black_market_pill_cost: '490',
      black_market_blood_cost: '4',
    },
    item179: {
      id: '179',
      name: 'Honor Coupon',
      desc1: 'Use to give yourself 1 Honor Point.',
      desc2: 'Honor Points are typically earned by playing VS.',
      desc3: 'You can spend them in the Honor Market in the',
      desc4: 'Competitive sub-section of the Control Panel.',
      allow_use_all: '1',
      candy_type: '5',
      candy_cost: '10',
      black_market_chance: '45',
      black_market_category: '1',
      black_market_max_count: '1',
      black_market_cash_cost: '53870',
      black_market_pill_cost: '325',
      black_market_blood_cost: '2',
      black_market_sell_value: '30000',
    },
    item180: {
      id: '180',
      name: '|~| Mint Candy',
      desc1: 'Candy for the halloween event',
    },
    item181: {
      id: '181',
      name: '|~| Caramel Candy',
      desc1: 'Candy for the halloween event',
    },
    item182: {
      id: '182',
      name: '|~| Fruity Candy',
      desc1: 'Candy for the halloween event',
    },
    item183: {
      id: '183',
      name: '|~| Sour Candy',
      desc1: 'Candy for the halloween event',
    },
    item184: {
      id: '184',
      name: '|~| Large bag of candy',
      desc1: 'A bag containing 30 random candy',
      spawn_on_map: '0',
      chance: '3',
      limit: '1',
      allow_use_all: '1',
      announce_on_grab: '1',
      model: 'models/props_swamp/parachute_bag01.mdl',
      angles: '-90.0 0.0 0.0',
    },
    item185: {
      id: '185',
      name: '|~| Medium bag of candy',
      desc1: 'A bag containing 10 random candy',
      spawn_on_map: '0',
      chance: '15',
      limit: '2',
      allow_use_all: '1',
      model: 'models/props_swamp/parachute_bag01.mdl',
      angles: '-90.0 0.0 0.0',
    },
    item186: {
      id: '186',
      name: '|~| Small bag of candy',
      desc1: 'A bag containing 3 random candy',
      spawn_on_map: '0',
      chance: '35',
      limit: '3',
      allow_use_all: '1',
      model: 'models/props_swamp/parachute_bag01.mdl',
      angles: '-90.0 0.0 0.0',
    },
    item187: {
      id: '187',
      name: 'Festival Box',
      desc1: 'Happy new year!',
    },
    item188: {
      id: '188',
      name: '|BENEFIT| Nimble',
      desc1: "Unlocks the 'Nimble' benefit for your clan.",
      desc2: '<Must be part of a clan to use>',
      category: '3',
    },
    item189: {
      id: '189',
      name: '|BENEFIT| Panic Toss',
      desc1: "Unlocks the 'Panic Toss' benefit for your clan.",
      desc2: '<Must be part of a clan to use>',
      category: '3',
    },
    item190: {
      id: '190',
      name: '|BENEFIT| Reactive Revival',
      desc1: "Unlocks the 'Reactive Revival' benefit for your clan.",
      desc2: '<Must be part of a clan to use>',
      category: '3',
    },
    item191: {
      id: '191',
      name: '|Buff| Ammo Dispenser',
      desc1: 'Unlocks Ammo Dispenser buff',
    },
    item192: {
      id: '192',
      name: '|Buff| Zapping Gun',
      desc1: 'Unlocks Zapping Gun buff',
    },
    item193: {
      id: '193',
      name: '|Buff| Helping Drone',
      desc1: 'Unlocks Helping Drone buff',
    },
    item194: {
      id: '194',
      name: '|BENEFIT| Smart Loader',
      desc1: "Unlocks the 'Smart Loader' benefit for your clan.",
      desc2: '<Must be part of a clan to use>',
      category: '3',
    },
    item195: {
      id: '195',
      name: 'Super Precious Box',
      desc1:
        'A box containing two of the following items: Mechanical Parts, Tempered Rock, Broken Flesh',
      desc2:
        'Prison Bar, Sealed Dark Toxic Gas, Razorblade Katana or Expired Flesh Eating Parasite.',
      model: 'models/items/l4d_gift.mdl',
      black_market_chance: '-5',
      black_market_category: '3',
      black_market_max_count: '1',
      black_market_cash_cost: '236810',
      black_market_pill_cost: '1020',
      black_market_blood_cost: '9',
    },
    item196: {
      id: '196',
      name: '???',
      desc1: '???',
      hidden: '1',
    },
    item197: {
      id: '197',
      name: '???',
      desc1: '???',
      hidden: '1',
    },
    item198: {
      id: '198',
      name: '???',
      desc1: '???',
      hidden: '1',
    },
    item199: {
      id: '199',
      name: 'Buff Enhancement Remover',
      desc1: "Removes a buff's enhancement",
      desc2: '<Non-Event enhancements return nothing on removal>',
      clan_transfer: '2',
    },
    item200: {
      id: '200',
      name: 'Exp/Cash Booster (x2, 1 Day)',
      desc1: 'When activated, increases the exp and cash earned from awards',
      traveller_token_cost: '25',
    },
    item201: {
      id: '201',
      name: 'Exp/Cash Booster (x3, 1 Day)',
      desc1: 'When activated, increases the exp and cash earned from awards',
    },
    item202: {
      id: '202',
      name: '|+| Furious Ignition',
      desc1:
        "Activates the 'Furious Ignition' buff enhancement for Anger Ignition",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item203: {
      id: '203',
      name: '|+| Strong Arm',
      desc1: "Activates the 'Strong Arm' buff enhancement for Shoving Expert",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item204: {
      id: '204',
      name: '|+| Fire Boost',
      desc1: "Activates the 'Fire Boost' buff enhancement for On Fire",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item205: {
      id: '205',
      name: '|+| Pocket Magnum',
      desc1: "Activates the 'Pocket Magnum' buff enhancement for Hidden Magnum",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item206: {
      id: '206',
      name: '|+| Vented Grenade Launcher',
      desc1:
        "Activates the 'Vented Grenade Launcher' buff enhancement for Floral Grenade Launch",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item207: {
      id: '207',
      name: '|+| Special Defibrillator',
      desc1:
        "Activates the 'Special Defibrillator' buff enhancement for Special First Aid",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item208: {
      id: '208',
      name: '|+| General Dominator',
      desc1:
        "Activates the 'General Dominator' buff enhancement for Finale Dominator",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item209: {
      id: '209',
      name: '|+| Finale Overlord',
      desc1:
        "Activates the 'Finale Overlord' buff enhancement for Finale Dominator",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item210: {
      id: '210',
      name: '|+| Six Slot Launcher',
      desc1:
        "Activates the 'Six Slot Launcher' buff enhancement for Advanced Grenade Launcher",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item211: {
      id: '211',
      name: '|+| Supplementary Pills',
      desc1:
        "Activates the 'Supplementary Pills' buff enhancement for Enhanced Pills",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item212: {
      id: '212',
      name: '|+| Scholar',
      desc1: "Activates the 'Scholar' buff enhancement for Student",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item213: {
      id: '213',
      name: '|+| Zombie Shredder',
      desc1:
        "Activates the 'Zombie Shredder' buff enhancement for Zombie Eradicator",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item214: {
      id: '214',
      name: '|+| Phantom Rounds',
      desc1: "Activates the 'Phantom Rounds' buff enhancement for Dummy Rounds",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item215: {
      id: '215',
      name: '|+| Tank Resister',
      desc1: "Activates the 'Tank Resister' buff enhancement for Tank Duelist",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item216: {
      id: '216',
      name: '|+| Juggernaut',
      desc1: "Activates the 'Juggernaut' buff enhancement for Berserk",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item217: {
      id: '217',
      name: "|+| Doctor's Orders",
      desc1:
        "Activates the 'Doctor's Orders' buff enhancement for Prescription Medication",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item218: {
      id: '218',
      name: '|+| Cleansing Aura',
      desc1:
        "Activates the 'Cleansing Aura' buff enhancement for Cleansing Pills",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item219: {
      id: '219',
      name: '|+| Explosive Automata',
      desc1:
        "Activates the 'Explosive Automata' buff enhancement for Slaying Automata",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item220: {
      id: '220',
      name: '|+| Safety Bomb',
      desc1: "Activates the 'Safety Bomb' buff enhancement for Sticky Bomb",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item221: {
      id: '221',
      name: 'Mutated Fruit',
      desc1:
        'Not really being a fruit, it is called that as a reference to the fruit',
      desc2:
        'Adam and Eve ate. It greatly increases the power of the survivor, reducing',
      desc3: 'their humanity in the process',
      desc4: '<Instantly promotes you to the next level>',
      desc5: '<Reduces teamwork reputation to 0 if positive>',
      show_in_pill_market: '1',
      cost: '200000',
    },
    item222: {
      id: '222',
      name: '|<| Broken Flesh Prison Bar',
      desc1: '<Rare Crafting Material>',
      desc2:
        "A piece of solidified flesh from the Imprisoner Tank's Flesh Prison",
      desc3:
        'Extremely durable and capable of withstanding multiple blows before breaking.',
      category: '1',
      clan_transfer: '1',
      black_market_chance: '15',
      black_market_category: '3',
      black_market_max_count: '1',
      black_market_cash_cost: '159510',
      black_market_pill_cost: '615',
      black_market_blood_cost: '6',
      black_market_sell_value: '75000',
    },
    item223: {
      id: '223',
      name: '|<| Sealed Dark Toxic Gas',
      desc1: '<Rare Crafting Material>',
      desc2:
        "A sealed container of the Toxic Gas emitted by the Shadow Tank's lethal abilities.",
      desc3:
        'The gas is thick and can impair breathing, thus prolonged exposure is not recommended.',
      category: '1',
      clan_transfer: '1',
      black_market_chance: '15',
      black_market_category: '3',
      black_market_max_count: '1',
      black_market_cash_cost: '159510',
      black_market_pill_cost: '615',
      black_market_blood_cost: '6',
      black_market_sell_value: '75000',
    },
    item224: {
      id: '224',
      name: '|<| Razorblade Katana',
      desc1: '<Rare Crafting Material>',
      desc2:
        "A Katana blade from the Supreme Duelist Tank. Labelled 'Razorblade' due to its",
      desc3: 'sharpness, merely touching the edge can inflicting bleeding.',
      category: '1',
      clan_transfer: '1',
      black_market_chance: '15',
      black_market_category: '3',
      black_market_max_count: '1',
      black_market_cash_cost: '159510',
      black_market_pill_cost: '615',
      black_market_blood_cost: '6',
      black_market_sell_value: '75000',
    },
    item225: {
      id: '225',
      name: '|<| Expired Flesh Eating Parasite',
      desc1: '<Rare Crafting Material>',
      desc2: "The remains of the Awakened Brutal's Flesh eating parasite.",
      desc3: 'Even in death it continues to secrete an acidic solution.',
      category: '1',
      clan_transfer: '1',
      black_market_chance: '15',
      black_market_category: '3',
      black_market_max_count: '1',
      black_market_cash_cost: '159510',
      black_market_pill_cost: '615',
      black_market_blood_cost: '6',
      black_market_sell_value: '75000',
    },
    item226: {
      id: '226',
      name: '|<| Mutation Enzyme',
      desc1: '<Ultra Rare Crafting Material>',
      desc2:
        'A strange chemical with unknown potential. It radiates energy and',
      desc3:
        'reacts to just about anything. Utmost caution must be kept when handling.',
      category: '1',
    },
    item227: {
      id: '227',
      name: '|<| Processor',
      desc1: '<Crafting Material>',
      desc2:
        'The brains behind any machine. Most modern day processors are capable',
      desc3:
        'of executing mathematical calculations in micro seconds, but some are',
      desc4: 'more specialized to certain fields, such as AI or decryption.',
      model: 'models/props/cs_office/computer_caseb_p4a.mdl',
      category: '1',
      show_in_pill_market: '1',
      cost: '250',
      clan_transfer: '1',
      spawn_on_map: '1',
      chance: '30',
      limit: '1',
      angles: '0.0 0.0 180.0',
      offset: '5.0 3.0 18.0',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '4475',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '895',
    },
    item228: {
      id: '228',
      name: '|<| Motherboard',
      desc1: '<Crafting Material>',
      desc2:
        'The base housing unit of any computer, the motherboard holds and connects',
      desc3:
        'several computer components together to achieve interoperability. They',
      desc4:
        'exist in various sizes to accommodate for different uses or preferences.',
      model: 'models/props/cs_office/computer_caseb_p7a.mdl',
      category: '1',
      show_in_pill_market: '1',
      cost: '250',
      clan_transfer: '1',
      spawn_on_map: '1',
      chance: '25',
      limit: '1',
      angles: '0.0 0.0 -90.0',
      offset: '5.0 -7.0 3.0',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '4550',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '910',
    },
    item229: {
      id: '229',
      name: '|<| Wire',
      desc1: '<Crafting Material>',
      desc2:
        'A small piece of copper wiring, typically used to transmit electricity',
      desc3:
        'between 2 objects. This piece alone is quite small and thus requires',
      desc4: 'multiple segments to be useful.',
      category: '1',
      show_in_pill_market: '1',
      model: 'models/gibs/furniture_gibs/furnituretable001a_chunk06.mdl',
      cost: '250',
      clan_transfer: '1',
      spawn_on_map: '1',
      chance: '40',
      limit: '2',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '3575',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '715',
    },
    item230: {
      id: '230',
      name: '|<| Professional Defibrillator',
      desc1: '<Crafting Material>',
      desc2:
        'A defibrillator used by trained medical staff to save patients by',
      desc3:
        'stabilizing their heart rate. Untrained usage may result in the death',
      desc4: 'of the subject, and thus should not be used.',
      model: 'models/w_models/weapons/w_eq_defibrillator_no_paddles.mdl',
      category: '1',
      show_in_pill_market: '1',
      cost: '250',
      clan_transfer: '1',
      spawn_on_map: '1',
      chance: '25',
      limit: '1',
      black_market_chance: '30',
      black_market_category: '2',
      black_market_max_count: '1',
      black_market_cash_cost: '4650',
      black_market_pill_cost: '50',
      black_market_blood_cost: '0',
      black_market_sell_value: '930',
    },
    item231: {
      id: '231',
      name: '|+| Searing Flames',
      desc1:
        "Activates the 'Searing Flames' buff enhancement for Flaming Sword",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item232: {
      id: '232',
      name: '|+| Guardian Recharger',
      desc1:
        "Activates the 'Guardian Recharger' buff enhancement for Guardian Moustachio",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item233: {
      id: '233',
      name: '|+| Boosted Morphine',
      desc1:
        "Activates the 'Boosted Morphine' buff enhancement for Morphine Injector",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item234: {
      id: '234',
      name: '|+| Disposable Knife',
      desc1: "Activates the 'Disposable Knife' buff enhancement for Knife Bag",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item235: {
      id: '235',
      name: '|+| Shared Excitement',
      desc1:
        "Activates the 'Shared Excitement' buff enhancement for Excitement",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item236: {
      id: '236',
      name: '|+| Underbarrel Autoloader',
      desc1:
        "Activates the 'Underbarrel Autoloader' buff enhancement for Unbarrel Launcher",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item237: {
      id: '237',
      name: '|+| Lingering Shocks',
      desc1:
        "Activates the 'Lingering Shocks' buff enhancement for Healing Shocks",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item238: {
      id: '238',
      name: '|+| Selective Zap',
      desc1: "Activates the 'Selective Zap' buff enhancement for Zapping Gun",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item239: {
      id: '239',
      name: '|+| Loaded Zap',
      desc1: "Activates the 'Loaded Zap' buff enhancement for Zapping Gun",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item240: {
      id: '240',
      name: '|+| Medic Drone',
      desc1: "Activates the 'Medic Drone' buff enhancement for Helping Drone",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item241: {
      id: '241',
      name: '|+| Scavenger Drone',
      desc1:
        "Activates the 'Scavenger Drone' buff enhancement for Helping Drone",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item242: {
      id: '242',
      name: '|+| Medic Assassin',
      desc1:
        "Activates the 'Medic Assassin' buff enhancement for Assassin Drone",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item243: {
      id: '243',
      name: '|+| Scavenger Assassin',
      desc1:
        "Activates the 'Scavenger Assassin' buff enhancement for Assassin Drone",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item244: {
      id: '244',
      name: '|+| Chainsaw Maniac',
      desc1:
        "Activates the 'Chainsaw Maniac' buff enhancement for Chainsaw Dumbass",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '95',
      enh_id: '1',
      candy_type: '1',
      candy_cost: '25',
      summer_market_cost: '5',
      christmas_market_cost: '5',
    },
    item245: {
      id: '245',
      name: '|+| Trusted Consumer',
      desc1:
        "Activates the 'Trusted Consumer' buff enhancement for Hunting Reward",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '98',
      enh_id: '1',
      candy_type: '1',
      candy_cost: '100',
      summer_market_cost: '20',
      christmas_market_cost: '15',
    },
    item246: {
      id: '246',
      name: '|+| Samurai Training',
      desc1: "Activates the 'Samurai Training' buff enhancement for Ninja",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '109',
      enh_id: '1',
      candy_type: '3',
      candy_cost: '50',
      summer_market_cost: '10',
      christmas_market_cost: '10',
    },
    item247: {
      id: '247',
      name: '|+| Armor Piercing Bullets',
      desc1:
        "Activates the 'Armor Piercing Bullets' buff enhancement for Piercing Bullets",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '124',
      enh_id: '1',
      candy_type: '3',
      candy_cost: '100',
      summer_market_cost: '20',
      christmas_market_cost: '15',
    },
    item248: {
      id: '248',
      name: '|+| Slow Repellent',
      desc1:
        "Activates the 'Slow Repellent' buff enhancement for Slow Resistance",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '130',
      enh_id: '1',
      candy_type: '2',
      candy_cost: '50',
      summer_market_cost: '10',
      christmas_market_cost: '10',
    },
    item249: {
      id: '249',
      name: '|+| Vigor',
      desc1: "Activates the 'Vigor' buff enhancement for Tenacity",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '185',
      enh_id: '1',
      candy_type: '2',
      candy_cost: '125',
      summer_market_cost: '25',
      christmas_market_cost: '15',
    },
    item250: {
      id: '250',
      name: '|+| Adren-Infused Pipe Bombs',
      desc1:
        "Activates the 'Adren-Infused Pipe Bombs' buff enhancement for Healing Pipe Bombs",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '197',
      enh_id: '1',
      candy_type: '2',
      candy_cost: '25',
      summer_market_cost: '5',
      christmas_market_cost: '5',
    },
    item251: {
      id: '251',
      name: '|+| Witch Guard',
      desc1: "Activates the 'Witch Guard' buff enhancement for Fatal Guard",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '200',
      enh_id: '1',
      candy_type: '1',
      candy_cost: '50',
      summer_market_cost: '10',
      christmas_market_cost: '10',
    },
    item252: {
      id: '252',
      name: '|+| Ranged Perfectionist',
      desc1:
        "Activates the 'Ranged Perfectionist' buff enhancement for Ranged Expert",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '278',
      enh_id: '1',
      candy_type: '3',
      candy_cost: '50',
      summer_market_cost: '15',
      christmas_market_cost: '15',
    },
    item253: {
      id: '253',
      name: '|+| Focused Enrage',
      desc1:
        "Activates the 'Focused Enrage' buff enhancement for Enraging Waves",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '307',
      enh_id: '1',
      candy_type: '4',
      candy_cost: '75',
      summer_market_cost: '15',
      christmas_market_cost: '10',
    },
    item254: {
      id: '254',
      name: '|+| Out With A Bang',
      desc1:
        "Activates the 'Out With A Bang' buff enhancement for Dying Berserk",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '318',
      enh_id: '1',
      candy_type: '4',
      candy_cost: '100',
      summer_market_cost: '20',
      christmas_market_cost: '15',
    },
    item255: {
      id: '255',
      name: '|+| Special Bait',
      desc1: "Activates the 'Special Bait' buff enhancement for Death Bait",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item256: {
      id: '256',
      name: '|+| Throwing Knives',
      desc1: "Activates the 'Throwing Knives' buff enhancement for Knife",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item257: {
      id: '257',
      name: '|+| Reusable Traps',
      desc1: "Activates the 'Reusable Traps' buff enhancement for Grenade Trap",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item258: {
      id: '258',
      name: '|+| Gas Mask',
      desc1: "Activates the 'Gas Mask' buff enhancement for Goggles",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item259: {
      id: '259',
      name: '|+| Super Shove',
      desc1: "Activates the 'Super Shove' buff enhancement for Deadly Shove",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item260: {
      id: '260',
      name: '|+| Short Fuse',
      desc1: "Activates the 'Short Fuse' buff enhancement for Seeking Grenade",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item261: {
      id: '261',
      name: '|+| Armor Piercing Grenades',
      desc1:
        "Activates the 'Armor Piercing Grenades' buff enhancement for Extra Powder",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item262: {
      id: '262',
      name: '|+| Clinic',
      desc1: "Activates the 'Clinic' buff enhancement for Infirmary",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item263: {
      id: '263',
      name: '|+| Accelerated Payment',
      desc1:
        "Activates the 'Accelerated Payment' buff enhancement for Survival Payment",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item264: {
      id: '264',
      name: '|+| Multi Dispenser',
      desc1:
        "Activates the 'Multi Dispenser' buff enhancement for Health Dispenser",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item265: {
      id: '265',
      name: '|+| Fall Spike',
      desc1: "Activates the 'Fall Spike' buff enhancement for Counter Slap",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item266: {
      id: '266',
      name: '|+| Sustained Bounty',
      desc1:
        "Activates the 'Sustained Bounty' buff enhancement for Starting Bounty",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item267: {
      id: '267',
      name: '|+| Hasty Crafter',
      desc1: "Activates the 'Hasty Crafter' buff enhancement for Artisan",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item268: {
      id: '268',
      name: '|+| Adaptive Shell',
      desc1:
        "Activates the 'Adaptive Shell' buff enhancement for Compressed Shell",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item269: {
      id: '269',
      name: '|+| Wish on Sight',
      desc1: "Activates the 'Wish on Sight' buff enhancement for Hope on Sight",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item270: {
      id: '270',
      name: '|+| Respite',
      desc1: "Activates the 'Respite' buff enhancement for Avenger",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item271: {
      id: '271',
      name: '|+| Destiny Bond',
      desc1:
        "Activates the 'Destiny Bond' buff enhancement for Love is in the air",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item272: {
      id: '272',
      name: '|+| Hyper Burst',
      desc1: "Activates the 'Hyper Burst' buff enhancement for Strength Burst",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item273: {
      id: '273',
      name: '|+| Burning Soul',
      desc1: "Activates the 'Burning Soul' buff enhancement for Immolation",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item274: {
      id: '274',
      name: '|+| Bullet Hunter',
      desc1:
        "Activates the 'Bullet Hunter' buff enhancement for Bullet Scavenger",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item275: {
      id: '275',
      name: '|+| Hawkeye',
      desc1: "Activates the 'Hawkeye' buff enhancement for Focused",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item276: {
      id: '276',
      name: '|+| Responsive Spotter',
      desc1:
        "Activates the 'Responsive Spotter' buff enhancement for Weak Point Spotter",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item277: {
      id: '277',
      name: '|+| Unyielding',
      desc1: "Activates the 'Unyielding' buff enhancement for Unstoppable",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item278: {
      id: '278',
      name: '|+| Lightning Hands',
      desc1:
        "Activates the 'Lightning Hands' buff enhancement for Tactical Reload",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item279: {
      id: '279',
      name: '|+| Restorative Bag',
      desc1:
        "Activates the 'Restorative Bag' buff enhancement for Protective Bag",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item280: {
      id: '280',
      name: '|+| Pledge',
      desc1: "Activates the 'Pledge' buff enhancement for Promise",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item281: {
      id: '281',
      name: '|+| Inner Courage',
      desc1: "Activates the 'Inner Courage' buff enhancement for Courage",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item282: {
      id: '282',
      name: '|+| Inner Might',
      desc1: "Activates the 'Inner Might' buff enhancement for Hidden Strength",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item283: {
      id: '283',
      name: '|+| Frontloader',
      desc1: "Activates the 'Frontloader' buff enhancement for Sideloader",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item284: {
      id: '284',
      name: '|+| Emetic Evasion',
      desc1: "Activates the 'Emetic Evasion' buff enhancement for Emetic Agent",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item285: {
      id: '285',
      name: '|+| Steady Sight',
      desc1:
        "Activates the 'Steady Sight' buff enhancement for Down but Not Out",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item286: {
      id: '286',
      name: '|+| Posthumous Pact',
      desc1:
        "Activates the 'Posthumous Pact' buff enhancement for Non-Aggression Pact",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item287: {
      id: '287',
      name: '|+| Personal Pact',
      desc1:
        "Activates the 'Personal Pact' buff enhancement for Non-Aggression Pact",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item288: {
      id: '288',
      name: '|+| Auto-Defib',
      desc1: "Activates the 'Auto-Defib' buff enhancement for Auto-Medical",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item289: {
      id: '289',
      name: '|+| Stress Induced Adrenaline',
      desc1:
        "Activates the 'Stress Induced Adrenaline' buff enhancement for Adrenaline Run",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item290: {
      id: '290',
      name: '|+| Blood Transfusion',
      desc1:
        "Activates the 'Blood Transfusion' buff enhancement for Mindset Change",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item291: {
      id: '291',
      name: '|+| Moustachio Grace',
      desc1:
        "Activates the 'Moustachio Grace' buff enhancement for Super Moustachio Bless",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      clan_transfer: '3',
    },
    item292: {
      id: '292',
      name: '|+| Trainee',
      desc1: "Activates the 'Trainee' buff enhancement for Observer",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item293: {
      id: '293',
      name: '|+| Momentum Blast',
      desc1:
        "Activates the 'Momentum Blast' buff enhancement for Mobility Master",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item294: {
      id: '294',
      name: '|+| Barbed Wire',
      desc1: "Activates the 'Barbed Wire' buff enhancement for Deflection Suit",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item295: {
      id: '295',
      name: '|+| Deflection Spark',
      desc1:
        "Activates the 'Deflection Spark' buff enhancement for Deflection Suit",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item296: {
      id: '296',
      name: '|+| Frostcharger',
      desc1: "Activates the 'Frostcharger' buff enhancement for Discharger",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item297: {
      id: '297',
      name: '|+| Incendiary Shrapnel',
      desc1:
        "Activates the 'Incendiary Shrapnel' buff enhancement for Shrapnel Shot",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item298: {
      id: '298',
      name: '|+| Overcharge Arms',
      desc1: "Activates the 'Overcharge Arms' buff enhancement for Hyperactive",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item299: {
      id: '299',
      name: '|+| Overcharge Legs',
      desc1: "Activates the 'Overcharge Legs' buff enhancement for Hyperactive",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
    },
    item300: {
      id: '300',
      name: '|~| Candy Pot',
      desc1: 'When picked up, gives one Small bag of candy to the user.',
      desc2: 'One use per round per survivor, but every survivor can use it!',
      desc3: "<This item shouldn't exist in anyone's inventory>",
      spawn_on_map: '0',
      chance: '70',
      limit: '1',
      model: 'models/props_c17/metalpot001a.mdl',
      offset: '0.0 0.0 6.5',
    },
    item301: {
      id: '301',
      name: '|BENEFIT| Magnet Grip',
      desc1: "Unlocks the 'Magnet Grip' benefit for your clan.",
      desc2: '<Must be part of a clan to use>',
      category: '3',
    },
    item302: {
      id: '302',
      name: '|BENEFIT| Post Conflict Recuperation',
      desc1: "Unlocks the 'Post Conflict Recuperation' benefit for your clan.",
      desc2: '<Must be part of a clan to use>',
      category: '3',
    },
    item303: {
      id: '303',
      name: '|BENEFIT| Fallback',
      desc1: "Unlocks the 'Fallback' benefit for your clan.",
      desc2: '<Must be part of a clan to use>',
      category: '3',
    },
    item304: {
      id: '304',
      name: '|BENEFIT| Refund Policy',
      desc1: "Unlocks the 'Refund Policy' benefit for your clan.",
      desc2: '<Must be part of a clan to use>',
      category: '3',
    },
    item305: {
      id: '305',
      name: '|BENEFIT| Arm Musculature Expertise',
      desc1: "Unlocks the 'Arm Musculature Expertise' benefit for your clan.",
      desc2: '<Must be part of a clan to use>',
      category: '3',
    },
    item306: {
      id: '306',
      name: 'Volatile Injector',
      desc1:
        'Grants access to the Volatile buff slot. This slot begins inactive but after',
      desc2:
        'activating from the !buy menu in the Misc. section (Command: sm_vslot)',
      desc3:
        'for 25 bounty, the slot becomes active for 5 minutes but you will be Mutated',
      desc4:
        'for 10 seconds. This cannot be removed, shortened, prevented or transferred.',
      desc5: "Volatile slot time doesn't tick if dead or Mutated.",
      desc6:
        '<✖VS, Bounty cost/Mutation time lowers for each use in the round>',
    },
    item307: {
      id: '307',
      name: 'Advanced Volatile Injector',
      desc1:
        'Grants access to the Volatile buff slot. This slot begins inactive but after',
      desc2:
        'activating from the !buy menu in the Misc. section (Command: sm_vslot)',
      desc3:
        'for 24 bounty, the slot becomes active for 6 minutes but you will be Mutated',
      desc4:
        'for 9 seconds. This cannot be removed, shortened, prevented or transferred.',
      desc5: "Volatile slot time doesn't tick if dead or Mutated.",
      desc6:
        '<✖VS, Bounty cost/Mutation time lowers for each use in the round>',
    },
    item308: {
      id: '308',
      name: 'Refined Volatile Injector',
      desc1:
        'Grants access to the Volatile buff slot. This slot begins inactive but after',
      desc2:
        'activating from the !buy menu in the Misc. section (Command: sm_vslot)',
      desc3:
        'for 23 bounty, the slot becomes active for 7 minutes but you will be Mutated',
      desc4:
        'for 8 seconds. This cannot be removed, shortened, prevented or transferred.',
      desc5: "Volatile slot time doesn't tick if dead or Mutated.",
      desc6:
        '<✖VS, Bounty cost/Mutation time lowers for each use in the round>',
    },
    item309: {
      id: '309',
      name: 'Supreme Volatile Injector',
      desc1:
        'Grants access to the Volatile buff slot. This slot begins inactive but after',
      desc2:
        'activating from the !buy menu in the Misc. section (Command: sm_vslot)',
      desc3:
        'for 22 bounty, the slot becomes active for 8 minutes but you will be Mutated',
      desc4:
        'for 7 seconds. This cannot be removed, shortened, prevented or transferred.',
      desc5: "Volatile slot time doesn't tick if dead or Mutated.",
      desc6:
        '<✖VS, Bounty cost/Mutation time lowers for each use in the round>',
    },
    item310: {
      id: '310',
      name: 'Ultimate Volatile Injector',
      desc1:
        'Grants access to the Volatile buff slot. This slot begins inactive but after',
      desc2:
        'activating from the !buy menu in the Misc. section (Command: sm_vslot)',
      desc3:
        'for 20 bounty, the slot becomes active for 10 minutes but you will be Mutated',
      desc4:
        'for 5 seconds. This cannot be removed, shortened, prevented or transferred.',
      desc5: "Volatile slot time doesn't tick if dead or Mutated.",
      desc6:
        '<✖VS, Bounty cost/Mutation time lowers for each use in the round>',
    },
    item311: {
      id: '311',
      name: '|Buff| Beyond The Grave',
      desc1: 'Unlocks Beyond The Grave buff',
    },
    item312: {
      id: '312',
      name: '|Buff| Promise',
      desc1: 'Unlocks Promise buff',
    },
    item313: {
      id: '313',
      name: '|Buff| Courage',
      desc1: 'Unlocks Courage buff',
    },
    item314: {
      id: '314',
      name: '|Buff| Deep Cuts',
      desc1: 'Unlocks Deep Cuts buff',
    },
    item315: {
      id: '315',
      name: '|Buff| Blunt Force Trauma',
      desc1: 'Unlocks Blunt Force Trauma buff',
    },
    item316: {
      id: '316',
      name: '|Buff| Cleansing Pills',
      desc1: 'Unlocks Cleansing Pills buff',
    },
    item317: {
      id: '317',
      name: '|Buff| Dopamine-Infused Meds',
      desc1: 'Unlocks Dopamine-Infused Meds buff',
    },
    item318: {
      id: '318',
      name: '|Buff| Dying Berserk',
      desc1: 'Unlocks Dying Berserk buff',
    },
    item319: {
      id: '319',
      name: '|Buff| Slaying Automata',
      desc1: 'Unlocks Slaying Automata buff',
    },
    item320: {
      id: '320',
      name: '|Buff| Duelist',
      desc1: 'Unlocks Duelist buff',
    },
    item321: {
      id: '321',
      name: '|Buff| Hidden Strength',
      desc1: 'Unlocks Hidden Strength buff',
    },
    item322: {
      id: '322',
      name: '|Buff| Sticky Bomb',
      desc1: 'Unlocks Sticky Bomb buff',
    },
    item323: {
      id: '323',
      name: '|Buff| Free Spirit',
      desc1: 'Unlocks Free Spirit buff',
    },
    item324: {
      id: '324',
      name: '|Buff| Bodyguard',
      desc1: 'Unlocks Bodyguard buff',
    },
    item325: {
      id: '325',
      name: '|+| Eco Knife',
      desc1: "Activates the 'Eco Knife' buff enhancement for Knife Recycle",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '132',
      enh_id: '1',
      candy_type: '1',
      candy_cost: '150',
      summer_market_cost: '30',
      christmas_market_cost: '25',
      halloween_market_cost: '25',
    },
    item326: {
      id: '326',
      name: '|+| General Tank Mastery',
      desc1:
        "Activates the 'General Tank Mastery' buff enhancement for General Tank Handler",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '174',
      enh_id: '1',
      candy_type: '2',
      candy_cost: '125',
      summer_market_cost: '25',
      christmas_market_cost: '20',
      halloween_market_cost: '20',
    },
    item327: {
      id: '327',
      name: '|+| Deadshot',
      desc1: "Activates the 'Deadshot' buff enhancement for Deadeye",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '283',
      enh_id: '1',
      candy_type: '4',
      candy_cost: '150',
      summer_market_cost: '30',
      christmas_market_cost: '25',
      halloween_market_cost: '20',
    },
    item328: {
      id: '328',
      name: '|+| Gory Munitions',
      desc1: "Activates the 'Gory Munitions' buff enhancement for Gory Killer",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '297',
      enh_id: '1',
      candy_type: '4',
      candy_cost: '75',
      summer_market_cost: '15',
      christmas_market_cost: '10',
      halloween_market_cost: '10',
    },
    item329: {
      id: '329',
      name: '|+| Wild Spirit',
      desc1: "Activates the 'Wild Spirit' buff enhancement for Free Spirit",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '323',
      enh_id: '1',
      candy_type: '2',
      candy_cost: '75',
      summer_market_cost: '15',
      christmas_market_cost: '10',
      halloween_market_cost: '10',
    },
    item330: {
      id: '330',
      name: '|+| Safekeeper',
      desc1: "Activates the 'Safekeeper' buff enhancement for Bodyguard",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '324',
      enh_id: '1',
      candy_type: '3',
      candy_cost: '100',
      summer_market_cost: '20',
      christmas_market_cost: '15',
      halloween_market_cost: '15',
    },
    item331: {
      id: '331',
      name: '|+| Steadfast Sprint',
      desc1:
        "Activates the 'Steadfast Sprint' buff enhancement for Steadfast Aim",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '328',
      enh_id: '1',
      candy_type: '2',
      candy_cost: '100',
      summer_market_cost: '20',
      christmas_market_cost: '15',
      halloween_market_cost: '15',
    },
    item332: {
      id: '332',
      name: '|+| Pain Analeptic',
      desc1:
        "Activates the 'Pain Analeptic' buff enhancement for Pain Stimulant",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '329',
      enh_id: '1',
      candy_type: '1',
      candy_cost: '50',
      summer_market_cost: '10',
      christmas_market_cost: '10',
      halloween_market_cost: '10',
    },
    item333: {
      id: '333',
      name: '|+| Adrenaline Accelerator',
      desc1:
        "Activates the 'Adrenaline Accelerator' buff enhancement for Adrenaline Area Booster",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '334',
      enh_id: '1',
      candy_type: '3',
      candy_cost: '75',
      summer_market_cost: '15',
      christmas_market_cost: '10',
      halloween_market_cost: '10',
    },
    item334: {
      id: '334',
      name: '|+| Steam Burst',
      desc1: "Activates the 'Steam Burst' buff enhancement for Spin-up M60",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '338',
      enh_id: '1',
      candy_type: '4',
      candy_cost: '100',
      summer_market_cost: '20',
      christmas_market_cost: '15',
      halloween_market_cost: '15',
    },
    item335: {
      id: '335',
      name: '|+| Lock-On Module',
      desc1:
        "Activates the 'Lock-On Module' buff enhancement for Rocket Launcher",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '339',
      enh_id: '1',
      candy_type: '3',
      candy_cost: '125',
      summer_market_cost: '25',
      christmas_market_cost: '25',
      halloween_market_cost: '25',
    },
    item336: {
      id: '336',
      name: '|+| Pain Transfer',
      desc1:
        "Activates the 'Pain Transfer' buff enhancement for Pain Moderation",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '342',
      enh_id: '1',
      candy_type: '1',
      candy_cost: '75',
      summer_market_cost: '15',
      christmas_market_cost: '10',
      halloween_market_cost: '10',
    },
    item337: {
      id: '337',
      name: '|+| Selective Inoculation',
      desc1:
        "Activates the 'Selective Inoculation' buff enhancement for Reactive Inoculation",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '345',
      enh_id: '1',
      candy_type: '1',
      candy_cost: '125',
      summer_market_cost: '25',
      christmas_market_cost: '20',
      halloween_market_cost: '20',
    },
    item340: {
      id: '340',
      name: 'Summer Token',
      desc1: 'A token that validates your enthusiasm to celebrate the Summer',
      desc2: 'season. Exchange it for exciting rewards!',
    },
    item341: {
      id: '341',
      name: '|·| Bloody Mary',
      desc1: 'A very well prepared cocktail.',
      desc2: 'Instantly reduces your current ability cooldowns by 20%',
      desc3: 'and increases ability cooldown speed by 20%.',
      desc4: '<Lasts for the entire current round>',
      desc5: '<WARNING: Drinks expire on event end!>',
      summer_market_cost: '1',
    },
    item342: {
      id: '342',
      name: '|·| Manhattan',
      desc1: 'A very well prepared cocktail.',
      desc2: 'Increases your attack by 30%.',
      desc3: '<Lasts for the entire current round>',
      desc4: '<WARNING: Drinks expire on event end!>',
      summer_market_cost: '1',
    },
    item343: {
      id: '343',
      name: '|·| Margarita',
      desc1: 'A very well prepared cocktail.',
      desc2: 'Increases your defense by 30% and blocks 10 flat',
      desc3: 'damage from all infected.',
      desc4: '<Lasts for the entire current round>',
      desc5: '<WARNING: Drinks expire on event end!>',
      summer_market_cost: '1',
    },
    item344: {
      id: '344',
      name: '|·| Tequila',
      desc1: 'A very well prepared strong cocktail.',
      desc2: 'Assigns you to a random Survivor Class, bypassing restrictions.',
      desc3: 'The class is set to level 3 if the chosen class level is lower.',
      desc4: '<Lasts for the entire current round>',
      desc5: "<Can't earn class exp during the duration>",
      desc6: '<WARNING: Drinks expire on event end!>',
      summer_market_cost: '2',
    },
    item345: {
      id: '345',
      name: 'Summer Gift Box',
      desc1: 'Enjoy the rest of the season!',
      desc2: ' ',
      desc3: 'Opening gives:',
      desc4: '- Exp, Cash and FR-Pills',
      desc5: '- Summer cocktails',
      desc6: '- Random surprises!',
    },
    item346: {
      id: '346',
      name: 'Summer Initiation Kit',
      desc1: "Let's start to celebrate!",
      desc2: ' ',
      desc3: 'Gives you a nice amount of extra Summer Tokens',
      desc4: 'to get the cheerful vibe going.',
    },
    item350: {
      id: '350',
      name: '|+| Frost Field',
      desc1:
        "Activates the 'Frost Field' buff enhancement for Freezing Grenade",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '148',
      enh_id: '1',
      summer_market_cost: '10',
      christmas_market_cost: '10',
      halloween_market_cost: '25',
    },
    item351: {
      id: '351',
      name: '|+| Tank Annihilator',
      desc1:
        "Activates the 'Tank Annihilator' buff enhancement for Tank Slayer",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '168',
      enh_id: '1',
      summer_market_cost: '20',
      christmas_market_cost: '20',
      halloween_market_cost: '30',
    },
    item352: {
      id: '352',
      name: '|+| Grave Suspension',
      desc1:
        "Activates the 'Grave Suspension' buff enhancement for Beyond the Grave",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '173',
      enh_id: '1',
      summer_market_cost: '15',
      christmas_market_cost: '15',
      halloween_market_cost: '25',
    },
    item353: {
      id: '353',
      name: '|+| Grave Recuperation',
      desc1:
        "Activates the 'Grave Recuperation' buff enhancement for Beyond the Grave",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '173',
      enh_id: '2',
      summer_market_cost: '15',
      christmas_market_cost: '15',
      halloween_market_cost: '25',
    },
    item354: {
      id: '354',
      name: '|+| Reactive Injector',
      desc1:
        "Activates the 'Reactive Injector' buff enhancement for Advanced Morphine Injector",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '198',
      enh_id: '1',
      summer_market_cost: '10',
      christmas_market_cost: '10',
      halloween_market_cost: '10',
    },
    item355: {
      id: '355',
      name: '|+| Anti-Tank Order',
      desc1:
        "Activates the 'Anti-Tank Order' buff enhancement for Tank Party Crasher",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '205',
      enh_id: '1',
      summer_market_cost: '15',
      christmas_market_cost: '15',
      halloween_market_cost: '30',
    },
    item356: {
      id: '356',
      name: '|+| Freezing Bag',
      desc1: "Activates the 'Freezing Bag' buff enhancement for Protective Bag",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '298',
      enh_id: '2',
      summer_market_cost: '25',
      christmas_market_cost: '20',
      halloween_market_cost: '20',
    },
    item357: {
      id: '357',
      name: '|+| Dopamine Guard',
      desc1:
        "Activates the 'Dopamine Guard' buff enhancement for Dopamine-Infused Meds",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '317',
      enh_id: '1',
      summer_market_cost: '20',
      christmas_market_cost: '20',
      halloween_market_cost: '20',
    },
    item358: {
      id: '358',
      name: '|+| Dopamine Wish',
      desc1:
        "Activates the 'Dopamine Wish' buff enhancement for Dopamine-Infused Meds",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '317',
      enh_id: '2',
      summer_market_cost: '20',
      christmas_market_cost: '20',
      halloween_market_cost: '20',
    },
    item359: {
      id: '359',
      name: '|+| Battle Roar',
      desc1: "Activates the 'Battle Roar' buff enhancement for Battle Cry",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '337',
      enh_id: '1',
      summer_market_cost: '25',
      christmas_market_cost: '25',
      halloween_market_cost: '30',
    },
    item360: {
      id: '360',
      name: '|+| Smasher Flurry',
      desc1: "Activates the 'Smasher Flurry' buff enhancement for Smasher Pan",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '400',
      enh_id: '1',
      summer_market_cost: '30',
      christmas_market_cost: '25',
      halloween_market_cost: '30',
    },
    item361: {
      id: '361',
      name: '|+| Electrified Wall',
      desc1:
        "Activates the 'Electrified Wall' buff enhancement for Repulsion Wall",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '402',
      enh_id: '1',
      summer_market_cost: '20',
      christmas_market_cost: '15',
      halloween_market_cost: '15',
    },
    item362: {
      id: '362',
      name: 'Christmas Cookie',
      desc1: 'A Christmas cookie that tastes very... Christmassy.',
      desc2: 'Exchange it for exciting rewards!',
    },
    item363: {
      id: '363',
      name: 'Christmas Gift',
      desc1: 'Merry Christmas',
      desc2: ' ',
      desc3: 'Opening gives:',
      desc4: '- Exp, Cash and FR-Pills',
      desc5: '- Holiday Beverages',
      desc6: '- Random surprises!',
    },
    item364: {
      id: '364',
      name: '|·| Eggnog',
      desc1: 'A very well prepared beverage.',
      desc2: 'Instantly reduces your current ability cooldowns by 20%',
      desc3: 'and increases ability cooldown speed by 20%.',
      desc4: '<Lasts for the entire current round>',
      desc5: '<WARNING: Expires on event end!>',
      christmas_market_cost: '1',
    },
    item365: {
      id: '365',
      name: '|·| Hot Chocolate',
      desc1: 'A very well prepared beverage.',
      desc2: 'Increases your attack by 30%.',
      desc3: '<Lasts for the entire current round>',
      desc4: '<WARNING: Expires on event end!>',
      christmas_market_cost: '1',
    },
    item366: {
      id: '366',
      name: '|·| Brandy Alexander',
      desc1: 'A very well prepared beverage.',
      desc2: 'Increases your defense by 30% and blocks 10 flat',
      desc3: 'damage from all infected.',
      desc4: '<Lasts for the entire current round>',
      desc5: '<WARNING: Expires on event end!>',
      christmas_market_cost: '1',
    },
    item367: {
      id: '367',
      name: '|·| Mulled Wine',
      desc1: 'A very well prepared beverage.',
      desc2: 'Assigns you to a random Survivor Class, bypassing restrictions.',
      desc3: 'The class is set to level 3 if the chosen class level is lower.',
      desc4: '<Lasts for the entire current round>',
      desc5: "<Can't earn class exp during the duration>",
      desc6: '<WARNING: Expires on event end!>',
      christmas_market_cost: '2',
    },
    item368: {
      id: '368',
      name: '|~| Voucher Gift Box',
      desc1: 'When picked up, gives one Christmas Voucher to the user.',
      desc2: 'One use per round per survivor, but every survivor can use it!',
      desc3: "<This item shouldn't exist in anyone's inventory>",
      spawn_on_map: '0',
      chance: '70',
      limit: '1',
      model: 'models/props/cs_office/cardboard_box01.mdl',
    },
    item370: {
      id: '370',
      name: '|+| Uber Charger Pet',
      desc1:
        "Activates the 'Uber Charger Pet' buff enhancement for Charger Pet",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '129',
      enh_id: '1',
      christmas_market_cost: '25',
      halloween_market_cost: '30',
    },
    item371: {
      id: '371',
      name: '|+| Group Spirit',
      desc1: "Activates the 'Group Spirit' buff enhancement for Group Morale",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '149',
      enh_id: '1',
      christmas_market_cost: '20',
      halloween_market_cost: '20',
    },
    item372: {
      id: '372',
      name: '|+| Infected Guard',
      desc1:
        "Activates the 'Infected Guard' buff enhancement for Infected Swarm Protection",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '164',
      enh_id: '1',
      christmas_market_cost: '20',
      halloween_market_cost: '20',
    },
    item373: {
      id: '373',
      name: '|+| Fentanyl Booster',
      desc1:
        "Activates the 'Fentanyl Booster' buff enhancement for Fentanyl Injector",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '165',
      enh_id: '1',
      christmas_market_cost: '20',
      halloween_market_cost: '20',
    },
    item374: {
      id: '374',
      name: '|+| Heartbreak',
      desc1: "Activates the 'Heartbreak' buff enhancement for Cupid's Arrow",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '178',
      enh_id: '1',
      christmas_market_cost: '30',
      halloween_market_cost: '15',
    },
    item375: {
      id: '375',
      name: '|+| Glacial Wing',
      desc1: "Activates the 'Glacial Wing' buff enhancement for Bird Whistle",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '300',
      enh_id: '1',
      christmas_market_cost: '35',
      halloween_market_cost: '40',
    },
    item376: {
      id: '376',
      name: '|+| Sacrifical Acceleration',
      desc1:
        "Activates the 'Sacrifical Acceleration' buff enhancement for Sacrifice",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '344',
      enh_id: '1',
      christmas_market_cost: '30',
      halloween_market_cost: '30',
    },
    item377: {
      id: '377',
      name: '|+| Sacrifical Deceleration',
      desc1:
        "Activates the 'Sacrifical Deceleration' buff enhancement for Sacrifice",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '344',
      enh_id: '2',
      christmas_market_cost: '30',
      halloween_market_cost: '30',
    },
    item378: {
      id: '378',
      name: '|+| Cocktail Mix',
      desc1: "Activates the 'Cocktail Mix' buff enhancement for Cocktail Maker",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '412',
      enh_id: '1',
      christmas_market_cost: '35',
      halloween_market_cost: '40',
    },
    item379: {
      id: '379',
      name: '|E| Christmas Voucher',
      desc1: 'Use the voucher to purchase powerful event buffs,',
      desc2: 'enhancements and consumables',
    },
    item380: {
      id: '380',
      name: '|E| Christmas Gift Box',
      desc1: 'A large gift box that contains various rewards inside',
      desc2: '<Must be at least Level 100 to open>',
    },
    item381: {
      id: '381',
      name: '|·| Mint Candy',
      desc1: 'A delicious candy.',
      desc2: 'Instantly reduces your current ability cooldowns by 20%',
      desc3: 'and increases ability cooldown speed by 20%.',
      desc4: '<Lasts for the entire current round>',
      desc5: '<WARNING: Expires on event end!>',
    },
    item382: {
      id: '382',
      name: '|·| Sour Candy',
      desc1: 'A delicious candy.',
      desc2: 'Increases your defense by 30% and blocks 10 flat',
      desc3: 'damage from all infected.',
      desc4: '<Lasts for the entire current round>',
      desc5: '<WARNING: Expires on event end!>',
    },
    item383: {
      id: '383',
      name: '|·| Caramel Candy',
      desc1: 'A delicious candy.',
      desc2: 'Increases your attack by 30%.',
      desc3: '<Lasts for the entire current round>',
      desc4: '<WARNING: Expires on event end!>',
    },
    item384: {
      id: '384',
      name: '|·| Fruity Candy',
      desc1: 'A delicious candy.',
      desc2: 'Assigns you to a random Survivor Class, bypassing restrictions.',
      desc3: 'The class is set to level 5.',
      desc4: '<Lasts for the entire current round>',
      desc5: "<Can't earn class exp during the duration>",
      desc6: '<WARNING: Expires on event end!>',
    },
    item385: {
      id: '385',
      name: '|·| Chocolate Candy',
      desc1: 'A delicious candy.',
      desc2: 'Can be used to change to a chosen unlocked survivor',
      desc3: 'class, bypassing all restrictions.',
      desc4: '<WARNING: Expires on event end!>',
    },
    item386: {
      id: '386',
      name: '|·| Phantom Candy',
      desc1: 'A delicious candy.',
      desc2:
        'When used, the next time you would permanently die this round, you will instead',
      desc3:
        'resuscitate at the nearest standing grounded ally not near or taunting Tanks.',
      desc4: '<Survivor source permanent death effects are not blocked>',
      desc5: '<If no targets found, you will resuscitate in place>',
      desc6: '<WARNING: Expires on event end!>',
    },
    item387: {
      id: '387',
      name: '|·| Neon Candy',
      desc1: 'A delicious candy.',
      desc2:
        'When used, for the rest of the round, you deal 200HP damage per second',
      desc3: 'to all infected near you.',
    },
    item388: {
      id: '388',
      name: '|·| Dark Matter Candy',
      desc1: 'A delicious candy.',
      desc2: 'Puts you in dying berserk mode for 1 minute. You also deal',
      desc3: '200% additional damage. After it ends, you die permanently.',
      desc4: '<Decays faster if last survivor alive>',
      desc5: '<Does not taunt tanks>',
    },
    item389: {
      id: '389',
      name: 'Tank Beacon (Tier 4)',
      desc1: 'Upon use, immediately spawns a random Tier 4 Tank',
      desc2: '<Can only be used on COOP servers>',
      desc3: '<Increases Tank Beacon stacks for this map by 4>',
      desc4: '<Cannot spawn more tanks than human players>',
      desc5: '<Cannot use if Tank Beacon stacks exceed 10>',
      category: '3',
    },
    item390: {
      id: '390',
      name: '|<| Summer Fragment',
      desc1: 'A material that allows you to craft a Summer Tank Bait',
      desc2: 'or Summer Tank Uber Bait',
      desc3: 'This item is lost each fortress finalization but 1/2/3 stacks',
      desc4: 'are given if a clan earns 250/1000/2500 community points.',
      desc5: '<Does not stack with fortress ownership bonus>',
    },
    item391: {
      id: '391',
      name: '|+| Phantom Guardian',
      desc1:
        "Activates the 'Phantom Guardian' buff enhancement for Guardian Dispenser",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '287',
      enh_id: '1',
      halloween_market_cost: '60',
    },
    item392: {
      id: '392',
      name: '|+| Spooky Teddy Bear',
      desc1:
        "Activates the 'Spooky Teddy Bear' buff enhancement for Mechanical Teddy Bear",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '407',
      enh_id: '1',
      halloween_market_cost: '60',
    },
    item393: {
      id: '393',
      name: '|+| Spooky Campfire',
      desc1: "Activates the 'Spooky Campfire' buff enhancement for Campfire",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '426',
      enh_id: '1',
      halloween_market_cost: '60',
    },
    item394: {
      id: '394',
      name: '|+| Blood Rite',
      desc1: "Activates the 'Blood Rite' buff enhancement for Nihilism",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '299',
      enh_id: '1',
      halloween_market_cost: '60',
    },
    item395: {
      id: '395',
      name: '|+| Compound Sharing',
      desc1:
        "Activates the 'Compound Sharing' buff enhancement for Sharing is Caring",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '418',
      enh_id: '1',
      halloween_market_cost: '60',
    },
    item396: {
      id: '396',
      name: '|+| Split Sharing',
      desc1:
        "Activates the 'Split Sharing' buff enhancement for Sharing is Caring",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '418',
      enh_id: '2',
      halloween_market_cost: '60',
    },
    item397: {
      id: '397',
      name: '|+| Organized Medic',
      desc1: "Activates the 'Organized Medic' buff enhancement for Field Medic",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '305',
      enh_id: '1',
      halloween_market_cost: '50',
    },
    item398: {
      id: '398',
      name: '|+| Bloodthirst',
      desc1: "Activates the 'Bloodthirst' buff enhancement for Butcher",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '163',
      enh_id: '1',
      halloween_market_cost: '50',
    },
    item399: {
      id: '399',
      name: '|+| Serrated Edge',
      desc1: "Activates the 'Serrated Edge' buff enhancement for Deep Cuts",
      desc2: '<Enhancements require the original buff to be owned>',
      desc3: "<Enhancements can't be replaced or removed without another item>",
      enh_buff_id: '314',
      enh_id: '1',
      halloween_market_cost: '60',
    },
    item400: {
      id: '391',
      name: 'Summer Tank Bait',
      desc1: 'Using this will immediately lure a summer tank in the server.',
      desc2: '<Can only be used in coop at ADVANCED difficulty>',
      desc3: '<Can only be used if no other tank is currently spawned>',
      desc4: '<Maybe not be used in some maps>',
      desc5:
        '<If the server crashes, contact an admin and they may refund your bait>',
    },
    item401: {
      id: '392',
      name: 'Summer Tank Uber Bait',
      desc1:
        'Using this will immediately lure the Supreme Summer tank in the server.',
      desc2: '<Can only be used in coop at EXPERTED difficulty>',
      desc3: '<Can only be used if no other tank is currently spawned>',
      desc4: '<Maybe not be used in some maps>',
      desc5:
        '<If the server crashes, contact an admin and they may refund your bait>',
    },
  },
};

export const exampleServerEvents = {
    "event_progression": {
        "summer": {
            "id": "1",
            "event_id": "11",
            "name": "Summer Event",
            "stages": {
                "stage1": {
                    "minimum_objectives_to_unlock": "1",
                    "objectives": {
                        "obj1": {
                            "target_event": "kill_special_infected",
                            "title": "Kill 15 special infected",
                            "count": "15"
                        },
                        "obj2": {
                            "target_event": "coop_chapter_completed",
                            "title": "Survive a coop round",
                            "count": "1"
                        },
                        "obj3": {
                            "target_event": "kill_tank",
                            "title": "Kill a tank",
                            "count": "1"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "345",
                            "amount": "1"
                        },
                        "item2": {
                            "item_id": "346",
                            "amount": "1"
                        }
                    }
                },
                "stage2": {
                    "minimum_objectives_to_unlock": "1",
                    "objectives": {
                        "obj1": {
                            "title": "Survive a coop round",
                            "count": "1",
                            "target_event": "coop_chapter_completed"
                        },
                        "obj2": {
                            "title": "Survive a finale",
                            "count": "1",
                            "target_event": "coop_finale_completed"
                        },
                        "obj3": {
                            "title": "Kill a tank",
                            "count": "1",
                            "target_event": "kill_tank"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "340",
                            "amount": "1"
                        }
                    }
                },
                "stage3": {
                    "minimum_objectives_to_unlock": "1",
                    "objectives": {
                        "obj1": {
                            "title": "Survive a coop round",
                            "count": "1",
                            "target_event": "coop_chapter_completed"
                        },
                        "obj2": {
                            "title": "Assist 20 allies",
                            "count": "20",
                            "target_event": "assist_ally"
                        },
                        "obj3": {
                            "title": "Kill 30 special infected",
                            "count": "30",
                            "target_event": "kill_special_infected"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "343",
                            "amount": "2"
                        },
                        "item2": {
                            "item_id": "340",
                            "amount": "1"
                        }
                    }
                },
                "stage4": {
                    "minimum_objectives_to_unlock": "1",
                    "objectives": {
                        "obj1": {
                            "title": "Heal 5 critical allies",
                            "count": "5",
                            "target_event": "coop_chapter_completed"
                        },
                        "obj2": {
                            "title": "Survive a finale",
                            "count": "1",
                            "target_event": "coop_finale_completed"
                        },
                        "obj3": {
                            "title": "Defeat 3 tanks",
                            "count": "3",
                            "target_event": "kill_tank"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "340",
                            "amount": "1"
                        },
                        "item2": {
                            "item_id": "342",
                            "amount": "2"
                        }
                    }
                },
                "stage5": {
                    "minimum_objectives_to_unlock": "1",
                    "objectives": {
                        "obj1": {
                            "title": "Survive 5 coop rounds",
                            "count": "5",
                            "target_event": "coop_chapter_completed"
                        },
                        "obj2": {
                            "title": "Survive 2 finales",
                            "count": "2",
                            "target_event": "coop_finale_completed"
                        },
                        "obj3": {
                            "title": "Kill a T2+ Tank",
                            "count": "1",
                            "target_event": "kill_t2_higher_tank"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "340",
                            "amount": "2"
                        },
                        "item2": {
                            "item_id": "345",
                            "amount": "1"
                        }
                    }
                },
                "stage6": {
                    "objectives": {
                        "obj1": {
                            "title": "Kill 5 tanks",
                            "count": "5",
                            "target_event": "kill_tank"
                        },
                        "obj2": {
                            "title": "Kill 2 T2+ Tank",
                            "count": "2",
                            "target_event": "kill_t2_higher_tank"
                        },
                        "obj3": {
                            "title": "Kill a T3 Tank",
                            "target_event": "kill_t3_higher_tank",
                            "count": "1"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "340",
                            "amount": "1"
                        },
                        "item2": {
                            "item_id": "101",
                            "amount": "1"
                        },
                        "item3": {
                            "item_id": "100",
                            "amount": "1"
                        }
                    },
                    "minimum_objectives_to_unlock": "1"
                },
                "stage7": {
                    "objectives": {
                        "obj1": {
                            "target_event": "assist_ally",
                            "title": "Assist 30 allies",
                            "count": "30"
                        },
                        "obj2": {
                            "title": "Heal 5 low health allies",
                            "target_event": "heal_critical_ally",
                            "count": "5"
                        },
                        "obj3": {
                            "title": "Kill 5 tanks",
                            "count": "5",
                            "target_event": "kill_tank"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "340",
                            "amount": "1"
                        },
                        "item2": {
                            "item_id": "20",
                            "amount": "1"
                        }
                    },
                    "minimum_objectives_to_unlock": "1"
                },
                "stage8": {
                    "objectives": {
                        "obj1": {
                            "target_event": "daily_quest_completed",
                            "title": "Complete a daily quest",
                            "count": "1"
                        },
                        "obj2": {
                            "title": "Play a scenario",
                            "target_event": "play_scenario",
                            "count": "1"
                        },
                        "obj3": {
                            "title": "Play a versus round",
                            "target_event": "play_versus_round",
                            "count": "1"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "340",
                            "amount": "1"
                        }
                    },
                    "minimum_objectives_to_unlock": "1"
                },
                "stage9": {
                    "objectives": {
                        "obj1": {
                            "target_event": "coop_finale_completed",
                            "title": "Survive 2 finales",
                            "count": "2"
                        },
                        "obj2": {
                            "title": "Complete 3 coop rounds",
                            "target_event": "coop_chapter_completed",
                            "count": "3"
                        },
                        "obj3": {
                            "title": "Kill a T3+ tank",
                            "target_event": "kill_t3_higher_tank",
                            "count": "1"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "340",
                            "amount": "1"
                        },
                        "item2": {
                            "item_id": "341",
                            "amount": "3"
                        },
                        "item3": {
                            "item_id": "342",
                            "amount": "2"
                        }
                    },
                    "minimum_objectives_to_unlock": "1"
                },
                "stage10": {
                    "objectives": {
                        "obj1": {
                            "title": "Win a scenario",
                            "target_event": "win_scenario",
                            "count": "1"
                        },
                        "obj2": {
                            "title": "Kill 10 tanks",
                            "target_event": "kill_tank",
                            "count": "10"
                        },
                        "obj3": {
                            "title": "Kill 2 T2+ tanks",
                            "target_event": "kill_t2_higher_tank",
                            "count": "2"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "345",
                            "amount": "1"
                        },
                        "item2": {
                            "item_id": "340",
                            "amount": "2"
                        },
                        "item3": {
                            "item_id": "344",
                            "amount": "1"
                        }
                    },
                    "minimum_objectives_to_unlock": "1"
                },
                "stage11": {
                    "objectives": {
                        "obj1": {
                            "title": "Kill 10 tanks",
                            "target_event": "kill_tank",
                            "count": "10"
                        },
                        "obj2": {
                            "title": "Kill 100 SI",
                            "target_event": "kill_special_infected",
                            "count": "100"
                        },
                        "obj3": {
                            "title": "Win a scenario",
                            "target_event": "play_scenario",
                            "count": "1"
                        }
                    },
                    "minimum_objectives_to_unlock": "1",
                    "item_rewards": {
                        "item1": {
                            "item_id": "340",
                            "amount": "1"
                        },
                        "item2": {
                            "item_id": "342",
                            "amount": "2"
                        }
                    }
                },
                "stage12": {
                    "item_rewards": {
                        "item1": {
                            "item_id": "340",
                            "amount": "1"
                        }
                    },
                    "objectives": {
                        "obj1": {}
                    }
                },
                "stage13": {
                    "objectives": {
                        "obj1": {}
                    }
                },
                "stage14": {
                    "uuid": "25a75755-3eb4-4bba-a8e4-684efcdbb27c",
                    "objectives": {
                        "obj1": {
                            "uuid": "346b49bc-374e-4877-8713-4e1d48295092"
                        }
                    }
                },
                "stage15": {
                    "uuid": "e87724a2-1681-48f7-9080-94545dfc6b2a",
                    "item_rewards": {
                        "item1": {
                            "uuid": "b2cc4d6b-ef6c-4050-be36-60abec429189",
                            "item_id": "345",
                            "amount": "1"
                        }
                    },
                    "objectives": {
                        "obj1": {
                            "uuid": "e3858684-beca-4193-93e5-9ad3eec9650b"
                        }
                    }
                },
                "stage16": {
                    "uuid": "eff0ee43-dfe5-455e-b07a-fdfcb842c71f",
                    "objectives": {
                        "obj1": {
                            "uuid": "7a82a573-96d6-4ba8-a759-9a087cd08bec"
                        }
                    }
                },
                "stage17": {
                    "uuid": "a0323c22-b8b6-41ea-955b-8763e35b1e91",
                    "objectives": {
                        "obj1": {
                            "uuid": "1504d9c3-4ccc-420d-8ea3-284074103f42"
                        }
                    }
                },
                "stage18": {
                    "uuid": "4628a9ed-09a2-4470-a0ac-49e7996745ce",
                    "objectives": {
                        "obj1": {
                            "uuid": "9417f979-3a7f-49ca-8cac-41323a22c1e7"
                        }
                    }
                },
                "stage19": {
                    "uuid": "19e6769b-07f3-4935-81e7-3fe2838df25d",
                    "objectives": {
                        "obj1": {
                            "uuid": "22d54f2f-ecb2-469f-a38c-44212c085026"
                        }
                    }
                },
                "stage20": {
                    "uuid": "8bad1aa1-8020-4a66-a724-ba1a9dd8f95a",
                    "objectives": {
                        "obj1": {
                            "uuid": "7ea58c09-26d8-463e-9e79-b30a6952cd49"
                        }
                    }
                },
                "stage21": {
                    "uuid": "99ad733d-c2e2-46f0-9d2f-d46054d149a1",
                    "objectives": {
                        "obj1": {
                            "uuid": "67b743ef-1c48-4e37-be0d-abb65a84fa9a"
                        }
                    }
                },
                "stage22": {
                    "uuid": "8292042f-8664-4923-bdfd-683245c82a88",
                    "objectives": {
                        "obj1": {
                            "uuid": "8bd63906-9fed-4e7e-9fe2-703417373e0d"
                        }
                    }
                },
                "stage23": {
                    "uuid": "9cf2629b-c248-4384-afee-3ac712894282",
                    "objectives": {
                        "obj1": {
                            "uuid": "55a7afe4-6a0c-4472-8573-bfb645d2cbaa"
                        }
                    }
                },
                "stage24": {
                    "uuid": "ae59ad4b-8384-4611-90bc-c04f8812d8c6",
                    "objectives": {
                        "obj1": {
                            "uuid": "80ddd5ad-40a3-410b-9932-a976201852e5"
                        }
                    }
                },
                "stage25": {
                    "uuid": "08e0df20-b19b-4654-8e6d-d69737261ffb"
                },
                "stage26": {
                    "uuid": "1450bd04-63d6-4e75-88b8-7fec6c8ef627"
                },
                "stage27": {
                    "uuid": "ac7722d7-068f-4875-95fe-dc5f213601f8"
                },
                "stage28": {
                    "uuid": "2ed82acb-e17a-453d-8072-1df7b84ab8e6"
                },
                "stage29": {
                    "uuid": "f4bc5c84-5629-40b5-93ac-df8c1b1f984b"
                },
                "stage30": {
                    "uuid": "9bb8a35c-2988-4440-a385-7ba5f7c5034d"
                },
                "stage31": {
                    "uuid": "5a72dde7-dac0-4948-9561-aeca3657737d"
                },
                "stage32": {
                    "uuid": "4511ceda-9063-4526-bbdd-bb072d8db268"
                },
                "stage33": {
                    "uuid": "a7602fb8-1823-4d37-a290-696349f00bb5"
                },
                "stage34": {
                    "uuid": "e9a710eb-cb90-42b0-aece-6db43fcad568"
                },
                "stage35": {
                    "uuid": "50785fc1-da53-4c5c-b3a9-bbcb7a0f53a8"
                },
                "stage36": {
                    "uuid": "68f85d36-d4d7-4300-aee3-d01f45353bd1"
                },
                "stage37": {
                    "uuid": "803a2370-be71-4c75-b965-6f3f2aea2556"
                },
                "stage38": {
                    "uuid": "c3567a49-3bae-472c-9872-f7aa29443cfd"
                },
                "stage39": {
                    "uuid": "9f2e49d1-00a5-4581-8174-de2e45ebf858"
                },
                "stage40": {
                    "uuid": "dc3f2a56-b5cf-4aa9-a3f5-2fd88741f5f2"
                },
                "stage41": {
                    "uuid": "4a970670-528c-4e22-b539-72c72500a8e6"
                },
                "stage42": {
                    "uuid": "d4c0f4e8-1309-4866-b1f5-a418d184a9d8"
                },
                "stage43": {
                    "uuid": "c9deb3d0-f509-4b50-aa59-286e45802f16"
                },
                "stage44": {
                    "uuid": "c1655dfd-a9e4-4b32-8fc0-2ebbb1e25564"
                },
                "stage45": {
                    "uuid": "689c70f6-56ca-4bcb-b952-996fffb0b7d5"
                },
                "stage46": {
                    "uuid": "7e20e188-540b-4c05-9bbc-7302be8789ff"
                },
                "stage47": {
                    "uuid": "84a6f43e-ec55-4298-8fc9-5ab158a0f356"
                },
                "stage48": {
                    "uuid": "7ae781df-9c86-4d7b-995e-1e537da7e8ea"
                },
                "stage49": {
                    "uuid": "4d740067-14e6-4943-9ae8-824a79bd3dae"
                },
                "stage50": {
                    "uuid": "81100914-9b14-4de8-a146-41da791012d1",
                    "minimum_objectives_to_unlock": "1",
                    "objectives": {},
                    "item_rewards": {
                        "item1": {
                            "uuid": "1ba87e2d-e8ff-4762-baae-99c043a5e5ed",
                            "item_id": "345",
                            "amount": "5"
                        }
                    }
                }
            }
        },
        "christmas": {
            "id": "2",
            "event_id": "12",
            "name": "Christmas Event",
            "stages": {
                "stage1": {
                    "minimum_objectives_to_unlock": "3",
                    "unlock_after_timestamp": "\"\"",
                    "objectives": {
                        "obj1": {
                            "target_event": "coop_chapter_completed",
                            "title": "Survive 1 COOP round",
                            "count": "1"
                        },
                        "obj2": {
                            "target_event": "coop_finale_completed",
                            "title": "Survive 1 COOP finale",
                            "count": "1"
                        },
                        "obj3": {
                            "target_event": "play_versus_round",
                            "title": "Complete a Versus round",
                            "count": "1"
                        },
                        "obj4": {
                            "target_event": "play_scenario",
                            "title": "Play a scenario",
                            "count": "1"
                        },
                        "obj5": {
                            "target_event": "kill_tank",
                            "title": "Defeat 2 tanks of any tier",
                            "count": "2"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "363",
                            "amount": "1"
                        },
                        "item2": {
                            "item_id": "362",
                            "amount": "10"
                        }
                    }
                },
                "stage2": {
                    "minimum_objectives_to_unlock": "3",
                    "unlock_after_timestamp": "\"\"",
                    "objectives": {
                        "obj1": {
                            "target_event": "coop_chapter_completed",
                            "title": "Survive 3 COOP rounds",
                            "count": "3"
                        },
                        "obj2": {
                            "target_event": "coop_finale_completed",
                            "title": "Survive a COOP finale",
                            "count": "1"
                        },
                        "obj3": {
                            "target_event": "play_versus_round",
                            "title": "Complete a Versus round",
                            "count": "1"
                        },
                        "obj4": {
                            "target_event": "play_scenario",
                            "title": "Play a scenario",
                            "count": "1"
                        },
                        "obj5": {
                            "target_event": "kill_t2_higher_tank",
                            "title": "Defeat a T2+ Tank",
                            "count": "1"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "363",
                            "amount": "1"
                        },
                        "item2": {
                            "item_id": "362",
                            "amount": "10"
                        }
                    }
                },
                "stage3": {
                    "minimum_objectives_to_unlock": "4",
                    "unlock_after_timestamp": "\"\"",
                    "objectives": {
                        "obj1": {
                            "target_event": "coop_chapter_completed",
                            "title": "Survive 5 COOP rounds",
                            "count": "5"
                        },
                        "obj2": {
                            "target_event": "coop_finale_completed",
                            "title": "Survive a COOP finale",
                            "count": "1"
                        },
                        "obj3": {
                            "target_event": "play_versus_round",
                            "title": "Complete 2 Versus rounds",
                            "count": "2"
                        },
                        "obj4": {
                            "target_event": "play_scenario",
                            "title": "Play a scenario",
                            "count": "1"
                        },
                        "obj5": {
                            "target_event": "kill_tank",
                            "title": "Defeat 5 tanks",
                            "count": "5"
                        },
                        "obj6": {
                            "target_event": "kill_t2_higher_tank",
                            "title": "Defeat a T2+ Tank",
                            "count": "1"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "363",
                            "amount": "1"
                        },
                        "item2": {
                            "item_id": "362",
                            "amount": "15"
                        }
                    }
                },
                "stage4": {
                    "minimum_objectives_to_unlock": "3",
                    "unlock_after_timestamp": "1608771267",
                    "objectives": {
                        "obj1": {
                            "target_event": "kill_t2_higher_tank",
                            "title": "Defeat 3 T2+ Tank",
                            "count": "3"
                        },
                        "obj2": {
                            "target_event": "kill_t3_higher_tank",
                            "title": "Defeat a T3+ Tank",
                            "count": "1"
                        },
                        "obj3": {
                            "target_event": "kill_special_infected_spree",
                            "title": "Get 10 Special Killing Sprees",
                            "count": "10"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "363",
                            "amount": "1"
                        },
                        "item2": {
                            "item_id": "362",
                            "amount": "15"
                        }
                    }
                },
                "stage5": {
                    "minimum_objectives_to_unlock": "3",
                    "unlock_after_timestamp": "\"\"",
                    "objectives": {
                        "obj1": {
                            "target_event": "coop_chapter_completed",
                            "title": "Survive 15 COOP rounds",
                            "count": "15"
                        },
                        "obj2": {
                            "target_event": "coop_finale_completed",
                            "title": "Survive 3 COOP finales",
                            "count": "3"
                        },
                        "obj3": {
                            "target_event": "play_versus_round",
                            "title": "Complete 2 Versus rounds",
                            "count": "2"
                        },
                        "obj4": {
                            "target_event": "play_scenario",
                            "title": "Play 2 scenarios",
                            "count": "2"
                        },
                        "obj5": {
                            "target_event": "daily_quest_completed",
                            "title": "Complete 6 Daily Quests",
                            "count": "6"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "363",
                            "amount": "1"
                        },
                        "item2": {
                            "item_id": "362",
                            "amount": "15"
                        }
                    }
                },
                "stage6": {
                    "minimum_objectives_to_unlock": "3",
                    "unlock_after_timestamp": "1608947709",
                    "objectives": {
                        "obj1": {
                            "target_event": "kill_special_infected_spree",
                            "title": "Get 15 Special Killing Sprees",
                            "count": "15"
                        },
                        "obj2": {
                            "target_event": "kill_tank",
                            "title": "Kill 20 tanks of any tier",
                            "count": "20"
                        },
                        "obj3": {
                            "target_event": "heal_critical_ally",
                            "title": "Heal 10 wounded allies",
                            "count": "10"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "363",
                            "amount": "1"
                        },
                        "item2": {
                            "item_id": "362",
                            "amount": "20"
                        }
                    }
                },
                "stage7": {
                    "minimum_objectives_to_unlock": "4",
                    "unlock_after_timestamp": "1609293309",
                    "objectives": {
                        "obj1": {
                            "target_event": "kill_special_infected_spree",
                            "title": "Get 20 Special Killing Sprees",
                            "count": "20"
                        },
                        "obj2": {
                            "target_event": "kill_tank",
                            "title": "Kill 25 tanks of any tier",
                            "count": "25"
                        },
                        "obj3": {
                            "target_event": "kill_uber_infected",
                            "title": "Kill 10 uber infected",
                            "count": "10"
                        },
                        "obj4": {
                            "target_event": "kill_t2_higher_tank",
                            "title": "Kill 10 T2+ tanks",
                            "count": "10"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "363",
                            "amount": "2"
                        },
                        "item2": {
                            "item_id": "362",
                            "amount": "30"
                        }
                    }
                },
                "stage8": {
                    "minimum_objectives_to_unlock": "6",
                    "unlock_after_timestamp": "1609466109",
                    "objectives": {
                        "obj1": {
                            "target_event": "kill_special_infected_spree",
                            "title": "Get 30 Special Killing Sprees",
                            "count": "30"
                        },
                        "obj2": {
                            "target_event": "kill_tank",
                            "title": "Kill 30 tanks of any tier",
                            "count": "30"
                        },
                        "obj3": {
                            "target_event": "kill_t2_higher_tank",
                            "title": "Kill 15 T2+ tanks",
                            "count": "15"
                        },
                        "obj4": {
                            "target_event": "kill_t3_higher_tank",
                            "title": "Kill 3 T3+ tanks",
                            "count": "3"
                        },
                        "obj5": {
                            "target_event": "heal_critical_ally",
                            "title": "Heal 20 wounded allies",
                            "count": "20"
                        },
                        "obj6": {
                            "target_event": "kill_uber_infected",
                            "title": "Kill 20 uber infected",
                            "count": "20"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "363",
                            "amount": "3"
                        },
                        "item2": {
                            "item_id": "362",
                            "amount": "40"
                        }
                    }
                }
            }
        },
        "halloween2021": {
            "id": "3",
            "event_id": "13",
            "name": "Christmas Event",
            "stages": {
                "stage1": {
                    "minimum_objectives_to_unlock": "3",
                    "objectives": {
                        "obj1": {
                            "target_event": "coop_chapter_completed",
                            "title": "Survive 1 COOP round",
                            "count": "1"
                        },
                        "obj2": {
                            "target_event": "coop_finale_completed",
                            "title": "Survive 1 COOP finale",
                            "count": "1"
                        },
                        "obj3": {
                            "target_event": "kill_tank",
                            "title": "Defeat 1 tank",
                            "count": "1"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "379",
                            "amount": "30"
                        },
                        "item2": {
                            "item_id": "387",
                            "amount": "1"
                        }
                    }
                },
                "stage2": {
                    "minimum_objectives_to_unlock": "3",
                    "objectives": {
                        "obj1": {
                            "target_event": "coop_chapter_completed",
                            "title": "Survive 3 COOP rounds",
                            "count": "3"
                        },
                        "obj2": {
                            "target_event": "coop_finale_completed",
                            "title": "Survive a COOP finale",
                            "count": "1"
                        },
                        "obj3": {
                            "target_event": "play_versus_round",
                            "title": "Complete a Versus round",
                            "count": "1"
                        },
                        "obj4": {
                            "target_event": "play_scenario",
                            "title": "Play a scenario",
                            "count": "1"
                        },
                        "obj5": {
                            "target_event": "kill_tank",
                            "title": "Defeat 3 tanks",
                            "count": "3"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "379",
                            "amount": "35"
                        },
                        "item2": {
                            "item_id": "386",
                            "amount": "1"
                        },
                        "item3": {
                            "item_id": "380",
                            "amount": "1"
                        }
                    }
                },
                "stage3": {
                    "minimum_objectives_to_unlock": "4",
                    "unlock_after_timestamp": "\"\"",
                    "objectives": {
                        "obj1": {
                            "target_event": "coop_chapter_completed",
                            "title": "Survive 5 COOP rounds",
                            "count": "5"
                        },
                        "obj2": {
                            "target_event": "coop_finale_completed",
                            "title": "Survive 2 COOP finales",
                            "count": "2"
                        },
                        "obj3": {
                            "target_event": "play_versus_round",
                            "title": "Complete 2 Versus rounds",
                            "count": "2"
                        },
                        "obj4": {
                            "target_event": "win_scenario",
                            "title": "Win 2 scenarios",
                            "count": "2"
                        },
                        "obj5": {
                            "target_event": "kill_tank",
                            "title": "Defeat 5 tanks",
                            "count": "5"
                        },
                        "obj6": {
                            "target_event": "kill_special_infected_spree",
                            "title": "Get 1 Special Killing Spree",
                            "count": "1"
                        },
                        "obj7": {
                            "target_event": "assist_ally",
                            "title": "Heal, revive or defib 1 time",
                            "count": "1"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "379",
                            "amount": "40"
                        },
                        "item2": {
                            "item_id": "388",
                            "amount": "1"
                        },
                        "item3": {
                            "item_id": "380",
                            "amount": "1"
                        }
                    }
                },
                "stage4": {
                    "minimum_objectives_to_unlock": "3",
                    "unlock_after_timestamp": "1640044800",
                    "objectives": {
                        "obj1": {
                            "target_event": "coop_finale_completed",
                            "title": "Survive 3 COOP finales",
                            "count": "3"
                        },
                        "obj2": {
                            "target_event": "kill_t2_higher_tank",
                            "title": "Defeat a T2+ tank",
                            "count": "1"
                        },
                        "obj3": {
                            "target_event": "daily_quest_completed",
                            "title": "Complete 3 daily quests",
                            "count": "3"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "379",
                            "amount": "45"
                        },
                        "item2": {
                            "item_id": "387",
                            "amount": "1"
                        },
                        "item3": {
                            "item_id": "380",
                            "amount": "1"
                        }
                    }
                },
                "stage5": {
                    "minimum_objectives_to_unlock": "4",
                    "unlock_after_timestamp": "\"\"",
                    "objectives": {
                        "obj1": {
                            "target_event": "kill_special_infected_spree",
                            "title": "Get 5 Special Killing Spree",
                            "count": "5"
                        },
                        "obj2": {
                            "target_event": "kill_t2_higher_tank",
                            "title": "Defeat 3 T2+ Tanks",
                            "count": "3"
                        },
                        "obj3": {
                            "target_event": "assist_ally",
                            "title": "Heal, revive or defib 10 times",
                            "count": "10"
                        },
                        "obj4": {
                            "target_event": "kill_tank",
                            "title": "Defeat 10 tanks",
                            "count": "10"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "379",
                            "amount": "50"
                        },
                        "item2": {
                            "item_id": "386",
                            "amount": "1"
                        },
                        "item3": {
                            "item_id": "380",
                            "amount": "1"
                        }
                    }
                },
                "stage6": {
                    "minimum_objectives_to_unlock": "3",
                    "unlock_after_timestamp": "\"\"",
                    "objectives": {
                        "obj1": {
                            "target_event": "coop_chapter_completed",
                            "title": "Survive 10 COOP rounds",
                            "count": "10"
                        },
                        "obj2": {
                            "target_event": "coop_finale_completed",
                            "title": "Survive 3 COOP finales",
                            "count": "3"
                        },
                        "obj3": {
                            "target_event": "play_versus_round",
                            "title": "Complete 3 Versus rounds",
                            "count": "3"
                        },
                        "obj4": {
                            "target_event": "win_scenario",
                            "title": "Win 3 scenarios",
                            "count": "3"
                        },
                        "obj5": {
                            "target_event": "daily_quest_completed",
                            "title": "Complete 6 Daily Quests",
                            "count": "6"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "379",
                            "amount": "55"
                        },
                        "item2": {
                            "item_id": "388",
                            "amount": "1"
                        },
                        "item3": {
                            "item_id": "380",
                            "amount": "1"
                        }
                    }
                },
                "stage7": {
                    "minimum_objectives_to_unlock": "4",
                    "unlock_after_timestamp": "1640217600",
                    "objectives": {
                        "obj1": {
                            "target_event": "kill_tank",
                            "title": "Defeat 30 tanks",
                            "count": "30"
                        },
                        "obj2": {
                            "target_event": "kill_t2_higher_tank",
                            "title": "Defeat 5 T2+ Tanks",
                            "count": "5"
                        },
                        "obj3": {
                            "target_event": "kill_t3_higher_tank",
                            "title": "Defeat a T3+ Tank",
                            "count": "1"
                        },
                        "obj4": {
                            "target_event": "kill_special_infected_spree",
                            "title": "Get 10 Special Killing Sprees",
                            "count": "10"
                        },
                        "obj5": {
                            "target_event": "kill_uber_infected",
                            "title": "Kill 10 Uber Infected",
                            "count": "10"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "379",
                            "amount": "60"
                        },
                        "item2": {
                            "item_id": "387",
                            "amount": "2"
                        },
                        "item3": {
                            "item_id": "380",
                            "amount": "2"
                        }
                    }
                },
                "stage8": {
                    "minimum_objectives_to_unlock": "4",
                    "unlock_after_timestamp": "\"\"",
                    "objectives": {
                        "obj1": {
                            "target_event": "coop_chapter_completed",
                            "title": "Survive 20 COOP rounds",
                            "count": "20"
                        },
                        "obj2": {
                            "target_event": "coop_finale_completed",
                            "title": "Survive 5 COOP finales",
                            "count": "5"
                        },
                        "obj3": {
                            "target_event": "daily_quest_completed",
                            "title": "Complete 10 Daily Quests",
                            "count": "10"
                        },
                        "obj4": {
                            "target_event": "assist_ally",
                            "title": "Heal, revive or defib 15 times",
                            "count": "15"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "379",
                            "amount": "65"
                        },
                        "item2": {
                            "item_id": "386",
                            "amount": "2"
                        },
                        "item3": {
                            "item_id": "380",
                            "amount": "2"
                        }
                    }
                },
                "stage9": {
                    "minimum_objectives_to_unlock": "4",
                    "unlock_after_timestamp": "1640390400",
                    "objectives": {
                        "obj1": {
                            "target_event": "coop_finale_completed",
                            "title": "Survive 10 COOP finales",
                            "count": "10"
                        },
                        "obj2": {
                            "target_event": "win_scenario",
                            "title": "Win 5 scenarios",
                            "count": "5"
                        },
                        "obj3": {
                            "target_event": "kill_t3_higher_tank",
                            "title": "Defeat 3 T3+ Tanks",
                            "count": "3"
                        },
                        "obj4": {
                            "target_event": "kill_uber_infected",
                            "title": "Kill 20 Uber Infected",
                            "count": "20"
                        },
                        "obj5": {
                            "target_event": "assist_ally",
                            "title": "Heal, revive or defib 20 times",
                            "count": "20"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "379",
                            "amount": "70"
                        },
                        "item2": {
                            "item_id": "388",
                            "amount": "2"
                        },
                        "item5": {
                            "item_id": "380",
                            "amount": "2"
                        },
                        "item3": {},
                        "item4": {}
                    }
                },
                "stage10": {
                    "minimum_objectives_to_unlock": "6",
                    "unlock_after_timestamp": "\"\"",
                    "objectives": {
                        "obj1": {
                            "target_event": "kill_special_infected_spree",
                            "title": "Get 25 Special Killing Sprees",
                            "count": "25"
                        },
                        "obj2": {
                            "target_event": "kill_t2_higher_tank",
                            "title": "Defeat 15 T2+ Tanks",
                            "count": "15"
                        },
                        "obj3": {
                            "target_event": "kill_t3_higher_tank",
                            "title": "Defeat 5 T3+ Tanks",
                            "count": "5"
                        },
                        "obj4": {
                            "target_event": "assist_ally",
                            "title": "Heal, revive or defib 30 times",
                            "count": "30"
                        },
                        "obj5": {
                            "target_event": "kill_tank",
                            "title": "Defeat 50 tanks",
                            "count": "50"
                        },
                        "obj6": {
                            "target_event": "kill_uber_infected",
                            "title": "Kill 30 Uber Infected",
                            "count": "30"
                        }
                    },
                    "item_rewards": {
                        "item1": {
                            "item_id": "379",
                            "amount": "75"
                        },
                        "item2": {
                            "item_id": "387",
                            "amount": "3"
                        },
                        "item3": {
                            "item_id": "386",
                            "amount": "3"
                        },
                        "item4": {
                            "item_id": "388",
                            "amount": "3"
                        },
                        "item5": {
                            "item_id": "380",
                            "amount": "3"
                        }
                    }
                }
            }
        }
    }
}