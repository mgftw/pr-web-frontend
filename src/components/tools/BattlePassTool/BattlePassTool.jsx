import React, { useState, useMemo } from "react";
import Select from "react-select";
import clone from "lodash/cloneDeep";
import { get, set, unset } from "lodash";
import { MdDelete } from "react-icons/md";
import { v4 as uuid } from "uuid";
import { objectToKv, parseKeyValues } from "$helpers/toolHelper";
import Input from "$components/Input";
import Button from "$components/Button";
import InventoryItem from "$components/InventoryItem";
import * as css from "./styles";

const eventTypes = [
  "assist_ally",
  "heal_critical_ally",
  "coop_finale_completed",
  "coop_chapter_completed",
  "daily_quest_completed",
  "play_scenario",
  "win_scenario",
  "kill_special_infected",
  "kill_special_infected_spree",
  "kill_tank",
  "kill_t2_higher_tank",
  "kill_t3_higher_tank",
  "kill_summer_tank",
  "kill_cherry_tank",
  "kill_shark_tank",
  "kill_decaying_tank",
  "kill_santa_tank",
  "kill_uber_infected",
  "headshot_kill_special",
  "gnome_carry_completed",
  "coop_mutation_completed",
  "melee_kill_special",
  "melee_kill_uber",
  "kill_hybrid_tank",
  "win_event_scenario",
  "token_box_pickup",
];

const eventTypesText = [
  "Assist {v} allies",
  "Heal {v} critical allies",
  "Survive {v} finales",
  "Survive {v} Coop rounds",
  "Complete {v} daily quests",
  "Play {v} scenarios",
  "Win {v} scenarios",
  "Kill {v} special infected",
  "Get {v} special killing sprees",
  "Kill {v} tanks",
  "Kill {v} T2+ tanks",
  "Kill {v} T3+ tanks",
  "Kill {v} Summer tanks",
  "Kill {v} Cherry tanks",
  "Kill {v} Shark tanks",
  "Kill {v} Decaying tanks",
  "Kill {v} Santa tanks",
  "Kill {v} uber infected",
  "Kill {v} specials with headshots",
  "Escort {v} gift gnomes",
  "Survive {v} coop maps with a mutation",
  "Melee kill {v} special infected",
  "Melee kill {v} uber infected",
  "Kill {v} Hybrid Tanks",
  "Win {v} event scenarios",
  "Find {v} Candy Pots", // This one will probably need changing based on item source/event
];

const eventTypeOptions = eventTypes.map((et) => ({
  label: et,
  value: et,
}));

const populateUUID = (file) => {
  const fileCopy = { ...file };
  // Each event
  Object.keys(file.event_progression).forEach((eventName) => {
    // Each stage
    Object.keys(get(fileCopy, `event_progression.${eventName}.stages`, {})).forEach(
      (_stage, stageIdx) => {
        set(fileCopy, `event_progression.${eventName}.stages.stage${stageIdx + 1}.uuid`, uuid());

        // Each objective
        Object.keys(
          get(
            fileCopy,
            `event_progression.${eventName}.stages.stage${stageIdx + 1}.objectives`,
            {},
          ),
        ).forEach((_obj, objIdx) => {
          set(
            fileCopy,
            `event_progression.${eventName}.stages.stage${stageIdx + 1}.objectives.obj${
              objIdx + 1
            }.uuid`,
            uuid(),
          );
        });

        // Each reward
        Object.keys(
          get(
            fileCopy,
            `event_progression.${eventName}.stages.stage${stageIdx + 1}.item_rewards`,
            {},
          ),
        ).forEach((_rew, rewIdx) => {
          set(
            fileCopy,
            `event_progression.${eventName}.stages.stage${stageIdx + 1}.item_rewards.item${
              rewIdx + 1
            }.uuid`,
            uuid(),
          );
        });
      },
    );
  });
  return fileCopy;
};

const depopulateUUID = (file) => {
  const fileCopy = clone(file);
  // Each event
  Object.keys(file.event_progression).forEach((eventName) => {
    // Each stage
    Object.keys(get(fileCopy, `event_progression.${eventName}.stages`, {})).forEach(
      (_stage, stageIdx) => {
        unset(fileCopy, `event_progression.${eventName}.stages.stage${stageIdx + 1}.uuid`);

        // Each objective
        Object.keys(
          get(
            fileCopy,
            `event_progression.${eventName}.stages.stage${stageIdx + 1}.objectives`,
            {},
          ),
        ).forEach((_obj, objIdx) => {
          unset(
            fileCopy,
            `event_progression.${eventName}.stages.stage${stageIdx + 1}.objectives.obj${
              objIdx + 1
            }.uuid`,
          );
        });

        // Each reward
        Object.keys(
          get(
            fileCopy,
            `event_progression.${eventName}.stages.stage${stageIdx + 1}.item_rewards`,
            {},
          ),
        ).forEach((_rew, rewIdx) => {
          set(
            fileCopy,
            `event_progression.${eventName}.stages.stage${stageIdx + 1}.item_rewards.item${
              rewIdx + 1
            }.uuid`,
          );
        });
      },
    );
  });
  return fileCopy;
};

const Reward = ({ itemId, amount, items = [], onItemChange, onAmountChange, onDelete }) => {
  const options = useMemo(
    () =>
      items.map((item) => ({
        label: `${item.id} - ${item.name}`,
        value: item.id,
      })),
    [items],
  );

  const selectedOpt = useMemo(() => options.find((opt) => opt.value === itemId), [itemId, options]);

  const selectedItem = useMemo(() => items.find((item) => item.id === itemId), [itemId, items]);

  return (
    <css.SingleRewardContainer>
      <InventoryItem item={selectedItem} quantity={amount ?? ""} />
      <Select
        placeholder="Choose Item..."
        options={options}
        value={selectedOpt}
        onChange={(newOpt) => {
          onItemChange(newOpt.value);
        }}
        styles={{
          option: (base) => ({
            ...base,
            color: "#555",
          }),
        }}
      />
      <Input
        value={amount}
        placeholder="Amount"
        options={options}
        onChange={(event) => onAmountChange(event.target.value)}
      />
      <div onClick={onDelete}>
        <MdDelete />
      </div>
    </css.SingleRewardContainer>
  );
};

const Objective = ({
  name,
  event,
  amount,
  onNameChange,
  onEventChange,
  onAmountChange,
  onDelete,
}) => {
  return (
    <css.ObjectiveContainer>
      <Input
        placeholder="Name"
        value={name}
        onChange={(event) => onNameChange(event.target.value)}
      />
      <Select
        options={eventTypeOptions}
        value={eventTypeOptions.find((opt) => opt.value === event)}
        onChange={(newOpt) => {
          onEventChange(newOpt.value);
        }}
        styles={{
          option: (base) => ({
            ...base,
            color: "#555",
          }),
        }}
      />
      <Input
        value={amount}
        placeholder="Nº"
        onChange={(event) => onAmountChange(event.target.value)}
      />
      <div onClick={onDelete}>
        <MdDelete />
      </div>
    </css.ObjectiveContainer>
  );
};

const BattlePassTool = () => {
  const [currentEvent, setCurrentEvent] = useState(null);
  const [currentStage, setCurrentStage] = useState(1);

  const [serverEvents, setServerEvents] = useState(null);
  const [invitems, setInvitems] = useState(null);

  const items = useMemo(() => Object.values(invitems?.premium ?? {}), [invitems]);

  const updatePath = (path, value) => {
    const newEvents = {
      ...serverEvents,
    };
    set(newEvents, path, value);
    setServerEvents(newEvents);
  };

  const exportContents = () => {
    const final = JSON.stringify(depopulateUUID(serverEvents), null, "\t");
    const element = document.createElement("a");
    const file = new Blob([final], {
      type: "application/json",
    });
    element.href = URL.createObjectURL(file);
    element.download = "server_events.json";
    document.body.appendChild(element);
    element.click();
  };

  const exportKV = () => {
    const final = objectToKv(depopulateUUID(serverEvents));
    const element = document.createElement("a");
    const file = new Blob([final], {
      type: "text/plain",
    });
    element.href = URL.createObjectURL(file);
    element.download = "server_events.txt";
    document.body.appendChild(element);
    element.click();
  };

  return (
    <css.Container>
      <div>
        <div>
          <label htmlFor="server_events">Choose server_events.txt</label>
          <input
            name="server_events"
            type="file"
            onChange={async (event) => {
              event.preventDefault();
              const reader = new FileReader();
              reader.onload = async (e) => {
                const text = e.target.result;

                const parsed = parseKeyValues(text);

                setServerEvents(populateUUID(parsed));
              };
              reader.readAsText(event.target.files[0]);
            }}
          />

          <label htmlFor="invitems">Choose invitems.txt</label>
          <input
            name="invitems"
            type="file"
            onChange={async (event) => {
              event.preventDefault();
              const reader = new FileReader();
              reader.onload = async (e) => {
                const text = e.target.result;
                setInvitems(parseKeyValues(text));
              };
              reader.readAsText(event.target.files[0]);
            }}
          />

          {serverEvents && (
            <div>
              Choose event to edit.
              {Object.keys(serverEvents.event_progression)
                .filter((k) => k !== "uuid")
                .map((eventKey) => (
                  <button
                    key={eventKey}
                    style={{ margin: "0 5px", backgroundColor: "revert" }}
                    onClick={() => {
                      setCurrentEvent(eventKey);
                    }}
                  >
                    {eventKey}
                  </button>
                ))}
            </div>
          )}
          <div>
            <Button onClick={exportContents}>Export JSON!</Button>
            <Button onClick={exportKV}>Export KV!</Button>
          </div>
        </div>
        {serverEvents && invitems && currentEvent ? (
          <css.EventContainer>
            <css.StageContainer>
              {Object.values(serverEvents.event_progression[currentEvent].stages).length} stages
              <css.AddStage
                onClick={() => {
                  updatePath(
                    `event_progression.${currentEvent}.stages.stage${
                      Object.values(serverEvents.event_progression[currentEvent].stages).length + 1
                    }`,
                    {
                      uuid: uuid(),
                    },
                  );
                }}
              >
                + Add
              </css.AddStage>
              <css.StagesListContainer>
                {Object.values(serverEvents.event_progression[currentEvent].stages).map(
                  (_, idx) => (
                    <css.Stage
                      isActive={idx + 1 === currentStage}
                      onClick={() => setCurrentStage(idx + 1)}
                    >
                      {`Stage ${idx + 1}`}
                    </css.Stage>
                  ),
                )}
              </css.StagesListContainer>
            </css.StageContainer>
            <css.StageContentContainer>
              <Input
                isRequired
                name="amount"
                onChange={(event) => {
                  const buffer = { ...serverEvents };
                  set(
                    buffer,
                    `event_progression.${currentEvent}.stages.stage${currentStage}.minimum_objectives_to_unlock`,
                    event.target.value ?? "",
                  );
                  setServerEvents(buffer);
                }}
                label="Minimum objectives to unlock"
                value={get(
                  serverEvents,
                  `event_progression.${currentEvent}.stages.stage${currentStage}.minimum_objectives_to_unlock`,
                  "",
                )}
              />
              {Object.values(
                get(
                  serverEvents,
                  `event_progression.${currentEvent}.stages.stage${currentStage}.objectives`,
                  {},
                ),
              ).map((obj, idx) => (
                <Objective
                  key={`${obj.target_event}-${obj.uuid}`}
                  name={obj.title}
                  event={obj.target_event}
                  amount={obj.count}
                  onNameChange={(name) =>
                    updatePath(
                      `event_progression.${currentEvent}.stages.stage${currentStage}.objectives.obj${
                        idx + 1
                      }.title`,
                      name,
                    )
                  }
                  onEventChange={(type) => {
                    updatePath(
                      `event_progression.${currentEvent}.stages.stage${currentStage}.objectives.obj${
                        idx + 1
                      }.target_event`,
                      type,
                    );
                    const eventIndex = eventTypes.indexOf(type);
                    obj.title = eventTypesText[eventIndex];
                  }}
                  onAmountChange={(amount) =>
                    updatePath(
                      `event_progression.${currentEvent}.stages.stage${currentStage}.objectives.obj${
                        idx + 1
                      }.count`,
                      amount,
                    )
                  }
                  onDelete={() => {
                    const oldObjectives = get(
                      serverEvents,
                      `event_progression.${currentEvent}.stages.stage${currentStage}.objectives`,
                      {},
                    );

                    const newKeys = Object.keys(oldObjectives).filter((k) => k !== `obj${idx + 1}`);

                    const newObjectives = {};
                    newKeys.forEach((k, idx2) => {
                      newObjectives[`obj${idx2 + 1}`] = oldObjectives[k];
                    });

                    updatePath(
                      `event_progression.${currentEvent}.stages.stage${currentStage}.objectives`,
                      newObjectives,
                    );
                  }}
                />
              ))}
              <Button
                onClick={() => {
                  updatePath(
                    `event_progression.${currentEvent}.stages.stage${currentStage}.objectives.obj${
                      Object.values(
                        get(
                          serverEvents,
                          `event_progression.${currentEvent}.stages.stage${currentStage}.objectives`,
                          {},
                        ),
                      ).length + 1
                    }`,
                    {
                      uuid: uuid(),
                    },
                  );
                }}
              >
                Add Objective
              </Button>
            </css.StageContentContainer>
            <css.RewardsContainer>
              {Object.values(
                get(
                  serverEvents,
                  `event_progression.${currentEvent}.stages.stage${currentStage}.item_rewards`,
                  {},
                ),
              ).map((reward, idx) => (
                <Reward
                  key={`${reward.uuid}`}
                  itemId={reward.item_id}
                  amount={reward.amount}
                  items={items}
                  onItemChange={(itemId) =>
                    updatePath(
                      `event_progression.${currentEvent}.stages.stage${currentStage}.item_rewards.item${
                        idx + 1
                      }.item_id`,
                      itemId,
                    )
                  }
                  onAmountChange={(amount) =>
                    updatePath(
                      `event_progression.${currentEvent}.stages.stage${currentStage}.item_rewards.item${
                        idx + 1
                      }.amount`,
                      amount,
                    )
                  }
                  onDelete={() => {
                    const oldItems = get(
                      serverEvents,
                      `event_progression.${currentEvent}.stages.stage${currentStage}.item_rewards`,
                      {},
                    );

                    const newKeys = Object.keys(oldItems).filter((k) => k !== `item${idx + 1}`);

                    const newItems = {};
                    newKeys.forEach((k, idx2) => {
                      newItems[`item${idx2 + 1}`] = oldItems[k];
                    });

                    updatePath(
                      `event_progression.${currentEvent}.stages.stage${currentStage}.item_rewards`,
                      newItems,
                    );
                  }}
                />
              ))}
              <Button
                onClick={() => {
                  updatePath(
                    `event_progression.${currentEvent}.stages.stage${currentStage}.item_rewards.item${
                      Object.values(
                        get(
                          serverEvents,
                          `event_progression.${currentEvent}.stages.stage${currentStage}.item_rewards`,
                          {},
                        ),
                      ).length + 1
                    }`,
                    {
                      uuid: uuid(),
                    },
                  );
                }}
              >
                Add Reward
              </Button>
            </css.RewardsContainer>
          </css.EventContainer>
        ) : (
          <div>Load data and choose event to start</div>
        )}
      </div>
    </css.Container>
  );
};

export default BattlePassTool;
