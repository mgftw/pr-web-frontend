import { Badge, Input } from "@nextui-org/react";
import tw from "twin.macro";
import _omit from "lodash/omit";
import { useDropzone } from "react-dropzone";
import questNPCS from "$constants/questNpcs";
import { useCallback, useState } from "react";
import styled from "styled-components";
import { useMutation } from "@tanstack/react-query";
import { updateNPCIcon } from "$api/adminApi";
import { TailSpin } from "react-loading-icons";
import { toast } from "react-toastify";
import { IQuestNPC } from "types/entities";
import NPCIcon from "$components/NPCIcon/NPCIcon";

const css = {
  Container: styled.div`
    ${tw`bg-first p-4`}
    display: grid;
    grid-template-rows: auto 1fr;
    row-gap: 2rem;
    max-height: 86vh;
  `,
  List: styled.div`
    height: 100%;
    overflow: scroll;

    > div {
      display: grid;
      row-gap: 0.5rem;
      column-gap: 0.5rem;
      grid-template-columns: repeat(10, auto);
    }
  `,
};

const Icon = ({
  npc,
  isLoading,
  updateIcon,
}: {
  npc: IQuestNPC;
  isLoading: boolean;
  updateIcon: (number, any) => void;
}) => {
  const { getRootProps, getInputProps } = useDropzone({
    onDrop: (acceptedFiles) => {
      const file = acceptedFiles[0];
      updateIcon(npc.id, file);
    },
    accept: {
      "image/jpeg": [],
      "image/png": [],
    },
    maxFiles: 1,
    maxSize: 8 * 1024 * 1024,
    multiple: false,
  });

  return isLoading ? (
    <TailSpin />
  ) : (
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      <Badge content={`#${npc.id} ${npc.name}`} color="primary" shape="circle">
        <div className="h-16 w-16">
          <NPCIcon npcId={npc.id} size="large" />
        </div>
      </Badge>
    </div>
  );
};

const NPCIconEditorTool = () => {
  const [filter, setFilter] = useState(null);
  const [updateHash, setUpdateHash] = useState({});

  const { mutateAsync: doUpdate } = useMutation((data: { npcId: number; file: any }) =>
    updateNPCIcon(data.npcId, data.file),
  );

  const updateIcon = useCallback(
    async (npcId, file) => {
      setUpdateHash((h) => ({
        ...h,
        [npcId]: true,
      }));

      try {
        await doUpdate({ npcId, file });
      } catch (e) {
        toast.error(e.toString());
      } finally {
        setUpdateHash((h) => _omit(h, npcId));
      }
    },
    [doUpdate, setUpdateHash],
  );

  const filteredNPCs =
    filter && filter !== ""
      ? questNPCS.filter((npc) => (npc.name ?? "").toLowerCase().includes(filter.toLowerCase()))
      : questNPCS;

  return (
    <css.Container>
      <h3>You can drag an image and the NPC image will be changed</h3>
      <Input
        placeholder="Search..."
        value={filter ?? ""}
        onChange={(event) => setFilter(event.target.value)}
      />

      <css.List>
        <div>
          {filteredNPCs.map((npc) => (
            <Icon key={npc.id} npc={npc} isLoading={!!updateHash[npc.id]} updateIcon={updateIcon} />
          ))}
        </div>
      </css.List>
    </css.Container>
  );
};

export default NPCIconEditorTool;
