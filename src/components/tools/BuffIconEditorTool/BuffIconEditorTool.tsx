import { Badge, Input } from "@nextui-org/react";
import tw from "twin.macro";
import _omit from "lodash/omit";
import { useDropzone } from "react-dropzone";
import BuffIcon from "$components/BuffIcon";
import useAllBuffs from "$hooks/useAllBuffs";
import { useCallback, useState } from "react";
import styled from "styled-components";
import { useMutation } from "@tanstack/react-query";
import { updateBuffIcon } from "$api/adminApi";
import { TailSpin } from "react-loading-icons";
import { toast } from "react-toastify";
import { IBuff } from "types/entities";

const css = {
  Container: styled.div`
    ${tw`bg-first p-4`}
    display: grid;
    grid-template-rows: auto 1fr;
    row-gap: 2rem;
    max-height: 86vh;
    overflow: scroll;
  `,
  BuffList: styled.div`
    height: 100%;
    overflow: scroll;

    > div {
      display: grid;
      row-gap: 0.5rem;
      column-gap: 0.5rem;
      grid-template-columns: repeat(10, auto);
    }
  `,
};

const Icon = ({
  buff,
  isLoading,
  updateIcon,
}: {
  buff: IBuff;
  isLoading: boolean;
  updateIcon: (number, any) => void;
}) => {
  const { getRootProps, getInputProps } = useDropzone({
    onDrop: (acceptedFiles) => {
      const file = acceptedFiles[0];
      updateIcon(buff.buff_id, file);
    },
    accept: {
      "image/jpeg": [],
      "image/png": [],
    },
    maxFiles: 1,
    maxSize: 8 * 1024 * 1024,
    multiple: false,
  });

  return isLoading ? (
    <TailSpin />
  ) : (
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      <Badge content={`#${buff.buff_id}`} color="primary" shape="circle">
        <div className="h-16 w-16">
          <BuffIcon key={buff.buff_id} buff={buff} size="small" />
        </div>
      </Badge>
    </div>
  );
};

const BuffBrowser = () => {
  const [filter, setFilter] = useState(null);
  const [updateHash, setUpdateHash] = useState({});

  const { mutateAsync: doUpdate } = useMutation((data: { buffId: number; file: any }) =>
    updateBuffIcon(data.buffId, data.file),
  );

  const updateIcon = useCallback(
    async (buffId, file) => {
      setUpdateHash((h) => ({
        ...h,
        [buffId]: true,
      }));

      try {
        await doUpdate({ buffId, file });
      } catch (e) {
        toast.error(e.toString());
      } finally {
        setUpdateHash((h) => _omit(h, buffId));
      }
    },
    [doUpdate, setUpdateHash],
  );

  const buffs = useAllBuffs().filter((buff) => buff && buff.infected === 9 && !(buff.flags & 128));

  const filteredBuffs =
    filter && filter !== ""
      ? buffs.filter((buff) => (buff.name ?? "").toLowerCase().includes(filter.toLowerCase()))
      : buffs;

  return (
    <css.Container>
      <h3>You can drag an image and the buff icon will be changed</h3>
      <Input
        placeholder="Search..."
        value={filter ?? ""}
        onChange={(event) => setFilter(event.target.value)}
      />

      <css.BuffList>
        <div>
          {filteredBuffs.map((buff) => (
            <Icon
              key={buff.buff_id}
              buff={buff}
              isLoading={!!updateHash[buff.buff_id]}
              updateIcon={updateIcon}
            />
          ))}
        </div>
      </css.BuffList>
    </css.Container>
  );
};

export default BuffBrowser;
