import React from "react";

export default () => (
  <svg
    width="100%"
    height="100%"
    viewBox="0 0 28 26"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M6.36084 10.4999L13.8625 2.99825L21.3642 10.4999H21.3625V22.9999H6.36252V10.4999H6.36084ZM3.86252 12.9982L2.0165 14.8443L0.25 13.0778L12.0962 1.23152C13.0717 0.256038 14.6533 0.256038 15.6288 1.23152L27.475 13.0778L25.7085 14.8443L23.8625 12.9983V22.9999C23.8625 24.3806 22.7432 25.4999 21.3625 25.4999H6.36252C4.98181 25.4999 3.86252 24.3806 3.86252 22.9999V12.9982Z"
      fill="currentColor"
    />
  </svg>
);
