import React from "react";

export default () => (
  <svg
    width="100%"
    height="100%"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M3.91992 15H9.00055V22.8507L20.0812 8.99995H15.0005V1.14917L3.91992 15ZM11.0005 13H8.08117L13.0005 6.85073V11H15.9199L11.0005 17.1492V13Z"
      fill="currentColor"
    />
  </svg>
);
