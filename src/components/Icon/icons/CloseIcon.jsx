import React from "react";

export default () => (
  <svg
    width="100%"
    height="100%"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M18.3847 16.9705L24.7486 23.3345L23.3344 24.7487L16.9705 18.3847L10.6065 24.7487L9.19228 23.3345L15.5562 16.9705L9.19228 10.6065L10.6065 9.19233L16.9705 15.5563L23.3344 9.19233L24.7486 10.6065L18.3847 16.9705Z"
      fill="currentColor"
    />
  </svg>
);
