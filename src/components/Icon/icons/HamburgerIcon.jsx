import React from "react";

export default () => (
  <svg
    width="100%"
    height="100%"
    viewBox="0 0 19 19"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M17.1391 5.68049V4.15271H1.86133V5.68049H17.1391ZM17.1391 8.73604V10.2638H1.86133V8.73604H17.1391ZM17.1391 13.3194V14.8472H1.86133V13.3194H17.1391Z"
      fill="currentColor"
    />
  </svg>
);
