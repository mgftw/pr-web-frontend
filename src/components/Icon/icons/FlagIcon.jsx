import React from "react";

export default () => (
  <svg
    width="100%"
    height="100%"
    viewBox="0 0 18 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M5.25 17.25H3.75H3V15.75H3.75V9V3V0.75H5.25V1.5H15.8436L13.5945 5.99993L15.8445 10.5H5.25V15.75H6V17.25H5.25ZM5.25 9V3H13.4169L11.9175 6.00007L13.4175 9H5.25Z"
      fill="currentColor"
    />
  </svg>
);
