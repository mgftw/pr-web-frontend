import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { rem } from "polished";

// A list of Icon components. Each component is
// an svg that has to have width=100% and height=100%
import * as icons from "./icons";

// Transform components into lowercase names to easily understand
// what icon is referred to
// "EditIcon" -> "icon-edit"
const iconComponentNames = Object.keys(icons);
const iconNames = iconComponentNames.map((iconCompName) =>
  [
    "icon",
    ...iconCompName
      .replace("Icon", "")
      .split(/([A-Z][a-z0-9]+)/g)
      .filter((e) => e !== "")
      .map((e) => e.toLowerCase()),
  ].join("-"),
);

// Create a conversion hash that maps icon names
// into components
// "icon-edit": EditIcon,
const availableIcons = {};
iconNames.forEach((iconName, index) => {
  availableIcons[iconName] = icons[iconComponentNames[index]];
});

// Container that handles sizing
const Container = styled.div(
  ({ size }) => `
  height: ${rem(`${size}px`)};
  width: ${rem(`${size}px`)};
`,
);

// The Icon components
const Icon = ({ icon: iconName, size }) => {
  const Comp = availableIcons[iconName];

  if (!Comp) {
    throw new Error(`Icon not found: ${iconName}`);
  }

  return (
    <Container size={size}>
      <Comp />
    </Container>
  );
};

Icon.propTypes = {
  // The name of the icon. The format is 'icon-###'
  icon: PropTypes.oneOf(iconNames).isRequired,

  // All icons are enclosed on a box.
  size: PropTypes.number,
};

Icon.defaultProps = {
  size: 16,
};

export const iconOptions = iconNames;
export default Icon;
