import { useState, useEffect, useMemo } from "react";
import { useQuery } from "@tanstack/react-query";

import { GiPirateFlag } from "react-icons/gi";
import { Tooltip } from "react-tippy";
import * as st from "./BlackMarketDetails.styles";
import { getBlackMarketInfo } from "$api/marketApi";
import { getMyInventory } from "$api/playerApi";
import { getGlobalDataInfo } from "$api/globalApi";
import BlackMarketItem from "$components/BlackMarketItem";
import { getOrdinal } from "$helpers/stringHelper";

import {
  GLOBAL_BLACK_MARKET as QUERY_GLOBAL_BLACK_MARKET,
  MY_INVENTORY as QUERY_MY_INVENTORY,
  GLOBAL_WORLD_STATE as QUERY_GLOBAL_WORLD_STATE,
} from "$constants/queries";
import Loader from "$components/Loader";
import InventoryItemIcon from "$components/InventoryItemIcon";
import Hint from "$components/Hint";

const BLACK_MARKET_PASS_ITEM_ID = 10;

const BlackMarketDetails = () => {
  const [remain, updateRemain] = useState("");

  const { data: bmItems = [], isLoading: isLoadingBlackMarket } = useQuery({
    queryKey: QUERY_GLOBAL_BLACK_MARKET,
    queryFn: () => getBlackMarketInfo(),
  });

  const { data: inventoryItems = [], isLoading: isLoadingInventory } = useQuery({
    queryKey: QUERY_MY_INVENTORY,
    queryFn: () => getMyInventory(),
  });

  const { data: globals = {}, isLoading: isLoadingGlobals } = useQuery({
    queryKey: QUERY_GLOBAL_WORLD_STATE,
    queryFn: () => getGlobalDataInfo(),
  });

  const lastUpdate = new Date(globals.black_market_last_update);
  let lastUpdateString = "";
  if (!Number.isNaN(lastUpdate.getTime())) {
    const ordinal = getOrdinal(lastUpdate.getUTCDate());
    lastUpdateString = ` ${lastUpdate.getUTCDate()}${ordinal} ${lastUpdate.toLocaleString(
      "default",
      { month: "long" },
    )}`;
  }

  const tomorrow = new Date();
  tomorrow.setDate(tomorrow.getUTCDate() + 1);
  tomorrow.setUTCHours(0);
  tomorrow.setUTCMinutes(0);
  tomorrow.setUTCSeconds(0);
  tomorrow.setUTCMilliseconds(0);

  function getRemainTime(t) {
    const day = new Date();
    let diff = Math.round((t.getTime() - day.getTime()) / 1000);

    if (diff < 0) diff = 0;
    const hour = Math.floor(diff / 3600)
      .toString()
      .padStart(2, "0");
    const mins = Math.floor((diff / 60) % 60)
      .toString()
      .padStart(2, "0");
    const secs = Math.floor(diff % 60)
      .toString()
      .padStart(2, "0");

    updateRemain(`${hour}:${mins}:${secs}`);
  }

  useEffect(() => {
    getRemainTime(tomorrow);
    const interval = setInterval(() => getRemainTime(tomorrow), 1000);
    return () => clearInterval(interval);
  }, []);

  const hasMarketPass = useMemo(
    () =>
      Boolean(inventoryItems.find((itemData) => itemData.item_id === BLACK_MARKET_PASS_ITEM_ID)),
    [inventoryItems],
  );

  return (
    <st.BlackMarketContainer>
      <st.BlackMarketHeader>
        <Tooltip
          html={
            <Hint title="Black Market" renderIcon={() => <GiPirateFlag size={16} />}>
              Gives you access to rare and unique items that are sold at relatively high prices. The
              market updates every day with a new pool of items, and the more cooperative and versus
              games that were played the day before, the more items that will be available
            </Hint>
          }
        >
          Black Market
        </Tooltip>
        {lastUpdateString && (
          <st.DateInfo>
            <st.DateInfoDay>{lastUpdateString}</st.DateInfoDay>
            <st.DateInfoRemaining>Resets in {remain}</st.DateInfoRemaining>
          </st.DateInfo>
        )}
      </st.BlackMarketHeader>
      <st.BlackMarketInfo>
        {isLoadingBlackMarket || isLoadingInventory || isLoadingGlobals ? (
          <Loader />
        ) : (
          <st.BlackMarketItemList>
            {hasMarketPass ? (
              bmItems.map((bmItem) => <BlackMarketItem key={bmItem.item_id} bmItem={bmItem} />)
            ) : (
              <st.BlackMarketNoAccess>
                <st.BlackMarketPass>
                  <InventoryItemIcon item={{ id: 10, name: "" }} />
                </st.BlackMarketPass>
                <p>You must acquire a Black Market Pass to view the Black Market</p>
              </st.BlackMarketNoAccess>
            )}
          </st.BlackMarketItemList>
        )}
      </st.BlackMarketInfo>
    </st.BlackMarketContainer>
  );
};

export default BlackMarketDetails;
