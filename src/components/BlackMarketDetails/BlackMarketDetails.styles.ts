import styled from "styled-components";
import tw from "twin.macro";

export const BlackMarketContainer = styled.div`
  ${tw`m-0 p-4 rounded-lg bg-second`}
  min-height: 18rem;
  animation: fadeIn ease 0.5s 0.3s;
  animation-fill-mode: both;

  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
`;

export const BlackMarketHeader = styled.div`
  ${tw`text-left font-bold text-primary text-xl flex -mt-1`}
`;

export const DateInfo = styled.div`
  ${tw`flex-col flex-grow items-end -mt-2`}
`;

export const DateInfoDay = styled.h6`
  ${tw`p-0 m-0 text-primary text-sm font-normal text-right`}
`;

export const DateInfoRemaining = styled.p`
  ${tw`p-0 m-0 text-xs text-secondary font-normal text-right`}
`;

export const BlackMarketLastUpdate = styled.div`
  ${tw`font-normal inline text-secondary text-xl tracking-tighter ml-1`}
`;

export const BlackMarketResetContainer = styled.div`
  ${tw`flex flex-row text-right font-normal text-secondary text-xl tracking-tighter`}
  float: right;
`;

export const BlackMarketResetTimer = styled.div`
  ${tw`text-right font-bold text-secondary text-xl pl-1`}
`;

export const BlackMarketInfo = styled.div`
  ${tw`flex flex-col my-2 text-base`}
`;

export const BlackMarketPass = styled.div`
  ${tw`h-32 w-32 mb-4`}
  filter: grayscale(1);
`;

export const BlackMarketNoAccess = styled.div`
  ${tw`p-2 font-karla font-bold text-red text-lg text-center py-8 items-center content-center flex flex-col`}
`;

export const BlackMarketItem = styled.div`
  ${tw`p-1 font-oswald`}
`;

export const BlackMarketTableHeader = styled.div`
  ${tw`flex flex-row justify-around mt-2 font-bold font-oswald text-center`}
`;

export const BlackMarketTableHeadItem = styled.div`
  ${tw``}
  flex-basis: 55%;
`;

export const BlackMarketTableHeadCash = styled.div`
  ${tw`flex justify-center`}
  flex-basis: 20%;
`;

export const BlackMarketTableHeadPill = styled.div`
  ${tw`flex justify-center`}
  flex-basis: 15%;
`;

export const BlackMarketTableHeadBlood = styled.div`
  ${tw`flex justify-center`}
  flex-basis: 10%;
`;

export const BlackMarketItemList = styled.div`
  ${tw`flex-col`}

  > * {
    margin-bottom: 0.6rem;
  }

  > *:last-child {
    margin-bottom: 0;
  }
`;
