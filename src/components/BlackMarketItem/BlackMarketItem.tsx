import { useMemo } from "react";
import PropTypes from "prop-types";
import { Tooltip } from "react-tippy";

import * as st from "./BlackMarketItem.styles";
import Badge from "$atoms/Badge";
import InventoryItemIcon from "$components/InventoryItemIcon";
import InventoryItemDetails from "$components/InventoryItemDetails";
import Icon from "$components/Icon";
import { useMyProfile } from "$hooks/useMyProfile";
import { useMyInventory } from "$hooks/useMyInventory";
import { IBlackMarketItem } from "types/entities";

const types = {
  "|Auto|": "auto",
  "|Buff|": "buff",
  "|+|": "enhancement",
  "|~|": "loot",
  "|<|": "craft",
};

const typeInfo = {
  auto: {
    name: "",
    icon: "",
  },
  buff: {
    name: "Buff",
    icon: "",
  },
  enhancement: {
    name: "Enhancement",
    icon: "",
  },
  craft: {
    name: "Material",
    icon: "icon-craft",
  },
};

const VOLATILE_TANK_BLOOD_ITEM_ID = 176;

const BlackMarketItem = ({ bmItem }: { bmItem: IBlackMarketItem }) => {
  const { profile, isLoading: isLoadingProfile } = useMyProfile();
  const { inventory = [], isLoading: isLoadingInventory } = useMyInventory();

  const bloodItemAmount = useMemo(
    () => inventory.find((item) => item.item_id === VOLATILE_TANK_BLOOD_ITEM_ID)?.quantity ?? 0,
    [inventory],
  );

  const item = useMemo(
    () => ({
      ...bmItem.item,
      id: bmItem.item_id,
    }),
    [bmItem],
  );

  const details = useMemo(() => <InventoryItemDetails item={item} />, [item]);

  const match = item.name.match(/\|(Auto|Buff|<|\+|~)\|/);
  const category = types[match?.[0]];

  let { name } = item;
  if (category) {
    name = name.replace(`${match[0]} `, "");
  }

  const type = typeInfo?.[category];

  const cashTooltip = useMemo(() => {
    const diff = (bmItem.cash_cost - profile.cash).toLocaleString();
    return (
      <st.ItemTypeTooltip>
        {!isLoadingProfile && profile.cash < bmItem.cash_cost
          ? `You can't afford this (${diff} left)`
          : "You have enough cash for this"}
      </st.ItemTypeTooltip>
    );
  }, [profile, isLoadingProfile, bmItem.cash_cost]);

  const pillTooltip = useMemo(() => {
    const diff = (bmItem.pill_cost - profile.pills).toLocaleString();
    return (
      <st.ItemTypeTooltip>
        {!isLoadingProfile && profile.pills < bmItem.pill_cost
          ? `You can't afford this (${diff} left)`
          : "You have enough pills for this"}
      </st.ItemTypeTooltip>
    );
  }, [profile, isLoadingProfile, bmItem.pill_cost]);

  const bloodTooltip = useMemo(() => {
    const diff = (bmItem.blood_cost - bloodItemAmount).toLocaleString();
    return (
      <st.ItemTypeTooltip>
        {!isLoadingInventory && bloodItemAmount < bmItem.blood_cost
          ? `You can't afford this (${diff} left)`
          : "You have enough Volatile Blood for this"}
      </st.ItemTypeTooltip>
    );
  }, [bloodItemAmount, isLoadingInventory, bmItem.blood_cost]);

  return (
    <st.BlackMarketItemContainer>
      <Tooltip trigger="mouseenter" html={details} position="bottom-start">
        <st.ItemImage>
          <InventoryItemIcon item={{ ...bmItem.item, id: bmItem.item_id }} size="medium" />
        </st.ItemImage>
      </Tooltip>
      <st.ItemInfo>
        <st.ItemName>
          {type?.icon && (
            <Tooltip
              trigger="mouseenter"
              position="top-start"
              html={<st.ItemTypeTooltip>{type.name}</st.ItemTypeTooltip>}
            >
              <st.ItemTypeIcon>
                <Icon icon={type.icon} size={14} />
              </st.ItemTypeIcon>
            </Tooltip>
          )}
          {name}
        </st.ItemName>
        <st.BadgesList>
          {bmItem.cash_cost > 0 && (
            <Tooltip html={cashTooltip}>
              <Badge
                icon="icon-cash"
                backgroundColor={
                  !isLoadingProfile && profile.cash < bmItem.cash_cost ? "red" : "green"
                }
              >
                {bmItem.cash_cost.toLocaleString()}
              </Badge>
            </Tooltip>
          )}

          {bmItem.pill_cost > 0 && (
            <Tooltip html={pillTooltip}>
              <Badge
                icon="icon-pills"
                backgroundColor={
                  !isLoadingProfile && profile.pills < bmItem.pill_cost ? "red" : "green"
                }
              >
                {bmItem.pill_cost.toLocaleString()}
              </Badge>
            </Tooltip>
          )}

          {bmItem.blood_cost > 0 && (
            <Tooltip html={bloodTooltip}>
              <Badge
                icon="icon-blood"
                backgroundColor={
                  !isLoadingInventory && bloodItemAmount < bmItem.blood_cost ? "red" : "green"
                }
              >
                {bmItem.blood_cost.toLocaleString()}
              </Badge>
            </Tooltip>
          )}
        </st.BadgesList>
      </st.ItemInfo>
    </st.BlackMarketItemContainer>
  );
};

BlackMarketItem.propTypes = {
  bmItem: PropTypes.shape({
    item: PropTypes.shape({
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
    }),
    cash_cost: PropTypes.number.isRequired,
    pill_cost: PropTypes.number.isRequired,
    blood_cost: PropTypes.number.isRequired,
  }).isRequired,
};

export default BlackMarketItem;
