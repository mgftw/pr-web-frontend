import styled from "styled-components";
import tw from "twin.macro";

export const BlackMarketItemContainer = styled.div`
  ${tw`flex flex-row font-oswald bg-first rounded-md p-2 items-start`}
`;

export const ItemName = styled.div`
  ${tw`text-base flex uppercase items-center content-center`}
`;

export const ItemTypeIcon = styled.div`
  ${tw`mr-1 rounded-full p-0.5 h-4 w-4 text-xs bg-primary text-zero flex items-center justify-center`}
  padding: 2px;
`;

export const ItemTypeTooltip = styled.div`
  ${tw`font-karla`}
`;

export const Description = styled.div`
  ${tw`pt-2 pb-2 pr-2 pl-2 bg-second flex rounded-lg text-sm font-karla`}
  width: 27.4rem;
`;

export const ItemImage = styled.div`
  ${tw`flex h-12 w-12 mr-2 flex-shrink-0`}
`;

export const ItemInfo = styled.div`
  ${tw`flex-col`}
`;

export const BadgesList = styled.div`
  ${tw`flex mt-1`}

  > * {
    margin-left: 0.5rem;
  }

  > *:first-child {
    margin-left: 0;
  }
`;
