import styled from "styled-components";
import tw from "twin.macro";

export const DailyResetContainer = styled.div`
  ${tw`m-0 p-4 mt-4 rounded-lg bg-second`}
  height: 4rem;
  width: 30rem;
  animation: fadeIn ease 0.5s;

  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
`;

export const DailyResetHeader = styled.div`
  ${tw`text-center font-bold text-primary text-xl w-full`}
`;

export const DailyResetTimer = styled.div`
  ${tw`text-center font-bold text-secondary text-4xl`}
`;
