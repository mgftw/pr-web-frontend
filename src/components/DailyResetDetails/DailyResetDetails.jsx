import React, { useState, useEffect } from "react";

import * as st from "./styles";

const DailyResetDetails = () => {
  const [remain, updateRemain] = useState(0);

  const tomorrow = new Date();
  tomorrow.setDate(tomorrow.getUTCDate() + 1);
  tomorrow.setUTCHours(0);
  tomorrow.setUTCMinutes(0);
  tomorrow.setUTCSeconds(0);
  tomorrow.setUTCMilliseconds(0);

  function getRemainTime(t) {
    const day = new Date();
    let diff = Math.round((t.getTime() - day.getTime()) / 1000);
    if (diff < 0) diff = 0;
    const hour = Math.floor(diff / 3600)
      .toString()
      .padStart(2, "0");
    const mins = Math.floor((diff / 60) % 60)
      .toString()
      .padStart(2, "0");
    const secs = Math.floor(diff % 60)
      .toString()
      .padStart(2, "0");
    updateRemain(`${hour}:${mins}:${secs}`);
  }

  useEffect(() => {
    getRemainTime(tomorrow);
    const interval = setInterval(() => getRemainTime(tomorrow), 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <st.DailyResetContainer>
      <st.DailyResetHeader>Daily Reset</st.DailyResetHeader>
      <st.DailyResetTimer>{remain}</st.DailyResetTimer>
    </st.DailyResetContainer>
  );
};

export default DailyResetDetails;
