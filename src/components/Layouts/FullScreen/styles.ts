import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`mx-auto h-screen bg-zero p-0 font-karla text-beige flex flex-row`}
  min-width: 1360px;
  min-height: 680px;
  overflow: hidden;
`;
