import { type ReactNode } from "react";

import NavBar from "$components/NavBar";

import * as st from "./styles";

const FullScreen = ({ children, hideNavBar }: { children: ReactNode; hideNavBar?: boolean }) => {
  return (
    <st.Container>
      {!hideNavBar && <NavBar />}
      {children}
    </st.Container>
  );
};

export default FullScreen;
