import React from "react";
import PropTypes from "prop-types";

import * as st from "./InventoryItemDetails.styles";

import InventoryItemIcon from "$components/InventoryItemIcon";
import Badge from "$atoms/Badge";

const regex = new RegExp("<.*>");

const types = {
  "|Auto|": "auto",
  "|Buff|": "buff",
  "|+|": "enhancement",
  "|~|": "loot",
  "|<|": "craft",
  "|Q|": "quest",
};

const typeInfo = {
  auto: {
    name: "",
    icon: "",
  },
  buff: {
    name: "Buff",
    icon: "",
  },
  enhancement: {
    name: "Enhancement",
    icon: "",
  },
  loot: {
    name: "Loot",
    icon: "",
  },
  craft: {
    name: "Material",
    icon: "icon-craft",
  },
  quest: {
    name: "Quest Item",
    icon: "icon-book",
  },
};

const InventoryItemDetails = ({ item }) => {
  const lines = item.description ? item.description.split("\n").filter((s) => s !== "") : [];

  const descLines = lines.filter((line) => !regex.test(line)).join(" ");
  const tags = lines.filter((line) => regex.test(line)).map((tag) => tag.replace(/<|>/g, ""));

  const match = item.name.match(/\|(Auto|Buff|<|\+|~|Q)\|/);
  const category = types[match?.[0]];

  let { name } = item;
  if (category) {
    name = name.replace(`${match[0]} `, "");
  }

  return (
    <st.Container>
      <st.BuffDescription>
        <st.IconContainer>
          <InventoryItemIcon item={item} />
        </st.IconContainer>
        <st.Info>
          <st.Name>
            {name.toUpperCase() || "???"}
            {category && typeInfo?.[category].name && (
              <st.BadgeContainer>
                <Badge icon={typeInfo[category].icon} textColor="zero" backgroundColor="primary">
                  {typeInfo[category].name}
                </Badge>
              </st.BadgeContainer>
            )}
          </st.Name>
          <st.Description>
            {descLines}
            {tags.map((tag) => (
              <st.DescriptionTag key={tag}>{tag}</st.DescriptionTag>
            ))}
          </st.Description>
        </st.Info>
      </st.BuffDescription>
    </st.Container>
  );
};

InventoryItemDetails.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    description: PropTypes.string,
    id: PropTypes.number,
  }).isRequired,
};

export default InventoryItemDetails;
