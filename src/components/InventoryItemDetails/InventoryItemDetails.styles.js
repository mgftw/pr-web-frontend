import styled from "styled-components";
import tw from "twin.macro";

import { buffCategoryColors } from "$constants/colors";

export const Container = styled.div`
  ${tw`bg-second rounded-lg p-4`}
  width: 500px;
  z-index: 300;
`;

export const BuffDescription = styled.div`
  ${tw`pr-2 pl-2 flex rounded-lg`}
`;

export const Placeholder = styled.div`
  ${tw`h-20 w-20 font-oswald text-center rounded-lg float-left absolute text-3xl`}
  ${({ subCat }) =>
    `background-color: ${buffCategoryColors[Object.keys(buffCategoryColors)[subCat - 1]]}`};
  box-shadow: 0px 0px 1px 2px black inset;
  text-shadow:
    -1px -1px 0 #000,
    1px -1px 0 #000,
    -1px 1px 0 #000,
    1px 1px 0 #000;
  line-height: 4.75rem;
`;

export const IconContainer = styled.div`
  ${tw`h-20 w-20 font-oswald text-center rounded-lg text-3xl flex-shrink-0 mr-3`}
`;

export const BuffImage = styled.div`
  ${tw`h-20 w-20 rounded-lg bg-primary flex-shrink-0 mr-3 bg-transparent z-10`}
  ${({ url }) => `background-image: url(${url})`};
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  box-shadow: 0px 0px 2px 3px black inset;
  ${({ category }) =>
    category !== null && `box-shadow: 0px 0px 4px 3px ${category}, 0px 0px 2px 3px black inset`};
`;

export const Info = styled.div`
  ${tw``}
`;

export const Name = styled.div`
  ${tw`font-oswald text-xl pb-1 flex items-center`}
`;

export const Category = styled.div`
  ${tw`bg-zero font-karla rounded-lg text-sm text-primary pr-1 pl-1 ml-2`}
  ${({ color }) => `background-color: ${color};`}
  width: fit-content;
  height: fit-content;
  padding-top: 2px;
  padding-bottom: 2px;
`;

export const Description = styled.div`
  ${tw`font-karla text-secondary text-left`}
`;

export const DescriptionTag = styled.div`
  ${tw`bg-zero rounded-lg text-primary p-1 pr-2 pl-2 m-1 mr-0 ml-0 first:mt-2 flex-grow-0`}
  width: fit-content;
`;

export const BadgeContainer = styled.div`
  ${tw`ml-2`}
`;
