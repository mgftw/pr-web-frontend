import styled from "styled-components";
import tw from "twin.macro";

const Button = styled.button`
  ${tw`bg-zero text-primary px-5 py-2 text-base rounded ml-1 mr-1 first:ml-0 font-oswald uppercase border-none`}
  cursor: pointer;
`;

export default Button;
