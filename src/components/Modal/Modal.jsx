import React from "react";
import PropTypes from "prop-types";

import { BiX } from "react-icons/bi";

import * as st from "./Modal.styles";

const ModalHeader = ({ children, onRequestClose, showCloseButton }) => (
  <st.ModalHeader>
    <st.HeaderContent>
      <h3>{children}</h3>
      {showCloseButton && (
        <st.IconContainer onClick={onRequestClose}>
          <BiX size={24} />
        </st.IconContainer>
      )}
    </st.HeaderContent>
    <st.Divider />
  </st.ModalHeader>
);

ModalHeader.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  showCloseButton: PropTypes.bool,
  onRequestClose: PropTypes.func.isRequired,
};

ModalHeader.defaultProps = {
  children: null,
  showCloseButton: true,
};

const ModalBody = ({ children }) => <st.ModalBody>{children}</st.ModalBody>;

ModalBody.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

ModalBody.defaultProps = {
  children: null,
};

const Modal = ({ isOpen, children }) => (
  <st.ModalBackground
    isOpen={isOpen}
    initial="hide"
    variants={{
      hide: {
        backgroundColor: "rgba(0, 0, 0, 0)",
      },
      show: {
        backgroundColor: "rgba(0, 0, 0, 0.50)",
      },
    }}
    animate={isOpen ? "show" : "hide"}
  >
    <st.ModalContainer isOpen={isOpen}>
      <st.Container
        isOpen={isOpen}
        initial="hide"
        variants={{
          hide: {
            opacity: 0,
          },
          show: {
            opacity: 1,
          },
        }}
        transition={{
          duration: 0.3,
          ease: "easeInOut",
        }}
        animate={isOpen ? "show" : "hide"}
      >
        {children}
      </st.Container>
    </st.ModalContainer>
  </st.ModalBackground>
);

Modal.propTypes = {
  // Whether or not the modal is visible
  isOpen: PropTypes.bool.isRequired,

  // Content
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

Modal.defaultProps = {
  children: null,
};

Modal.Header = ModalHeader;
Modal.Body = ModalBody;

export default Modal;
