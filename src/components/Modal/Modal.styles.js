import styled from "styled-components";
import { motion } from "framer-motion";
import tw from "twin.macro";

export const ModalBackground = styled(motion.div)`
  width: 100vw;
  height: 100vh;
  pointer-events: ${({ isOpen }) => (isOpen ? "inherit" : "none")};
  position: fixed;
  top: 0;
  left: 0;
  z-index: 20000;
`;

export const ModalContainer = styled.div`
  position: relative;
  pointer-events: ${({ isOpen }) => (isOpen ? "inherit" : "none")};
  width: 100%;
  height: 100%;
`;

export const Container = styled(motion.div)`
  ${tw`bg-first`}
  pointer-events: ${({ isOpen }) => (isOpen ? "inherit" : "none")};
  min-width: 25rem;
  max-width: 80vw;
  width: fit-content;
  height: fit-content;
  min-height: 10rem;
  max-height: 95vh;
  position: absolute;
  left: 0;
  margin: auto auto;
  right: 0;
  top: 10vh;
  align-self: center;
  align-items: center;
  border-radius: 1rem;
`;

export const Header = styled.div`
  padding-left: 1rem;
  padding-right: 1rem;
  height: 3rem;
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

export const IconContainer = styled.div`
  ${tw`pr-1 text-primary items-center justify-center flex`}
  cursor: pointer;
`;

export const Body = styled.div`
  position: relative;
  overflow: auto;
  max-height: calc(40vh - 3rem);
  margin-left: 2rem;
  margin-right: 2rem;
`;

export const Title = styled.div`
  ${tw`text-primary font-karla text-lg font-bold flex flex-1`}
`;

export const ModalHeader = styled.div`
  display: flex;
  flex-direction: column;
`;
export const HeaderContent = styled.div`
  ${tw`text-lg p-4`}
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: center;

  > h3 {
    margin: 0;
    padding: 0;
    flex: 1;
  }
`;
export const Divider = styled.div`
  ${tw`bg-zero`}
  height: 3px;
`;

export const ModalBody = styled.div`
  ${tw`p-4`}
`;
