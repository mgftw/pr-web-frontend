import PropTypes from "prop-types";
import { useDrag } from "react-dnd";

import BuffIcon from "$components/BuffIcon";

import * as st from "./styles";

import { IOwnedBuffInfo } from "types/entities";

const types = {
  "|Q|": "quest",
  "|E|": "event",
  "|V|": "versus",
  "|C|": "craft",
};

const BuffCard = ({ ownedBuffInfo }: { ownedBuffInfo: IOwnedBuffInfo }) => {
  const { buff } = ownedBuffInfo;
  const match = buff.name.match(/\|(C|E|V|Q)\|/);
  const category = match !== null ? types[match[0]] : null;

  // Format buff name
  let { name } = buff;
  if (category !== null) {
    name = name.replace(`${match[0]} `, "");
  }

  // Add +1 if enhanced
  const { current_enhancement: enhanced } = ownedBuffInfo;
  const enhancedString = enhanced ? ` +${enhanced}` : "";

  const [, dragRef] = useDrag({
    type: "OWNED_BUFF",
    item: {
      buff_id: buff.buff_id,
    },
  });

  const isSpecialBuff = category === "event";

  return (
    <st.MasterContainer ref={dragRef}>
      {isSpecialBuff && <st.Decorator role="presentation" />}
      <st.CardContainer $category={category}>
        <st.IconContainer>
          <BuffIcon buff={buff} />
        </st.IconContainer>
        <st.NameContainer>
          {name}
          {enhancedString}
        </st.NameContainer>
      </st.CardContainer>
    </st.MasterContainer>
  );
};

BuffCard.propTypes = {
  buff: PropTypes.shape({
    name: PropTypes.string,
  }).isRequired,
};

export default BuffCard;
