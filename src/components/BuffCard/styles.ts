import styled, { keyframes } from "styled-components";
import tw from "twin.macro";

const colors = {
  quest: "#6E837C",
  event: "transparent",
  versus: "#B85B5B",
  craft: "#B88D5B",
};

const bgAnim = keyframes`
50% {
  background-position: 100% 50%;
}
`;

const borderWidth = "1px";

export const MasterContainer = styled.div`
  position: relative;
`;

export const CardContainer = styled.div<{ $category: string }>(({ $category }) => [
  tw`flex pt-1 pb-1 rounded-lg bg-zero`,
  `
    z-index: 2;
    cursor: grab;
    position:relative;
  `,
  $category !== null
    ? `
      border: 1px solid ${colors[$category]};
      box-shadow: -10px 0px 10px -10px ${colors[$category]} inset;
    `
    : "",
]);

export const IconContainer = styled.div`
  ${tw`flex-grow-0 h-10 w-10 ml-1`}
`;

export const NameContainer = styled.div`
  ${tw`flex-grow pl-5 flex items-center font-oswald text-lg uppercase`}
`;

export const Decorator = styled.div`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  z-index: 1;
  font-size: 2.5rem;
  text-transform: uppercase;
  color: white;
  border-radius: 8px;

  &::after {
    position: absolute;
    content: "";
    top: calc(-1 * ${borderWidth});
    left: calc(-1 * ${borderWidth});
    z-index: -1;
    width: calc(100% + ${borderWidth} * 2);
    height: calc(100% + ${borderWidth} * 2);
    background: linear-gradient(
      60deg,
      hsl(224, 85%, 66%),
      hsl(269, 85%, 66%),
      hsl(314, 85%, 66%),
      hsl(359, 85%, 66%),
      hsl(44, 85%, 66%),
      hsl(89, 85%, 66%),
      hsl(134, 85%, 66%),
      hsl(179, 85%, 66%)
    );
    background-size: 300% 300%;
    background-position: 0 50%;
    border-radius: 8px;
    animation: ${bgAnim} 4s alternate infinite;
    filter: blur(1px);
  }
`;
