import { RootState } from "$redux/store";
import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";

const ProtectedRoute = ({ redirectPath = "/auth" }: { redirectPath?: string }) => {
  const accessToken = useSelector((state: RootState) => state.sessionState.token);

  if (!accessToken) {
    return <Navigate to={redirectPath} />;
  }

  return <Outlet />;
};

export default ProtectedRoute;
