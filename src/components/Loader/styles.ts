import styled from "styled-components";
import tw from "twin.macro";

import { motion } from "framer-motion";

export const Container = styled.div`
  ${tw`flex flex-col pt-5 items-center justify-center`}
  min-width: 20rem;
`;

export const Content = styled.div`
  ${tw`bg-zero flex flex-col items-center justify-center pl-5 pr-5 pt-8 pb-8 rounded-lg`}
`;

export const Asset = styled(motion.img)`
  ${tw`object-contain w-40 h-24`};
`;

export const Text = styled.div`
  ${tw`pt-2 text-lg`}
`;
