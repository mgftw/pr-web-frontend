import loaderAsset from "$assets/loader.png";

import * as st from "./styles";

const Loader = () => (
  <st.Container>
    <st.Content>
      <st.Asset
        animate={{
          y: [0, 5, 0, 5, 0],
          rotate: [0, 5, -5, 0, 5, -5, 0],
        }}
        transition={{
          duration: 2,
          ease: "easeInOut",
          repeat: Infinity,
        }}
        src={loaderAsset}
      />
      <st.Text>Loading...</st.Text>
    </st.Content>
  </st.Container>
);

export default Loader;
