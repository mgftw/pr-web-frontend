import { Link as RTLink } from "react-router-dom";
import styled from "styled-components";

const LinkStyle = styled.div`
  a {
    color: inherit;
    text-decoration: inherit;
    &:hover,
    &:active,
    &:visited {
      color: inherit;
      text-decoration: inherit;
    }
  }
`;

const Link = (props: typeof RTLink) => (
  <LinkStyle>
    {/* @ts-expect-error: Compatibility */}
    <RTLink {...props} />
  </LinkStyle>
);

export default Link;
