import styled from "styled-components";
import tw from "twin.macro";
import { motion } from "framer-motion";

export const Container = styled(motion.div)`
  ${tw`fixed h-screen`}
  width: 18rem;
  right: 0;
  top: 0;
  z-index: 3000;
`;

export const Wrapper = styled.div`
  ${tw`bg-second h-full w-full relative flex flex-col`}
`;

export const Close = styled.div`
  ${tw`absolute`}
  right: 0.5rem;
  cursor: pointer;
`;

export const Header = styled.div`
  ${tw`border-zero flex flex-col p-10 items-center justify-center`}
  border-bottom: 2px solid #2E3638;
`;

export const Content = styled.div`
  ${tw`flex-grow`}

  > li {
    list-style: none;
    padding: 1rem;
    cursor: pointer;
  }
`;

export const Footer = styled.div`
  ${tw`h-12 flex`}
  border-top: 2px solid #2E3638;
`;

export const Logout = styled.div`
  ${tw`h-12 pr-4 pl-4 text-lg text-primary flex items-center justify-center`}
  cursor: pointer;
`;

export const Links = styled.div`
  ${tw`h-12 flex items-center flex-grow`}
`;

export const Link = styled.img`
  ${tw`rounded-full flex-grow-0 h-6 w-6 object-cover mr-1 ml-1 border-solid border-secondary`}
  border-width: 2px;
`;

export const Name = styled.div`
  ${tw`text-xl font-bold truncate mt-1`}
`;

export const Avatar = styled.div<{ $url: string }>`
  ${tw`rounded-full bg-secondary h-24 w-24 mb-2`}
  ${({ $url }) => ($url ? `background-image: url(${$url});` : "")}
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

export const Level = styled.div`
  ${tw`text-left leading-loose text-gray-300`}
  white-space: nowrap;
  line-height: 1.2;
`;

export const ExpProgressContainer = styled.div`
  ${tw`w-full items-center flex`}
  margin-top: 2px;
`;

export const LevelPercent = styled.div`
  ${tw`flex text-center flex-grow justify-center font-bold pl-2 text-sm`}
`;

export const Contributors = styled.div`
  ${tw`font-karla text-center w-64`}
`;

export const ContributorList = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 0.5rem;
`;

export const ContributorCard = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
  column-gap: 0.5rem;
  align-items: center;

  border-radius: 5px;

  ${tw`bg-zero p-1`}

  > img {
    height: 2.5rem;
    width: 2.5rem;
    border-radius: 99999px;
  }

  > div {
    display: flex;
    flex-direction: column;
  }

  h3 {
    ${tw`font-karla p-0 m-0 text-base`}
  }

  p {
    ${tw`font-karla p-0 m-0 text-sm`}
  }
`;
