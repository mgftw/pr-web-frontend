import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";

import { Tooltip } from "react-tippy";
import * as sessionActions from "$actions/sessionActions";

import ProgressBar from "$components/ProgressBar";
import Icon from "$components/Icon";

import discord from "$assets/discord.png";
import steam from "$assets/steam_logo.png";
import bans from "$assets/bans.png";
import love from "$assets/love.png";

import { useMyProfile } from "$hooks/useMyProfile";
import Modal from "$components/Modal";
import { useQuery } from "@tanstack/react-query";
import { getPlayerProfile } from "$api/playerApi";
import ItemBrowser from "$components/tools/ItemBrowser";
import * as st from "./styles";

import contributorsData from "../../../contributors.json";

const variants = {
  hidden: {
    x: 300,
    transition: { ease: [0.22, 1, 0.36, 1] },
  },
  visible: {
    x: 0,
    transition: { ease: [0.22, 1, 0.36, 1] },
  },
};

const ContributorCard = ({ contributor }) => {
  const { data: player } = useQuery({
    queryKey: ["player", contributor.steam_id],
    queryFn: () => getPlayerProfile(contributor.steam_id),
    refetchIntervalInBackground: false,
    refetchOnReconnect: false,
    refetchOnWindowFocus: false,
    refetchOnMount: false,
  });

  return (
    <st.ContributorCard>
      <img src={player?.avatar_url ?? ""} />
      <div>
        <h3>{contributor.name}</h3>
        <p>{contributor.message}</p>
      </div>
    </st.ContributorCard>
  );
};

const Footer = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [isContributorsModalOpen, setIsContributorsModalOpen] = useState(false);
  return (
    <st.Footer>
      <Modal isOpen={isContributorsModalOpen}>
        <Modal.Header onRequestClose={() => setIsContributorsModalOpen(false)}>
          PR Web Contributors
        </Modal.Header>
        <Modal.Body>
          <div>
            <h4>Shotout to our contributors!</h4>
            <p>If you would like to become one, ping us in discord at the #pr-web channel</p>
            <st.ContributorList>
              {(contributorsData ?? []).map((contributor) => (
                <ContributorCard key={contributor.steam_id} contributor={contributor} />
              ))}
            </st.ContributorList>
          </div>
        </Modal.Body>
      </Modal>
      <st.Links>
        <Tooltip html={<st.Contributors>Go to our discord server</st.Contributors>}>
          <a target="_blank" href="https://discord.gg/n97ZSX7" rel="noreferrer">
            <st.Link src={discord} alt="Join our Discord server!" />
          </a>
        </Tooltip>

        <Tooltip html={<st.Contributors>Go to our Steam Group</st.Contributors>}>
          <a target="_blank" href="https://steamcommunity.com/groups/MGFTW" rel="noreferrer">
            <st.Link src={steam} alt="Steam group page" />
          </a>
        </Tooltip>
        <Tooltip html={<st.Contributors>Go to bans</st.Contributors>}>
          <a target="_blank" href="https://bans.mgftw.com/" rel="noreferrer">
            <st.Link src={bans} alt="See server bans" />
          </a>
        </Tooltip>
        <div style={{ cursor: "pointer" }}>
          <Tooltip
            // @ts-expect-error compatibility
            zIndex={1000000}
            html={
              <st.Contributors>
                Made with love by the admins and our awesome contributors
                <span role="img" aria-label="Love">
                  ❤️‍🔥
                </span>
              </st.Contributors>
            }
          >
            <st.Link
              src={love}
              alt="Made possible by..."
              onClick={() => setIsContributorsModalOpen(true)}
            />
          </Tooltip>
        </div>
      </st.Links>
      <st.Logout
        onClick={() => {
          dispatch(sessionActions.onLogout());
          navigate("/auth", {
            replace: true,
          });
        }}
      >
        Logout
      </st.Logout>
    </st.Footer>
  );
};

const SideMenu = ({ onClose, isOpen }) => {
  const { profile: stats } = useMyProfile();
  const [isItemBrowserOpen, setIsItemBrowserOpen] = useState(false);

  return (
    <st.Container variants={variants} initial="hidden" animate={isOpen ? "visible" : "hidden"}>
      <st.Wrapper>
        <st.Close onClick={onClose}>
          <Icon icon="icon-close" size={24} />
        </st.Close>
        <st.Header>
          <st.Avatar $url={stats.avatar_url} />
          <st.Level>Lv. {stats.level !== undefined ? stats.level : "-"}</st.Level>
          <st.Name>{stats.name || "Loading..."}</st.Name>
          <st.ExpProgressContainer>
            <ProgressBar
              height={8}
              width={240}
              value={Math.floor(stats.exp_percent * 100)}
              backColor="secondary"
            />
            <st.LevelPercent>
              {stats.exp_percent > 1 ? 100 : Math.floor(stats.exp_percent * 100)}%
            </st.LevelPercent>
          </st.ExpProgressContainer>
        </st.Header>
        <st.Content>
          <Modal isOpen={isItemBrowserOpen}>
            <Modal.Header onRequestClose={() => setIsItemBrowserOpen(false)}>
              Item Browser
            </Modal.Header>
            <Modal.Body>
              <ItemBrowser />
            </Modal.Body>
          </Modal>
          <li onClick={() => setIsItemBrowserOpen(true)}>Item Browser</li>
        </st.Content>
        <Footer />
      </st.Wrapper>
    </st.Container>
  );
};

export default SideMenu;
