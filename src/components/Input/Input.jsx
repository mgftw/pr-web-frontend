import React from "react";
import Icon from "$components/Icon";

import * as st from "./styles";

const Input = React.forwardRef(
  ({ rightIcon, leftIcon, label, isRequired, error, ...rest }, ref) => (
    <st.Container>
      {label && (
        <st.Label>
          {label}
          {isRequired && <st.Required>*</st.Required>}
        </st.Label>
      )}
      <st.InputContainer>
        {leftIcon && (
          <st.LeftIcon>
            <Icon icon={leftIcon} />
          </st.LeftIcon>
        )}
        <st.Input {...rest} ref={ref} />
        {rightIcon && (
          <st.RightIcon>
            <Icon icon={rightIcon} />
          </st.RightIcon>
        )}
      </st.InputContainer>
      {error && <st.Error>{error}</st.Error>}
    </st.Container>
  ),
);

export default Input;
