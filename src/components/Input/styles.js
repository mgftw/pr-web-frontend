import styled from "styled-components";
import tw from "twin.macro";

import colors from "$constants/colors";

export const Container = styled.div`
  ${tw`text-primary font-karla`}
`;

export const Input = styled.input`
  ${tw`appearance-none text-primary`}
  line-height: rem(21px);
  background-color: rgba(0, 0, 0, 0);
  border: none;
  font-size: rem(16px);
  font-weight: 500;
  height: 2rem;
  width: calc(100%);
  outline: none;

  &:focus {
    outline: none;
    &:-webkit-autofill {
      -webkit-box-shadow: 0 0 0 30px $white inset;
      -webkit-text-fill-color: $black !important;
      transition: all 0.3s;
    }
  }

  &:-webkit-autofill,
  &:-webkit-autofill:hover,
  &:-webkit-autofill:focus {
    -webkit-box-shadow: 0 0 0 30px white inset;
    -webkit-text-fill-color: $black !important;
    transition: all 0.2s;
  }

  &::placeholder {
    color: ${colors.secondary};
  }
`;

export const LeftIcon = styled.div``;

export const RightIcon = styled.div``;

export const InputContainer = styled.div`
  ${tw`bg-zero pl-2 pr-2 text-primary rounded`}
`;

export const Label = styled.div`
  ${tw`text-primary font-bold flex`}
`;

export const Error = styled.div`
  ${tw`text-red font-bold`}
`;

export const Required = styled.div`
  ${tw`text-red font-bold pl-1`}
`;
