import React, { useState, useEffect } from "react";
import { Tooltip } from "react-tippy";
import { sortBy, get } from "lodash";
import { MdCheck } from "react-icons/md";

import { BUCKET_URL } from "$constants/url";

import EventDescription from "$components/EventDescription";
import { getEvent } from "$api/serverApi";
import { getMyEventProgress } from "$api/playerApi";

import * as st from "./styles";

const EventDetails = () => {
  const [event, setEvent] = useState({});
  const [progress, setProgress] = useState({});
  useEffect(() => {
    getEvent().then((passedProgress) => {
      setEvent(passedProgress);
    });
  }, []);

  useEffect(() => {
    getMyEventProgress().then((passedEvent) => {
      setProgress(passedEvent);
    });
  }, []);

  if (!event || !event.event_id) {
    return null;
  }

  return (
    <st.Container>
      <st.Title>Event</st.Title>
      <st.Content>
        <Tooltip
          trigger="click"
          html={
            <EventDescription
              description={event.description || ""}
              name={event.name}
              banner={`${BUCKET_URL}/event${event.event_id}.png`}
            />
          }
        >
          <st.Banner src={`${BUCKET_URL}/event${event.event_id}.png`} />
        </Tooltip>
        <st.Stages>
          {sortBy(event.stages, "stage_number").map((stage, index) => {
            const isDone = stage.stage_number < get(progress, "stage", 0);

            const registry = get(progress, "registry", []);
            return (
              <st.StageContainer key={stage.stage_number}>
                <Tooltip
                  trigger="click"
                  html={
                    isDone ? (
                      <st.StageDetails>Stage completed!</st.StageDetails>
                    ) : (
                      <st.StageDetails>
                        Objectives:
                        <st.Objectives>
                          {stage.objectives.map((o) => {
                            const current =
                              (registry.find((r) => r.event_name === o.event_name) || {}).amount ||
                              0;
                            return (
                              <st.Objective
                                key={o.objective_number}
                                isCompleted={current > o.amount}
                              >
                                {`${o.title} (${current} / ${o.amount})`}
                              </st.Objective>
                            );
                          })}
                        </st.Objectives>
                        <st.Minimum>
                          {`Complete ${stage.minimum_objectives_to_unlock} ${
                            stage.minimum_objectives_to_unlock === 1 ? "objective" : "objectives"
                          } to complete the stage`}
                        </st.Minimum>
                      </st.StageDetails>
                    )
                  }
                >
                  <st.Stage isDone={isDone}>{!isDone ? stage.stage_number : <MdCheck />}</st.Stage>
                </Tooltip>
                {index !== event.stages.length - 1 && <st.Line />}
              </st.StageContainer>
            );
          })}
        </st.Stages>
      </st.Content>
    </st.Container>
  );
};

export default EventDetails;
