import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`bg-second rounded-lg p-4`}
  z-index: 300;
`;

export const Banner = styled.img`
  ${tw`bg-secondary rounded object-cover w-full h-32 mr-4 mb-1`}
  cursor: pointer;
`;

export const EventDescription = styled.div`
  ${tw`pr-2 pl-2 flex rounded-lg`}
`;

export const Info = styled.div`
  ${tw``}
`;

export const Name = styled.div`
  ${tw`font-oswald text-primary text-xl pb-1 flex items-center`}
`;

export const Description = styled.div`
  ${tw`font-karla text-left flex flex-col`}
`;

export const Line = styled.div`
  ${tw`text-primary text-sm`}
  padding-bottom: 6px;
  font-weight: medium;
`;

export const DescriptionTag = styled.div`
  ${tw`bg-zero rounded-lg text-primary p-1 pr-2 pl-2 m-1 mr-0 ml-0 first:mt-2 flex-grow-0 text-sm`}
  width: fit-content;
`;
