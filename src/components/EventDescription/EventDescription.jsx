import React from "react";
import PropTypes from "prop-types";

import * as st from "./EventDescription.styles";

const regex = new RegExp("<.*>");

const EventDescription = ({ description, banner }) => {
  const lines = description.split("\n").filter((s) => s !== "");

  const descLines = lines.filter((line) => !regex.test(line));
  const tags = lines.filter((line) => regex.test(line)).map((tag) => tag.replace(/<|>/g, ""));
  return (
    <st.Container>
      <st.EventDescription>
        <st.Info>
          <st.Banner src={banner} />
          <st.Description>
            {descLines.map((line) => (
              <st.Line>{line}</st.Line>
            ))}
            {tags.map((tag) => (
              <st.DescriptionTag key={tag}>{tag}</st.DescriptionTag>
            ))}
          </st.Description>
        </st.Info>
      </st.EventDescription>
    </st.Container>
  );
};

EventDescription.propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  banner: PropTypes.string.isRequired,
};

export default EventDescription;
