import { SURVIVOR_CLASSES } from "$constants/types";

export default {
  beige: "#E8E6D5",
  primary: "#E8E6D5",
  secondary: "#A2ABAB",
  quaternary: "#70798C",
  zero: "#1B2021",
  first: "#2E3638",
  second: "#49565A",
  red: "#B85B5B",
  green: "#52796F",
};

export const buffCategoryColors = {
  offensive: "#c33e3e",
  defensive: "#385f3f",
  ability: "#958901",
  equipment: "#dedede",
  utility: "#5e4a9f",
  economy: "#2977ad",
  misc: "#995334",
};

export const getClassGradient = (classId) => {
  switch (classId) {
    case SURVIVOR_CLASSES.TECHNICIAN: {
      return {
        start: "#99C6D9",
        end: "#2B82A7",
      };
    }
    case SURVIVOR_CLASSES.SUPPORT: {
      return {
        start: "#8BBE8B",
        end: "#65B865",
      };
    }
    case SURVIVOR_CLASSES.CONTROLLER: {
      return {
        start: "#D75C5B",
        end: "#9F2928",
      };
    }
    case SURVIVOR_CLASSES.AGILE: {
      return {
        start: "#AE6AAD",
        end: "#7F487F",
      };
    }
    case SURVIVOR_CLASSES.DEBUFFER: {
      return {
        start: "#F2D519",
        end: "#B5A120",
      };
    }
    case SURVIVOR_CLASSES.HUNTER: {
      return {
        start: "#8E522D",
        end: "#562E15",
      };
    }
    case SURVIVOR_CLASSES.GLADIATOR: {
      return {
        start: "#5462AC",
        end: "#404C8D",
      };
    }
    default:
      return null;
  }
};
