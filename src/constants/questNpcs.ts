// TODO: We dont have this in the API yet. We will use a hard-coded list

import { IQuestNPC } from "types/entities";

const questNPCS: IQuestNPC[] = [
  {
    id: 1,
    name: "Kyle",
    bio: "A doctor that needs help from immune survivors to survive the first days of the flu.",
    min_level: 1,
  },
  {
    id: 2,
    name: "Roger",
    bio: "A very degenerate old man that tries to use infected as sexual slaves.",
    min_level: 3,
  },
  {
    id: 3,
    name: "The Voice",
    bio: "An unknown man that apparently has access to a lot of knowledge regarding the Green Flu. Helps survivors to increase their power, but also tends to put survivors in very dangerous situations. It's not clear if he is an ally or an enemy, but immune survivors listen to him to acquire more power.",
    min_level: 200,
    needs_clan: true,
  },
  {
    id: 4,
    name: "Keith",
    bio: "A doctor that was around at the beginning of the outbreak.",
    min_level: 15,
  },
  {
    id: 5,
    name: "Sara",
    bio: "A very skilled survivor, leader of the Hopebringers clan, founded by herself. She tends to be very hostile with almost everyone outside of her clan, but she does a lot of good work to make sure non-immune survivors have a place in this new world.",
    min_level: 10,
  },
  {
    id: 6,
    name: "Bryan",
    bio: "A teenager struggling to remain alive in the chaos that the Green Flu has caused. His main goal is to reunite with whoever is left from his family.",
    min_level: 20,
  },
  {
    id: 7,
    name: "William",
    bio: "A very skilled hunter that after a dumb accident with an axe, has given up on surviving for long and wishes to have his last collections completed.",
    min_level: 50,
  },
  {
    id: 8,
    name: "Theodora",
    bio: "Bryan's sister. After meeting with her brother, they both plan to go to this New hope refugee that was broadcasted through the radio.",
    min_level: 40,
  },
  {
    id: 9,
    name: "Darwin",
    bio: "A very skilled researcher that tries to find a cure for the Green Flu.",
    min_level: 65,
  },
  {
    id: 10,
    name: "Thief",
    bio: "A thief looking for the most valuable material goods, even with the Green Flu around.",
    min_level: 57,
  },
  {
    id: 11,
    name: "Will",
    bio: "The leader and founder of the Colourblind movement that aims to be the organization that brings peace to the new chaotic world. They are responsible for coordinating transactions with immune survivors.",
    min_level: 100,
  },
  {
    id: 12,
    name: "David",
    bio: "A father desperately trying to survive the Green Flu.",
    min_level: 60,
  },
  {
    id: 16,
    name: "Celine",
    bio: "Dr. Darwin's assistant. She is attempting to complete his unfinished work with the player's help.",
    min_level: 80,
  },
  {
    id: 17,
    name: "The Gunsmith",
    bio: "A weapon handyman looking for players to test out his new invention.",
    min_level: 220,
  },
  {
    id: 19,
    name: "Sierra",
    bio: "A black market peddler that wants to expand her network to other players by offering daily necessities for the greater good.",
    min_level: 210,
  },
  {
    id: 20,
    name: "~ Vault of memories ~",
    bio: "An unknown voice that tells the player to assist other survivors, namely Emily and a missing squad.",
    min_level: 120,
  },
  {
    id: 21,
    name: "Bruce",
    min_level: 120,
  },
  {
    id: 22,
    name: "Carlos",
    bio: "Sara's faithful Latin companion. He's in charge of coordinating all the teams in her command.",
    min_level: 180,
  },
  {
    id: 23,
    name: "Tyler",
    bio: "Sara's and Ethan's companion. He trains survivors to become special soldiers against the infected.",
    min_level: 240,
  },
  {
    id: 24,
    name: "Ethan",
    bio: "Tyler's companion. He trains survivors to become special soldiers against the infected.",
    min_level: 240,
  },
];

export default questNPCS;
