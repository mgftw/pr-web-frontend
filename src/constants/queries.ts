export const IS_ADMIN_QUERY = ["admin", "me"];
export const ADMIN_LIST_SERVERS = ["admin", "servers"];

// Global queries
export const GLOBAL_WORLD_STATE = ["global", "world-state"];
export const GLOBAL_BLACK_MARKET = ["global", "black-market"];
export const GLOBAL_INVENTORY_ITEMS = ["global", "inventory-items"];
export const GLOBAL_QUEST_INFO = ["global", "quest-info"];
export const GLOBAL_FORTRESS_OCCUPATION = ["global", "fortress"];
export const GLOBAL_EVENT = ["global", "event"];
export const GLOBAL_ALL_ITEMS = ["global", "all-items"];
export const GLOBAL_ALL_BUFFS = ["global", "all-buffs"];

export const GLOBAL_CLAN_RANK_VICTORY_POINTS = ["global", "clan-rank", "victory-points"];
export const GLOBAL_CLAN_RANK_COMMUNITY_POINTS = ["global", "clan-rank", "community-points"];
export const GLOBAL_CLAN_RANK_DOMINATION_POINTS = ["global", "clan-rank", "domination-points"];

// PLayers queries
export const MY_PROFILE = ["me", "profile"];
export const MY_ACCESS_FLAGS = ["me", "access_flags"];
export const MY_INVENTORY = ["me", "inventory"];
export const MY_EVENT_PROGRESS = ["me", "event", "progress"];
export const MY_QUEST_PROGRESS = ["me", "quest", "progress"];
export const MY_CLASS_PROGRESS = ["me", "class", "progress"];

// Clan
export const CLAN_INFO = (id) => ["clan", "info", id];
export const CLAN_MEMBER_LIST = (id) => ["clan", "members", id];
