const FEATURE_FLAGS = {
  ENABLE_QUEST_SYSTEM: import.meta.env.VITE_API_URL ?? false, // Show or not the quest system to end users.
};

export default FEATURE_FLAGS;
