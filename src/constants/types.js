export const SURVIVOR_CLASSES = {
  TECHNICIAN: 1,
  SUPPORT: 2,
  CONTROLLER: 3,
  AGILE: 4,
  DEBUFFER: 5,
  HUNTER: 6,
  GLADIATOR: 7,
};

export const CLAN_POSITIONS = {
  LEADER: 4,
  CO_LEADER: 3,
  MEMBER: 1,
  NONE: 0,
};

export const FORTRESS_TYPES = {
  NONE: 0,
  PRISON: 1,
  HELMS_DEEP: 2,
};
