import React, { useEffect } from "react";
import { useQuery } from "@tanstack/react-query";
import { getMyProfile } from "$api/playerApi";

import { MY_PROFILE as QUERY_MY_PROFILE } from "$constants/queries";
import { toast } from "react-toastify";
import { useNavigate } from "react-router";

const initialState = {
  profile: null,
  isLoading: true,
};

const Context = React.createContext(initialState);

export const MyProfileProvider = ({ children }) => {
  const navigate = useNavigate();
  const {
    data: profile = {},
    isLoading,
    error,
  } = useQuery({
    queryKey: QUERY_MY_PROFILE,
    queryFn: () => getMyProfile(),
  });
  useEffect(() => {
    if (error?.response?.status === 401) {
      toast.error("Session expired");
      navigate("/auth", {
        replace: true,
      });
    }
  }, [error, history]);

  return (
    <Context.Provider
      value={{
        profile,
        isLoading,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useMyProfile = () => React.useContext(Context);
