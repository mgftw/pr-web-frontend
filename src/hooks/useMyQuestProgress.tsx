import { useQuery } from "@tanstack/react-query";
import { useQuestData } from "./useQuestData";
import { getMyQuests } from "$api/playerApi";
import questNPCS from "$constants/questNpcs";
import { MY_QUEST_PROGRESS } from "$constants/queries";
import { IPlayerQuestProgress } from "types/entities";
import { useMemo } from "react";

// Calculates what data should be shown in the UI

const useMyQuestProgress = () => {
  const { quests } = useQuestData();

  const { data: questProgress = [] } = useQuery<IPlayerQuestProgress[]>(
    MY_QUEST_PROGRESS,
    () => {
      return getMyQuests();
    },
    {
      refetchOnWindowFocus: false,
      refetchOnReconnect: false,
      refetchOnMount: false,
    },
  );

  // TODO: Optimize this processing
  const npcsWithProgress = useMemo(() => {
    return questNPCS.map((npc) => {
      const questsForNpc = quests
        .filter((quest) => quest.npc_id === npc.id)
        .map((quest) => {
          const progressForQuest = questProgress.find(
            (questProgress) => questProgress.quest_id === quest.quest_id,
          );

          return {
            ...quest,
            progress: progressForQuest,
          };
        });

      return {
        ...npc,
        quests: questsForNpc,
      };
    });
  }, [quests, questProgress]);

  return npcsWithProgress;
};

export default useMyQuestProgress;
