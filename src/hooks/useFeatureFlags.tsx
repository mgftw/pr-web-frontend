import FEATURE_FLAGS from "$constants/flags";

const useFeatureFlags = () => {
  return FEATURE_FLAGS;
};

export default useFeatureFlags;
