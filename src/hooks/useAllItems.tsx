import { useQuery } from "@tanstack/react-query";

import { GLOBAL_ALL_ITEMS } from "$constants/queries";
import { getAllItems } from "$api/serverApi";
import { IInventoryItem } from "types/entities";

const useAllItems = () => {
  const { data } = useQuery<IInventoryItem[]>({
    queryKey: GLOBAL_ALL_ITEMS,
    queryFn: () => getAllItems(),
    retry: 5,
    refetchOnMount: false,
    refetchIntervalInBackground: false,
    refetchOnWindowFocus: false,
    refetchOnReconnect: false,
  });

  return data ?? [];
};

export default useAllItems;
