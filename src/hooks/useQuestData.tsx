import React, { useMemo } from "react";
import { useQuery } from "@tanstack/react-query";
import { getQuests } from "$api/questApi";

import { GLOBAL_QUEST_INFO as QUERY_GLOBAL_QUEST_INFO } from "$constants/queries";
import { IQuest } from "types/entities";

const initialState = {
  quests: [],
  questRegistry: {},
  isLoading: true,
};

const Context = React.createContext<{
  isLoading: boolean;
  quests: IQuest[];
  questRegistry: Record<number, IQuest>;
}>(initialState);

export const QuestDataProvider = ({ children }) => {
  const { data: quests = [], isLoading } = useQuery<IQuest[]>({
    queryKey: QUERY_GLOBAL_QUEST_INFO,
    queryFn: () => getQuests(),
    refetchOnWindowFocus: false,
    refetchOnReconnect: false,
    refetchOnMount: false,
  });

  const questRegistry = useMemo<Record<number, IQuest>>(
    () =>
      quests.reduce((accum, quest) => {
        return {
          ...accum,
          [quest.quest_id]: quest,
        };
      }, {}),
    [quests],
  );

  return (
    <Context.Provider
      value={{
        quests,
        isLoading,
        questRegistry,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useQuestData = () => React.useContext(Context);
