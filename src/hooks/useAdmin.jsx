import { useQuery } from "@tanstack/react-query";

import { validateIfAdmin } from "$api/adminApi";
import { IS_ADMIN_QUERY } from "$constants/queries";

const useAdmin = () => {
  const { data, isLoading, error } = useQuery(
    {
      queryKey: IS_ADMIN_QUERY,
      queryFn: () => validateIfAdmin(),
    },
    {
      retry: 10,
    },
  );

  if (isLoading || error) {
    return "maybe";
  }

  return data.is_admin ? "yes" : "no";
};

export default useAdmin;
