import React from "react";
import { useQuery } from "@tanstack/react-query";
import { getMyInventory } from "$api/playerApi";

import { MY_INVENTORY as QUERY_MY_INVENTORY } from "$constants/queries";

const initialState = {
  inventory: [],
  isLoading: true,
};

const Context = React.createContext(initialState);

export const MyInventoryProvider = ({ children }) => {
  const { data: inventory = [], isLoading } = useQuery({
    queryKey: QUERY_MY_INVENTORY,
    queryFn: () => getMyInventory(),
  });

  return (
    <Context.Provider
      value={{
        inventory,
        isLoading,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useMyInventory = () => React.useContext(Context);
