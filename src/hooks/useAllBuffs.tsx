import { useQuery } from "@tanstack/react-query";

import { GLOBAL_ALL_BUFFS } from "$constants/queries";
import { getBuffs } from "$api/buffApi";

const useAllBuffs = () => {
  const { data } = useQuery({
    queryKey: GLOBAL_ALL_BUFFS,
    queryFn: () => getBuffs(),
    retry: 5,
    refetchOnMount: false,
    refetchIntervalInBackground: false,
    refetchOnWindowFocus: false,
    refetchOnReconnect: false,
  });

  return data ?? [];
};

export default useAllBuffs;
