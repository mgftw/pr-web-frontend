import React from "react";
import { useQuery } from "@tanstack/react-query";

import { getMyAccessFlags } from "$api/playerApi";

import { MY_ACCESS_FLAGS } from "$constants/queries";

const initialState = {
  canEditQuests: false,
};

const Context = React.createContext(initialState);

export const MyAccessFlagsProvider = ({ children }) => {
  const { data: access = {} } = useQuery({
    queryKey: MY_ACCESS_FLAGS,
    queryFn: () => getMyAccessFlags(),
  });

  return (
    <Context.Provider
      value={{
        canEditQuests: Boolean(access.can_edit_quests),
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useMyAccessFlags = () => React.useContext(Context);
