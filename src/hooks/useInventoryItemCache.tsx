import { useMemo } from "react";
import useAllItems from "./useAllItems";
import { IInventoryItem } from "types/entities";

const useInventoryItemCache = () => {
  const items = useAllItems();

  const cache = useMemo(() => {
    const cache: Record<number, IInventoryItem> = {};
    items.forEach((item) => {
      cache[item.id] = item;
    });
    return cache;
  }, [items]);

  return cache;
};

export default useInventoryItemCache;
