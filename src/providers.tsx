import PropTypes from "prop-types";
import { MyInventoryProvider } from "$hooks/useMyInventory";
import { MyProfileProvider } from "$hooks/useMyProfile";
import { QuestDataProvider } from "$hooks/useQuestData";
import { MyAccessFlagsProvider } from "$hooks/useMyAccess";

import { NextUIProvider } from "@nextui-org/react";

const AppProviders = ({ children }) => (
  <NextUIProvider>
    <MyProfileProvider>
      <MyAccessFlagsProvider>
        <QuestDataProvider>
          <MyInventoryProvider>{children}</MyInventoryProvider>
        </QuestDataProvider>
      </MyAccessFlagsProvider>
    </MyProfileProvider>
  </NextUIProvider>
);

AppProviders.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AppProviders;
