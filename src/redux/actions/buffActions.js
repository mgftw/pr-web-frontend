import { buffTypes as types } from "./index";

export const updateBuffList = (buffs) => ({
  type: types.UPDATE_BUFF_LIST,
  buffs,
});
