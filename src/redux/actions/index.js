export const sessionTypes = {
  ON_AUTH_SUCCESS: "SESSION_ON_AUTH_SUCCESS",
  LOGOUT: "LOGOUT",
};

export const buffTypes = {
  UPDATE_BUFF_LIST: "UPDATE_BUFF_LIST",
};
