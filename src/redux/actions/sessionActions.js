import { sessionTypes as types } from "./index";

export const onAuthSuccess = ({ token }) => ({
  type: types.ON_AUTH_SUCCESS,
  token,
});

export const onLogout = () => ({
  type: types.LOGOUT,
});
