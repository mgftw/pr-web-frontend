import { applyMiddleware, createStore, compose } from "redux";
import { persistReducer, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";
import middleware from "./middleware";
import reducer from "./reducers";

/* Enable redux dev tools only in development.
 * We suggest using the standalone React Native Debugger extension:
 * https://github.com/jhen0409/react-native-debugger
 */

// const enhancer = composeEnhancers(...enhancers);

// Prsisted data
const persistConfig = {
  key: "root",
  storage,
  version: 52,
  whitelist: ["sessionState", "buffState"],
};

const persistedReducer = persistReducer(persistConfig, reducer);

const store = createStore(
  persistedReducer,
  {},
  compose(
    applyMiddleware(...middleware),
    // @ts-expect-error compatibility
    window.devToolsExtension ? window.devToolsExtension() : (f) => f,
  ),
);

export type RootState = ReturnType<typeof store.getState>;

export const persistor = persistStore(store);

export default store;
