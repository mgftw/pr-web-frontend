import { REHYDRATE } from "redux-persist";
import { get } from "lodash";
import { sessionTypes as types } from "$actions";

const initialState = {
  token: null,
};

const sessionReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOGOUT: {
      return {
        ...initialState,
      };
    }

    case REHYDRATE: {
      return get(action, "payload.sessionState")
        ? { ...action.payload.sessionState }
        : { ...state };
    }

    case types.ON_AUTH_SUCCESS: {
      const { token } = action;
      return {
        ...state,
        token,
      };
    }

    default: {
      return state;
    }
  }
};

export default sessionReducer;
