import { REHYDRATE } from "redux-persist";
import { get } from "lodash";
import { buffTypes as types } from "$actions";

const initialState = {
  buffs: [],
};

const buffReducer = (state = initialState, action) => {
  switch (action.type) {
    case REHYDRATE: {
      return get(action, "payload.buffState") ? { ...action.payload.buffState } : { ...state };
    }

    case types.UPDATE_BUFF_LIST: {
      const { buffs } = action;
      return {
        ...state,
        buffs,
      };
    }

    default: {
      return state;
    }
  }
};

export default buffReducer;
