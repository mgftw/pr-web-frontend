import { combineReducers } from "redux";

import sessionState from "./sessionReducer";
import buffState from "./buffReducer";

const rootReducer = combineReducers({
  sessionState,
  buffState,
});

export default rootReducer;
