import "vite/client";
declare module "*.module.scss";

declare module "*.png" {
  const value: any;
  export = value;
}

declare module "*.svg" {
  const value: any;
  export = value;
}

declare module "react-tippy" {
  export interface TooltipProps {
    children?: React.ReactNode;
  }
}

declare const window: Window & { location: Location };
