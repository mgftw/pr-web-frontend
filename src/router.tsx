import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { ToastContainer } from "react-toastify";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

import { BrowserRouter as Router, Routes, Route, Navigate } from "react-router-dom";

import store, { persistor } from "$redux/store";

// Components
import ProtectedRoute from "$components/ProtectedRoute";

// Pages
import AuthPage from "$pages/Auth";
import DashboardPage from "$pages/Dashboard";
import ProfilePage from "$pages/ProfilePage";
import QuestsPage from "$pages/QuestsPage";
import AdminPage from "$pages/AdminPage";

// Providers
import BaseStyle from "$components/BaseStyle";
import ClanPage from "$pages/ClanPage";
import AppProviders from "./providers";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      retry: 2,
    },
  },
});

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <DndProvider backend={HTML5Backend}>
          <BaseStyle />
          <ToastContainer closeButton={<div className="custom-toastify-button" />} />
          <QueryClientProvider client={queryClient}>
            <Router>
              <AppProviders>
                <Routes>
                  <Route path="/auth" element={<AuthPage />} />

                  <Route element={<ProtectedRoute />}>
                    <Route index element={<Navigate to="/dashboard" replace />} />
                    <Route path="/dashboard" element={<DashboardPage />} />
                    <Route path="/profile" element={<ProfilePage />}>
                      <Route index element={<ProfilePage.GeneralTab />} />
                      <Route path="buffs" element={<ProfilePage.BuffsTab />} />
                    </Route>
                    <Route path="/clan" element={<ClanPage />}>
                      <Route index element={<ClanPage.GeneralTab />} />
                    </Route>
                    <Route path="/quest" element={<QuestsPage />}>
                      <Route index element={<Navigate to="./world" />} />
                      <Route path="world" element={<QuestsPage.WorldTab />} />
                      <Route path="progress" element={<QuestsPage.ProgressTab />} />
                    </Route>
                    <Route path="/admin" element={<AdminPage />}>
                      <Route index element={<Navigate to="./servers" />} />
                      <Route path="tools" element={<AdminPage.ToolsTab />} />
                      <Route path="data" element={<AdminPage.DataTab />} />
                      <Route path="servers" element={<AdminPage.ServersTab />} />
                    </Route>
                  </Route>
                </Routes>
              </AppProviders>
            </Router>
          </QueryClientProvider>
        </DndProvider>
      </PersistGate>
    </Provider>
  );
}
