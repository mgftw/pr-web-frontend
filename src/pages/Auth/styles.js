import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`h-full w-full flex items-center justify-center`};
`;

export const AuthBox = styled.form`
  ${tw`max-w-md mx-auto flex flex-col bg-first rounded-lg shadow-xl object-center w-1/4 py-6 px-8`};
`;

export const Logo = styled.img`
  ${tw`h-20 mt-2 mb-1 object-contain`};
`;

export const Title = styled.div`
  ${tw`font-oswald text-lg pb-2`};
`;

export const Explanation = styled.div`
  ${tw`text-secondary pb-5`};
`;

export const Disclaimer = styled.div`
  ${tw`text-xs text-second pt-5`};
`;

export const Error = styled.div`
  ${tw`text-base text-red`};
`;

export const SteamButton = styled.img`
  ${tw``};
  cursor: pointer;
`;

export const Login = styled.button`
  ${tw`bg-transparent border-none outline-none flex justify-center`};
`;
