import { useEffect } from "react";
import * as sessionActions from "$actions/sessionActions";

import Logo from "$assets/logo.png";
import Steam from "$assets/steam.png";

// Constants
import { API_URL } from "$constants/url";

// Components
import FullScreen from "$components/Layouts/FullScreen";

import * as st from "./styles";
import { useLocation, useNavigate } from "react-router";
import { useDispatch } from "react-redux";

const AuthPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const onSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();

    // @ts-expect-error legacy outside redirect
    window.location = `${API_URL}/auth/steam`;
  };

  useEffect(() => {
    const queryParams = location.search;
    const token = (queryParams ? queryParams.split("token=")[1] : null) || null;

    if (token) {
      dispatch(sessionActions.onAuthSuccess({ token }));
      navigate("/", {
        replace: true,
      });
    }
  }, [location, dispatch, navigate]);

  return (
    <FullScreen hideNavBar>
      <st.Container>
        <st.AuthBox onSubmit={onSubmit}>
          <st.Logo src={Logo} />
          <st.Title>HELLO THERE</st.Title>
          <st.Explanation>
            We need to know who you are before you enter the web control panel
          </st.Explanation>
          <st.Login type="submit">
            <st.SteamButton src={Steam} alt="Sign in through steam" />
          </st.Login>
          <st.Disclaimer>
            You will log in securely through the Steam OpenID service. We will not get sensitive
            information of your account.
          </st.Disclaimer>
        </st.AuthBox>
      </st.Container>
    </FullScreen>
  );
};

export default AuthPage;
