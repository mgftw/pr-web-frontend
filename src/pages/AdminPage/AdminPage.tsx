import React from "react";

import ServersTab from "./ServersTab";
import ToolsTab from "./ToolsTab";
import DataTab from "./DataTab";
import BasePage from "$components/BasePage/BasePage";

const AdminPage: React.FC<object> = () => {
  const options = [
    {
      label: "Servers",
      url: `./servers`,
      regex: RegExp("/admin/servers.*"),
    },
    {
      label: "Data",
      url: `./data`,
      regex: RegExp("/admin/data.*"),
    },
    {
      label: "Tools",
      url: `./tools`,
      regex: RegExp("/admin/tools.*"),
    },
  ];
  return <BasePage icon="icon-admin" title="Admin" tabOptions={options} />;
};

type AdminPageComponent<P = object> = React.FC<P> & {
  ToolsTab: typeof ToolsTab;
  ServersTab: typeof ServersTab;
  DataTab: typeof DataTab;
};

export default AdminPage as AdminPageComponent;
