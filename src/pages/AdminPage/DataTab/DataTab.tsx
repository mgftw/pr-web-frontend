import QuestEditor from "$components/editors/QuestEditor";
import Tabs from "$components/Tabs";
import { useState } from "react";

type EditorType = "quest";

const DataTab = () => {
  const [activeEditor] = useState<EditorType>("quest");

  const editors: { label: string; value: EditorType; Component: React.FC }[] = [
    {
      label: "Quest",
      value: "quest",
      Component: () => <QuestEditor />,
    },
  ];

  const Component = editors.find((editor) => editor.value === activeEditor)?.Component;

  return (
    <div>
      <div className="mb-2">
        <Tabs>
          {editors.map((editor) => (
            <Tabs.Tab isActive={activeEditor === editor.value}>{editor.label}</Tabs.Tab>
          ))}
        </Tabs>
      </div>
      <div>{Component && <Component />}</div>
    </div>
  );
};

export default DataTab;
