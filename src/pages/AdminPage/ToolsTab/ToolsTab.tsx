import { useState } from "react";
import BattlePassTool from "$components/tools/BattlePassTool";
import ItemIconEditorTool from "$components/tools/ItemIconEditorTool";
import * as css from "./styles";
import BuffIconEditorTool from "$components/tools/BuffIconEditorTool";
import NPCIconEditorTool from "$components/tools/NPCIconEditorTool";

const ToolsTab = () => {
  const [currentTool, setCurrentTool] = useState(0);

  const tools = [
    {
      name: "Battle Pass Tool",
      component: <BattlePassTool />,
    },
    {
      name: "Item Icon Editor",
      component: <ItemIconEditorTool />,
    },
    {
      name: "Buff Icon Editor",
      component: <BuffIconEditorTool />,
    },
    {
      name: "NPC Icon Editor",
      component: <NPCIconEditorTool />,
    },
  ];

  return (
    <css.Container>
      <css.ToolList>
        {tools.map((tool, idx) => (
          <li
            key={tool.name}
            onClick={() => setCurrentTool(idx)}
            className={idx === currentTool ? "bg-zero" : "bg-second"}
          >
            {tool.name}
          </li>
        ))}
      </css.ToolList>
      <css.ToolContainer>{tools[currentTool].component}</css.ToolContainer>
    </css.Container>
  );
};

export default ToolsTab;
