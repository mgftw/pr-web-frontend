import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

export const ToolList = styled.ul`
  width: 20rem;
  padding: 0;
  margin: 0;
  box-sizing: border-box;
  list-style: none;
  column-gap: 2;
  height: 100%;

  ${tw`p-4 bg-first`}

  > li {
    ${tw`p-4`}
    cursor: pointer;
    &:hover {
      ${tw`bg-zero`}
    }
  }
`;

export const ToolContainer = styled.div`
  ${tw`p-4`}
  height: 100%;
  width: 100%;
  overflow: hidden;
`;
