import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`flex flex-row pt-5 pl-0 gap-5`};
`;
export const ServerList = styled.div`
  ${tw`flex-auto`};
  display: flex;
  justify-content: center;
  flex: 0.6;

  tbody tr td:nth-child(4) {
    color: transparent;
    text-shadow: 0 0 10px rgba(255, 255, 255, 0.5);
  }

  tbody tr td:nth-child(4):active {
    color: revert;
    text-shadow: none;
  }
`;
export const ServerForm = styled.div`
  ${tw`flex-auto mr-5`};
  flex: 0.4;
`;
export const Form = styled.form`
  ${tw`flex flex-col`}
`;
