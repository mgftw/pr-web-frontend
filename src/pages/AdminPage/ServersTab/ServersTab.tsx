import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query";
import { useForm } from "react-hook-form";

import { toast } from "react-toastify";

import { ADMIN_LIST_SERVERS } from "$constants/queries";

import { listServers, createServer, deleteServer } from "$api/adminApi";

import * as st from "./ServersTab.styles";
import {
  Button,
  CircularProgress,
  Input,
  Select,
  SelectItem,
  Table,
  TableBody,
  TableCell,
  TableColumn,
  TableHeader,
  TableRow,
} from "@nextui-org/react";

const ServerList = () => {
  const queryClient = useQueryClient();
  const {
    data: servers = [],
    isLoading: isLoadingList,
    error: loadingError,
  } = useQuery({
    queryKey: ADMIN_LIST_SERVERS,
    queryFn: () => listServers(),
  });

  const { mutateAsync: doDeleteServer } = useMutation(
    (serverId: number) => deleteServer(serverId),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(ADMIN_LIST_SERVERS);
      },
    },
  );

  const onDelete = async (serverId) => {
    const result = await window.confirm(
      "Are you sure? The server wont be able to use the API anymore. This will render it useless",
    );

    if (result) {
      try {
        await doDeleteServer(serverId);
        toast.success("Server was removed. It wont be able to connect to the API anymore");
      } catch (_) {
        toast.error("Couldnt delete server");
      }
    }
  };

  if (isLoadingList) {
    return (
      <div>
        <CircularProgress />
      </div>
    );
  }

  if (loadingError) return <div>Something went wrong: {loadingError.toString()}</div>;

  if (servers.length === 0) return <div>No servers yet..</div>;

  return (
    <div>
      <Table aria-label="Example static collection table">
        <TableHeader>
          <TableColumn>Name</TableColumn>
          <TableColumn>Region</TableColumn>
          <TableColumn>Scope</TableColumn>
          <TableColumn>Key (Hold Click to Reveal)</TableColumn>
          <TableColumn>Actions</TableColumn>
        </TableHeader>
        <TableBody>
          {servers.map((server) => (
            <TableRow key={server.id}>
              <TableCell>{server?.name}</TableCell>
              <TableCell>{server?.region}</TableCell>
              <TableCell>{server?.type}</TableCell>
              <TableCell>{server?.token}</TableCell>
              <TableCell className="flex gap-1">
                <Button
                  onClick={() => {
                    navigator.clipboard.writeText(server?.token);
                  }}
                  color="secondary"
                  size="sm"
                >
                  Copy Key
                </Button>
                <Button onClick={() => onDelete(server.id)} color="danger" size="sm">
                  Delete
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  );
};

const ServerForm = () => {
  const queryClient = useQueryClient();
  const { register, handleSubmit, reset } = useForm({
    defaultValues: {
      name: "",
      region: null,
      type: null,
    },
  });
  const { mutateAsync: addServer, isLoading: isAddingServer } = useMutation(
    (data) => createServer(data),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(ADMIN_LIST_SERVERS);
      },
    },
  );

  const onAddServer = async (data) => {
    try {
      await addServer(data);
      toast.success("Server created!");
      reset();
    } catch (_e) {
      toast.error("Could not add the server");
    }
  };

  return (
    <form
      className="flex flex-col bg-first p-4 gap-2 rounded-lg"
      onSubmit={handleSubmit(onAddServer)}
    >
      <h3>New server API Key</h3>
      <Input label="Name" {...register("name", { required: true })} />
      <Select
        items={[
          {
            label: "Asia",
            value: "asia",
          },
          {
            label: "USA",
            value: "usa",
          },
          {
            label: "EU",
            value: "eu",
          },
          {
            label: "GLOBAL (Use for bot and scripts)",
            value: "global",
          },
        ]}
        {...register("region", { required: true })}
        label="Region"
        placeholder="Choose a region"
      >
        {(item) => <SelectItem key={item.value}>{item.label}</SelectItem>}
      </Select>

      <Select
        items={[
          {
            label: "Coop",
            value: "coop",
          },
          {
            label: "VS",
            value: "vs",
          },
          {
            label: "Scenario",
            value: "scenario",
          },
          {
            label: "Event",
            value: "event",
          },
          {
            label: "Bot",
            value: "bot",
          },
        ]}
        {...register("type", { required: true })}
        label="Type"
        placeholder="Choose a type"
      >
        {(item) => <SelectItem key={item.value}>{item.label}</SelectItem>}
      </Select>
      <Button
        className="mt-5"
        type="submit"
        disabled={isAddingServer}
        color="primary"
        isLoading={isAddingServer}
      >
        {isAddingServer ? "Creating server..." : "Create"}
      </Button>
    </form>
  );
};

const ServersTab = () => {
  return (
    <st.Container>
      <st.ServerList>
        <ServerList />
      </st.ServerList>
      <st.ServerForm>
        <ServerForm />
      </st.ServerForm>
    </st.Container>
  );
};

export default ServersTab;
