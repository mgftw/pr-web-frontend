import AdminPage from "./AdminPage";
import AdmingPage from "./AdminPage";
import ServersTab from "./ServersTab/ServersTab";
import DataTab from "./DataTab";
import ToolsTab from "./ToolsTab/ToolsTab";

AdmingPage.ServersTab = ServersTab;
AdminPage.ToolsTab = ToolsTab;
AdminPage.DataTab = DataTab;

export default AdminPage;
