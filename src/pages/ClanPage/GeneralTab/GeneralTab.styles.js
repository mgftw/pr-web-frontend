import styled from "styled-components";
import tw from "twin.macro";
import hdFortress from "$assets/fortress-hd.png";
import prisonFortress from "$assets/fortress-prison.png";
import { FORTRESS_TYPES } from "$constants/types";

export const Container = styled.section`
  ${tw`mr-4 h-full`}
  height: 100%;

  display: grid;
  grid-template-rows: min-content 1fr;
  grid-gap: 1.5rem;
  overflow: auto;
`;

export const Modules = styled.div`
  display: grid;
  grid-template-columns: 0.4fr 0fr;
  grid-gap: 1.5rem;
  overflow: auto;
`;

export const MemberListContainer = styled.ul`
  ${tw`p-0 m-0`}
  list-style: none;
`;

export const MemberRow = styled.li`
  ${tw`mb-4`}

  p {
    ${tw`p-0 m-0`}
  }

  > p {
    &:last-child {
      ${tw`text-secondary font-normal text-xs`}
    }
  }
  display: grid;
  align-items: center;
  grid-template-columns:
    minmax(1rem, 0.05fr) 1fr minmax(1.5rem, 0.1fr)
    minmax(5rem, 0.2fr);
  grid-gap: 0.5rem;
`;

export const Position = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  > * {
    margin-right: 0.5rem;
    &:last-child {
      margin: 0;
    }
  }
`;

export const ClassEmblem = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  > * {
    margin-right: 0.5rem;
    &:last-child {
      margin: 0;
    }
  }
`;
export const MemberCard = styled.div`
  ${tw`rounded-md bg-second p-3`}
  display: grid;
  grid-template-columns: min-content 1fr;
  img {
    ${tw`h-10 w-10 rounded-full bg-tertiary`}
    object-fit: contain;
    padding: 2px;

    &:before {
      content: url("image.jpg");
    }

    &:after {
      content: "(url: " attr(src) ")";
    }
  }

  > div {
    ${tw`flex flex-col ml-4 text-sm`}
    p {
      ${tw`text-base flex justify-end`}
    }
    bold {
      ${tw`flex-grow`}
    }
    small {
      ${tw`text-secondary`}
    }
  }

  p {
    ${tw`m-0 p-0 mb-2`}
  }
`;

export const ClanInfoContainer = styled.div`
  ${tw`rounded-lg bg-second p-4 px-16`}
  display: grid;
  grid-template-columns: min-content 1fr min-content min-content;
`;

export const ClanAvatar = styled.div`
  position: relative;
  img {
    ${tw`h-32 w-32 rounded-full bg-tertiary border-solid border-8 border-primary`}
    object-fit: contain;

    &:before {
      content: url("image.jpg");
    }

    &:after {
      content: "(url: " attr(src) ")";
    }
  }

  button {
    ${tw`rounded-full bg-zero text-zero h-8 w-8 border-none flex items-center justify-center`}
    position: absolute;
    bottom: 0;
    right: 0;
    transition: transform 150ms;

    &:hover {
      transform: translateY(-1px);
    }
  }
`;
export const ClanInfo = styled.div`
  ${tw`ml-8`}
  h3 {
    ${tw`text-2xl uppercase font-oswald p-0 m-0 mb-4`}
  }
`;
export const ClanFear = styled.div`
  ${tw`h-full flex flex-col mx-16`}

  > div {
    ${tw`flex-grow my-2`}
    > * {
      ${tw`flex-grow`}
    }
  }

  p {
    ${tw`m-0 p-0 flex flex-col`}
  }
`;
export const ClanFortressInfo = styled.div`
  ${tw`flex flex-col items-end justify-center`}
`;

export const ImageContainer = styled.div`
  ${tw`w-64 h-20 flex flex-col items-center justify-center bg-first rounded-lg`}

  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;

  ${({ fortress }) => {
    switch (fortress) {
      case FORTRESS_TYPES.PRISON:
        return `background-image: url(${prisonFortress});`;
      case FORTRESS_TYPES.HELMS_DEEP:
        return `background-image: url(${hdFortress});`;
      default:
        return "";
    }
  }}

  h5 {
    ${tw`font-oswald text-xl uppercase text-white m-0 p-0`}
  }
`;
export const Badges = styled.div`
  ${tw`flex mt-2`}

  > * {
    ${tw`mr-2`}

    &:last-child {
      ${tw`mr-0`}
    }
  }
`;

export const ClanCurrency = styled.div`
  ${tw`flex mt-4`}

  p {
    ${tw`m-0 p-0 flex-grow`}
  }

  > * {
    ${tw`mr-2`}

    &:last-child {
      ${tw`mr-0`}
    }
  }
`;

export const AvatarDrop = styled.div`
  ${tw`border-dotted border-4 border-primary rounded-md bg-zero py-16 flex items-center justify-center flex-col font-bold`}
  cursor: pointer;
  i {
    ${tw`mb-4 mt-8`}
  }

  p {
    ${tw`m-0 p-0 w-48 text-center`}
  }
`;

export const AvatarPreview = styled.div`
  ${tw`flex flex-col items-center justify-center pt-4`}
  h6 {
    ${tw`text-base m-0 p-0`}
  }
  img {
    ${tw`h-10 w-10 rounded-full bg-tertiary mb-4 mt-4`}
    object-fit: contain;
  }
`;
