/* eslint-disable camelcase */
import React, { useMemo, useCallback, useState } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { useMutation, useQuery } from "@tanstack/react-query";
import { RiVipCrown2Fill, RiUser5Fill, RiPencilFill, RiImageFill } from "react-icons/ri";
import { GiGhost, GiLockedFortress } from "react-icons/gi";
import { Tooltip } from "react-tippy";
import { toast } from "react-toastify";
import { useDropzone } from "react-dropzone";
import { useMyProfile } from "$hooks/useMyProfile";
import { changeClanAvatar, getClanInfo, getClanMembers } from "$api/clanApi";
import {
  CLAN_INFO as QUERY_CLAN_INFO,
  CLAN_MEMBER_LIST as QUERY_CLAN_MEMBER_LIST,
} from "$constants/queries";

import ModuleCard from "$components/ModuleCard";

import * as st from "./GeneralTab.styles";
import { CLAN_POSITIONS, FORTRESS_TYPES } from "$constants/types";
import ClassEmblem from "$components/ClassEmblem";
import ProgressBar from "$components/ProgressBar";
import VerticalProgressBar from "$components/VerticalProgressBar";
import Loader from "$components/Loader";
import Badge from "$atoms/Badge";
import { formatCurrency } from "$helpers/currencyHelper";
import { getRequiredClanLevelUpRequirement } from "$helpers/statsHelper";
import colors from "$constants/colors";
import Modal from "$components/Modal";
import Button from "$components/Button";
import Hint from "$components/Hint";

const getClanPositionName = (position) => {
  switch (position) {
    case CLAN_POSITIONS.LEADER:
      return "Leader";
    case CLAN_POSITIONS.CO_LEADER:
      return "Co-Leader";
    case CLAN_POSITIONS.MEMBER:
      return "Member";
    default:
      return null;
  }
};

const getFortressName = (fortress) => {
  switch (fortress) {
    case FORTRESS_TYPES.PRISON:
      return "Prison";
    case FORTRESS_TYPES.HELMS_DEEP:
      return "Helms Deep";
    default:
      return null;
  }
};

const MemberRow = ({ member }) => (
  <st.MemberRow>
    <st.Position>
      <Tooltip title={getClanPositionName(member.clan_pos)}>
        {member.clan_pos === CLAN_POSITIONS.LEADER && (
          <RiVipCrown2Fill size={16} color={colors.primary} />
        )}
        {member.clan_pos === CLAN_POSITIONS.CO_LEADER && (
          <RiVipCrown2Fill size={16} color={colors.secondary} />
        )}
        {member.clan_pos === CLAN_POSITIONS.MEMBER && (
          <RiUser5Fill size={16} color={colors.secondary} />
        )}
      </Tooltip>
    </st.Position>
    <st.MemberCard>
      <img src={member.avatar_url} alt="" />
      <div>
        <p>
          <bold>{member.name}</bold> <small>Lv. {member.level}</small>
        </p>
        <ProgressBar height={10} value={0} showPercent />
      </div>
    </st.MemberCard>
    <st.ClassEmblem>
      <Tooltip title={member?.class?.name ?? "No class"}>
        {member?.class?.id ? (
          <ClassEmblem classId={member?.class?.id} size={24} />
        ) : (
          <p>{member?.class?.name ?? "-"}</p>
        )}
      </Tooltip>
    </st.ClassEmblem>

    <p>{member.last_active ? moment(member.last_active).fromNow() : "Never"}</p>
  </st.MemberRow>
);

MemberRow.propTypes = {
  member: PropTypes.shape({
    avatar_url: PropTypes.string,
    name: PropTypes.string,
    clan_pos: PropTypes.number,
    level: PropTypes.number,
    last_active: PropTypes.string,
    class: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    }),
  }).isRequired,
};

const GeneralTab = () => {
  const [avatarModal, setAvatarModal] = useState(false);
  const [avatarPreview, setAvatarPreview] = useState(null);
  const { profile, isLoading: isLoadingProfile } = useMyProfile();
  const { data: clan = null, isLoading: isLoadingClan } = useQuery({
    queryKey: QUERY_CLAN_INFO(profile?.clan_id),
    queryFn: () => getClanInfo(profile?.clan_id),
    enabled: !!profile?.clan_id,
  });

  const { data: clanMembers = [] } = useQuery(
    QUERY_CLAN_MEMBER_LIST(clan?.clan_id),
    () => getClanMembers(clan?.clan_id),
    {
      enabled: Boolean(clan),
    },
  );

  const { mutateAsync: doChangeAvatar, isLoading: isChangingAvatar } = useMutation((file) =>
    changeClanAvatar(profile?.clan_id, file),
  );

  const sortedList = useMemo(
    () =>
      clanMembers.sort((a, b) => {
        const diff = b.clan_pos - a.clan_pos;

        if (diff === 0) {
          return b.level - a.level;
        }
        return diff;
      }),
    [clanMembers],
  );

  const requiredInfluence = getRequiredClanLevelUpRequirement(clan?.level ?? 1);

  const influencePercent = Math.round(
    clan?.level === 20 ? 100 : (clan?.influence ?? 0 / requiredInfluence) * 100,
  );
  const onUpload = useCallback(async () => {
    try {
      await doChangeAvatar(avatarPreview);
      toast.success("Avatar changed! It might take a while to reflect in the UI");
      setAvatarModal(false);
      setAvatarPreview(null);
    } catch (error) {
      toast.error(`Could not change avatar: ${error.toString()}`);
    }
  }, [doChangeAvatar, avatarPreview]);
  const onDrop = useCallback((acceptedFiles) => {
    const file = acceptedFiles[0];

    setAvatarPreview(Object.assign(file, { preview: URL.createObjectURL(file) }));
  }, []);

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: "image/jpeg, image/png",
    maxFiles: 1,
    maxSize: 8 * 1024 * 1024,
    multiple: false,
  });

  if (!isLoadingProfile && profile?.clan_id === 0) {
    return (
      <st.Container>
        <p>
          You are not part of a clan. Visit our{" "}
          <a href="https://discord.gg/n97ZSX7">Discord Server</a> to find one
        </p>
      </st.Container>
    );
  }

  return (
    <st.Container>
      <Modal isOpen={avatarModal}>
        <Modal.Header showCloseButton onRequestClose={() => setAvatarModal(false)}>
          Change Clan Avatar
        </Modal.Header>
        <Modal.Body>
          {isChangingAvatar ? (
            <Loader />
          ) : (
            <>
              <st.AvatarDrop {...getRootProps()}>
                <input {...getInputProps()} />
                <i>
                  <RiImageFill size={50} color={colors.secondary} />
                </i>
                <p>Drag an image here or click to open file selector</p>
              </st.AvatarDrop>
              {avatarPreview && (
                <st.AvatarPreview>
                  <h6>Preview</h6>
                  <img src={avatarPreview.preview} alt="" />
                  <Button type="button" onClick={onUpload}>
                    Use it
                  </Button>
                </st.AvatarPreview>
              )}
            </>
          )}
        </Modal.Body>
      </Modal>
      {isLoadingProfile || isLoadingClan ? (
        <Loader />
      ) : (
        <>
          <st.ClanInfoContainer>
            <st.ClanAvatar>
              <img src={clan.avatar_url?.large} alt="" />
              {profile.clan_pos >= CLAN_POSITIONS.CO_LEADER && (
                <button onClick={() => setAvatarModal(true)} type="button">
                  <RiPencilFill size={18} color={colors.primary} />
                </button>
              )}
            </st.ClanAvatar>
            <st.ClanInfo>
              <h3>{clan.name}</h3>
              <Tooltip
                html={
                  <Hint title="Influence">
                    Level up your clan with influence to unlock features and more member slots
                  </Hint>
                }
              >
                <ProgressBar height={24} value={influencePercent} showPercent />
              </Tooltip>
              <st.ClanCurrency>
                <p>Lv. {clan.level}</p>
                <Tooltip
                  html={
                    <Hint title="Victory Points" icon="icon-trophy">
                      Your clan earns these points by performing impressive actions, such as winning
                      battles and completing hard scenarios together
                    </Hint>
                  }
                >
                  <Badge icon="icon-trophy" backgroundColor="zero">
                    {formatCurrency(clan.victory_points)}
                  </Badge>
                </Tooltip>
                <Tooltip
                  html={
                    <Hint title="Clan Cash" icon="icon-cash">
                      You can transfer cash to your clan so Leaders and Co-Leaders can purchase more
                      benefits for you and other members
                    </Hint>
                  }
                >
                  <Badge icon="icon-cash" backgroundColor="zero">
                    {formatCurrency(clan.cash)}
                  </Badge>
                </Tooltip>
              </st.ClanCurrency>
            </st.ClanInfo>
            <Tooltip
              html={
                <Hint title="Fear" renderIcon={() => <GiGhost />}>
                  Some actions performed by your clan can easily scare survivors and could result in
                  them declaring war against you. To maintain peace, some actions cannot be done if
                  your clan is too feared by the people
                </Hint>
              }
            >
              <st.ClanFear>
                <small>{clan.fear}%</small>
                <div>
                  <VerticalProgressBar width={16} value={clan.fear} />
                </div>
                <p>
                  <GiGhost />
                </p>
              </st.ClanFear>
            </Tooltip>
            <st.ClanFortressInfo>
              <Tooltip
                html={
                  <Hint title="Fortress" renderIcon={() => <GiLockedFortress />}>
                    A fortress allows your clan members to start certain scenarios for free, plus
                    some other benefits
                  </Hint>
                }
              >
                <st.ImageContainer fortress={clan.fortress}>
                  {clan.fortress > 0 ? (
                    <h5>{getFortressName(clan.fortress)}</h5>
                  ) : (
                    <p>No fortress</p>
                  )}
                </st.ImageContainer>
              </Tooltip>
              <st.Badges>
                <Tooltip
                  html={
                    <Hint title="Community Points" icon="icon-peace">
                      Collect points by playing <strong>COOP</strong> games with your clan. Being
                      the clan with the most community points will allow your clan to take over the{" "}
                      <strong>Prison Fortress</strong>
                    </Hint>
                  }
                >
                  <Badge icon="icon-peace" backgroundColor="green">
                    {clan.community_points}
                  </Badge>
                </Tooltip>
                <Tooltip
                  html={
                    <Hint title="Domination Points" icon="icon-lightning">
                      Collect points by killing tanks with your clan. Being the clan with the most
                      Domination Points will allow your clan to take over the{" "}
                      <strong>Helms Deep Fortress</strong>
                    </Hint>
                  }
                >
                  <Badge icon="icon-lightning" backgroundColor="red">
                    {clan.domination_points}
                  </Badge>
                </Tooltip>
              </st.Badges>
            </st.ClanFortressInfo>
          </st.ClanInfoContainer>

          <st.Modules>
            <ModuleCard title="Members">
              <st.MemberListContainer>
                {sortedList.map((member) => (
                  <MemberRow key={member.steam_id} member={member} />
                ))}
              </st.MemberListContainer>
            </ModuleCard>
          </st.Modules>
        </>
      )}
    </st.Container>
  );
};

export default GeneralTab;
