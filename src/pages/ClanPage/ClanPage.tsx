import BasePage from "$components/BasePage";
import GeneralTab from "./GeneralTab/GeneralTab";

const ClanPage: React.FC<object> = () => {
  const options = [
    {
      label: "General",
      url: `./`,
      regex: RegExp("/clan$"),
    },
  ];
  return <BasePage icon="icon-flag" title="Clan" tabOptions={options} />;
};

type ClanPageComponent<P = object> = React.FC<P> & {
  GeneralTab: typeof GeneralTab;
};

export default ClanPage as ClanPageComponent;
