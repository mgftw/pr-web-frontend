import FullScreen from "$components/Layouts/FullScreen";

import BlackMarketDetails from "$components/BlackMarketDetails";
import EventDetails from "$components/EventBanner";

import * as st from "./Dashboard.styles";
import PageTitle from "$components/PageTitle";
import FortressOccupation from "./FortressOccupation";
import ClanRanking from "./ClanRanking";

const Dashboard = () => {
  return (
    <FullScreen>
      <st.Container>
        <PageTitle
          icon="icon-home"
          title="Home"
          description="Quickly find the most relevant information about you and the community"
        />
        <st.Content>
          <st.LeftContent>
            <EventDetails />
            <FortressOccupation />
            <BlackMarketDetails />
          </st.LeftContent>
          <st.RightContent>
            <ClanRanking />
          </st.RightContent>
        </st.Content>
      </st.Container>
    </FullScreen>
  );
};

export default Dashboard;
