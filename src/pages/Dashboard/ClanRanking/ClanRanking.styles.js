import styled from "styled-components";
import tw from "twin.macro";

export const Position = styled.li`
  ${tw`gap-8 flex-auto`}
  grid-template-columns: 2rem 1fr;

  > p {
    ${tw`font-karla rounded-full h-8 w-8 bg-primary text-first flex items-center justify-center font-bold`}
  }
`;

export const RankList = styled.ul`
  ${tw`p-0 m-1 mt-2`}
  list-style: none;
  flex: 1 1 50%;
  li {
    ${tw`mb-4`}
    &:last-child {
      ${tw`mb-0`}
    }
  }
  h4 {
    margin-top: 0;
    margin-bottom: 1rem;
    text-align: center;
  }
`;

export const ClanCardContainer = styled.div`
  ${tw`bg-first gap-5 rounded-md p-2 pb-0`}
  display: grid;
  grid-template-columns: min-content 1fr min-content;
  position: relative;
`;

export const RankPosition = styled.p`
  ${tw`absolute rounded-full text-center bottom-0 right-0 h-8 w-8 text-second flex items-center justify-center mr-4 mb-2 font-bold`}
`;

export const ClanCardAvatar = styled.div`
  img {
    ${tw`h-20 w-20 rounded-full bg-secondary`}
    object-fit: contain;
  }
`;

export const ClanCardInfo = styled.div`
  ${tw`flex flex-col`}

  h2 {
    ${tw`m-0 p-0 text-lg font-oswald uppercase`}
  }

  h5 {
    ${tw`m-0 p-0 text-base mb-1 mt-1`}
  }

  p {
    ${tw`m-0 p-0 text-quaternary text-sm`}
  }
`;

export const Badges = styled.div`
  ${tw`gap-1`}
  display: grid;
  grid-template-rows: 1fr 1fr 1fr;
`;

export const ClanRankMain = styled.div`
  ${tw`bg-second gap-4 rounded-md p-2`}
`;

export const ClanRankHeader = styled.div`
  ${tw`text-left font-bold text-primary text-xl -mt-1 -mb-1`}
  h3 {
    margin: 0.5rem;
  }
`;

export const RankHead = styled.div`
  ${tw`rounded-full`}
  ${({ backgroundColor }) => `
    background-color: ${backgroundColor};
  `}
`;

export const ClanRankBody = styled.div`
  ${tw`flex`}
`;

export const NoRank = styled.div`
  ${tw`bg-first text-quaternary text-center rounded-md italic p-2`}
`;
