/* eslint-disable camelcase */
import React, { useMemo, useState } from "react";
import PropTypes from "prop-types";
import { useQuery } from "@tanstack/react-query";
import { Tooltip } from "react-tippy";
import {
  GLOBAL_CLAN_RANK_COMMUNITY_POINTS,
  GLOBAL_CLAN_RANK_DOMINATION_POINTS,
} from "$constants/queries";
import { getClanRanking } from "$api/serverApi";

import * as st from "./ClanRanking.styles";
import Loader from "$components/Loader";
import Badge from "$atoms/Badge";
import Hint from "$components/Hint";
import defaultAvatar from "$assets/default-clan-avatar.png";
import colors from "$constants/colors";

const ClanCard = ({ clan, column }) => {
  const [fallback, setFallback] = useState(false);
  return (
    <st.ClanCardContainer>
      <st.ClanCardAvatar>
        <img
          src={fallback ? defaultAvatar : clan?.avatar_url?.medium}
          alt=""
          onError={() => setFallback(true)}
        />
      </st.ClanCardAvatar>
      <st.ClanCardInfo>
        <h2>{clan?.name ?? "--"}</h2>
        <h5>Lv. {clan?.level}</h5>
        <p>{(clan?.members ?? []).length} member(s)</p>
      </st.ClanCardInfo>
      <st.Badges>
        {/* <Tooltip
          html={
            <Hint title="Victory Points" icon="icon-trophy">
              Your clan earns these points by performing impressive actions,
              such as winning battles and completing hard scenarios together
            </Hint>
          }
        >
          <Badge icon="icon-trophy" backgroundColor="zero">
            {formatCurrency(clan.victory_points)}
          </Badge>
        </Tooltip> */}
        {column == 0 && (
          <Badge icon="icon-peace" backgroundColor="green">
            {clan.community_points}
          </Badge>
        )}
        {column == 1 && (
          <Badge icon="icon-lightning" backgroundColor="red">
            {clan.domination_points}
          </Badge>
        )}
      </st.Badges>
      <st.RankPosition>{/* {index} */}</st.RankPosition>
    </st.ClanCardContainer>
  );
};

ClanCard.propTypes = {
  clan: PropTypes.shape({
    avatar_url: PropTypes.shape({
      medium: PropTypes.string,
    }),
    name: PropTypes.string,
    level: PropTypes.number,
    members: PropTypes.arrayOf(PropTypes.shape({})),
    domination_points: PropTypes.number,
    community_points: PropTypes.number,
    victory_points: PropTypes.number,
  }).isRequired,
};

const ClanRanking = () => {
  const { data: communityPointsRank = [], isLoading: isLoadingCommunity } = useQuery({
    queryKey: GLOBAL_CLAN_RANK_COMMUNITY_POINTS,
    queryFn: () => getClanRanking("community_points"),
  });

  const { data: dominationPointsRank = [], isLoading: isLoadingDomination } = useQuery({
    queryKey: GLOBAL_CLAN_RANK_DOMINATION_POINTS,
    queryFn: () => getClanRanking("domination_points"),
  });

  const comRank = useMemo(() => {
    return communityPointsRank;
  }, [communityPointsRank]);

  const domRank = useMemo(() => {
    return dominationPointsRank;
  }, [dominationPointsRank]);

  const isLoading = useMemo(() => {
    return isLoadingDomination || isLoadingCommunity;
  }, [isLoadingDomination, isLoadingCommunity]);

  return (
    <>
      {isLoading ? (
        <st.ClanRankMain>
          <st.ClanRankHeader>
            <h3>Clan Fortress Ranking</h3>
          </st.ClanRankHeader>
          <Loader />
        </st.ClanRankMain>
      ) : (
        <st.ClanRankMain>
          <st.ClanRankHeader>
            <h3>Clan Fortress Ranking</h3>
          </st.ClanRankHeader>
          <st.ClanRankBody>
            <st.RankList>
              <Tooltip
                html={
                  <Hint title="Community Points" icon="icon-peace">
                    Collect points by <strong>playing COOP</strong> games with your clan. Being the
                    clan with the most community points will allow your clan to take over the{" "}
                    <strong>Prison Fortress</strong>
                  </Hint>
                }
              >
                <st.RankHead backgroundColor={colors.green}>
                  <h4>Community Points</h4>
                </st.RankHead>
              </Tooltip>

              {comRank.map((clan, index) => (
                <st.Position key={clan.clan_id}>
                  {/* <p>{index + 1}</p> */}
                  <ClanCard clan={clan} index={index + 1} column={0} />
                </st.Position>
              ))}
              {comRank.length === 0 && <st.NoRank>No clans in the rankings yet</st.NoRank>}
            </st.RankList>
            <st.RankList>
              <Tooltip
                html={
                  <Hint title="Domination Points" icon="icon-lightning">
                    Collect points by <strong>defeating Tanks</strong> in COOP with your clan. Being
                    the clan with the most Domintation Points will allow your clan to take over the{" "}
                    <strong>Helms Deep Fortress</strong>
                  </Hint>
                }
              >
                <st.RankHead backgroundColor={colors.red}>
                  <h4>Domination Points</h4>
                </st.RankHead>
              </Tooltip>
              {domRank.map((clan, index) => (
                <st.Position key={clan.clan_id}>
                  {/* <p>{index + 1}</p> */}
                  <ClanCard clan={clan} index={index + 1} column={1} />
                </st.Position>
              ))}
              {domRank.length === 0 && <st.NoRank>No clans in the rankings yet</st.NoRank>}
            </st.RankList>
          </st.ClanRankBody>
        </st.ClanRankMain>
      )}
    </>
  );
};

export default ClanRanking;
