import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`pt-5 pl-0 mx-5 w-full`};
  display: grid;
  grid-template-rows: min-content 1fr;
`;

export const Content = styled.div`
  ${tw`pt-3 h-full gap-5`}
  overflow: auto;
  display: grid;
  grid-template-columns: 3fr 7fr;
`;

export const LeftContent = styled.div`
  ${tw`flex flex-col`}

  > * {
    ${tw`mb-5`}
    &:last-child {
      ${tw`mb-0`}
    }
  }
`;

export const RightContent = styled.div`
  ${tw`flex flex-col pb-16`}
`;
