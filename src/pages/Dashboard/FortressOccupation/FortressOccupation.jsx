/* eslint-disable camelcase */
import React from "react";
import PropTypes from "prop-types";
import { useQuery } from "@tanstack/react-query";
import { Tooltip } from "react-tippy";
import { GiHandcuffs, GiWizardStaff } from "react-icons/gi";
import { getFortressOccupation } from "$api/serverApi";
import { GLOBAL_FORTRESS_OCCUPATION } from "$constants/queries";

import * as st from "./FortressOccupation.styles";
import { FORTRESS_TYPES } from "$constants/types";
import Hint from "$components/Hint";

const Fortress = ({ fortress, fortressName, fortressId }) => (
  <st.Fortress>
    <st.FortressDecor fortressId={fortressId}>
      <h4>{fortressName}</h4>
    </st.FortressDecor>
    <st.ClanInfo>
      {fortress?.clan && <img src={fortress?.clan?.avatar_url?.small} alt="" />}
      {fortress?.clan?.name ? <p>{fortress.clan.name}</p> : <small>Not taken</small>}
    </st.ClanInfo>
  </st.Fortress>
);

Fortress.propTypes = {
  fortress: PropTypes.shape({
    clan: PropTypes.shape({
      name: PropTypes.string,
      avatar_url: PropTypes.shape({
        small: PropTypes.string,
      }),
    }),
  }),
  fortressName: PropTypes.string.isRequired,
  fortressId: PropTypes.number.isRequired,
};

const FortressOccupation = () => {
  const { data: occupation = [] } = useQuery({
    queryKey: GLOBAL_FORTRESS_OCCUPATION,
    queryFn: () => getFortressOccupation(),
  });

  return (
    <st.Container>
      <st.OccupationHeader>Clan Fortress Occupation</st.OccupationHeader>
      <Tooltip
        html={
          <Hint title="Prison Fortress" renderIcon={() => <GiHandcuffs size={16} />}>
            Gives your clan 3 free tries at the <strong>Prison</strong> scenario,{" "}
            <strong>100% bonus EXP</strong> and gives members the <strong>Contributor</strong>{" "}
            title. Additionally provides the clan with <strong>Tank Spawn</strong> items and{" "}
            <strong>10% discount</strong> in the FR-Pill Market
          </Hint>
        }
      >
        <Fortress
          fortress={occupation.find((fortress) => fortress.fortress_id === FORTRESS_TYPES.PRISON)}
          fortressName="Prison"
          fortressId={FORTRESS_TYPES.PRISON}
        />
      </Tooltip>
      <Tooltip
        html={
          <Hint title="Helms Deep" renderIcon={() => <GiWizardStaff size={16} />}>
            Gives your clan 3 free tries at the <strong>Hell in the deeps</strong> scenario,{" "}
            <strong>100% bonus Cash</strong> and gives members the <strong>Dominator</strong> title.
            Additionally provides the clan with <strong>Tank Spawn</strong> items and{" "}
            <strong>10% discount</strong> in the FR-Pill Market
          </Hint>
        }
      >
        <Fortress
          fortress={occupation.find(
            (fortress) => fortress.fortress_id === FORTRESS_TYPES.HELMS_DEEP,
          )}
          fortressName="Helms Deep"
          fortressId={FORTRESS_TYPES.HELMS_DEEP}
        />
      </Tooltip>
    </st.Container>
  );
};

export default FortressOccupation;
