import styled from "styled-components";
import tw from "twin.macro";
import hdFortress from "$assets/fortress-hd.png";
import prisonFortress from "$assets/fortress-prison.png";
import { FORTRESS_TYPES } from "$constants/types";

export const Container = styled.div`
  ${tw`p-4 bg-second rounded-md`}

  display: grid;
  grid-template-rows: auto auto;
  grid-template-columns: 1fr 1fr;
  grid-gap: 1rem;
`;
export const Fortress = styled.div`
  ${tw`rounded-md bg-quaternary flex flex-col pb-2`}
  overflow: hidden;
`;
export const FortressDecor = styled.div`
  ${tw`bg-zero h-12 flex items-center justify-center font-oswald uppercase text-white`}
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  ${({ fortressId }) => {
    switch (fortressId) {
      case FORTRESS_TYPES.PRISON:
        return `background-image: url(${prisonFortress});`;
      case FORTRESS_TYPES.HELMS_DEEP:
        return `background-image: url(${hdFortress});`;
      default:
        return "";
    }
  }};
`;
export const ClanInfo = styled.div`
  ${tw`mt-2 flex items-center justify-center h-8`}

  img {
    ${tw`rounded-full h-8 w-8 bg-primary mr-2`}
    object-fit: contain;
    outline: none;
  }

  p {
    ${tw`text-sm text-primary m-0 p-0 font-bold`}
  }

  small {
    ${tw`text-sm text-secondary m-0 p-0 font-bold`}
  }
`;

export const OccupationHeader = styled.div`
  ${tw`text-left font-bold text-primary text-xl -mt-1 -mb-1`}
  grid-column: span 2;
`;
