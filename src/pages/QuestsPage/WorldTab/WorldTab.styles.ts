import styled, { css } from "styled-components";

import tw from "twin.macro";
import decorAsset from "$assets/quest-decor.png";

import pillsAsset from "$assets/feature-pills.png";
import radioAsset from "$assets/feature-radio.png";
import boxAsset from "$assets/feature-box.png";

export const Container = styled.div`
  ${tw`pb-32`}
  background-image: url(${decorAsset});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  height: 100%;
  display: flex;
  justify-content: center;
  overflow-y: scroll;
`;

export const Content = styled.div`
  ${tw`flex-col items-center`}
  max-width: 50rem;
  padding-bottom: 10rem;
`;
export const Header = styled.h1`
  ${tw`font-karla text-3xl text-red font-bold mt-8 text-center`}
`;

export const SubHeader = styled.h2`
  ${tw`font-karla text-2xl text-primary font-bold mt-2 text-center mb-16`}
`;

export const CardsContainer = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
  li {
    margin-bottom: 5rem;
  }
`;

export const Card = styled.li<{ $leftImage?: boolean }>(({ $leftImage = false }) => [
  css`
    list-style: none;
    padding: 0;
    margin: 0;
    position: relative;
  `,
  tw`rounded-lg bg-second p-4`,
  `${$leftImage ? `padding-left: 5rem;` : `padding-right: 5rem;`}`,
]);

export const CardTitle = styled.h4`
  ${tw`p-0 m-0 text-red font-oswald font-bold text-xl uppercase`}
`;
export const CardBody = styled.p`
  ${tw`p-0 mt-2`}
`;

const BaseIllustration = styled.div`
  background-position: center;
  background-size: contain;
  background-repeat: no-repeat;
  position: absolute;
`;

export const PillsIllustration = styled(BaseIllustration)`
  height: 16rem;
  width: 16rem;
  transform: translate(8rem, -4rem);
  right: 0;
  top: 0;
  background-image: url(${pillsAsset});
`;
export const RadioIllustration = styled(BaseIllustration)`
  height: 16rem;
  width: 16rem;
  transform: translate(-15rem, -2rem);
  right: left;
  top: 0;
  background-image: url(${radioAsset});
`;
export const SupplyIllustration = styled(BaseIllustration)`
  height: 16rem;
  width: 16rem;
  transform: translate(8rem, -4rem);
  right: 0;
  top: 0;
  background-image: url(${boxAsset});
`;
