import * as st from "./WorldTab.styles";

const WorldTab = () => (
  <st.Container>
    <st.Content>
      <st.Header>THE GREEN FLU HAS TAKEN OVER</st.Header>
      <st.SubHeader>But humanity refuses to die out</st.SubHeader>

      <st.CardsContainer>
        <st.Card>
          <st.CardTitle>Immunity</st.CardTitle>
          <st.CardBody>
            You are immune to the flu, but others aren’t. Those who are still alive periodically
            take special pills <b>(FR-Pills)</b> that keep the infection and mutations at bay. These
            pills have to be created from raw infected flesh, which is a risky process for
            non-immune survivors.
          </st.CardBody>
          <st.PillsIllustration />
        </st.Card>

        <st.Card $leftImage>
          <st.CardTitle>Radio</st.CardTitle>
          <st.CardBody>
            Survivors organize themselves in communities to survive, and there are those who are
            still isolated. You can talk to them through the radio to offer them help with things
            they might need. Most survivors understand the value of immune survivors as you are the
            only ones who can fight the infected for longs periods of time
          </st.CardBody>
          <st.RadioIllustration />
        </st.Card>

        <st.Card>
          <st.CardTitle>Immunity</st.CardTitle>
          <st.CardBody>
            A trade system has been established through high technology military supply boxes.
            People leave stuff inside, and others leave it out. You can use it to leave items and
            supplies for the survivors you help. We don’t recommend taking stuff that isn’t yours,
            or you will enter the KOS making you pretty much an enemy to everyone
          </st.CardBody>
          <st.SupplyIllustration />
        </st.Card>
      </st.CardsContainer>
    </st.Content>
  </st.Container>
);

export default WorldTab;
