import useMyQuestProgress from "$hooks/useMyQuestProgress";
import { useMemo, useState } from "react";
import NPCCard from "./NPCCard";
import decorAsset from "$assets/quest-decor.png";
import styled from "styled-components";
import tw from "twin.macro";
import QuestCard from "./QuestCard";
import NPCIcon from "$components/NPCIcon/NPCIcon";
import { QuestState } from "types/entities";
import Tabs from "$components/Tabs";
import { useMyProfile } from "$hooks/useMyProfile";

const Container = styled.div`
  ${tw`pb-32 pl-16 pr-16 pt-8`}
  background-image: url(${decorAsset});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  height: 100%;
  display: flex;
  gap: 3rem;
`;

const NPCList = styled.div`
  width: 20rem;
  display: flex;
  flex-direction: column;
  gap: 2rem;
  height: 100%;
  overflow-y: auto;
  padding-top: 2rem;
  padding-right: 2rem;
`;

const NPCContent = styled.div`
  ${tw`bg-second rounded-xl overflow-y-scroll`}
  flex: 1;
`;

const ProgressTab = () => {
  const [filter, setFilter] = useState("new");
  const [currentNPCId, setCurrentNPCId] = useState<number>(1);
  const progress = useMyQuestProgress();
  const me = useMyProfile();

  const currentNpc = useMemo(
    () => progress.find((npcProgress) => npcProgress.id === currentNPCId),
    [currentNPCId, progress],
  );

  const classified = useMemo(() => {
    const inProgress = currentNpc.quests
      .filter(
        (quest) =>
          quest.progress &&
          quest.progress.quest_state.id !== QuestState.Done &&
          quest.required_level !== 5000 &&
          quest.required_level !== 500,
      )
      .sort((a, b) => a.required_level - b.required_level);
    const finished = currentNpc.quests
      .filter(
        (quest) =>
          quest.progress &&
          quest.progress.quest_state.id === QuestState.Done &&
          quest.required_level !== 5000 &&
          quest.required_level !== 500,
      )
      .sort((a, b) => a.required_level - b.required_level);
    const notTaken = currentNpc.quests
      .filter(
        (quest) => !quest.progress && quest.required_level !== 5000 && quest.required_level !== 500,
      )
      .sort((a, b) => a.required_level - b.required_level);
    const hasNonRepeatQuest = inProgress.some((quest) => quest.is_daily !== true);
    const firstNotDailyQuestTaken = notTaken.find((quest) => quest.is_daily !== true);
    const dailyQuest = notTaken.filter(
      (quest) => quest.is_daily === true && quest.required_level <= me.profile.level,
    );
    const canTaken =
      !hasNonRepeatQuest &&
      firstNotDailyQuestTaken &&
      firstNotDailyQuestTaken.required_level <= me.profile.level
        ? dailyQuest
            .concat(firstNotDailyQuestTaken)
            .sort((a, b) => a.required_level - b.required_level)
        : dailyQuest;
    const notUnlocked = notTaken.filter((quest) => !canTaken.includes(quest));
    return {
      inProgress,
      finished,
      canTaken,
      notUnlocked,
    };
  }, [currentNpc]);

  return (
    <Container>
      <NPCList>
        {progress.map((npcProgress) => (
          <div key={npcProgress.id} onClick={() => setCurrentNPCId(npcProgress.id)}>
            <NPCCard
              id={npcProgress.id}
              minLvl={npcProgress.min_level}
              name={npcProgress.name}
              isSelected={npcProgress.id === currentNPCId}
            />
          </div>
        ))}
      </NPCList>

      <NPCContent key={currentNpc?.id}>
        {currentNpc && (
          <>
            <div className="flex bg-first p-8 mb-4">
              <div className="flex-1">
                <h2 className="text-red text-lg font-oswald uppercase">{currentNpc.name}</h2>
                <p className="text-md text-quaternary">{currentNpc.bio}</p>
              </div>
              <div className="h-16 w-16">
                <NPCIcon npcId={currentNpc.id} size="medium" />
              </div>
            </div>

            <div className="mx-4 h-full overflow-y-scroll">
              <Tabs>
                <Tabs.Tab onClick={() => setFilter("new")} isActive={filter === "new"}>
                  Available
                </Tabs.Tab>
                <Tabs.Tab onClick={() => setFilter("completed")} isActive={filter === "completed"}>
                  Completed
                </Tabs.Tab>
              </Tabs>
              <div className="flex flex-col gap-1 mt-4">
                {filter === "new" && (
                  <>
                    {classified.inProgress.length +
                      classified.canTaken.length +
                      classified.notUnlocked.length ===
                      0 && (
                      <div className="bg-first p-4 italic text-quaternary rounded-xl">
                        No quests available
                      </div>
                    )}
                    {classified.inProgress.map((quest) => (
                      <div key={quest.quest_id}>
                        <QuestCard quest={quest} locked={false} />
                      </div>
                    ))}

                    {classified.canTaken.map((quest) => (
                      <div key={quest.quest_id}>
                        <QuestCard quest={quest} locked={false} />
                      </div>
                    ))}

                    {classified.notUnlocked.map((quest) => (
                      <div key={quest.quest_id}>
                        <QuestCard quest={quest} locked={true} />
                      </div>
                    ))}
                  </>
                )}

                {filter === "completed" && (
                  <>
                    {classified.finished.length === 0 && (
                      <div className="bg-first p-4 italic text-quaternary rounded-xl">
                        No quests completed
                      </div>
                    )}
                    {classified.finished.map((quest) => (
                      <div key={quest.quest_id}>
                        <QuestCard quest={quest} />
                      </div>
                    ))}
                  </>
                )}
              </div>
            </div>
          </>
        )}
      </NPCContent>
    </Container>
  );
};

export default ProgressTab;
