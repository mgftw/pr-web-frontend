import Icon from "$components/Icon";
import NPCIcon from "$components/NPCIcon/NPCIcon";
import { useMyProfile } from "$hooks/useMyProfile";
import styled from "styled-components";
import tw from "twin.macro";

const Container = styled.div<{ $isSelected: boolean }>(({ $isSelected }) => [
  tw`flex bg-first`,
  `
    height: 3rem;
    width: 90%;
    align-items: center;
    justify-content: center;
    transition: all 200ms;
    border-radius: 6px;

    &:hover {
      cursor: pointer;
      width: 100%;
    }
  `,
  ...($isSelected ? [tw`bg-primary`, ` width: 100%;`] : []),
]);

const IconContainer = styled.div`
  width: 4rem;
  height: 4rem;
  transform: translate(1rem);
`;

const Name = styled.h1<{ $isSelected: boolean }>(({ $isSelected }) => [
  tw`text-center uppercase font-oswald`,
  ...($isSelected ? [tw`text-quaternary`] : []),
]);
const NPCCard = ({
  name,
  id,
  isSelected,
  minLvl,
}: {
  id: number;
  name: string;
  isSelected: boolean;
  minLvl: number;
}) => {
  const { profile } = useMyProfile();
  return (
    <Container $isSelected={isSelected}>
      <div className="flex-1">
        {profile.level >= minLvl ? (
          <Name $isSelected={isSelected}>{name}</Name>
        ) : (
          <Name $isSelected={isSelected}>
            <div className="flex items-center flex-row flex-col">
              <Icon icon="icon-lock" size={24} />
              <p className="text-red text-sm">Unlock at Lv. {minLvl}</p>
            </div>
          </Name>
        )}
      </div>
      <IconContainer>
        <NPCIcon npcId={id} />
      </IconContainer>
    </Container>
  );
};

export default NPCCard;
