import Badge from "$atoms/Badge";
import CurrencyIndicator from "$components/CurrencyIndicator";
import InventoryItem from "$components/InventoryItem";
import { formatCurrency } from "$helpers/currencyHelper";
import useInventoryItemCache from "$hooks/useInventoryItemCache";
import { IPlayerQuestProgress, IQuest, QuestState, QuestTaskState } from "types/entities";
import { CircularProgress } from "@nextui-org/progress";
import { Accordion, AccordionItem } from "@nextui-org/react";
import { useMyProfile } from "$hooks/useMyProfile";
import { PiCheckCircleFill } from "react-icons/pi";
import Icon from "$components/Icon";
import colors from "$constants/colors";
import { memo } from "react";
import { BiCheck } from "react-icons/bi";

interface IExtendedQuest extends IQuest {
  progress: IPlayerQuestProgress;
}

interface QuestCardProps {
  quest: Partial<IExtendedQuest>;
  locked?: boolean;
}

const QuestCard = ({ quest, locked }: QuestCardProps) => {
  const itemCache = useInventoryItemCache();
  const me = useMyProfile();

  const isDisabled = (!quest?.progress && quest.required_level > me.profile.level) || locked;
  const isTaken = !!quest?.progress;
  const isQuestCompleted = quest?.progress?.quest_state?.id === QuestState.Done;

  const renderLeftIndicator = () => {
    if (isDisabled) {
      return (
        <div className="w-8 h-8 flex items-center ml-2">
          <Icon icon="icon-lock" size={20} />
        </div>
      );
    }

    if (!isTaken) {
      return <div className="w-8 h-8 flex items-center mr-2"></div>;
    }

    if (isTaken && quest.progress.quest_state.id === QuestState.Done) {
      return (
        <div className="w-8 h-8 flex items-center mr-2">
          <PiCheckCircleFill color={colors.green} size="100%" />
        </div>
      );
    }

    const completedTasks = quest.progress.tasks.filter(
      (taskProgress) => taskProgress?.progress?.task_state === QuestTaskState.Done,
    );

    return (
      <div className="mr-2">
        <CircularProgress
          aria-label="progress"
          classNames={{
            svg: "w-8 h-8",
          }}
          value={(completedTasks.length / quest.progress.tasks.length) * 100}
          showValueLabel={true}
        />
      </div>
    );
  };

  return (
    <div className="bg-first p-1 rounded-xl">
      <Accordion>
        <AccordionItem
          isDisabled={isDisabled}
          classNames={{
            trigger: "p-0 flex",
            startContent: "grow",
            titleWrapper: "grow-0",
          }}
          startContent={
            <div className="flex items-center h-12 flex-grow">
              {renderLeftIndicator()}

              <div className="flex grow gap-1 items-baseline">
                {quest.is_daily && (
                  <h2 className="text-zero text-sm bg-secondary p-1 py-0 rounded-lg">Daily</h2>
                )}
                {quest.is_weekly && (
                  <h2 className="text-zero text-sm bg-secondary p-1 py-0 rounded-lg">Weekly</h2>
                )}
                <h2>{quest.title}</h2>
                <p className="text-quaternary text-sm">Lv. {quest.required_level}</p>
              </div>

              <div className="flex flex-auto flex-1 items-center gap-2 justify-end pr-2">
                {quest.pill_reward > 0 && (
                  <CurrencyIndicator
                    icon="icon-pills"
                    amount={quest.pill_reward}
                    backgroundColor="zero"
                  />
                )}
                {quest.cash_reward > 0 && (
                  <CurrencyIndicator
                    icon="icon-cash"
                    amount={quest.cash_reward}
                    backgroundColor="zero"
                  />
                )}
                {quest.exp_reward > 0 && (
                  <Badge backgroundColor="zero">{`${formatCurrency(quest.exp_reward)} XP`}</Badge>
                )}

                {quest.item_rewards?.length > 0 && (
                  <div className="flex gap-4">
                    {quest.item_rewards.map(({ item_id, quantity }) => {
                      if (!itemCache[item_id]) return null;
                      return (
                        <div key={item_id} className="h-10 w-10 shrink-0">
                          <InventoryItem
                            item={itemCache[item_id]}
                            quantity={quantity}
                            size="small"
                          />
                        </div>
                      );
                    })}
                  </div>
                )}
              </div>
            </div>
          }
        >
          <div className="flex gap-4">
            <div className="flex flex-col gap-2 w-96 grow-0 shrink-0 mt-4">
              <h2 className="font-oswald uppercase text-red">Tasks</h2>
              {quest.tasks?.map((task) => {
                const taskProgress = quest.progress?.tasks[task.task_id - 1];

                const isCompleted = taskProgress?.progress?.task_state === QuestTaskState.Done;
                return (
                  <div
                    key={task.task_id}
                    className="bg-second rounded-md flex items-center gap-1 px-2 py-1"
                  >
                    <p
                      className={`text-sm ml-2 grow ${isCompleted || isQuestCompleted ? "line-through" : ""}`}
                    >
                      {task.description?.substring(1) ?? "unknown"}
                    </p>
                    {taskProgress && (
                      <Badge
                        backgroundColor={isCompleted || isQuestCompleted ? "green" : "quaternary"}
                      >
                        <div className="text-xs">
                          {isCompleted || isQuestCompleted ? (
                            <BiCheck size={16} />
                          ) : (
                            `${taskProgress.progress?.task_progress ?? "-"} / ${task.amount}`
                          )}
                        </div>
                      </Badge>
                    )}
                  </div>
                );
              })}
            </div>
            <div className="grow bg-zero p-4 overflow-y-scroll h-64 text-md text-quaternary rounded-xl">
              <h2 className="font-oswald uppercase text-red">Dialog</h2>
              <p className="text-sm">{quest.web_story}</p>
            </div>
          </div>
        </AccordionItem>
      </Accordion>
    </div>
  );
};

export default memo(QuestCard);
