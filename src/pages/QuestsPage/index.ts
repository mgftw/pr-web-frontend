import QuestPage from "./QuestsPage";
import WorldTab from "./WorldTab";
import ProgressTab from "./ProgressTab";

QuestPage.WorldTab = WorldTab;
QuestPage.ProgressTab = ProgressTab;

export default QuestPage;
