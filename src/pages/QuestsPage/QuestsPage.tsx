import WorldTab from "./WorldTab";
import ProgressTab from "./ProgressTab";
import BasePage from "$components/BasePage/BasePage";
import useFeatureFlags from "$hooks/useFeatureFlags";

const QuestPage: React.FC<object> = () => {
  const flags = useFeatureFlags();

  const options = [
    {
      label: "World",
      url: `./world`,
      regex: RegExp("/quest/world.*"),
    },
    ...(flags.ENABLE_QUEST_SYSTEM
      ? [
          {
            label: "Progress",
            url: `./progress`,
            regex: RegExp("/quest/progress.*"),
          },
        ]
      : []),
  ];

  return <BasePage icon="icon-book" title="Quests" tabOptions={options} addNavbarSpacing={false} />;
};

type QuestPageComponent<P = object> = React.FC<P> & {
  WorldTab: typeof WorldTab;
  ProgressTab: typeof ProgressTab;
};

export default QuestPage as QuestPageComponent;
