import UserProfile from "./UserProfile";
import Inventory from "./Inventory";

import * as st from "./styles";

const GeneralTab = () => {
  return (
    <st.Container>
      <st.ProfileContainer>
        <UserProfile />
      </st.ProfileContainer>
      <st.InventoryContainer>
        <Inventory />
      </st.InventoryContainer>
    </st.Container>
  );
};

export default GeneralTab;
