import { useState, useMemo } from "react";

import InventoryItem from "$components/InventoryItem";
import Loader from "$components/Loader";

import * as st from "./Inventory.styles";
import { useMyInventory } from "$hooks/useMyInventory";
import Tabs from "$components/Tabs";

const variants = {
  hidden: {
    opacity: 0,
  },
  visible: (custom) => ({
    opacity: 1,
    scale: [0.95, 1.02, 1.0],
    transition: { delay: custom < 60 ? custom * 0.01 : 60 * 0.01 },
  }),
};

const options = {
  all: {
    name: "All",
    filter: () => true,
  },
  materials: {
    name: "Materials",
    filter: (item) => (item?.item?.name ?? "").includes("|<|"),
  },
  quest: {
    name: "Quest",
    filter: (item) => (item?.item?.name ?? "").includes("|Q|"),
  },
  loot: {
    name: "Loot",
    filter: (item) => (item?.item?.name ?? "").includes("|~|"),
  },
  enhancements: {
    name: "Enhancements",
    filter: (item) => (item?.item?.name ?? "").includes("|+|"),
  },
  buff: {
    name: "Buff",
    filter: (item) => (item?.item?.name ?? "").includes("|Buff|"),
  },
  store: {
    name: "Store",
    filter: () => false,
  },
};

const Inventory = () => {
  const { inventory: items = [], isLoading } = useMyInventory();

  const [filter, setFilter] = useState("all");

  const selectedOption = useMemo(() => options[filter], [filter]);

  const filteredItems = useMemo(
    () => items.filter(selectedOption.filter ?? (() => true)),
    [items, selectedOption],
  );

  return (
    <st.Container>
      <st.Header>
        <st.Title>Inventory</st.Title>
      </st.Header>
      <st.Divider />
      <st.Body>
        {isLoading ? (
          <Loader />
        ) : (
          <>
            <st.TabsContainer>
              <Tabs>
                {Object.keys(options).map((optKey) => (
                  <Tabs.Tab
                    key={optKey}
                    onClick={() => setFilter(optKey)}
                    isActive={filter === optKey}
                  >
                    {options[optKey].name}
                  </Tabs.Tab>
                ))}
              </Tabs>
            </st.TabsContainer>
            {filteredItems.length === 0 && (
              <st.EmptyInventory>No items match this filter</st.EmptyInventory>
            )}
            <st.ItemsContainer>
              {filteredItems.map((ownedItem, index) => (
                <st.Item
                  key={ownedItem.item_id}
                  variants={variants}
                  custom={index}
                  initial="hidden"
                  animate="visible"
                >
                  <InventoryItem
                    item={{
                      ...ownedItem.item,
                      id: ownedItem.item_id,
                    }}
                    quantity={ownedItem.quantity}
                  />
                </st.Item>
              ))}
            </st.ItemsContainer>
          </>
        )}
      </st.Body>
    </st.Container>
  );
};

export default Inventory;
