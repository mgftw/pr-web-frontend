import styled from "styled-components";
import tw from "twin.macro";
import { motion } from "framer-motion";
import colors from "$constants/colors";

export const Container = styled.div`
  ${tw`bg-first flex-grow p-4 flex-col`}
  border-radius: 0.5rem;
  height: 100%;
`;

export const Header = styled.div`
  height: 2.2rem;
`;

export const Title = styled.div`
  ${tw`self-start flex items-center text-xl`}
`;

export const Divider = styled.div`
  height: 3px;
  background-color: ${colors.zero};
`;

export const Body = styled.div`
  ${tw`pt-3 pb-3 flex-grow`}
  overflow: auto;
  height: calc(100% - 3rem);
`;

export const ItemsContainer = styled.div`
  display: grid;
  padding-top: 1rem;
  grid-gap: 1rem;
  grid-template-columns: repeat(auto-fill, 5rem);
`;

export const Item = styled(motion.div)`
  ${tw`h-20 w-20`}
  cursor: pointer;
`;

export const TabsContainer = styled.div`
  ${tw`mb-1`}
`;

export const EmptyInventory = styled.div`
  ${tw`font-karla text-primary text-2xl pt-32 flex flex-grow text-center items-center`}
  justify-content: center;
`;
