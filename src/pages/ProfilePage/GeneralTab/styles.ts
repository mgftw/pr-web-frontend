import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`flex flex-row h-full ml-5`}
  animation: fadeIn ease 0.5s;
  margin-top: 1.5rem;

  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
`;

export const ProfileContainer = styled.div`
  ${tw`w-1/4 pb-5 pt-0`}
`;

export const InventoryContainer = styled.div`
  ${tw`w-3/4 pt-0 pl-5 pr-5 pb-5`}
`;

export const SettingsContainer = styled.div`
  ${tw`flex-grow`}
`;
