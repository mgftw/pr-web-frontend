import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`flex rounded p-2 px-6 flex-col justify-center items-center`}
`;

export const ClanIcon = styled.div`
  ${tw`rounded-full bg-secondary h-32 w-32 mb-2`}
  img {
    height: 100%;
    width: auto;
    vertical-align: bottom;
  }
`;

export const ClanName = styled.div`
  ${tw`flex text-2xl uppercase font-oswald font-bold text-primary my-2`}
`;
