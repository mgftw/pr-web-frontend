import React, { useState, useEffect } from "react";

import { getClanInfo } from "$api/clanApi";

import * as st from "./styles";

const ClanInfo = ({ clanId }) => {
  const [clanData, setClanData] = useState({});
  useEffect(() => {
    getClanInfo(clanId).then((clan) => {
      setClanData(clan);
    });
  }, []);
  return (
    <st.Container>
      <st.ClanIcon>
        <img src={clanData.avatar_url?.large} alt="" />
      </st.ClanIcon>
      <st.ClanName>{clanData.name}</st.ClanName>
    </st.Container>
  );
};

export default ClanInfo;
