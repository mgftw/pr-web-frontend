import CurrencyIndicator from "$components/CurrencyIndicator";
import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`h-full w-full flex flex-row relative`}
`;

export const ProfileHeader = styled.div`
  ${tw`w-full absolute flex justify-center`}
  z-index: 10;
`;

export const ProfileCard = styled.div`
  ${tw`h-16 bg-second rounded-lg flex items-center relative`}
  width: calc(100% - 1rem);
`;

export const Avatar = styled.div<{ $url: string }>`
  ${tw`rounded-full bg-secondary h-24 w-24 absolute`}
  transform: translateX(-1rem);
  ${({ $url }) => ($url ? `background-image: url(${$url});` : "")}
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
`;

export const MainInfo = styled.div`
  ${tw`h-full mt-1`}
  width: calc(100% - 1rem);
  padding-left: 6rem;
`;

export const Name = styled.div`
  ${tw`text-lg w-56 font-bold flex-grow truncate mt-1`}
`;

export const Level = styled.div`
  ${tw`text-lg text-right font-bold pl-1 pr-2 mt-1`}
  white-space: nowrap;
`;

export const Header = styled.div`
  ${tw`flex`}
`;

export const ExpProgressContainer = styled.div`
  ${tw`flex flex-row items-center`}
`;

export const LevelPercent = styled.div`
  ${tw`flex text-center flex-grow justify-center font-bold pl-2 pr-2`}
`;

export const BottomCardContainer = styled.div`
  ${tw`mt-10 w-full flex flex-col`}
  z-index: 5;
`;

export const BottomCard = styled.div`
  ${tw`bg-first rounded w-full pt-10 pb-1`}
`;

export const Currencies = styled.div`
  ${tw`flex w-full justify-center mt-2`}
`;

export const Currency = styled.div`
  ${tw`mx-2 mt-1 w-full`}
`;

export const BoosterCard = styled.div`
  ${tw`w-full pt-2 pb-2 flex mt-6 mb-4`}
  background: linear-gradient(90deg, #64767B 0%, #49565A 100%);
  border-radius: 0px 12px;
`;

export const BoosterIcon = styled.img`
  ${tw`h-16 w-16 object-contain mr-4 ml-4`}
`;

export const BoosterInfo = styled.div`
  ${tw`flex-grow text-sm font-bold flex text-base flex-col justify-center items-center`}
`;

export const BoosterMultiplier = styled.div`
  ${tw`text-3xl font-oswald font-bold leading-tight`}
`;

export const ClassCard = styled.div`
  ${tw`m-4 p-4 my-8 rounded-lg bg-second text-center font-bold text-gray-300 text-lg`}
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  h6 {
    ${tw`p-0 m-0 mb-4 text-xl`};
  }

  p {
    ${tw`text-3xl uppercase font-oswald text-primary m-0 mt-2 mb-2`}
  }
`;

export const ClassInfo = styled.div`
  ${tw`flex m-4 mt-2`}
`;

export const ClassName = styled.div`
  ${tw`flex font-oswald font-bold text-gray-300 text-2xl uppercase text-left`}
`;

export const ClassLevel = styled.div`
  ${tw`flex text-lg text-white text-left font-normal mt-auto pl-2`}
  padding-bottom: 0.125rem;
`;

export const ClanCard = styled.div`
  ${tw`font-bold text-center m-4 p-4 my-8 rounded-lg bg-second font-bold text-gray-300 text-xl`}
`;

export const ClassEmblemContainer = styled.div`
  cursor: pointer;
`;

export const ProfileCurrencyIndicator = styled(CurrencyIndicator)`
  ${tw`p-2`}
`;
