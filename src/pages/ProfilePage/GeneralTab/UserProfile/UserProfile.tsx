import { useState } from "react";

import boosterImage from "$assets/booster.png";
import ProgressBar from "$components/ProgressBar";

import ClanInfo from "$pages/ProfilePage/GeneralTab/ClanInfo";

import * as st from "./styles";
import { useMyProfile } from "$hooks/useMyProfile";
import ClassEmblem from "$components/ClassEmblem";
import ClassSwitchModal from "$components/ClassSwitchModal";

const UserProfile = () => {
  const { profile: stats } = useMyProfile();

  const [classSwitchModal, setClassSwitchModal] = useState(false);

  const classId = stats?.class?.id;

  return (
    <st.Container>
      <ClassSwitchModal
        initialClass={classId}
        isOpen={classSwitchModal}
        onRequestClose={() => setClassSwitchModal(false)}
      />
      <st.ProfileHeader>
        <st.ProfileCard>
          <st.Avatar $url={stats.avatar_url} />
          <st.MainInfo>
            <st.Header>
              <st.Name>{stats.name}</st.Name>
              <st.Level>Lv. {stats.level !== undefined ? stats.level : "-"}</st.Level>
            </st.Header>
            <st.ExpProgressContainer>
              <ProgressBar height={4} width={275} value={Math.floor(stats.exp_percent * 100)} />
              <st.LevelPercent>
                {stats.exp_percent > 1 ? 100 : Math.floor(stats.exp_percent * 100)}%
              </st.LevelPercent>
            </st.ExpProgressContainer>
          </st.MainInfo>
        </st.ProfileCard>
      </st.ProfileHeader>

      <st.BottomCardContainer>
        <st.BottomCard>
          <st.Currencies>
            <st.Currency>
              <st.ProfileCurrencyIndicator
                icon="icon-cash"
                amount={stats.cash}
                borderRadius={"0.5rem"}
                compress={false}
              />
            </st.Currency>
            <st.Currency>
              <st.ProfileCurrencyIndicator
                icon="icon-pills"
                amount={stats.pills}
                borderRadius={"0.5rem"}
                compress={false}
              />
            </st.Currency>
          </st.Currencies>

          {stats.booster !== null && stats.booster !== undefined && (
            <st.BoosterCard>
              <st.BoosterIcon src={boosterImage} />

              <st.BoosterInfo>
                Active Booster
                <st.BoosterMultiplier>x{stats.booster}</st.BoosterMultiplier>
              </st.BoosterInfo>
            </st.BoosterCard>
          )}

          {stats.class !== null && stats.class !== undefined && stats.class.id > 0 && (
            <>
              <st.ClassCard>
                <h6>CLASS</h6>
                <st.ClassEmblemContainer onClick={() => setClassSwitchModal(true)}>
                  <ClassEmblem classId={stats.class.id} size={128} classLevel={stats.class.level} />
                </st.ClassEmblemContainer>
                <p>{stats.class.name}</p>
              </st.ClassCard>
            </>
          )}

          {stats.clan_id > 0 && (
            <st.ClanCard>
              CLAN
              <ClanInfo clanId={stats.clan_id} />
            </st.ClanCard>
          )}
        </st.BottomCard>
      </st.BottomCardContainer>
    </st.Container>
  );
};

export default UserProfile;
