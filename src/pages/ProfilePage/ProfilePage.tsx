import React from "react";

import GeneralTab from "./GeneralTab";
import BuffsTab from "./BuffsTab";

import BasePage from "$components/BasePage";

const ProfilePage: React.FC<object> = () => {
  const options = [
    {
      label: "General",
      url: `./`,
      regex: RegExp("/profile?/$"),
    },
    {
      label: "Buffs",
      url: `./buffs`,
      regex: RegExp("/profile/buffs.*"),
    },
  ];

  return (
    <BasePage icon="icon-profile" title="Profile" tabOptions={options} addNavbarSpacing={false} />
  );
};

type ProfilePageComponent<P = object> = React.FC<P> & {
  GeneralTab: typeof GeneralTab;
  BuffsTab: typeof BuffsTab;
};

export default ProfilePage as ProfilePageComponent;
