import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`rounded-lg bg-first ml-4 mr-4`}
  height: calc(100% - 2.5rem);
  width: calc(100% - 2rem);
`;

export const Title = styled.div`
  ${tw`text-lg text-primary font-bold pt-3 items-center flex`}
`;

export const ListControlContainer = styled.div`
  ${tw`rounded-lg bg-first ml-4 mr-4 mb-2`}
  height: auto;
  overflow: auto;
`;

export const ListContainer = styled.div`
  ${tw`bg-first ml-4 mr-4`}
  height: calc(100% - 6em);
  overflow: auto;
`;

export const SearchBar = styled.div`
  ${tw`pt-1 flex items-center flex-1`}
  width: 20rem;
`;

export const SearchBarContainer = styled.div`
  ${tw`flex-1`}
`;

export const RemoveBuff = styled.div`
  ${tw`text-primary bg-zero rounded-full h-4 w-4 flex justify-center items-center`}
  z-index: 100;
  position: absolute;
  right: -0.3rem;
  top: -0.3rem;
  cursor: pointer;
`;

export const BuffIconContainer = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
  opacity: 1;

  > div:first-child {
    opacity: 0;
    pointer-events: none;
    transition: opacity 200ms;
  }

  &:hover {
    > div:first-child {
      opacity: 1;
      pointer-events: auto;
      transition: opacity 200ms;
    }
  }
`;

export const List = styled.div`
  margin-top: -0.75rem;
  margin-bottom: -1rem;
`;

export const Item = styled.div`
  ${tw`bg-second mt-4 mb-4 pt-2 pb-2 pr-4 pl-4 rounded-lg`}
  position: relative;
`;

export const ListTitle = styled.div`
  ${tw`font-oswald text-lg  flex items-center`}
  font-variant: small-caps;
`;

export const Edit = styled.div`
  ${tw`pl-2 text-xs font-karla uppercase`}
  cursor: pointer;
  text-decoration: underline;
`;

export const ListCode = styled.div`
  ${tw`pl-2 text-xs font-karla uppercase`}
  cursor: pointer;
  text-decoration: underline;
`;

export const ListDescription = styled.div`
  ${tw`text-secondary`}
`;

export const FullSlotsContainer = styled.div`
  ${tw`flex`}
`;

export const FullSlots = styled.div``;

export const SlotsContainer = styled.div`
  ${tw`w-full h-12 overflow-auto pt-2 pl-1`}
`;

export const Slots = styled.div`
  ${tw`flex flex-row`}
`;

export const SlotContent = styled.div`
  ${tw`h-10 w-10 mr-2 flex-shrink-0`}
`;

export const SlotLoading = styled.div`
  ${tw`bg-secondary h-full w-full rounded-lg`}
`;

export const SlotEmpty = styled.div`
  ${tw`bg-zero h-full w-full rounded-lg flex items-center justify-center`}
  cursor: pointer;
`;

export const SlotEmptyDecor = styled.div`
  ${tw`bg-zero h-5 w-5 bg-second`}
  border-radius: 100%;
`;

export const SlotLocked = styled.div`
  ${tw`bg-zero h-full w-full rounded-lg text-second flex items-center justify-center`}
  cursor: pointer;
`;

export const UnlockReason = styled.div`
  ${tw`font-karla text-primary`}
`;

export const AddContainer = styled.div`
  ${tw`ml-1 flex items-center`}
  cursor: pointer;
`;

export const BuffEditorContainer = styled.div`
  ${tw`pb-4`}
`;

export const Spacing = styled.div`
  ${tw`py-1`}
`;

export const Delete = styled.div`
  ${tw`pr-1 pt-1 text-primary text-lg`}
  position: absolute;
  top: 0;
  right: 0;
  cursor: pointer;
`;

export const ShowNamesContainer = styled.div`
  ${tw`flex items-center pl-3`}
`;

export const ShowNamesLabel = styled.div`
  ${tw`text-sm`}
`;

export const FullBuffSlotContainer = styled.div`
  ${tw`w-48 p-1 rounded-lg my-1 flex items-center bg-first mr-2`}
`;

export const FullSlotName = styled.div`
  ${tw`text-primary font-oswald text-sm uppercase`}
`;
