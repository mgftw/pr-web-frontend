/* eslint-disable no-else-return */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import shortid from "shortid";
import { compose } from "redux";
import { useForm } from "react-hook-form";
import { get, uniqWith } from "lodash";
import { connect } from "react-redux";
import { Tooltip } from "react-tippy";
import { useDrop } from "react-dnd";
import { toast } from "react-toastify";
import { MdAddCircle, MdClose } from "react-icons/md";

import {
  getMyBuffs,
  getMyBuffLists,
  getMyBuffSlots,
  equipBuff,
  createBuffList,
  deleteBuffList,
  updateBuffList,
} from "$api/playerApi";
import Modal from "$components/Modal";

import BuffIcon from "$components/BuffIcon";
import BuffDetails from "$components/BuffDetails";
import Input from "$components/Input";
import Button from "$components/Button";
import Icon from "$components/Icon";
import Switch from "$components/Switch";

import errorSound from "$assets/beep_error01.wav";
import successSound from "$assets/menu_click04.wav";
import emptySound from "$assets/menu_click01.wav";

import * as st from "./styles";

const errorSoundAudio = new Audio(errorSound);
const successSoundAudio = new Audio(successSound);
const emptySoundAudio = new Audio(emptySound);

function getDigitSum(value) {
  let sum = 0;
  let cVal = value;
  while (cVal) {
    sum += cVal % 10;
    cVal = Math.floor(cVal / 10);
  }
  return sum;
}

function convertBase(value, fromBase, toBase) {
  const range = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.,".split("");
  const fromRange = range.slice(0, fromBase);
  const toRange = range.slice(0, toBase);

  let decValue = value
    .split("")
    .reverse()
    .reduce(function (carry, digit, index) {
      // if (fromRange.indexOf(digit) === -1) throw new Error('Invalid digit `'+digit+'` for base '+fromBase+'.');
      let val = carry;
      val += fromRange.indexOf(digit) * fromBase ** index;
      return val;
    }, 0);

  let newValue = "";
  while (decValue > 0) {
    newValue = toRange[decValue % toBase] + newValue;
    decValue = (decValue - (decValue % toBase)) / toBase;
  }
  return newValue || "0";
}

// copied almost directly from bufflistgen
function getBuffListCode(bufflist) {
  bufflist.sort(function (a, b) {
    return a.slot_id - b.slot_id;
  });

  let blText = "";
  let curBlText = "";
  let finalBlText = "";
  let loopCount = 0;
  let checkSum = 0;

  bufflist.forEach((buff) => {
    let bVal = buff.buff_id;
    if (isNaN(bVal)) bVal = 0;

    curBlText += bVal.toString().padStart(3, "0");
    loopCount += 1;
    if (loopCount % 3 === 0) {
      if (parseInt(curBlText, 10) !== 0) {
        const digitSum = getDigitSum(parseInt(curBlText, 10));
        checkSum += digitSum;
        blText += `${convertBase(`1${curBlText}`, 10, 36)}-`;
      }
      curBlText = "";
    }
  });

  if (curBlText !== "" && parseInt(curBlText, 10) !== 0) {
    const digitSum = getDigitSum(parseInt(curBlText, 10));
    checkSum += digitSum;
    blText += `${convertBase(`1${curBlText}`, 10, 36)}-`;
  }
  finalBlText = `${convertBase(`9${checkSum.toString()}`, 10, 36)}-${blText.replace(/-+$/, "")}`;
  return finalBlText;
}

const GetBuffListCodeNotification = (code) => {
  const link = `https://mgftw.com/bufflistgen/index.html?i=${code}`;

  const linkStyle = {
    color: "#fff",
    display: "inline-block",
  };

  return (
    <div>
      Buff list code copied to clipboard!{" "}
      <a style={linkStyle} href={link} rel="noreferrer" target="_blank">
        Open in Buff List Generator
      </a>
    </div>
  );
};

const getBuffSlotName = ({ list, slot, buffSlots, buffs, myBuffs }) => {
  if (!buffSlots[slot]) {
    return "Loading...";
  }

  // Slot locked
  else if (!buffSlots[slot].is_unlocked) {
    return `Locked: ${buffSlots[slot].locked_reason}`;
  }

  const slotInfo = list.buffs.find((equippedBuff) => equippedBuff.slot_id === slot);
  if (!slotInfo) {
    return "Empty";
  }

  const buffInfo = (buffs || []).find((buff) => buff.buff_id === slotInfo.buff_id);
  if (buffInfo) {
    let result = buffInfo.name;
    const myBuff = myBuffs.find(
      (e) => e.buff && e.buff.buff_id === buffInfo.buff_id && e.current_enhancement,
    );

    if (myBuff) {
      result += ` +${myBuff.current_enhancement}`;
    }
    return result;
  }

  return "Empty";
};

const BuffSlot = ({
  list: listEditing,
  slot: slotEditing,
  buffs,
  buffSlots,
  onBuffEquipped,
  listIndex,
  ownedBuffs,
}) => {
  let curBuff = {};
  if (ownedBuffs.length > 0 && buffSlots[slotEditing] && buffSlots[slotEditing].is_unlocked) {
    const slotInfo = listEditing.buffs.find((equippedBuff) => equippedBuff.slot_id === slotEditing);
    if (slotInfo) {
      const buffInfo = (buffs || []).find((buff) => buff.buff_id === slotInfo.buff_id);
      if (buffInfo) {
        curBuff = ownedBuffs.find((e) => e.buff && e.buff.buff_id === buffInfo.buff_id);
      }
    }
  }
  const [, drop] = useDrop({
    accept: "OWNED_BUFF",
    drop: (item) => {
      equipBuff({
        templateId: listEditing.template_id,
        slotId: slotEditing,
        buffId: item.buff_id,
      })
        .then(() => {
          onBuffEquipped(listEditing.template_id, slotEditing, item.buff_id);
          successSoundAudio.play();
        })
        .catch((e) => {
          toast.error(get(e, "response.data.error", "Cant equip the buff. Are you online?"));
          errorSoundAudio.play();
        });
    },
  });

  const renderSlot = (list, slot) => {
    // Not loaded yet
    if (!buffSlots[slot]) {
      return <st.SlotLoading key={`${slot}-LOADING`} />;

      // Slot locked
    } else if (!buffSlots[slot].is_unlocked) {
      return (
        <Tooltip html={<st.UnlockReason>{buffSlots[slot].locked_reason}</st.UnlockReason>}>
          <st.SlotLocked key={`${slot}-LOCKED`}>
            <Icon icon="icon-lock" size={30} />
          </st.SlotLocked>
        </Tooltip>
      );
    }
    const slotInfo = list.buffs.find((equippedBuff) => equippedBuff.slot_id === slot);
    if (!slotInfo) {
      // Nothing quipped
      return (
        <Tooltip html={<st.UnlockReason>Empty</st.UnlockReason>}>
          <st.SlotEmpty key={`${slot}-EMPTY`}>
            <st.SlotEmptyDecor />
          </st.SlotEmpty>
        </Tooltip>
      );
    }

    const buffInfo = (buffs || []).find((buff) => buff.buff_id === slotInfo.buff_id);
    if (buffInfo) {
      return (
        <st.BuffIconContainer key={`${slot}-${buffInfo.buff_id}`}>
          <st.RemoveBuff
            onClick={() => {
              equipBuff({
                templateId: listEditing.template_id,
                slotId: slotEditing,
                buffId: null,
              })
                .then(() => {
                  onBuffEquipped(listEditing.template_id, slotEditing, null);
                  emptySoundAudio.play();
                })
                .catch((e) => {
                  toast.error(
                    get(e, "response.data.error", "Cant equip the buff. Are you online?"),
                  );
                  errorSoundAudio.play();
                });
            }}
          >
            <MdClose size={16} />
          </st.RemoveBuff>
          <Tooltip
            trigger="click"
            html={<BuffDetails buffInfo={curBuff} />}
            position={listIndex === 0 ? "bottom-end" : "top-end"}
          >
            <BuffIcon buff={buffInfo} key={`${slot}-${buffInfo.buff_id}`} showOutline />
          </Tooltip>
        </st.BuffIconContainer>
      );
    }

    return (
      <Tooltip html={<st.UnlockReason>Empty</st.UnlockReason>}>
        <st.SlotEmpty key={`${slot}-DEPRECATED`}>
          <st.SlotEmptyDecor />
        </st.SlotEmpty>
      </Tooltip>
    );
  };

  return <st.SlotContent ref={drop}>{renderSlot(listEditing, slotEditing)}</st.SlotContent>;
};

BuffSlot.propTypes = {
  buffs: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  buffSlots: PropTypes.shape({}).isRequired,
  list: PropTypes.shape().isRequired,
  slot: PropTypes.number.isRequired,
};

const BuffLists = (props) => {
  const { buffs } = props;
  const [lists, setLists] = useState([]);
  const [myBuffs, setMyBuffs] = useState([]);
  const [buffSlots, setBuffSlots] = useState({});
  const [modalOpen, setModalOpen] = useState(false);
  const [editingList, setEditingList] = useState(null);
  const [filter, setFilter] = useState(null);
  const [showNames, setShowNames] = useState(false);
  useEffect(() => {
    getMyBuffs().then((apiBuffs) => {
      setMyBuffs(apiBuffs);
    });

    getMyBuffLists().then((apiLists) => {
      setLists(apiLists);
    });

    getMyBuffSlots().then((slots) => {
      setBuffSlots(slots);
    });
  }, []);

  const { register, handleSubmit, errors, reset } = useForm();

  const onCreateBuffList = async ({ name, description }) => {
    reset({});
    setModalOpen(false);

    if (lists.length >= 125) {
      toast.error("You have too many lists (Max: 125)");
      return;
    }

    // Currently not working...
    /*
    // -1: no import code provided, 0: invalid import code, 1: valid import code
    let validImport = -1;

    let newBuffListContents = [];
    if (importcode !== "") {
      const splitCode = importcode.split("-");
      let trueChecksum = 0;
      let finalChecksum = 0;
      validImport = 1;

      splitCode.forEach((currBuff, index) => {
        if(currBuff !== "" && validImport === 1) {
          let blockVal = convertBase(currBuff, 36, 10);
          if(blockVal[0] === '9') {
            blockVal = blockVal.substring(1);
            trueChecksum = parseInt(blockVal, 10);
          }
          else if(blockVal[0] === '1') {
            blockVal = blockVal.substring(1);
            const idCount = blockVal.length/3;
            for(let j = 0; j < idCount; j += 1) {
              const sId = blockVal.substring(j*3, (j*3)+3);
              const buff = parseInt(sId, 10);
              if(buff > 0) {
                finalChecksum += getDigitSum(buff);
                const slot = ((index-1)*3)+j;
                newBuffListContents[slot] = buff;
              }
            }
          }
          else {
            validImport = 0;
          }
        }
      });

      if(trueChecksum === 0 || trueChecksum !== finalChecksum) {
        validImport = 0;
        newBuffListContents = [];
      }
      else {
        const tempList = newBuffListContents;
        
        newBuffListContents = [];
        tempList.forEach((buff, index) => {
          const listObj = {"buff_id": buff, "slot_id": index+1};
          newBuffListContents.push(listObj);
        });

        // console.log(newBuffListContents);
      }
    } 

    if(validImport === 0) {
      toast.error('Imported code is invalid!');
      errorSoundAudio.play();
      return;
    };
    */

    setLists([
      ...lists,
      {
        active: false,
        class: 0,
        role: 0,
        name,
        description,
        buffs: [],
        template_id: shortid.generate(),
      },
    ]);
    try {
      await createBuffList({ name, description });
    } catch (e) {
      toast.error(get(e, "response.data.error", "Cant create the list"));
    }
    const apiLists = await getMyBuffLists();
    setLists(apiLists);
  };

  const onUpdateBuffList = async ({ name, description }) => {
    reset({});
    setModalOpen(false);

    const id = editingList.template_id;
    setLists(
      lists.map((list) =>
        list.template_id === id
          ? {
              ...list,
              name,
              description,
            }
          : list,
      ),
    );
    await updateBuffList(id, { name, description });
    const apiLists = await getMyBuffLists();
    setLists(apiLists);
  };

  const onDeleteBuffList = async (id) => {
    setLists(lists.filter(({ template_id: tempId }) => tempId !== id));
    await deleteBuffList(id);
    const apiLists = await getMyBuffLists();
    setLists(apiLists);
  };

  const filtered = lists.filter(
    (list) =>
      !filter ||
      !get(list, "name", null) ||
      (filter && list.name.toLowerCase().includes(filter.toLowerCase())),
  );

  return (
    <st.Container>
      <Modal onRequestClose={() => setModalOpen(false)} isOpen={modalOpen}>
        <Modal.Header onRequestClose={() => setModalOpen(false)}>
          {editingList ? "Edit buff list" : "New buff list"}
        </Modal.Header>
        <Modal.Body>
          <st.BuffEditorContainer>
            <form onSubmit={handleSubmit(editingList ? onUpdateBuffList : onCreateBuffList)}>
              <Input
                {...register("name", { required: true, maxLength: 32 })}
                isRequired
                maxlength={32}
                error={errors?.name && "Required. Up to 32 characters"}
                label="Name"
              />
              <st.Spacing />
              <Input
                {...register("description", { maxLength: 64 })}
                name="description"
                maxlength={64}
                error={errors?.description && "Up to 64 characters"}
                label="Description"
              />
              <st.Spacing />
              {!editingList && (
                <>
                  <Input
                    {...register("importcode", { maxLength: 64 })}
                    placeholder="(disabled)"
                    disabled
                    maxlength={64}
                    error={errors?.importcode && "Up to 64 characters"}
                    label="Import List Code"
                  />
                  <st.Spacing />
                </>
              )}
              <Button type="submit">Save</Button>
            </form>
          </st.BuffEditorContainer>
        </Modal.Body>
      </Modal>
      <st.ListControlContainer>
        <st.Title>
          Buff Lists
          <st.AddContainer
            onClick={() => {
              reset({});
              setEditingList(null);
              setModalOpen(true);
            }}
          >
            <MdAddCircle />
          </st.AddContainer>
        </st.Title>
        <st.SearchBar>
          <st.SearchBarContainer>
            <Input
              placeholder="Search by buff list name"
              onChange={(e) => {
                setFilter(e.target.value);
              }}
            />
          </st.SearchBarContainer>
          <st.ShowNamesContainer>
            <st.ShowNamesLabel>See names</st.ShowNamesLabel>
            <Switch active={showNames} onClick={() => setShowNames(!showNames)} />
          </st.ShowNamesContainer>
        </st.SearchBar>
      </st.ListControlContainer>
      <st.ListContainer>
        <st.List>
          {filtered.map((list, listIndex) => (
            <st.Item key={list.template_id}>
              <st.Delete
                onClick={() => {
                  const retVal = window.confirm(`Delete buff list <${list.name}>?`);
                  if (retVal === true) {
                    onDeleteBuffList(list.template_id);
                  }
                }}
              >
                <MdClose size={20} />
              </st.Delete>
              <st.ListTitle>
                {list.name}
                <st.Edit
                  onClick={() => {
                    reset({
                      name: list.name,
                      description: list.description,
                    });
                    setEditingList(list);
                    setModalOpen(true);
                  }}
                >
                  (edit)
                </st.Edit>
                <st.ListCode
                  onClick={() => {
                    const code = getBuffListCode(list.buffs);
                    navigator.clipboard.writeText(code);
                    toast.info(GetBuffListCodeNotification(code), {
                      autoClose: 7500,
                    });
                  }}
                >
                  (Copy List Code)
                </st.ListCode>
              </st.ListTitle>
              <st.ListDescription>{list.description}</st.ListDescription>

              {showNames ? (
                <st.FullSlotsContainer>
                  {new Array(4).fill(0).map((_, packSlot) => (
                    <st.FullSlots key={packSlot}>
                      {new Array(4).fill(0).map((__, index) => {
                        const slot = 4 * packSlot + (index + 1);
                        return (
                          <st.FullBuffSlotContainer>
                            <BuffSlot
                              onBuffEquipped={(templateId, slotId, buffId) => {
                                const newLists = lists.map((buffList) =>
                                  buffList.template_id === templateId
                                    ? {
                                        ...buffList,
                                        buffs: uniqWith(
                                          [
                                            {
                                              buff_id: buffId,
                                              slot_id: slotId,
                                            },
                                            ...buffList.buffs
                                              // Remove where this buff_id is already equipped
                                              .filter(
                                                (buff) =>
                                                  !(
                                                    buff.buff_id === buffId &&
                                                    slotId !== buff.slot_id
                                                  ),
                                              )
                                              // Apply new slot
                                              .map((buff) =>
                                                buff.slot_id === slotId
                                                  ? {
                                                      ...buff,
                                                      buff_id: buffId,
                                                    }
                                                  : buff,
                                              ),
                                          ],
                                          (a, b) =>
                                            a.buff_id === b.buff_id && a.slot_id === b.slot_id,
                                        ),
                                      }
                                    : buffList,
                                );
                                setLists(newLists);
                              }}
                              list={list}
                              slot={slot}
                              key={slot}
                              buffSlots={buffSlots}
                              buffs={buffs}
                              listIndex={listIndex}
                              ownedBuffs={myBuffs}
                            />
                            <st.FullSlotName>
                              {getBuffSlotName({
                                list,
                                slot,
                                buffSlots,
                                buffs,
                                myBuffs,
                              })}
                            </st.FullSlotName>
                          </st.FullBuffSlotContainer>
                        );
                      })}
                    </st.FullSlots>
                  ))}
                </st.FullSlotsContainer>
              ) : (
                <st.SlotsContainer>
                  <st.Slots>
                    {new Array(16).fill(0).map((_, index) => {
                      const slot = index + 1;
                      return (
                        <BuffSlot
                          seeNames={showNames}
                          onBuffEquipped={(templateId, slotId, buffId) => {
                            const newLists = lists.map((buffList) =>
                              buffList.template_id === templateId
                                ? {
                                    ...buffList,
                                    buffs: uniqWith(
                                      [
                                        { buff_id: buffId, slot_id: slotId },
                                        ...buffList.buffs
                                          // Remove where this buff_id is already equipped
                                          .filter(
                                            (buff) =>
                                              !(buff.buff_id === buffId && slotId !== buff.slot_id),
                                          )
                                          // Apply new slot
                                          .map((buff) =>
                                            buff.slot_id === slotId
                                              ? {
                                                  ...buff,
                                                  buff_id: buffId,
                                                }
                                              : buff,
                                          ),
                                      ],
                                      (a, b) => a.buff_id === b.buff_id && a.slot_id === b.slot_id,
                                    ),
                                  }
                                : buffList,
                            );
                            setLists(newLists);
                          }}
                          list={list}
                          slot={slot}
                          key={slot}
                          buffSlots={buffSlots}
                          buffs={buffs}
                          listIndex={listIndex}
                          ownedBuffs={myBuffs}
                        />
                      );
                    })}
                  </st.Slots>
                </st.SlotsContainer>
              )}
            </st.Item>
          ))}
        </st.List>
      </st.ListContainer>
    </st.Container>
  );
};

function mapStateToProps(stores) {
  return {
    buffs: stores.buffState.buffs,
  };
}

const mapDispatchToProps = {};

export default compose(connect(mapStateToProps, mapDispatchToProps))(BuffLists);
