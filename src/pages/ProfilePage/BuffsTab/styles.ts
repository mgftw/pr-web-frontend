import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`w-full h-full relative flex`}
`;

export const OwnedBuffs = styled.div`
  ${tw`h-full ml-5`}

  flex: 0.25;
`;

export const BuffLists = styled.div`
  ${tw`h-full flex justify-center`}
  flex: 0.75;
`;

export const BindedBuffs = styled.div`
  ${tw`absolute h-16 right-0`}
  right: 2rem;
  top: 0.6rem;
`;
