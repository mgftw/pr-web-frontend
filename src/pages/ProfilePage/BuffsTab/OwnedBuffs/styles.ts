import styled from "styled-components";
import tw from "twin.macro";
import { motion } from "framer-motion";

export const Container = styled.div`
  ${tw`rounded-lg bg-first w-full relative`}
  height: calc(100% - 2.5rem);
`;

export const Legend = styled.div`
  ${tw`text-primary pl-4`}
  right: 0;
  cursor: pointer;
`;

export const ColorLegend = styled.div`
  ${tw`font-karla`}
`;

export const LegendItem = styled.div`
  ${tw`text-primary flex font-karla pt-1`}
`;

export const LegendColor = styled.div<{ $color: string }>(({ $color }) => [
  tw`rounded-full h-4 w-4 mr-2 font-karla font-medium`,
  `background-color: ${$color};`,
]);

export const NoResults = styled.div`
  ${tw`flex w-full text-center text-lg justify-center pt-4`}
`;

export const Title = styled.div`
  ${tw`text-lg text-primary font-bold text-left p-2 pl-4 flex items-center`}
`;

export const SearchContainer = styled.div`
  ${tw`pl-4 pr-4 pb-2`}
`;

export const BuffList = styled.div`
  ${tw`overflow-auto`}
  height: calc(100% - 6em);
  overflow-x: hidden;
  gap: 0.5rem;
  display: flex;
  flex-direction: column;
`;

export const BuffContainer = styled(motion.div)`
  ${tw`pr-2 pl-2`}
`;

export const BuffCardOpacity = styled.div<{ $isOwned: boolean }>`
  opacity: ${({ $isOwned = true }) => ($isOwned ? 1.0 : 0.2)};
  width: 100%;
`;

export const AllContainer = styled.div`
  ${tw`flex-1 flex justify-end items-center`}
`;

export const AllLabel = styled.div`
  ${tw`text-sm`}
`;
