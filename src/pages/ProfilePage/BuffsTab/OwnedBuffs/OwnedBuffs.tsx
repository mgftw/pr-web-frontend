import { useState, useEffect } from "react";
import { get as _get } from "lodash";
import { connect } from "react-redux";
import { Tooltip } from "react-tippy";

import { getMyBuffs } from "$api/playerApi";

import BuffCard from "$components/BuffCard";
import BuffDetails from "$components/BuffDetails";
import Input from "$components/Input";
import Icon from "$components/Icon";
import Switch from "$components/Switch";

import Loader from "$components/Loader";

import { buffCategoryColors } from "$constants/colors";

import * as st from "./styles";

const categories = [
  { name: "Offensive", color: buffCategoryColors.offensive },
  { name: "Defensive", color: buffCategoryColors.defensive },
  { name: "Ability", color: buffCategoryColors.ability },
  { name: "Equipment", color: buffCategoryColors.equipment },
  { name: "Utility", color: buffCategoryColors.utility },
  { name: "Economy and Exp", color: buffCategoryColors.economy },
  { name: "Miscellaneous", color: buffCategoryColors.misc },
];

const OwnedBuffs = ({ buffs: allBuffs }) => {
  const [buffs, setBuffs] = useState([]);
  const [filter, setFilter] = useState(null);
  const [loading, setLoading] = useState(true);
  const [seeAll, setSeeAll] = useState(false);

  useEffect(() => {
    getMyBuffs().then((apiBuffs) => {
      setBuffs(
        apiBuffs.filter(
          (ownedBuff) =>
            ownedBuff.buff &&
            (ownedBuff.buff.infected === 9 ||
              (ownedBuff.buff.infected === 12 && !(ownedBuff.buff.flags & 128))),
        ),
      );
      setLoading(false);
    });
  }, []);

  const filtered = (seeAll ? allBuffs.map((b) => ({ buff: b, buff_id: b.id })) : buffs)
    .filter(
      (ownedBuff) =>
        ownedBuff.buff &&
        (ownedBuff.buff.infected === 9 || ownedBuff.buff.infected === 12) &&
        !(ownedBuff.buff.flags & 128),
    )
    .filter((ownedBuff) => {
      if (!filter) {
        return true;
      }

      const matchesName =
        !_get(ownedBuff, "buff.name", null) ||
        (filter && ownedBuff.buff.name.toLowerCase().includes(filter.toLowerCase()));
      const matchesDesc =
        !_get(ownedBuff, "buff.name", null) ||
        !_get(ownedBuff, "buff.description", null) ||
        (filter && ownedBuff.buff.description.toLowerCase().includes(filter.toLowerCase()));
      return matchesName || matchesDesc;
    });

  const variants = {
    hidden: {
      opacity: 0,
    },
    visible: (custom) => ({
      opacity: 1,
      scale: [0.95, 1.02, 1.0],
      transition: { delay: custom < 20 ? custom * 0.05 : 20 * 0.05 },
    }),
  };

  return (
    <st.Container>
      <st.Title>
        Survivor buffs
        <Tooltip
          html={
            <st.ColorLegend>
              {categories.map((cat) => (
                <st.LegendItem key={cat.name}>
                  <st.LegendColor $color={cat.color} />
                  {cat.name}
                </st.LegendItem>
              ))}
            </st.ColorLegend>
          }
        >
          <st.Legend>
            <Icon icon="icon-question-circle" />
          </st.Legend>
        </Tooltip>
        <st.AllContainer>
          <Switch active={seeAll} onClick={() => setSeeAll(!seeAll)} />
          <st.AllLabel>See All</st.AllLabel>
        </st.AllContainer>
      </st.Title>

      <st.SearchContainer>
        <Input
          // @ts-expect-error Fix this once input types are fixed.
          placeholder="Search for buff..."
          onChange={(e) => {
            setFilter(e.target.value);
          }}
        />
      </st.SearchContainer>

      {!loading && filtered.length === 0 && <st.NoResults>No results</st.NoResults>}

      {loading ? (
        <Loader />
      ) : (
        <st.BuffList>
          {filtered.map((ownedBuff, index) => {
            const isOwned =
              (buffs || []).find((b) => {
                return b.buff.buff_id === ownedBuff.buff.buff_id;
              }) !== undefined;
            return (
              <Tooltip html={<BuffDetails buffInfo={ownedBuff} />} position="right" trigger="click">
                <st.BuffContainer
                  key={ownedBuff.buff.buff_id}
                  variants={variants}
                  custom={index}
                  initial="hidden"
                  animate="visible"
                >
                  <st.BuffCardOpacity $isOwned={isOwned}>
                    <BuffCard ownedBuffInfo={ownedBuff} />
                  </st.BuffCardOpacity>
                </st.BuffContainer>
              </Tooltip>
            );
          })}
        </st.BuffList>
      )}
    </st.Container>
  );
};

function mapStateToProps(stores) {
  return {
    buffs: (stores.buffState.buffs || []).filter(
      (b) => !b.flags_human.deprecated && !b.flags_human.disabled,
    ),
  };
}

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(OwnedBuffs);
