import { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import * as buffActions from "$actions/buffActions";
import { getBuffs } from "$api/buffApi";

import OwnedBuffs from "./OwnedBuffs";
import BuffLists from "./BuffLists";
import BindedBuffs from "./BindedBuffs";

import * as st from "./styles";

const BuffsTab = (props) => {
  const { updateBuffList } = props;
  useEffect(() => {
    getBuffs().then((apiBuffs) => {
      updateBuffList(apiBuffs);
    });
  }, []);

  return (
    <st.Container>
      <st.OwnedBuffs>
        <OwnedBuffs />
      </st.OwnedBuffs>

      <st.BuffLists>
        <BuffLists />
      </st.BuffLists>
      <st.BindedBuffs>
        <BindedBuffs />
      </st.BindedBuffs>
    </st.Container>
  );
};

BuffsTab.propTypes = {
  updateBuffList: PropTypes.func.isRequired,
};

function mapStateToProps() {
  return {};
}

const mapDispatchToProps = {
  updateBuffList: buffActions.updateBuffList,
};

export default connect(mapStateToProps, mapDispatchToProps)(BuffsTab);
