import React, { useState, useEffect } from "react";
import { Tooltip } from "react-tippy";

import { getMyBuffs } from "$api/playerApi";

import BuffIcon from "$components/BuffIcon";
import BuffDetails from "$components/BuffDetails";

import * as st from "./styles";

const BindedBuffs = () => {
  const [buffs, setBuffs] = useState([]);
  // const [loading, setLoading] = useState(true);

  useEffect(() => {
    getMyBuffs().then((apiBuffs) => {
      setBuffs(apiBuffs);
      // setLoading(false);
    });
  }, []);

  const filtered = buffs.filter((binded) => binded.buff && binded.is_binded);
  if (filtered.length <= 0) return <></>;

  return (
    <st.Container>
      <st.Title>Binded:</st.Title>
      {filtered.map(
        (binded, index) =>
          index < 3 && (
            <st.SlotContent key={index}>
              <Tooltip trigger="click" html={<BuffDetails buffInfo={binded} />}>
                <BuffIcon buff={binded.buff} key={`${binded.buff.buff_id}`} />
              </Tooltip>
            </st.SlotContent>
          ),
      )}
    </st.Container>
  );
};

export default BindedBuffs;
