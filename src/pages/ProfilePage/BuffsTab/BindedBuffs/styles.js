import styled from "styled-components";
import tw from "twin.macro";

export const Container = styled.div`
  ${tw`rounded-lg bg-second ml-4 mr-4 flex flex-row items-center p-2`}
  border: 5px solid rgb(27,32,33);
`;

export const Title = styled.div`
  ${tw`flex pr-2 font-bold`}
`;

export const Item = styled.div`
  ${tw`bg-second mt-4 mb-4 pt-2 pb-2 pr-4 pl-4 rounded-lg`}
`;

export const ListTitle = styled.div`
  ${tw`font-oswald text-lg uppercase`}
`;

export const ListDescription = styled.div`
  ${tw`text-secondary`}
`;

export const SlotsContainer = styled.div`
  ${tw`w-full h-12 overflow-auto pt-2`}
`;

export const Slots = styled.div`
  ${tw`flex flex-row`}
`;

export const SlotContent = styled.div`
  ${tw`h-10 w-10 mr-2 flex-shrink-0`}
`;

export const SlotLoading = styled.div`
  ${tw`bg-secondary h-full w-full rounded-lg`}
`;

export const SlotEmpty = styled.div`
  ${tw`bg-zero h-full w-full rounded-lg flex items-center justify-center`}
  cursor: pointer;
`;

export const SlotEmptyDecor = styled.div`
  ${tw`bg-zero h-5 w-5 bg-second`}
  border-radius: 100%;
`;

export const SlotLocked = styled.div`
  ${tw`bg-zero h-full w-full rounded-lg text-second flex items-center justify-center`}
  cursor: pointer;
`;

export const UnlockReason = styled.div`
  ${tw`font-karla text-primary`}
`;
