import ProfilePage from "./ProfilePage";

import GeneralTab from "./GeneralTab";
import BuffsTab from "./BuffsTab";

ProfilePage.GeneralTab = GeneralTab;
ProfilePage.BuffsTab = BuffsTab;

export default ProfilePage;
