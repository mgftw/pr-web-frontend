import { createRoot } from "react-dom/client";
import * as Sentry from "@sentry/react";
import { BrowserTracing } from "@sentry/tracing";
import "react-toastify/dist/ReactToastify.css";
import "react-tippy/dist/tippy.css";

import "./main.scss";

import Router from "./router";

Sentry.init({
  dsn: "https://a3c4f6bb777f4c3d817ccc4b7d4e97ed@o462408.ingest.sentry.io/6526314",
  integrations: [new BrowserTracing()],
  environment: import.meta.env.VITE_SENTRY_ENVIRONMENT,

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 0.02,
});

const root = createRoot(document.getElementById("root"));
root.render(<Router />);
