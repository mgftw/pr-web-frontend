export interface IBuff {
  buff_id: number;
  name: string;
  description: string;

  cash_cost: number;
  pill_cost: number;
  required_level: number;
  sub_category: number;
  enh_count: number;
  flags: number;
  flags_human: object;
  infected: number;
}

export interface IOwnedBuffInfo {
  buff: IBuff;
  current_enhancement: number;
  does_expire: boolean;
  expiration_date: Date;
  is_binded: boolean;
}

export interface IBlackMarketItem {
  blood_cost: number;
  cash_cost: number;
  item: {
    name: string;
    description: string;
  };
  item_id: number;
  pill_cost: number;
}

export interface IQuestNPC {
  id: number;
  name: string;
  min_level: number;
  needs_clan?: boolean;
  bio?: string;
}

export interface IQuest {
  quest_id: number;
  npc_id: number;

  title: string;
  web_story: string;
  web_end_story: string;
  required_level: number;
  required_quest: number;

  pill_reward: number;
  cash_reward: number;
  exp_reward: number;
  tasks: IQuestTask[];

  item_rewards: IQuestItemReward[];

  is_daily: boolean;
  is_weekly: boolean;
  is_disabled: boolean;
}

export interface IQuestItemReward {
  item_id: number;
  quantity: number;
}

export interface IQuestTask {
  description?: string;
  amount?: string;
  coordinates?: string;
  foreign_id?: number;
  game_class?: string;
  map?: string;
  trigger_range?: number;
  task_type: number;
  task_id: number;
}

export enum QuestTaskType {
  TaskType_Collect = 1,
  TaskType_Kill,
  TaskType_UseNPC,
  TaskType_Witness,
  TaskType_Custom,
}

export enum QuestState {
  NotTaken = 0,
  Done = 1,
  InProgress = 2,
  ReadyToFinish = 3,
}

export enum QuestTaskState {
  Done = 0,
  Active = 1,
  Confirm = 2,
}

export interface IPlayerQuestProgress {
  date_finished?: Date;
  quest: Partial<IQuest>;
  quest_id: number;
  npc_id: number;
  is_daily: boolean;
  quest_state: {
    id: number;
    name: string;
  };
  tasks: IPlayerQuestTaskProgress[];
}

export interface IPlayerQuestTaskProgress {
  amount: number;
  progress?: {
    task_progress: number;
    task_state: number;
    task_state_string: string;
  };
}

export interface IInventoryItem {
  id: number;
  name: string;
  description: string;
}

export enum PlayerClass {
  Technician = 1,
  Support = 2,
  Controller = 3,
  Agile = 4,
  Debuffer = 5,
  Hunter = 6,
}

export interface IPlayerClassProgress {
  class_id: PlayerClass;
  class_level: number;
  class_exp: number;
}
