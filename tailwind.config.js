import { nextui } from "@nextui-org/react";

export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        karla: ["Karla"],
        oswald: ["Oswald"],
        konsole: ["VT323"],
      },
      colors: {
        beige: "#E8E6D5",
        primary: "#E8E6D5",
        secondary: "#A2ABAB",
        tertiary: "#A2ABAB",
        quaternary: "#70798C",
        zero: "#1B2021",
        first: "#2E3638",
        second: "#49565A",
        red: "#B85B5B",
        green: "#52796F",
        veryRed: "#bb3939",
        danger: "#B85B5B",
      },
    },
  },
  darkMode: "class",
  variants: {},
  plugins: [
    nextui({
      defaultTheme: "dark",
      themes: {
        dark: {
          layout: {},
          colors: {
            beige: "#E8E6D5",
            primary: {
              DEFAULT: "#E8E6D5",
              foreground: "#1B2021",
            },
            secondary: {
              DEFAULT: "#49565A",
              foreground: "#1B2021",
            },
            error: "#bb3939",
            tertiary: "#A2ABAB",
            quaternary: "#70798C",
            zero: "#1B2021",
            first: "#2E3638",
            second: "#49565A",
            red: "#B85B5B",
            green: "#52796F",
            veryRed: "#bb3939",
          },
        },
      },
    }),
  ],
};
