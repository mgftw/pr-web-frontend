import os
from os import path
import sys
import json
import io
from paramiko import SSHClient, AutoAddPolicy, RSAKey
from scp import SCPClient

branch = os.getenv("CI_COMMIT_REF_NAME", None)
swarm = os.getenv("IS_SWARM", None) is not None
project = os.getenv("CI_PROJECT_NAMESPACE")
service = os.getenv("CI_PROJECT_NAME")
ci_token = os.getenv("CI_JOB_TOKEN")
registry_uri = os.getenv("CI_REGISTRY")

files = None

if path.exists(".deploy/files.json"):
    with open(".deploy/files.json", "r") as f:
        files = json.load(f)

server = None
secret = None

print(f"Running deploy for {branch}")
print(f"Swarm mode is {'enabled' if swarm else 'disabled'}")
print("Retrieving configuration variables")

if branch == "develop":
    server = os.getenv("SSH_STAGING_SERVER", None)
    secret = os.getenv("SSH_STAGING_PRIVATE", None)
elif branch == "master":
    server = os.getenv("SSH_MASTER_SERVER", None)
    secret = os.getenv("SSH_MASTER_PRIVATE", None)

if server is None or secret is None:
    print("No server or secret defined, halting!", file=sys.stderr)
    sys.exit(1)

print(f"Connecting to {server}")
print("Setting up SSH/SCP clients")
ssh = SSHClient()

ssh.set_missing_host_key_policy(AutoAddPolicy())
key = RSAKey.from_private_key(io.StringIO(secret))

ssh.connect(hostname=server, username="deploy", pkey=key, passphrase="")
scp = SCPClient(ssh.get_transport())

service_folder = (f"~/{project}/" if swarm else f"~/{project}/{branch}/{service}/")
deploy_file = (f".deploy/{branch}/stack.yml" if swarm
               else f".deploy/{branch}/docker-compose.yml")
deployed_file = (f"{service}.yml" if swarm else "docker-compose.yml")

print("Making project folder, if non-existant")
ssh.exec_command(f"mkdir -p {service_folder}")

print(f"Copying deployment file {deploy_file}")
print(
  f"(source: ./{deploy_file}, destination: {service_folder}{deployed_file})"
)

scp.put(f'./{deploy_file}', f'{service_folder}{deployed_file}')

if files is not None and branch in files:
    print(f"Additional {len(files[branch])} will be copied")
    for source, destination in files[branch].items():
        print(f'Copying {source} to {destination}')
        scp.put(f'./{source}', f'{service_folder}{destination}')

commands = (f"cd {service_folder}"
            f"&& docker login -u gitlab-ci-token -p {ci_token} {registry_uri}")

if swarm:
    commands += (
      f" && docker stack deploy -c {deployed_file} --with-registry-auth"
      f" {project}_{service}"
    )
else:
    commands += (
      " && docker-compose down && docker-compose pull"
      " && docker-compose up -d --build"
    )

print("Running commands for deployment")

stdin, stdout, stderr = ssh.exec_command(commands)

success = stdout.channel.recv_exit_status() == 0

print("Cleaning docker login information execution")
ssh.exec_command(f"docker logout {registry_uri}")

if success:
    print("Everything went fine, exiting")
    sys.exit(0)
else:
    print("There was an error while deploying")
    print("stderr output was: ")
    for line in stderr.readlines():
        print(line)
    sys.exit(1)
