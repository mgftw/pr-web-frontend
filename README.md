# Points Reloaded Web App

## Development

To start the dev server in port `3000` run:

`npm run dev`

By default, environment variables from `.env` will be loaded to specify API URL and others. If you don't have a `.env` file, create one by copying the contents from `.env.example`.
