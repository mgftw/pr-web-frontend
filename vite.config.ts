import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import * as path from "path";
import macrosPlugin from "vite-plugin-babel-macros";
import packageJson from "./package.json";

const createAlias = (alias, out?) => ({
  find: `$${alias}`,
  replacement: path.resolve(__dirname, `src/${out ?? alias}`),
});

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 3000,
  },
  plugins: [react(), macrosPlugin()],
  define: {
    __APP_VERSION__: JSON.stringify(packageJson.version),
    "process.env": {},
  },
  css: {
    preprocessorOptions: {
      scss: {
        quietDeps: true,
      },
    },
  },
  resolve: {
    alias: [
      {
        find: `types`,
        replacement: path.resolve(__dirname, `src/types`),
      },
      createAlias("api"),
      createAlias("assets"),
      createAlias("atoms"),
      createAlias("components"),
      createAlias("constants"),
      createAlias("helpers"),
      createAlias("hooks"),
      createAlias("redux"),
      createAlias("actions", "redux/actions"),
      createAlias("reducers", "redux/recucers"),
      createAlias("pages"),
    ],
  },
});
